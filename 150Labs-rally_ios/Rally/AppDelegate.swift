//
//  AppDelegate.swift
//  Rally
//
//  Created by Ian Moses on 5/18/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

//
//  Copyright (c) 2015 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
// Register for remote notifications with google messaging service
// https://developers.google.com/cloud-messaging/ios/client?configured=true&ver=swift
//

import UIKit
import AddressBook
import Contacts
import Firebase
import UserNotifications

import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    
    var connectedToGCM = false
    let gcmMessageIDKey = "gcm.message_id" // not sure if need this
    var subscribedToTopic = false
    var gcmSenderID: String?
    var registrationOptions = [String: AnyObject]()
    
    let registrationKey = "onRegistrationCompleted"
    var messageKey = "onMessageReceived"
    let subscriptionTopic = "/topics/global"
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func resetKeychainPrivatePublicKeys() {
        print("class AppDelegate:func resetKeychainPrivatePublicKeys is running...")
        let keychain = Keychain()
        keychain["privateKey"] = nil
        keychain["publicKey"] = nil
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //self.resetKeychainPrivatePublicKeys()
        
        // Override point for customization after application launch.
        //line below originally read 'return true'. Added the statement following return per fb integration tutorial
        // http://nshipster.com/swift-system-version-checking/
        print("class AppDelegate:func application is running...")
        // ContainerVC uses timer to track how long continued splash screen should display for
        StartupRunTimer.start = Date()
        // load User info saved in local files
        //User.PhoneNumber["Mobile"] = nil
        if let value = UserDefaults.standard.value(forKey: "userPhoneNumberKey") as? String {
            MYUser.phoneNumberList["Mobile"] = value
        }
        if let value = UserDefaults.standard.value(forKey: "userIDKey") as? Int {
            MYUser.ID = value
        }
        if let value = UserDefaults.standard.value(forKey: "userStockImageIDKey") as? Int {
            MYUser.stockImageID = value
        }
        // user name from local file
        //
        if let value = UserDefaults.standard.value(forKey: "userFirstNameKey") as? String {
            MYUser.firstName = value
        }
        if let value = UserDefaults.standard.value(forKey: "userLastNameKey") as? String {
            MYUser.lastName = value
        }
        let defaults = UserDefaults.standard
        
        let dictionary = defaults.dictionary(forKey: "UserThumbnailUserIDDictionary") as? [String:Int] ?? [String:Int]() // ?? [Int:Int]() makes variable safe for use prior to array containing any values key-value pairs
        var properTypeDictionary = [Int: Int]()
        dictionary.forEach { properTypeDictionary[Int($0.0)!] = $0.1 }
        communityMemberThumbnailUserIDsGlobalDictionary = properTypeDictionary
        LoadLocalFile.loadLargeProfileImage()
        LoadLocalFile.loadThumbnailProfileImage()
        
        // PermissionsFlags.permissionsNotEnabledUserIDNil = true
        // http://stackoverflow.com/questions/28793331/creating-a-navigationcontroller-programatically-swift
        // http://stackoverflow.com/questions/26572901/nsfontattributename-has-changed-to-string
        // http://stackoverflow.com/questions/24687238/changing-navigation-bar-color-in-swift
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let verifyPhoneNumberViewControllerInstance: VerifyPhoneNumberViewController!
        let editProfileViewControllerInstance: EditProfileViewController!
        let homeContainerViewControllerInstance: ContainerViewController = ContainerViewController(type: .home)
        let nav1 = UINavigationController(rootViewController: homeContainerViewControllerInstance)
        window!.rootViewController = nav1
        
        // http://stackoverflow.com/questions/24687238/changing-navigation-bar-color-in-swift
        UINavigationBar.appearance().barTintColor = MYColor.rallyBlue
        UINavigationBar.appearance().isTranslucent = false
        nav1.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 18)!]
        self.window!.makeKeyAndVisible()
        // self.window!.backgroundColor = MYColor.rallyBlue
        nav1.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: self.window!.frame.size.width, height: 40)), forKeyPath: "navigationBar")
        //application.isStatusBarHidden = false

        /*if webCallStatus.fetchAddressBookContacts == 0 {
            var accessGranted = false
            let addressBookHelperClassInstance = addressBookHelperClass()
            if #available(iOS 9.0, *) {
                let (contactsIOS9AccessGranted,_): (Bool,CNAuthorizationStatus) = addressBookHelperClassInstance.addressBookIOS9AccessStatus()
                if contactsIOS9AccessGranted {
                    accessGranted = true
                }
            }
            if accessGranted {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    let addressBookHelperClassInstance = addressBookHelperClass()
                    if #available(iOS 9.0, *) {
                        addressBookHelperClassInstance.loadAddressBookContacts()
                    }
                })
            }
        }*/
        // determine if notifactions have been denied, if so, assign a bullshit gcm token to avoid problems in code
        if let permissionsValue = UserDefaults.standard.string(forKey: "remoteNotificationsPermissionsStatus") { // user has responded to notifcations permissions request
            if permissionsValue == "Denied" { // permission have been denied, assign a bullshit gcm token to stuff inside CreateUser call to get to home screen (this is where user is prompted to enable permissions, then CreateUser call is updated)
                MYUser.GCMToken = "fakeToken"
            }
        }
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // assign delegate object to the UNUserNotificationCenter object to receive display notifications
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        else {
            // Fallback on earlier versions
        }
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                                         name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        MYUser.GCMToken = Messaging.messaging().fcmToken
        
        LoadLocalFile.stockImages()
        LoadLocalFile.loadUpToDateStockImagesIntoStockImageGlobalDictionary()
        // if remote notifications are enabled, register for remote notifications to get registration token. regristration token is returned in registration handler method which is responsible for posting postUser webservice call needed to kick-off loading all user data
        if UIApplication.shared.isRegisteredForRemoteNotifications {
            print("isRegisteredForRemoteNotifications is true")
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        // set root view controller
        if !defaults.bool(forKey: "userNameKey") { // this means phone number has been confirmed as well
            if defaults.string(forKey: "userPhoneNumberKey") != nil { //
                //editProfileViewControllerInstance = EditProfileViewController(nibName: nil, bundle: nil)
                //nav1.viewControllers = [editProfileViewControllerInstance] // user must go enter phone number
            }
            else {
                verifyPhoneNumberViewControllerInstance = VerifyPhoneNumberViewController(nibName: nil, bundle: nil)
                nav1.viewControllers = [verifyPhoneNumberViewControllerInstance] // user must go enter phone number
            }
        }
        return true
    }
    
    //func application function directly below (all 9 lines) was added per fb integration tutorial instructions
    func application(_ application: UIApplication,
        open url: URL,
        sourceApplication: String?,
        annotation: Any) -> Bool {
            print("func application is running...")
            return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                open: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("func application:didRegisterUserNotificationSettings is running...")
        print("notificationSettings:\(notificationSettings)")
        var userInfo = [String:Bool]()
        // broadcast notification so active VC can observe and respond to notification settings
        if notificationSettings.types != UIUserNotificationType() { // check to see if user enabled permissions
            //application.registerForRemoteNotifications()
            userInfo["enabledRemoteNotificationPermissionTypes"] = true
        }
        else {
            userInfo["enabledRemoteNotificationPermissionTypes"] = false
        }
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "userEnabledRemoteNotificationPermissions"), object: nil, userInfo: userInfo)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        print("func applicationWillResignActive is running...")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("func applicationDidEnterBackground is running...")
        /*
        // [START disconnect_gcm_service]
        GCMService.sharedInstance().disconnect()
        // [START_EXCLUDE]
        self.connectedToGCM = false
        // [END_EXCLUDE]
        // [END disconnect_gcm_service]
        */
        Messaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("func applicationWillEnterForeground is running...")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //Added line directly below 'FBSDKAppEvents.activateApp()' per fb integration tutorial instructions
        print("running AppDelegate func applicationDidBecomeActive...")
        //NSNotificationCenter.defaultCenter().postNotificationName("retrieveAddressBookContacts", object: self)
        /*
        // [START connect_gcm_service]
        // Connect to the GCM server to receive non-APNS notifications
        GCMService.sharedInstance().connectWithHandler({
            (error) -> Void in
            if error != nil {
                print("Could not connect to GCM: \(error.localizedDescription)")
            } else {
                self.connectedToGCM = true
                print("Connected to GCM")
                // [START_EXCLUDE]
                self.subscribeToTopic()
                // [END_EXCLUDE]
            }
        })
        // [END connect_gcm_service]
        */
        connectToFcm()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("running AppDelegate func applicationWillTerminate...")
    }
    /*
    func subscribeToTopic() {
        // If the app has a registration token and is connected to GCM, proceed to subscribe to the
        // topic
        print("func subscribeToTopic is running...")
        if(registrationToken != nil && connectedToGCM) {
            GCMPubSub.sharedInstance().subscribeWithToken(self.registrationToken, topic: subscriptionTopic,
                options: nil, handler: {(error) -> Void in
                    if (error != nil) {
                        // Treat the "already subscribed" error more gently
                        if error.code == 3001 {
                            print("Already subscribed to \(self.subscriptionTopic)")
                        } else {
                            print("Subscription failed: \(error.localizedDescription)");
                        }
                    } else {
                        self.subscribedToTopic = true;
                        NSLog("Subscribed to \(self.subscriptionTopic)");
                    }
            })
        }
    }
     */
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        MYUser.GCMToken = fcmToken
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    /*
    // [START receive_apns_token]
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: NSData ) {
            print("func application:didRegisterForRemoteNotificationsWithDeviceToken is running...")
            print("APNs device token:\(deviceToken)")
            // [END receive_apns_token]
            // [START get_gcm_reg_token]
            // Create a config and set a delegate that implements the GGLInstanceIDDelegate protocol.
            let instanceIDConfig = GGLInstanceIDConfig.defaultConfig()
            instanceIDConfig.delegate = self
            // Start the GGLInstanceID shared instance with that config and request a registration
            // token to enable reception of notifications
            GGLInstanceID.sharedInstance().startWithConfig(instanceIDConfig)
            registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken,
                kGGLInstanceIDAPNSServerTypeSandboxOption:true]
            GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
                scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
            // [END get_gcm_reg_token]
    }
    */
    
    // [START receive_apns_token_error]
    func application( _ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
        error: Error ) {
            print("func application:didFailToRegisterForRemoteNotificationsWithError is running...")
            print("Registration for remote notification failed with error: \(error.localizedDescription)")
            // [END receive_apns_token_error]
            let userInfo = ["error": error.localizedDescription]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: registrationKey), object: nil, userInfo: userInfo)
    }
    
    // [START ack_message_reception]
    func application( application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
            print("func application:didReceiveRemoteNotification is running...")
            print("Notification received: \(userInfo)")
            Messaging.messaging().appDidReceiveMessage(userInfo)
            // Handle the received message
            // [START_EXCLUDE]
        let dataPackage = userInfo as NSDictionary
        messageKey = dataPackage["payloadType"] as! String
        print("postNotification name:\(messageKey)")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: messageKey), object: nil,
                userInfo: userInfo)
            // [END_EXCLUDE]
    }
    /*
    func application( application: UIApplication,
        didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
        fetchCompletionHandler handler: (UIBackgroundFetchResult) -> Void) {
            print("func application:didReceiveRemoteNotification:fetchCompletionHandler is running...")
            print("Notification received:fetchCompletionHandler:\(userInfo)")
            // This works only if the app started the GCM service
            GCMService.sharedInstance().appDidReceiveMessage(userInfo);
            // Handle the received message
            let dataPackage = userInfo as NSDictionary
            messageKey = dataPackage["payloadType"] as! String
            // Invoke the completion handler passing the appropriate UIBackgroundFetchResult value
            // [START_EXCLUDE]
        print("postNotification fetchCompletionHandler name:\(messageKey)")
            NSNotificationCenter.defaultCenter().postNotificationName(messageKey, object: nil,
                userInfo: userInfo)
            handler(UIBackgroundFetchResult.NoData);
            // [END_EXCLUDE]
    }
    // [END ack_message_reception]
    */
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        print("func application:didReceiveRemoteNotification:fetchCompletionHandler is running...")
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        // This works only if the app started the GCM service
        //GCMService.sharedInstance().appDidReceiveMessage(userInfo);
        // Handle the received message
        let dataPackage = userInfo as NSDictionary
        messageKey = dataPackage["payloadType"] as! String
        // Invoke the completion handler passing the appropriate UIBackgroundFetchResult value
        // [START_EXCLUDE]
        print("postNotification fetchCompletionHandler name:\(messageKey)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: messageKey), object: nil,
                                                                  userInfo: userInfo)
       // handler(UIBackgroundFetchResult.NoData);
        // [END_EXCLUDE]
    }
    // [END receive_message]
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("func messaging:didReceiveRemoteNotification is running...")
        print("remoteMessage: \(remoteMessage.appData)")
        let dataPackage = remoteMessage.appData
        messageKey = dataPackage["payloadType"] as! String
        print("remoteMessage.appData:\(remoteMessage.appData)")
        print("messageKey:\(messageKey)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: messageKey), object: nil,
                                        userInfo: dataPackage)
    }
    
    /*
    func registrationHandler(registrationToken: String!, error: NSError!) {
        print("func registrationHandler is running...")
        if (registrationToken != nil) {
            self.registrationToken = registrationToken
            print("Registration Token: \(registrationToken)")
            let defaults = NSUserDefaults.standardUserDefaults()
            User.GCMToken = self.registrationToken
            // need to have registration token to make webcall. also need FB info and phone number
            if let _ = User.FacebookUserID {
                if let _ = defaults.stringForKey("userPhoneNumberKey") {
                    dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
                        postUserToWebServiceClass.callAndDetermineResponseToPostUserToWebServiceOutput({(success: Bool) -> Void in
                            if success {
                            }
                        })
                    }
                }
            }
            self.subscribeToTopic()
            let userInfo = ["registrationToken": registrationToken]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        } else {
            print("Registration to GCM failed with error: \(error.localizedDescription)")
            let userInfo = ["error": error.localizedDescription]
            NSNotificationCenter.defaultCenter().postNotificationName(
                self.registrationKey, object: nil, userInfo: userInfo)
        }
    }s
    */
    /*
    // [START on_token_refresh]
    func onTokenRefresh() {
        print("func onTokenRefresh is running...")
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        print("The GCM registration token needs to be changed.")
        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
    }
    // [END on_token_refresh]
    */
    
    func tokenRefreshNotification(_ notification: Notification) {
        print("func tokenRefreshNotification is running...")
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            MYUser.GCMToken = refreshedToken
            /*
            let defaults = UserDefaults.standard
            // need to have registration token to make webcall. also need FB info and phone number
            if let _ = defaults.string(forKey: "userPhoneNumberKey") {
                let webServiceCallManagerInstance = WebServiceCallManager(intent: .createUser)
                webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                    if succeeded {
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                    else { // post message saying could not join group. try again later
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                })
            }
             */
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    // [START upstream_callbacks]
    func willSendDataMessageWithID(_ messageID: String!, error: NSError!) {
        print("func willSendDataMessageWithID is running...")
        if (error != nil) {
            // Failed to send the message.
        } else {
            // Will send message, you can save the messageID to track the message
        }
    }
    
    func didSendDataMessageWithID(_ messageID: String!) {
        print("func didSendDataMessageWithID is running...")
        // Did successfully send message identified by messageID
    }
    // [END upstream_callbacks]
    
    func didDeleteMessagesOnServer() {
        print("func didDeleteMessagesOnServer is running...")
        // Some messages sent to this device were deleted on the GCM server before reception, likely
        // because the TTL expired. The client should notify the app server of this, so that the app
        // server can resend those messages.
    }
    
    func connectToFcm() {
        print("func connectToFcm is running...")
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
                let token = InstanceID.instanceID().token()
                print("token:\(String(describing: token))")
                MYUser.GCMToken = token
                let defaults = UserDefaults.standard
                // need to have registration token to make webcall. also need FB info and phone number
                if let _ = defaults.string(forKey: "userPhoneNumberKey") {
                    if WebCallStatus.createUserWebCall == 0 { // if call hasn't already been made
                        let webServiceCallManagerInstance = WebServiceCallManager(intent: .createUser)
                        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                            if succeeded {
                                DispatchQueue.main.async(execute: { () -> Void in
                                })
                            }
                            else { // post message saying could not join group. try again later
                                DispatchQueue.main.async(execute: { () -> Void in
                                })
                            }
                        })
                    }
                }
            }
        }
    }
}
