//
//  AppPermissionsHelper.swift
//  Rally
//
//  Created by Ian Moses on 4/14/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import Contacts

class permissionsHelperClass: NSObject, CLLocationManagerDelegate {
    static let permissionsHelperClassSingleton = permissionsHelperClass()
    static let locationManagerSingleton: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 200
        manager.delegate = permissionsHelperClass.permissionsHelperClassSingleton
        return manager
    }()
    var navigationControllerClassVariable: UINavigationController!
    static var userLocation: CLLocationCoordinate2D?
    
    fileprivate override init() { // marking private disallows any initialization of a permissionsHelperClass instance from outside class (to avoid duplicates), may only be accessed from inside which permissionsHelperClassSingleton meets this criteria
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(permissionsHelperClass.respondToUserRemoteNotificationDialogSelection(_:)), name: NSNotification.Name(rawValue: "userEnabledRemoteNotificationPermissions"), object: nil)
    }
    
    func definePermissionsView(_ permissionsNotAuthorized: [permissionCase])->UIView {
        print("func definePermissionsView is running...")
        let superView = UIView()
        superView.clipsToBounds = true
        superView.tag = 890
        superView.frame.size = CGSize(width: ScreenDimension.width*83/100,height: ScreenDimension.height*44/100)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            superView.frame.size = CGSize(width: ScreenDimension.width*78.5/100,height: ScreenDimension.height*51/100)
        default:
            print("I am not equipped to handle this device")
        }
        let superViewYOffset = (ScreenDimension.height - UINavigationBarTaller.navigationBarHeight-UIApplication.shared.statusBarFrame.height)/2
        superView.center = CGPoint(x: ScreenDimension.width/2, y: superViewYOffset)
        superView.layer.cornerRadius = 5
        superView.backgroundColor = UIColor.white
        let blueSubview = UIView(frame: CGRect(x: 0,y: 0,width: superView.frame.width,height: superView.frame.height*19/100))
        blueSubview.backgroundColor = MYColor.rallyBlue
        superView.addSubview(blueSubview)
        let headerLabel = UILabel()
        headerLabel.textColor = MYColor.lightBlack
        headerLabel.font = UIFont.boldSystemFont(ofSize: 17)
        headerLabel.text = "Rally your friends and communities"
        headerLabel.numberOfLines = 1
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            headerLabel.numberOfLines = 2
            headerLabel.frame.size.width = superView.frame.width*85/100
        default:
            print("I am not equipped to handle this device")
        }
        headerLabel.lineBreakMode = .byWordWrapping
        headerLabel.textAlignment = .center
        headerLabel.sizeToFit()
        headerLabel.center.x = superView.frame.width/2
        headerLabel.frame.origin.y = superView.frame.height*31/100
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            headerLabel.frame.origin.y = superView.frame.height*26/100
        default:
            print("I am not equipped to handle this device")
        }
        superView.addSubview(headerLabel)
        let subheaderLabel = UILabel()
        subheaderLabel.textColor = MYColor.midDarkGrey
        subheaderLabel.font = UIFont.systemFont(ofSize: 13.75)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            subheaderLabel.font = UIFont.systemFont(ofSize: 13.5)
            
        default:
            print("I am not equipped to handle this device")
        }
        subheaderLabel.text = "We need a couple things first." // before you can Rally
        subheaderLabel.numberOfLines = 1
        subheaderLabel.lineBreakMode = .byWordWrapping
        subheaderLabel.textAlignment = .center
        subheaderLabel.sizeToFit()
        subheaderLabel.center.x = superView.frame.width/2
        let subheaderLabelYOffset = headerLabel.frame.origin.y + headerLabel.frame.height + superView.frame.height*5.0/100
        subheaderLabel.frame.origin.y = subheaderLabelYOffset
        superView.addSubview(subheaderLabel)
        let checkImage = UIImage(named:"check")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        // generate remote notifications button
        let remoteNotificationsButton = UIButton()
        remoteNotificationsButton.tag = 300
        remoteNotificationsButton.frame.size = CGSize(width: superView.frame.width*86/100,height: superView.frame.height*13.5/100)
        remoteNotificationsButton.center.x = superView.frame.width/2
        remoteNotificationsButton.frame.origin.y = superView.frame.height*59.5/100
        remoteNotificationsButton.layer.cornerRadius = remoteNotificationsButton.frame.height/2
        remoteNotificationsButton.layer.borderColor = MYColor.rallyBlue.cgColor
        remoteNotificationsButton.layer.borderWidth = 0.75
        remoteNotificationsButton.setTitle("Enable Notifications", for: UIControlState())
        if permissionsNotAuthorized.contains(.remoteNotifications) {
            remoteNotificationsButton.setTitleColor(MYColor.rallyBlue, for: UIControlState())
            remoteNotificationsButton.imageView!.tintColor = MYColor.rallyBlue
            remoteNotificationsButton.setImage(UIImage(named:"notifications")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        }
        else {
            remoteNotificationsButton.setTitleColor(UIColor.white, for: UIControlState())
            remoteNotificationsButton.imageView!.tintColor = UIColor.white
            remoteNotificationsButton.setImage(checkImage, for: UIControlState())
            remoteNotificationsButton.backgroundColor = MYColor.rallyBlue
            remoteNotificationsButton.isUserInteractionEnabled = false
        }
        remoteNotificationsButton.addTarget(permissionsHelperClass.permissionsHelperClassSingleton, action: #selector(permissionsHelperClass.onRemoteNotificationsButtonTouch(_:)), for: .touchUpInside)
        superView.addSubview(remoteNotificationsButton)
        // generate location button
        let locationButton = UIButton()
        locationButton.tag = 302
        locationButton.frame.size = CGSize(width: superView.frame.width*86/100,height: superView.frame.height*13.5/100)
        locationButton.center.x = superView.frame.width/2
        locationButton.frame.origin.y = remoteNotificationsButton.frame.origin.y+remoteNotificationsButton.frame.height+superView.frame.height*6/100
        locationButton.layer.cornerRadius = remoteNotificationsButton.frame.height/2
        locationButton.layer.borderColor = MYColor.rallyBlue.cgColor
        locationButton.layer.borderWidth = 0.75
        locationButton.setTitle("Enable Location", for: UIControlState())
        if permissionsNotAuthorized.contains(.location) || permissionsNotAuthorized.contains(.locationEnabled) {
            locationButton.setTitleColor(MYColor.rallyBlue, for: UIControlState())
            locationButton.setImage(UIImage(named:"location")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
            locationButton.imageView!.tintColor = MYColor.rallyBlue
        }
        else {
            locationButton.imageView!.tintColor = UIColor.white
            locationButton.setImage(checkImage, for: UIControlState())
            locationButton.setTitleColor(UIColor.white, for: UIControlState())
            locationButton.backgroundColor = MYColor.rallyBlue
            locationButton.isUserInteractionEnabled = false
        }
        locationButton.addTarget(permissionsHelperClass.permissionsHelperClassSingleton, action: #selector(permissionsHelperClass.onLocationButtonTouch(_:)), for: .touchUpInside)
        superView.addSubview(locationButton)
        /*
        // generate contacts button
        let contactsButton = UIButton()
        contactsButton.tag = 301
        contactsButton.frame.size = CGSizeMake(superView.frame.width*88/100,superView.frame.height*10.85/100)
        contactsButton.center.x = superView.frame.width/2
        contactsButton.frame.origin.y = locationButton.frame.origin.y+locationButton.frame.height+superView.frame.height*5/100
        contactsButton.layer.cornerRadius = remoteNotificationsButton.frame.height/2
        contactsButton.layer.borderColor = MYColor.rallyBlue.CGColor
        contactsButton.layer.borderWidth = 0.75
        contactsButton.setTitle("Enable Contacts", forState: .Normal)
        if permissionsNotAuthorized.contains(.contacts) {
            contactsButton.imageView!.tintColor = MYColor.rallyBlue
            contactsButton.setImage(UIImage(named:"contacts")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: .Normal)
            contactsButton.setTitleColor(MYColor.rallyBlue, forState: .Normal)
        }
        else {
            contactsButton.imageView!.tintColor = UIColor.whiteColor()
            contactsButton.setImage(checkImage, forState: .Normal)
            contactsButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            contactsButton.backgroundColor = MYColor.rallyBlue
            contactsButton.userInteractionEnabled = false
        }
        contactsButton.addTarget(permissionsHelperClass.permissionsHelperClassSingleton, action: "onContactsButtonTouch:", forControlEvents: .TouchUpInside)
        superView.addSubview(contactsButton)
         */
        let permissionsButtonArray = [remoteNotificationsButton,locationButton]
        for button in permissionsButtonArray {
            button.titleLabel!.font = UIFont.systemFont(ofSize: 17)
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                button.titleLabel!.font = UIFont.systemFont(ofSize: 15)
            default:
                print("I am not equipped to handle this device")
            }
            let buttonSuperViewCenterPoint = button.frame.width/2
            var imageInset = button.frame.height*13/100
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                imageInset = button.frame.height*25/100
            default:
                print("I am not equipped to handle this device")
            }
            let imageViewLeftInsetValue = button.frame.width*2/100+imageInset
            let imageViewRightInsetValue = button.frame.width*98/100-button.imageView!.frame.width+imageInset
            let buttonSuperViewToImageViewHeightDifference = button.frame.height-button.imageView!.frame.height
            button.imageEdgeInsets = UIEdgeInsetsMake(buttonSuperViewToImageViewHeightDifference/2+imageInset,imageViewLeftInsetValue,buttonSuperViewToImageViewHeightDifference/2+imageInset,imageViewRightInsetValue)
            let currentTitleLabelXOffset = button.titleLabel!.frame.origin.x
            let neededTitleLabelXOffset = buttonSuperViewCenterPoint-button.titleLabel!.frame.width/2
            let currentTitleLabelRightSideXOffset = button.frame.width-currentTitleLabelXOffset-button.titleLabel!.frame.width
            let neededTitleLabelRightSideXOffset = button.frame.width-neededTitleLabelXOffset-button.titleLabel!.frame.width
            button.titleEdgeInsets = UIEdgeInsetsMake(0,neededTitleLabelXOffset-currentTitleLabelXOffset,0,neededTitleLabelRightSideXOffset-currentTitleLabelRightSideXOffset)
        }
        return superView
    }
    
    func definePermissionsDeniedAlert(_ permissionCaseValue: permissionCase, superView: UIView)->UIView {
        print("func definePermissionsDeniedAlert is running...")
        // generate superview
        let permissionsDeniedAlert = UIView()
        permissionsDeniedAlert.tag = 709
        permissionsDeniedAlert.frame = CGRect(x: 0, y: superView.frame.height*19/100, width: superView.frame.width,height: superView.frame.height*38.5/100)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
        default:
            print("I am not equipped to handle this device")
        }
        permissionsDeniedAlert.backgroundColor = MYColor.lightGrey
        let permissionsDeniedAlertRectShape = CAShapeLayer()
        permissionsDeniedAlertRectShape.bounds = permissionsDeniedAlert.frame
        permissionsDeniedAlertRectShape.position = permissionsDeniedAlert.center
        permissionsDeniedAlertRectShape.path = UIBezierPath(roundedRect: permissionsDeniedAlert.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 18, height: 18)).cgPath
        //Here I'm masking the views's layer with permissionsDeniedAlertRectShape layer
        permissionsDeniedAlert.layer.mask = permissionsDeniedAlertRectShape
        
        var yOffset = permissionsDeniedAlert.frame.height*5/100 // first alert header yOffset value, value is updated as subviews are added
        let subviewVerticalSpacing = permissionsDeniedAlert.frame.height*3/100
        superView.addSubview(permissionsDeniedAlert)
        var alertHeaderText: String!
        var alertBodyText: String!
        var tagValueForHeader: Int!
        switch permissionCaseValue {
        case .location:
            alertHeaderText = "Rally doesn't have access to your location services."
            alertBodyText = "Rally needs access to search for communities and places nearby. To enable access, tap Settings."
            tagValueForHeader = 710
        case .contacts:
            alertHeaderText = "Rally doesn't have access to your contacts."
            // 
            alertBodyText = "Rally needs access to your iPhone's contacts to help you connect with other people. To enable access, tap Settings."
            tagValueForHeader = 711
        case .remoteNotifications:
            alertHeaderText = "Rally doesn't have access to send notifications."
            // Rally would like to notify you when friends send you a rally invite.
            alertBodyText = "Rally needs access in order to be notified about rallys you’re invited to, tap Settings."
            tagValueForHeader = 712
        default:
            break
        }
        // generate alert header
        let alertHeader = UILabel(frame: CGRect(x: 0,y: 0,width: superView.frame.width*90/100, height: CGFloat.greatestFiniteMagnitude))
        alertHeader.tag = tagValueForHeader
        alertHeader.lineBreakMode = .byWordWrapping
        alertHeader.numberOfLines = 2
        alertHeader.textAlignment = .center
        alertHeader.textColor = MYColor.midDarkGrey
        alertHeader.font = UIFont.boldSystemFont(ofSize: 15)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            alertHeader.font = UIFont.boldSystemFont(ofSize: 14)
        default:
            print("I am not equipped to handle this device")
        }
        alertHeader.text = alertHeaderText
        alertHeader.sizeToFit()
        alertHeader.center.x = permissionsDeniedAlert.frame.width/2
        alertHeader.frame.origin.y = yOffset
        yOffset += alertHeader.frame.height+subviewVerticalSpacing // set yOffset to correct value for next subview's y-origin value
        permissionsDeniedAlert.addSubview(alertHeader)
        // generate alert body
        let alertBody = UILabel(frame: CGRect(x: 0,y: 0,width: superView.frame.width*90/100, height: CGFloat.greatestFiniteMagnitude))
        alertBody.lineBreakMode = .byWordWrapping
        alertBody.numberOfLines = 3
        alertBody.textAlignment = .center
        alertBody.textColor = MYColor.midGrey
        alertBody.font = UIFont.systemFont(ofSize: 13.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            alertBody.font = UIFont.systemFont(ofSize: 12.25)
        default:
            print("I am not equipped to handle this device")
        }
        alertBody.text = alertBodyText
        alertBody.sizeToFit()
        alertBody.center.x = permissionsDeniedAlert.frame.width/2
        alertBody.frame.origin.y = permissionsDeniedAlert.frame.height*37/100
        yOffset += alertHeader.frame.height+subviewVerticalSpacing // set yOffset to correct value for next subview's y-origin value
        permissionsDeniedAlert.addSubview(alertBody)
        // generate settings button
        let settingsButton = UIButton()
        settingsButton.setTitle("Settings", for: UIControlState())
        settingsButton.setTitleColor(MYColor.rallyBlue, for: UIControlState())
        settingsButton.titleLabel!.font = UIFont.systemFont(ofSize: 20.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            settingsButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
        default:
            print("I am not equipped to handle this device")
        }
        settingsButton.sizeToFit()
        settingsButton.center.x = permissionsDeniedAlert.frame.width/2
        settingsButton.frame.origin.y = permissionsDeniedAlert.frame.height*72/100
        settingsButton.addTarget(self, action: #selector(permissionsHelperClass.onSettingsButtonTouch(_:)), for: .touchUpInside)
        permissionsDeniedAlert.addSubview(settingsButton)
        return superView
    }
    
    func onSettingsButtonTouch(_ sender: UIButton!) {
        print("func onSettingsButtonTouch is running...")
        // open rally app settings
        // http://stackoverflow.com/questions/28152526/how-do-i-open-phone-settings-when-a-button-is-clicked-ios/34024467#34024467
        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
    }
    
    /*
    func defineLocationServicesDisabledAlert(navigationController: UINavigationController) {
        print("func defineLocationServicesDisabledAlert is running...")
        let alert = UIAlertController(title: "Alert", message: "Location services must be enabled to use the map, create or find groups features. Please go to Settings to enable.", preferredStyle: UIAlertControllerStyle.Alert)
        let settings = UIAlertAction(title: "Settings", style: .Default, handler: { (action) -> Void in
            // http://stackoverflow.com/questions/28152526/how-do-i-open-phone-settings-when-a-button-is-clicked-ios
            UIApplication.sharedApplication().openURL(NSURL(string:"prefs:root=LOCATION_SERVICES")!)
        })
        let dismiss = UIAlertAction(title: "Dismiss", style: .Cancel) { (action) -> Void in
            print("Cancel Button Pressed")
        }
        alert.addAction(settings)
        alert.addAction(settings)
        // this may error if alert is presented with parent VC that is type alert (if user leaves app while alert is being presented and turns off location, this may cause issues, not sure if UIAlertController would behave properly as presenting controller)
        navigationController.visibleViewController!.presentViewController(alert, animated: true, completion: nil)
    }
    */
    
    func permissionsAccessHandler(_ permissionsStatusCase: permissionCase, navigationController: UINavigationController)->Bool { // determines permission access status. requests permission if not determined, displays alert to enable if restricted or denied, and does nothing allowing app to continue if authorized
        print("func permissionsAccessHandler is running...")
        navigationControllerClassVariable = permissionsControllerNav
        let viewControllerView = navigationControllerClassVariable.view as UIView // get instance of current view controller's view
        var permissionsNotAuthorized = [permissionCase]()
        switch permissionsStatusCase {
        case .location:
            print("Switch case: .location")
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined, .restricted, .denied:
                    print("location either not determined, restricted, or denied, display permissions view")
                    permissionsNotAuthorized.append(.location)
                case .authorizedAlways, .authorizedWhenInUse:
                    print("location authorized, do nothing")
                    if navigationControllerClassVariable.visibleViewController != nil {
                        print("visible VC")
                        if WebCallStatus.testVariable == 1 {
                            print("visible VC is loaded")
                            //print("NSThread.currentThread:\(Thread.current)")
                            let viewControllerView = permissionsHelperClass.permissionsHelperClassSingleton.navigationControllerClassVariable.view as UIView // get instance of current view controller's view
                            // if dulling view and permissions view exist (should always exist as pair), dismiss. this case occurs when address book permission was denied on app start , user enabled permissions, then ran logic to load contacts into friends array
                            if (viewControllerView.viewWithTag(890)) != nil {
                                if (viewControllerView.viewWithTag(165)) != nil {
                                    permissionsHelperClass.permissionsHelperClassSingleton.dismissDullingAndPermissionsView(viewControllerView)
                                }
                            }
                            permissionEnabledHandler(.location) // ensures button is marked blue if user enables in settings and comes back to app
                            //webCallStatus.testVariable = 0
                        }
                    }
                }
            }
            else { // location services off (Settings > Privacy > Location Services), present alert to turn on location
                // add location services not enabled variable
                permissionsNotAuthorized.append(.locationEnabled)
            }
            fallthrough
        case .contacts: // true or false check vs status check bc if not authorized, all other cases take you to permissions view
            print("Switch case: .contacts")
            /*let addressBookHelperClassInstance = addressBookHelperClass()
            if #available(iOS 9.0, *) {
                print("Switch case: .contacts:ios9 case is running...")
                let (contactsIOS9AccessGranted,_): (Bool,CNAuthorizationStatus) = addressBookHelperClassInstance.addressBookIOS9AccessStatus()
                print("Switch case: .contacts:ios9 case: contactsIOS9AccessGranted:\(contactsIOS9AccessGranted)")
                if !contactsIOS9AccessGranted {
                    permissionsNotAuthorized.append(.contacts)
                }
            }
            else { // for iOS8
                let (contactsIOS8AccessGranted,_): (Bool,ABAuthorizationStatus) = addressBookHelperClassInstance.addressBookIOS8AccessStatus()
                if !contactsIOS8AccessGranted {
                    permissionsNotAuthorized.append(.contacts)
                }
            }*/
            fallthrough
        case .remoteNotifications:
            print("Switch case: .remoteNotifications")
            //var remoteNotificationsAccessGranted: Bool!
            // http://stackoverflow.com/questions/29787736/isregisteredforremotenotifications-returns-true-even-though-i-disabled-it-comple
            //remoteNotificationsAccessGranted = UIApplication.sharedApplication().isRegisteredForRemoteNotifications() // determine if app is registered for remote notifications
            print("isRegisteredForRemoteNotifications():\(UIApplication.shared.isRegisteredForRemoteNotifications)")
            //print("permissionsStatusHandler:remoteNotifications:accessGranted:\(permissionDenied)")
            let enabledTypes = UIApplication.shared.currentUserNotificationSettings
            print("enabledTypes:\(String(describing: enabledTypes?.types))")
            if enabledTypes!.types.rawValue == 0 { // types are not enabled
                if let _ = UserDefaults.standard.value(forKey: "remoteNotificationsPermissionsStatus") as? Data { // doesn't matter if authorized, only display if user hasn't been prompted to enable remote notifications yet
                }
                else { // hasn't been determined yet
                    permissionsNotAuthorized.append(.remoteNotifications)
                }
            }
            else {
                if let permissionsView = viewControllerView.viewWithTag(890) {
                    if let remoteNotificationsButton = permissionsView.viewWithTag(300) as? UIButton {
                        //remoteNotificationsButton.userInteractionEnabled = false
                        /*
                        // turn button blue
                        remoteNotificationsButton.backgroundColor = MYColor.rallyBlue
                        remoteNotificationsButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        remoteNotificationsButton.imageView!.tintColor = UIColor.whiteColor()
                        let checkImage = UIImage(named:"check")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                        remoteNotificationsButton.setImage(checkImage, forState: .Normal)*/
                        if remoteNotificationsButton.isUserInteractionEnabled {
                            print("remoteNotificationsButton.userInteractionEnabled is true, above permissionEnabledHandler call")
                            permissionEnabledHandler(.remoteNotifications) // ensures button is marked blue if user enables in settings and comes back to app
                            //webCallStatus.testVariable = 0
                        }
                    }
                }
            }
            //if !remoteNotificationsAccessGranted {
                //permissionsNotAuthorized.append(.remoteNotifications)
            //}
            fallthrough
        default:
            print("Switch case: default")
            print("permissionsNotAuthorized.count equal zero:\(permissionsNotAuthorized)")
            if permissionsNotAuthorized.count  > 0 {
                // check if denied permissions view was displayed
                if let permissionsView = viewControllerView.viewWithTag(890) {
                    if let permissionsDeniedView = permissionsView.viewWithTag(709) {
                        // check if specific case that was being alerted for is now enabled. if so dismiss
                        if permissionsDeniedView.viewWithTag(710) != nil { // location case
                            if permissionsNotAuthorized.contains(.location) {
                                // do nothing, still not authorized
                            }
                            else {
                                permissionsDeniedView.removeFromSuperview()
                            }
                        }
                        if permissionsDeniedView.viewWithTag(711) != nil { // contactsCase
                            if permissionsNotAuthorized.contains(.contacts) {
                                // do nothing, still not authorized
                            }
                            else {
                                permissionsDeniedView.removeFromSuperview()
                            }
                        }
                        if permissionsDeniedView.viewWithTag(712) != nil { // remoteNotificationsCase
                            if permissionsNotAuthorized.contains(.remoteNotifications) {
                                // do nothing, still not authorized
                            }
                            else {
                                permissionsDeniedView.removeFromSuperview()
                            }
                        }
                    }
                }
                print("permissionsNotAuthorized.count greater than zero:\(permissionsNotAuthorized)")
                if viewControllerView.viewWithTag(890) == nil { // if permissionView already exists, do nothing
                    let dullingViewForViewControllerView = DullingViewClass.defineDullingView(0.47)
                    let permissionsView = definePermissionsView(permissionsNotAuthorized)
                    displayDullingAndPermissionsView(viewControllerView,dullingViewForViewControllerView: dullingViewForViewControllerView, permissionsView: permissionsView)
                }
                return false // all permissions authorized is false
            }
            else {
                if let _ = viewControllerView.viewWithTag(890) { // 890 is permissionsView tag
                    dismissDullingAndPermissionsView(viewControllerView)
                }
                return true // all permissions authorized is true
            }
        }
    }
    
    func permissionEnabledHandler(_ permissionCaseValue: permissionCase) {
        print("func permissionEnabledHandler is running...")
        // get button instance for enabled permission
        let viewControllerView = navigationControllerClassVariable.view
        print("permissionEnabledHandler:viewControllerView:\(String(describing: viewControllerView))")
        if let permissionsView = viewControllerView?.viewWithTag(890) {
            var permissionsButton: UIButton!
            switch permissionCaseValue {
            case .remoteNotifications:
                if let remoteNotificationsButton = permissionsView.viewWithTag(300) as? UIButton {
                    print("remoteNotificationsButton")
                    permissionsButton = remoteNotificationsButton
                }
            case .contacts:
                if let contactsButton = permissionsView.viewWithTag(301) as? UIButton {
                    print("permissionEnabledHandler:contactsButton")
                    permissionsButton = contactsButton
                }
            case .location:
                if let locationButton = permissionsView.viewWithTag(302) as? UIButton {
                    print("locationButton")
                    permissionsButton = locationButton
                }
            default:
                break
            }
            // disable button
            permissionsButton.isUserInteractionEnabled = false
            // turn button blue
            permissionsButton.backgroundColor = MYColor.rallyBlue
            permissionsButton.setTitleColor(UIColor.white, for: UIControlState())
            permissionsButton.imageView!.tintColor = UIColor.white
            let checkImage = UIImage(named:"check")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            permissionsButton.setImage(checkImage, for: UIControlState())
            // determine if all permissions are enabled, if so dismiss permissionsView
            let accessGranted = permissionsAccessHandler(.location,navigationController: navigationControllerClassVariable)
            if accessGranted { // if true, all permissions are enabled, so dismiss permissionsView
                // move view down off page, when complete removeFromSuperview()
                //if webCallStatus.updateContactsInfoFromWebService == 1 { // if contacts were already loaded properly on app load and permissions were disabled mid-use, you may dismiss views instead of holding views until contacts are loaded since they already are loaded
                    dismissDullingAndPermissionsView(viewControllerView!)
                //}
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) { // used as a completion handler to user responding to location services alert request, not used to determine permission access status
        print("func locationManager:didChangeAuthorizationStatus is running...")
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            print("Do nothing")
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            //respondToUserRemoteNotificationDialogSelection()
            permissionEnabledHandler(.location)
            break
            // below case is not currently requested but may switch to it in future
            /*
             case .AuthorizedAlways:
             // If always authorized
             permissionsHelperClass.userLocation = nil
             locationManager.startUpdatingLocation()
             break
             */
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("func locationManager:didUpdateLocations is running...")
        // http://stackoverflow.com/questions/31790264/location-constantly-updating-problems-ios-swift
        let location = locations.last
        if let locationValue = location {
            let center = CLLocationCoordinate2D(latitude: locationValue.coordinate.latitude, longitude: locationValue.coordinate.longitude)
            if permissionsHelperClass.userLocation == nil {
                permissionsHelperClass.userLocation = center
                manager.stopUpdatingLocation()
                //print("permissionsHelperClass.userLocation:\(permissionsHelperClass.permissionsHelperClassSingleton.userLocation)")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "locationManagerFetchedUserRegion"), object: nil)
            }
        }
    }
    
    func respondToUserRemoteNotificationDialogSelection(_ notification: Notification) {
        print("func respondToUserRemoteNotificationDialogSelection is running...")
        guard let userInfo = (notification as NSNotification).userInfo else {
            print("func updateRallyDetails: guard let userInfo = notification.userInfo else {")
            return
        }
        guard let permissionEnabled = userInfo["enabledRemoteNotificationPermissionTypes"] as? Bool  else {
            print("func updateRallyDetails: guard let permissionEnabled = userInfo[] as? Bool  else {")
            return
        }
        var result: NSString!
        if permissionEnabled {
            result = "Enabled"
            permissionEnabledHandler(.remoteNotifications)
        }
        else {
            result = "Denied"
        }
        let data = result.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: true)
        if let permissionsStatus: Data = data {
            NSUserDefaultsClass.updateRemoteNotificationsPermissionsStatus(permissionsStatus)
        }
        if !permissionEnabled { // need to update NSUserDefaultsClass, updateRemoteNotificationsPermissionsStatus for this to return an up to date accessGranted value
            let viewControllerView = navigationControllerClassVariable.view
            let accessGranted = permissionsAccessHandler(.location,navigationController: navigationControllerClassVariable)
            if accessGranted { // if true, all permissions are enabled, so dismiss permissionsView
                // move view down off page, when complete removeFromSuperview()
                //if webCallStatus.updateContactsInfoFromWebService == 1 { // if contacts were already loaded properly on app load and permissions were disabled mid-use, you may dismiss views instead of holding views until contacts are loaded since they already are loaded
                dismissDullingAndPermissionsView(viewControllerView!)
                //}
            }
        }
    }
    
    func onRemoteNotificationsButtonTouch(_ sender: UIButton!) {
        print("func onRemoteNotificationsButtonTouch is running...")
        // Register for remote notifications
        if let _ = UserDefaults.standard.value(forKey: "remoteNotificationsPermissionsStatus") as? Data {
            // user has already denied notifications permissions, display denied permissions view (though value can hold enabled or denied, this method can only be triggered upon touching button which can only occur if permissions haven't been enabled (occurs only for restricted, denied, or undetermined cases)
            _ = permissionsHelperClass.permissionsHelperClassSingleton.definePermissionsDeniedAlert(.remoteNotifications, superView: sender.superview!)
        }
        else { // value for key was nil, that means notifications haven't been determined yet
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        // [END register_for_remote_notifications]
    }
    
    func onContactsButtonTouch(_ sender: UIButton!) {
        print("func onContactsButtonTouch is running...")
        /*
        let addressBookHelperClassInstance = addressBookHelperClass()
        let (_,contactsIOS9AccessStatus): (Bool,CNAuthorizationStatus) = addressBookHelperClassInstance.addressBookIOS9AccessStatus()
        switch contactsIOS9AccessStatus {
        case .notDetermined:
            print("onContactsButtonTouch:.notDetermined case is running...")
            // request permission
            addressBookHelperClassInstance.requestForAccess { (accessGranted) -> Void in
                if accessGranted {
                    // change box color to blue and mark enabled contacts complete
                    // enable standard contacts workflow
                    self.permissionEnabledHandler(.contacts)
                    // compile contacts from address book, send to backend, return updated address book, format, and append up to date contact list to friends array
                    /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                        addressBookHelperClassInstance.loadAddressBookContacts()
                    })*/
                }
                else {
                    // display permissionsDeniedAlert
                    _ = permissionsHelperClass.permissionsHelperClassSingleton.definePermissionsDeniedAlert(.contacts, superView: sender.superview!)
                }
            }
        case .restricted, .denied:
            print("onContactsButtonTouch:.Restricted case is running...")
            _ = permissionsHelperClass.permissionsHelperClassSingleton.definePermissionsDeniedAlert(.contacts, superView: sender.superview!)
        case .authorized:
            // highly unlikely user could accept permissions before code executes but do anyway; change box color to blue and mark enabled contacts complete
            print("onContactsButtonTouch:.Authorized is running...")
            // compile contacts from address book, send to backend, return updated address book, format, and append up to date contact list to friends array
            /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    addressBookHelperClassInstance.loadAddressBookContacts()
            })*/
        }
         */
    }
    
    func onLocationButtonTouch(_ sender: UIButton!) {
        print("func onLocationButtonTouch is running...")
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                permissionsHelperClass.locationManagerSingleton.requestWhenInUseAuthorization()
                print(".NotDetermined")
            case .restricted, .denied:
                _ = permissionsHelperClass.permissionsHelperClassSingleton.definePermissionsDeniedAlert(.location, superView: sender.superview!)
            case .authorizedAlways, .authorizedWhenInUse:
                print("do nothing")
            }
        }
        else { // location services off (Settings > Privacy > Location Services), present alert to turn on location
            //permissionsHelperClass.sharedInstance.definePermissionsDeniedAlert(.locationEnabled, superView: view) // may need to dismiss alert on re-entering foreground
            permissionsHelperClass.locationManagerSingleton.requestWhenInUseAuthorization() // method call when location services disabled prompts Apple to display alert for user to re-enable location services with proper deep-link. apple only displays alert one time per app per disabling of location services
        }
    }
    
    fileprivate lazy var permissionsWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = self.permissionsControllerNav
        return window
    }()
    
    let appDelegateReference  = AppDelegate.getAppDelegate()
    
    fileprivate lazy var permissionsControllerNav:UINavigationController = {
        let customNavBar = NoStatusOrNavigationBarViewController()
        let permissionsControllerNavigationController = UINavigationController(rootViewController: customNavBar)
        //customNavBar
        customNavBar.setNeedsStatusBarAppearanceUpdate()
        
        /*
        override var prefersStatusBarHidden : Bool {
            get {
                return self.statusBarDisplayState
            }
        }
        */
        // hide status bar
        permissionsControllerNavigationController.isNavigationBarHidden = true
        return permissionsControllerNavigationController
    }()
    
    func displayDullingAndPermissionsView(_ viewControllerView: UIView, dullingViewForViewControllerView: UIView, permissionsView: UIView) {
        print("func displayDullingAndPermissionsView is running...")
        let permissionsViewYDisplacement = viewControllerView.frame.height-permissionsView.frame.origin.y
        permissionsView.frame.origin.y += permissionsViewYDisplacement
        let dullingViewBackgroundColor: UIColor = dullingViewForViewControllerView.backgroundColor!
        //dullingViewForViewControllerView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.0)
        dullingViewForViewControllerView.backgroundColor = .clear
        
        // set permissionsView and dullingView as subviews of permissions window nav controller view
        // reset default window prior to setting permissionsWindow as top-most window
        if let defaultWindow = appDelegateReference.window {
            defaultWindow.makeKeyAndVisible()
        }
        permissionsWindow.makeKeyAndVisible()
        permissionsWindow.rootViewController!.view.addSubview(dullingViewForViewControllerView)
        permissionsWindow.rootViewController!.view.addSubview(permissionsView)
        
        // animate subviews onto screen
        UIView.animate(withDuration: 0.5,
                       delay: 0.22,
                       options: UIViewAnimationOptions.curveLinear,
                       animations: { () -> Void in
                        print("animateWithDuration permissions view is running...")
                        permissionsView.center.y -= permissionsViewYDisplacement
                        dullingViewForViewControllerView.backgroundColor = dullingViewBackgroundColor
        },
                       completion: { finished in
                        print("searchBarShouldBeginEditing:animateWithDuration:completion")
        })
    }
    
    func dismissDullingAndPermissionsView(_ viewControllerView: UIView) {
        print("func dismissDullingAndPermissionsView is running...")
        if let permissionsView = viewControllerView.viewWithTag(890) {
            if let dullingViewForViewControllerView = viewControllerView.viewWithTag(165) {
                let permissionsViewYDisplacement = viewControllerView.frame.height-permissionsView.frame.origin.y
                UIView.animate(withDuration: 0.5,
                   delay: 0.22,
                   options: UIViewAnimationOptions.curveLinear,
                   animations: { () -> Void in
                    print("animateWithDuration permissions view is running...")
                    permissionsView.center.y += permissionsViewYDisplacement
                    dullingViewForViewControllerView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.0)
                },
                completion: { finished in
                    print("animateWithDuration permissions view: completion")
                })
                delay(0.75) {
                    permissionsView.removeFromSuperview()
                    dullingViewForViewControllerView.removeFromSuperview()
                    // reset default window prior to setting permissionsWindow as top-most window
                    if let defaultWindow = self.appDelegateReference.window {
                        defaultWindow.makeKeyAndVisible()
                    }
                }
            }
        }
    }
}
