//
//  CameraPhotoLibrary.swift
//  Rally
//
//  Created by Ian Moses on 8/15/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class CameraPhotoLibrary: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    fileprivate var referenceViewController: UIViewController!
    
    func displayCameraAndPhotosActionSheet(_ viewControllerReference: UIViewController) {
        print("func displayCameraAndPhotosActionSheet is running...")
        // 1
        referenceViewController = viewControllerReference
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // 2
        let takeProfilePhotoAction = UIAlertAction(title: "Take profile photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCameraButton()
        })
        let chooseFromLibraryAction = UIAlertAction(title: "Choose from library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openPhotoLibraryButton()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            optionMenu.dismiss(animated: true, completion: nil)
        })
        
        
        // 4
        optionMenu.addAction(takeProfilePhotoAction)
        optionMenu.addAction(chooseFromLibraryAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        viewControllerReference.present(optionMenu, animated: true, completion: nil)
    }
    
    func openCameraButton() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            print("func openCameraButton is running...")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            // default sourceType is .PhotoLibrary
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            referenceViewController.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibraryButton() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            print("func openPhotoLibraryButton is running...")
            let imagePicker = MYImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            //referenceViewController.navigationController?.navigationBar.barTintColor = UIColor.green
            referenceViewController.present(imagePicker, animated: true, completion: nil)
            // set cancel button to white
            UINavigationBar.appearance().tintColor = UIColor.white
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        //navigationController.navigationBar.barTintColor = UIColor.green
        viewController.navigationItem.title = "Choose Profile Picture"
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        print("func imagePickerController is running...")
        guard let fullSizeData = UIImageJPEGRepresentation(image, 1.0) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        let numberOfBytes: Int = fullSizeData.count
        // 300 kb is target size to reduce full size image to to get large profile picture size
        var largeScaleRatio: CGFloat = CGFloat(300/numberOfBytes)
        if largeScaleRatio > 1 { // protects against accidentally scaling up image in case data was below 300
            largeScaleRatio = 1.0
        }
        // 75 kb is target size to reduce full size image to to get thumbnail profile picture size
        var thumbnailScaleRatio: CGFloat = CGFloat(75/numberOfBytes)
        if thumbnailScaleRatio > 1 { // protects against accidentally scaling up image in case data was below 300
            thumbnailScaleRatio = 1.0
        }
        //let ciImage = CIImage(image: image)!
        //let context = CIContext(options: nil)
        //let imageAsCGImage = context.createCGImage(ciImage, from: ciImage.extent)!
        //print(imageAsCGImage.height)
        //print(imageAsCGImage.width)
        //let bytesPerPixel: Int = imageAsCGImage.bitsPerPixel/8
        // resize to get large profile picture
        guard let largeData = UIImageJPEGRepresentation(image.correctlyOrientedImage(), largeScaleRatio) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        let largeProfilePicture = UIImage(data: largeData)!
        LoadLocalFile.saveLargeProfileImageToLocalFile(largeProfileImage: largeProfilePicture)
        MYUser.largeProfileImage = largeProfilePicture
        // resize to get thumbnail profile picture
        guard let thumbnailData = UIImageJPEGRepresentation(image.correctlyOrientedImage(), thumbnailScaleRatio) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        let thumbnailProfileImage = UIImage(data: thumbnailData)!
        LoadLocalFile.saveThumbnailProfileImageToLocalFile(thumbnailProfileImage: thumbnailProfileImage)
        MYUser.thumbnailProfileImage = thumbnailProfileImage
        // update database with profile picture
        self.uploadImage(largeData)
        // dismiss action sheet
        referenceViewController.dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(_ largeData: Data) {
        print("func uploadImage is running...")
        let intent = WebServiceCallIntent.uploadLargeProfileImage(largeData)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                
                // update user's profile picture in communityMemberThumbnailsGlobalDictionary
                communityMemberThumbnailsGlobalDictionary[MYUser.ID] = MYUser.thumbnailProfileImage!
                
                // post notification user profile pic has changed
                DispatchQueue.main.async(execute: { () -> Void in
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "profilePictureUploadResultReturnedFromWCF"), object: nil)
                })
            }
            else { // post message saying could not update profile pic. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
}

