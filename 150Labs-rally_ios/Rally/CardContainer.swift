//
//  CardContainer.swift
//  Rally
//
//  Created by Ian Moses on 1/15/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class CardContainer: UIView { // card container view only holds a single card type
    var cardType: CardType!
    var quadrant: CGFloat!
    var containerSide: CardContainerSide {
        get {
            if self.frame.origin.x > ScreenDimension.width*3/2 {
                return .right
            }
            else {
                return .left
            }
        }
    }
    
    init(cardType: CardType) {
        // initialize properties
        self.cardType = cardType
        // set default quadrants
        switch cardType {
        case .publicCommunity:
            self.quadrant = 1.0
        case .privateCommunity:
            self.quadrant = 2.0
        default:
            break
        }
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func animate(toPosition: CGFloat) {
        print("func animateCommunityCardContainer is running...")
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: .curveLinear,animations: {self.center.x = toPosition}, completion: nil)
    }
}
