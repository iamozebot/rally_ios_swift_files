//
//  CollectionViewProtocol.swift
//  Rally
//
//  Created by Ian Moses on 11/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

protocol ThreadsDirectMessagesCollectionView {
    var leftPanelDataComputedProperty: LeftPanelViewControllerDataSource { get }
    mutating func filter(threads: [Thread], by prefix: String) -> [Thread]
    mutating func filter(members: [Friend], by prefix: String) -> [Friend]
}

extension ThreadsDirectMessagesCollectionView {
    func filter(members: [Friend], by prefix: String) -> [Friend] {
        var filteredMembers = [Friend]()
        // filter members by prefix
        for member in members {
            guard let firstName = member.firstName else { // if no name, don't try filtering
                continue
            }
            // compare query term to first name length
            if prefix.characters.count <= firstName.characters.count { // if search term is less or equal to member name, check for inclusion in filtered results. Otherwise, do nothing
                // match by start of any word in search i.e.: "gro" matches to "pokemon group"
                let memberNameSplitByWordsArray = firstName.characters.split{$0 == " "}.map(String.init)
                for word in memberNameSplitByWordsArray {
                    if word.lowercased().hasPrefix(prefix.lowercased()) {
                        filteredMembers.append(member)
                        continue // move to next member in community
                    }
                }
            }
        }
        return filteredMembers
    }
    
    func filter(threads: [Thread], by prefix: String) -> [Thread] {
        var filteredThreads = [Thread]()
        // filter threads by prefix
        for thread in threads {
            // compare query term to thread name length
            guard let title = thread.title else { // if thread has no title, don't try filtering
                continue
            }
            if prefix.characters.count <= title.characters.count { // if search term is less or equal to thread title, check for inclusion in filtered results. Otherwise, do nothing
                // match by start of any word in search i.e.: "gro" matches to "pokemon group"
                let threadTitleSplitByWordsArray = title.characters.split{$0 == " "}.map(String.init)
                for word in threadTitleSplitByWordsArray {
                    if word.lowercased().hasPrefix(prefix.lowercased()) {
                        filteredThreads.append(thread)
                        continue // move to next of user's threads
                    }
                }
            }
        }
        return filteredThreads
    }
}
