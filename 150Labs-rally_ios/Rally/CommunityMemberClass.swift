//
//  CommunityMemberClass.swift
//  Rally
//
//  Created by Ian Moses on 10/1/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation


class CommunityMember: User, Invitable { // community members can be invited to threads and rallys
    var community: Community
    var selected = false // default value
    var inviteMethods: [InviteMethod] = [InviteMethod.rally] // default value (only case currently, may change in future by allowing users to push invites through multiple services)
    // community member should include thread class properties, not user class
    
    init(community: Community) {
        print("class CommunityMember:init is running...")
        self.community = community
    }
}

class RallyMember: User, Invitable {
    var rally: Rally
    var selected = true // default value
    var inviteMethods: [InviteMethod] = [InviteMethod.rally] // default value (only case currently, may change in future by allowing users to push invites through multiple services)
    // community member should include thread class properties, not user class
    
    init(rally: Rally) {
        print("class RallyMember:init is running...")
        self.rally = rally
    }
}
