//
//  CommunityMessagingViewController.swift
//  Rally
//
//  Created by Ian Moses on 8/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommunityMessagingDataSource {
    var profilePicture: passThroughView!
    var activeThread: Thread? {
        get {
            if let value = self.community {
                if let secondValue = value.activeThread {
                    return secondValue
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
        set(newValue) {
            self.community?.activeThread = newValue
        }
    }
    var community: Community? = MYUser.defaultCommunityForFocus() // default value exists if communities are returned so could be nil

    var messageBlurbs = [MYMessageBlurb]()
    var parentVC: ContainerViewController!
    
    init() {
        // generate community profile picture
        self.profilePicture = initializeCommunityProfilePicture()
        //self.notificationsStatus = community.notificationsStatus
    }
    
    func initializeCommunityProfilePicture()-> passThroughView {
        //let picturesDisplayData: PicturesDisplayDataSource = PicturesDisplayDataSource(nil, userRallysArrayIndex: nil, pictureContainer: pictureContainer, cardTypeValue: nil, imageSideLength: )
        //let communityPictureContainer: passThroughView = PicturesDisplayHelper(picturesDisplayData).defineRallyOrGroupCardPictures()
        return passThroughView()
    }
}

class CommunityMessagingViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, PassDataToViewControllerDelegate {
    let scrollView = UIScrollView()
    var textBox: messagingTextView!
    var keyboardIsShown = false
    var delegate: CenterViewControllerDelegate?
    var communityMessagingData = CommunityMessagingDataSource()
    var profilePictureContainer = passThroughView() // navbar image
    var returningFromCreateThreadVC: Bool = false
    var returningFromDetailsVC: Bool = false
    var slidableHeaderSection: UIView?
    var textBoxAndSendButtonSuperview: UIView!
    var sendButton: UIButton! // UIButton(type: .system)
    var keyboardIsActive: Bool = false // default value
    
    fileprivate lazy var largeProfileImageWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = self.largeProfileImageControllerNav
        return window
    }()
    
    let appDelegateReference  = AppDelegate.getAppDelegate()
    
    fileprivate lazy var largeProfileImageControllerNav:UINavigationController = {
        let customNavBar = NoStatusOrNavigationBarViewController()
        let largeProfileImageControllerNavigationController = UINavigationController(rootViewController: customNavBar)
        //customNavBar
        customNavBar.setNeedsStatusBarAppearanceUpdate()
        
        /*
         override var prefersStatusBarHidden : Bool {
         get {
         return self.statusBarDisplayState
         }
         }
         */
        // hide status bar
        largeProfileImageControllerNavigationController.isNavigationBarHidden = true
        return largeProfileImageControllerNavigationController
    }()

    
    
    override func viewDidLoad() {
        print("class CommunityMessagingViewController func ViewDidLoad is running...")
        super.viewDidLoad()
        WebCallStatus.communityMessagingViewControllerLoaded = 1
        view.backgroundColor = MYColor.lightGrey
        
        // initialize UI
        defineScrollView()
        defineTextBox()
        defineSendButton()
        defineTextBoxAndSendButtonSuperview()
        defineSlidableHeaderSection()
    }
    
    func reloadUI() {
        print("func reloadUI is running...")
        
        // reload community picture given updated data
        defineProfilePictureContainer()
        let communityPictureTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.handlecommunityPictureTap(_:)))
        profilePictureContainer.addGestureRecognizer(communityPictureTap)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profilePictureContainer)
        
        // reload community title
        if let community = communityMessagingData.community {
            navigationItem.title = community.name
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 18)!, NSForegroundColorAttributeName: UIColor.white]
        }
        
        // reload slidable header section data (active thread name)
        defineSlidableHeaderSection()
        
        // reload messages data (message blurbs)
        defineMessageBlurbs()
        
        // reload keyboard data (text in text box)
    }
    
    func childViewControllerResponse(returningFromCreateThreadVC: Bool) {
        print("func childViewControllerResponse is running...")
        self.returningFromCreateThreadVC = returningFromCreateThreadVC
    }
    
    func fetchActiveThreadMessagesFromWebService() {
        print("func fetchActiveThreadMessagesFromWebService is running...")
        guard communityMessagingData.community != nil else {
            print("communityMessagingData.community is nil, guard returning")
            return
        }
        let intent = WebServiceCallIntent.getCommThreadMessages(for: communityMessagingData.activeThread!)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    // load any messages
                    print("successfully completed fetching active thread messages..prepare to reload UI")
                    self.reloadUI()
                })
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func addNewMessageToMessagingThread(_ notification: Notification){
        print("func addNewMessageToMessagingThread is running...")
        // format message from gcm notification as a string to add the message in a new message blurb
        guard let userInfo = (notification as NSNotification).userInfo else {
            print("func updateRallyDetails: guard let userInfo = notification.userInfo else {")
            return
        }
        guard let payload = userInfo["payload"] as? NSString  else {
            return
        }
        let optionalDataPackage = payload.data(using: String.Encoding.utf8.rawValue)
        guard let unwrappedDataPackage = optionalDataPackage else {
            return
        }
        let swiftyJSONDataPackage = JSON(data:unwrappedDataPackage)
        let commThreadMessageID = swiftyJSONDataPackage["CommThreadMessageID"].int
        guard let unwrappedCommThreadMessageID = commThreadMessageID else {
            return
        }
        print("unwrappedCommThreadMessageID:\(unwrappedCommThreadMessageID)")
        let intent = WebServiceCallIntent.getCommThreadMessage(commThreadMessageID: unwrappedCommThreadMessageID)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                // reload data??? or just reload newest message at end of communityMessagingData.activeThread!.messages
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getCommThreadMessage(threadID, message) = returnIntent {
                        // update view with new message
                        if String(threadID) == self.communityMessagingData.activeThread!.threadID { // reload UI if new message is for activeThread
                            // if successful, add picture
                        }
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                    // if unsuccessful, do nothing (for now)? afterward, add picture showing message did not successfully send
                })
            }
        })
    }
    
    func appWillEnterForeground(_ notification:Notification) {
        print("func CommunityMessagingViewController:appWillEnterForeground is running...")
        // this assures user doesn't exit app mid-instance and turn off a permission, then re-enter and attempt to use permission when disabled and crash app
        // checks if any permissions are disabled, if so displays permissions access view handler
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        // check to determine if user should get kicked back to homeVC (rally is expired)
        // fetch updated rally info from webservice
        
        /*
        let communityID = communityMessagingData.community!.communityID as Int
        let getUpdatedRallyPackageintent = WebServiceCallIntent.getUpdatedCommunityPackage(communityMessagingData.community, communityID)
        let webServiceCallManagerGetUpdatedRallyPackage = WebServiceCallManager(intent: getUpdatedRallyPackageintent)
        webServiceCallManagerGetUpdatedRallyPackage.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally) = returnIntent {
                        DispatchQueue.main.async(execute: { () -> Void in
                            // check if rallyisActive and if rallystartDateTime has passed
                            self.communityMessagingData.community = updatedRally
                            if self.communityMessagingData.community!.isRallyActive == 1 {
                                print("do nothing, rally has not expired")
                            }
                            else {
                                if let rallyStartDateTime = self.communityMessagingData.community!.rallyStartDateTime {
                                    let order = dateHelperClass().timeFromNowUntilDate(rallyStartDateTime)
                                    // switch cases for whether or not current time is past start time
                                    switch order {
                                    case .orderedDescending:
                                        // rally started and is no longer active (it has expired), so kick user back to homeVC
                                        self.navigationController!.popToRootViewController(animated: true)
                                    case .orderedAscending:
                                        // rally has not started yet
                                        print("do nothing")
                                    case .orderedSame:
                                        // rally is starting at this very second
                                        print("do nothing")
                                    }
                                }
                            }
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        */
        // Update rally details (location, members joined, title, date, time to display in container)
        
        
        /*
        if let slidableHeaderSection = view.viewWithTag(564) {
            slidableHeaderSection.removeFromSuperview()
        }
        defineSlidableHeaderSection()*/
        
        
        // fetch updated messages
        
        /*
        let intent = WebServiceCallIntent.getThreadMessages(communityMessagingData.activeThread)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getRallyMessages(updatedRallyObject) = returnIntent {
                        self.communityMessagingData.community = updatedRallyObject
                        DispatchQueue.main.async(execute: { () -> Void in
                            // remove old message blurbs
                            for subview in self.scrollView.subviews {
                                subview.removeFromSuperview()
                            }
                            // load any messages
                            self.defineMessageBlurbs()
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        */
    }
    
    func userCommunitiesReturnedFromBackend(_ notification:Notification) {
        print("func userCommunitiesReturnedFromBackend is running...")
        // upon user communities returned from backend:
        // 1. assign default community for default app focus
        communityMessagingData.community = MYUser.defaultCommunityForFocus()
        // 2. assign default thread for default app focus
        guard let community = communityMessagingData.community else {
            return
        }
        // reload community bar data (this is done to make UI feel more responsive, even though reloadUI is recalled after activeThreadMessages are fetched from web service successfully
            // reload data
        reloadUI()
        communityMessagingData.activeThread = community.defaultThreadForFocus()
        // fetch messages for active thread
        fetchActiveThreadMessagesFromWebService()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class CommunityMessagingViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        
        // set status bar UI
        communityMessagingData.parentVC.statusBarDisplayState = false
        communityMessagingData.parentVC.statusBarAnimationState = .none
        communityMessagingData.parentVC.setNeedsStatusBarAppearanceUpdate()
        
        if !returningFromCreateThreadVC && !returningFromDetailsVC {
            // set nav bar UI
            navigationController?.isNavigationBarHidden = true
            navigationController?.setToolbarHidden(true, animated: false)
            navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
            navigationController?.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: 40)), forKeyPath: "navigationBar")
            
            // initialize sidePanelButton
            defineProfilePictureContainer()
            let communityPictureTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.handlecommunityPictureTap(_:)))
            profilePictureContainer.addGestureRecognizer(communityPictureTap)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profilePictureContainer)
        }
        else {
            //communityMessagingData.parentVC.statusBarDisplayState = true
            //communityMessagingData.parentVC.statusBarAnimationState = .slide
        }
        
        // set title
        if let community = communityMessagingData.community {
            navigationItem.title = community.name
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 18)!, NSForegroundColorAttributeName: UIColor.white]
        }
        
        // initialize keyboard notification broadcast observers
        startObservingNotificationEvents()
    }
    
    override func viewDidLayoutSubviews() {
        print("func viewDidLayoutSubviews is running...")
        super.viewDidLayoutSubviews()
        if returningFromCreateThreadVC {
            self.communityMessagingData.parentVC.hideStatusBar()
            returningFromCreateThreadVC = false // reset flag
        }
        if returningFromDetailsVC {
            returningFromDetailsVC = false // reset flag
        }
        //.layoutIfNeeded()
        //.layoutSubviews()
    }
    
    func defineProfilePictureContainer() {
        print("func defineProfilePictureContainer is running...")
        let itemSideLength: CGFloat = UINavigationBarTaller.navigationBarHeight*39/100
        
        /*
        let barButtonItem = self.navigationItem.leftBarButtonItem!
        print("barButtonItem non-nil")
        let buttonItemView = barButtonItem.value(forKey:"view")
        //var buttonItemSize = buttonItemView?.size
        let itemSideLength: CGFloat = (buttonItemView as! UIView).frame.size.height
        */
        
        var communityMembers = [Friend]()
        if let community = communityMessagingData.community {
            communityMembers = community.members
        }
        let picturesDisplayData: PicturesDisplayDataSource = PicturesDisplayDataSource(nil, communityMembers, profilePictureContainer, .createCommunity(nil), itemSideLength)
        profilePictureContainer = PicturesDisplayHelper(picturesDisplayData: picturesDisplayData).defineRallyOrGroupCardPictures()
        profilePictureContainer.frame.size = CGSize(width:itemSideLength*2,height:itemSideLength*2)
        
        profilePictureContainer.center.x = profilePictureContainer.center.x-itemSideLength/4
        profilePictureContainer.center.y = profilePictureContainer.center.y-itemSideLength/4
        profilePictureContainer.layer.cornerRadius = profilePictureContainer.frame.height/2
        profilePictureContainer.clipsToBounds = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("func class MessagingViewController, viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        // badges managed in viewWillDisappear to avoid badge relevant to screen user is on being added. in this case, need to be able to offset badge since user sees new information update live on screen
        // subtract badges associated with new Rallys
        // UIApplication.sharedApplication().applicationIconBadgeNumber -=
        stopObservingNotificationEvents()
    }
    
    func handleSlidableHeaderSectionTap(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func handleSlidableHeaderSectionTap is running...")
        if let secondaryContainerVC = communityMessagingData.parentVC.secondaryContainerViewController as? ContainerViewController {
            switch secondaryContainerVC.currentState {
            case .bothCollapsed:
                secondaryContainerVC.animateBottomPanel(shouldExpand: true)
            case .bottomPanelExpanded:
                secondaryContainerVC.view.frame.origin.y = 80
                communityMessagingData.parentVC.centerNavigationControllerCP.isNavigationBarHidden = false
                secondaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = true
                secondaryContainerVC.animateCenterPanelYPosition(targetPosition: ScreenDimension.height-ScreenDimension.threadBarHeight) { _ in
                    secondaryContainerVC.currentState = .bothCollapsed
                }
            case .bothExpanded:
                secondaryContainerVC.animateLeftPanel(shouldExpand: false)
            }
            // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
            view.endEditing(true)
        }
    }
    
    func handlecommunityPictureTap(_ sender: UIBarButtonItem!) {
        print("func handlecommunityPictureTap is running...")
        // animate open the community side panel view controller
        if let secondaryContainerVC = communityMessagingData.parentVC.secondaryContainerViewController as? ContainerViewController {
            switch secondaryContainerVC.currentState {
            case .bottomPanelExpanded:
                secondaryContainerVC.horizontalPanBegan(panDirection: .right)
                secondaryContainerVC.animateLeftPanel(shouldExpand: true)
            case .bothExpanded:
                secondaryContainerVC.animateLeftPanel(shouldExpand: false)
            default:
                break
            }
        }
    }
    
    func startObservingNotificationEvents() {
        print("func startObservingKeyboardEvents is running...")
        
        // subscribe to gcm remote notification broadcast to be notified of new messages
        NotificationCenter.default.addObserver(self, selector: #selector(CommunityMessagingViewController.addNewMessageToMessagingThread(_:)), name: NSNotification.Name(rawValue: "CommThreadMessage"), object: nil)
        
        // 'fetch user communities' from backend- status complete observer
        NotificationCenter.default.addObserver(self, selector: #selector(CommunityMessagingViewController.userCommunitiesReturnedFromBackend(_:)), name: NSNotification.Name(rawValue: "userCommunitiesReturnedFromBackend") , object: nil)

        // app enters foreground observer
        NotificationCenter.default.addObserver(self, selector: #selector(CommunityMessagingViewController.appWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        // keyboard observers
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CommunityMessagingViewController.keyboardWillShow),
                                                         name:NSNotification.Name.UIKeyboardWillShow,
                                                         object:nil)
        NotificationCenter.default.addObserver(self,
                                                         selector:#selector(CommunityMessagingViewController.keyboardWillHide),
                                                         name:NSNotification.Name.UIKeyboardWillHide,
                                                         object:nil)
        //textBox.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    func stopObservingNotificationEvents() {
        print("func stopObservingNotificationEvents is running...")
        
        // 'fetch user communities' from backend- status complete observer
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userCommunitiesReturnedFromBackend"), object: nil)
        
        // keyboard observers
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // observing announcements for new messages within community; believe notification name is "RallyMessage" but not positive
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RallyMessage:"), object: nil)
        
        // app enters foreground observer
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        /*
         // not sure why but navigationController is nil at this point, I'm guessing it's releasing before this method runs or smt.In any case, textBox is not in memory since this view is completely removed from stack so no real need to remove observer, just best practice I'm trying to follow
         textBox!.removeObserver(self, forKeyPath: "contentSize")
         */
    }
    
    func defineSlidableHeaderSection() {
        print("func defineSlidableHeaderSection is running...")
        
        func generateSHCSubviews(in sHC: UIView, using thread: Thread) {
            // generate thread name text view
            var threadNameTextViewFrame: CGRect!
            
            if let threadNameTextView = slidableHeaderSection?.viewWithTag(706) as? UILabel {
                threadNameTextView.font = UIFont.systemFont(ofSize: 19.75, weight: UIFontWeightMedium)
                if let title = thread.title { // gets title instance if thread & title are non-nil
                    threadNameTextView.text = title
                }
                threadNameTextView.sizeToFit()
                threadNameTextView.center.y = sHC.frame.height*62/100
                
                // change thread name font size based on user phone type
                var fontSize: CGFloat = 22
                let minSize: CGFloat = 16 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
                while fontSize > minSize &&  threadNameTextView.sizeThatFits(CGSize(width: CGFloat(Float.greatestFiniteMagnitude),height: threadNameTextView.frame.size.height)).width >= threadNameTextView.frame.size.width {
                    fontSize -= 1.0
                    threadNameTextView.font = UIFont.systemFont(ofSize: fontSize)
                }
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    threadNameTextView.font = UIFont.systemFont(ofSize: fontSize-1)
                default:
                    print("Do nothing...")
                }
                threadNameTextViewFrame = threadNameTextView.frame
            }
            else {
                let threadNameTextView = UILabel(frame: CGRect(x: ScreenDimension.width*4/100,y: 0,width: ScreenDimension.width,height: CGFloat.greatestFiniteMagnitude))
                threadNameTextView.tag = 706
                threadNameTextView.isUserInteractionEnabled = false
                threadNameTextView.textColor = MYColor.lightBlack
                threadNameTextView.font = UIFont.systemFont(ofSize: 19.75, weight: UIFontWeightMedium)
                if let title = thread.title { // gets title instance if thread & title are non-nil
                    threadNameTextView.text = title
                }
                threadNameTextView.sizeToFit()
                threadNameTextView.center.y = sHC.frame.height*62/100
                sHC.addSubview(threadNameTextView)
                
                // change thread name font size based on user phone type
                var fontSize: CGFloat = 22
                let minSize: CGFloat = 16 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
                while fontSize > minSize &&  threadNameTextView.sizeThatFits(CGSize(width: CGFloat(Float.greatestFiniteMagnitude),height: threadNameTextView.frame.size.height)).width >= threadNameTextView.frame.size.width {
                    fontSize -= 1.0
                    threadNameTextView.font = UIFont.systemFont(ofSize: fontSize)
                }
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    threadNameTextView.font = UIFont.systemFont(ofSize: fontSize-1)
                default:
                    print("Do nothing...")
                }
                threadNameTextViewFrame = threadNameTextView.frame
            }
            
            // generate downfacing carrot button
            if (slidableHeaderSection?.viewWithTag(909) as? UIButton) == nil {
                let downfacingCarrotButton = UIButton()
                let downFacingCarrotImage = UIImage(named:"downfacingCarrot")!.withRenderingMode(.alwaysTemplate)
                downfacingCarrotButton.setImage(downFacingCarrotImage, for: UIControlState())
                downfacingCarrotButton.tag = 909
                downfacingCarrotButton.imageView!.tintColor = MYColor.lightBlack
                //let inset = menuContainer.frame.width*10.75/100
                //threadNotificationToggleButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
                downfacingCarrotButton.sizeToFit()
                downfacingCarrotButton.frame.origin.x = threadNameTextViewFrame.origin.x + threadNameTextViewFrame.width //+ ScreenDimension.screenWidth*0.25/100
                downfacingCarrotButton.center.y = sHC.frame.height*72.25/100
                downfacingCarrotButton.addTarget(self, action: #selector(CommunityMessagingViewController.onThreadDropdownTouch(_:)), for: .touchUpInside)
                sHC.addSubview(downfacingCarrotButton)
                //threadNameTextViewFrame = downfacingCarrotButton.frame
            }
            
            // define right-side button side lengths
            let imageViewSideLength = sHC.frame.width*8.2/100
            let imageviewSpacing = sHC.frame.width*6.5/100
            
            
            // HOOKING UP FEATURE IN FUTURE RELEASE
            /*
            // generate thread notification toggle button
            let threadNotificationToggleButton = MYToggleButton<NotificationsStatus>(toggleState: .active)
            let notificationStatusImage = UIImage(named:"notificationsOn")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            threadNotificationToggleButton.frame.size = CGSize(width: imageViewSideLength, height: imageViewSideLength)
            threadNotificationToggleButton.setImage(notificationStatusImage, for: UIControlState())
            threadNotificationToggleButton.imageView!.tintColor = MYColor.lightBlack
            //let inset = menuContainer.frame.width*10.75/100
            //threadNotificationToggleButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
            threadNotificationToggleButton.frame.origin.x = sHC.frame.width-3*imageViewSideLength-3*imageviewSpacing
            threadNotificationToggleButton.center.y = sHC.frame.height*50/100
            threadNotificationToggleButton.addTarget(self, action: #selector(CommunityMessagingViewController.onThreadNotificationToggleTouch(_:)), for: .touchUpInside)
            sHC.addSubview(threadNotificationToggleButton)
            */
            
            // generate add friends to thread button
            if (slidableHeaderSection?.viewWithTag(910) as? UIButton) == nil {
                let addFriendsToThreadButton = UIButton()
                addFriendsToThreadButton.frame.size = CGSize(width: imageViewSideLength, height: imageViewSideLength)
                addFriendsToThreadButton.setImage(UIImage(named:"addMember")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
                addFriendsToThreadButton.tag = 910
                addFriendsToThreadButton.imageView!.tintColor = MYColor.lightBlack
                //let inset = menuContainer.frame.width*10.75/100
                //threadNotificationToggleButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
                addFriendsToThreadButton.frame.origin.x = sHC.frame.width-2*imageViewSideLength-2*imageviewSpacing
                addFriendsToThreadButton.center.y = sHC.frame.height*50/100
                addFriendsToThreadButton.addTarget(self, action: #selector(CommunityMessagingViewController.onAddFriendsToThreadButtonTouch(_:)), for: .touchUpInside)
                sHC.addSubview(addFriendsToThreadButton)
            }
            
            // generate rally thread button
            if (slidableHeaderSection?.viewWithTag(911) as? UIButton) == nil {
                let rallyThreadButton = UIButton()
                rallyThreadButton.frame.size = CGSize(width: imageViewSideLength, height: imageViewSideLength)
                rallyThreadButton.setImage(UIImage(named:"rallyDuck")!.withRenderingMode(.alwaysOriginal), for: UIControlState())
                    rallyThreadButton.tag = 911
                //let inset = menuContainer.frame.width*10.75/100
                //threadNotificationToggleButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
                rallyThreadButton.clipsToBounds = true
                rallyThreadButton.layer.cornerRadius = rallyThreadButton.frame.height/2
                rallyThreadButton.frame.origin.x = sHC.frame.width-imageViewSideLength-imageviewSpacing
                rallyThreadButton.center.y = sHC.center.y
                rallyThreadButton.addTarget(self, action: #selector(CommunityMessagingViewController.handleRallyThreadButtonTap(_:)), for: .touchUpInside)
                sHC.addSubview(rallyThreadButton)
            }
        }
        
        // check for previously generated slidableHeaderSection subview
        if let sHC = slidableHeaderSection {
            
            // update interaction status (avoids user from pulling up slidable section if communities haven't returned)
            if communityMessagingData.community != nil {
                sHC.isUserInteractionEnabled = true
            }
            else {
                sHC.isUserInteractionEnabled = false
            }
            
            // display sHC thread subviews if active thread exists
            if let thread = communityMessagingData.activeThread {
                generateSHCSubviews(in: sHC, using: thread)
            }
        }
        else { // initialize slidableContainer
            // generate slidable container
            let sHC = UIView(frame: CGRect(x: 0,y: 0,width: ScreenDimension.width,height: ScreenDimension.height*9.16/100))
            slidableHeaderSection = sHC
            sHC.backgroundColor = .white
            
            // set interaction status (avoids user from pulling up slidable section if communities haven't returned)
            if communityMessagingData.community != nil {
                sHC.isUserInteractionEnabled = true
            }
            else {
                sHC.isUserInteractionEnabled = false
            }
            
            // display sHC thread subviews if active thread exists
            if let thread = communityMessagingData.activeThread {
                generateSHCSubviews(in: sHC, using: thread)
            }
            else { // add code to display message saying user is not a part of any communities, prompty to create or join a community
                print("No active threads")
                // initialize 'not in any communities, create or join a new community' textview
                
            }
            
            // set tap gesture recognizer
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.handleSlidableHeaderSectionTap(_:)))
            sHC.addGestureRecognizer(tap)
            
            // set vertical pan gesture recognizer
            if let secondaryContainerVC = communityMessagingData.parentVC.secondaryContainerViewController as? ContainerViewController {
                // add pan gesture for thread bar vertical dragging
                let verticalPanGestureRecognizer = UIPanGestureRecognizer(target: secondaryContainerVC, action: #selector(ContainerViewController.handleThreadBarPanGesture))
                sHC.addGestureRecognizer(verticalPanGestureRecognizer)
            }
            
            // add slidableHeaderSection as subview of view
            view.addSubview(sHC)
        }
    }
    
    func onThreadDropdownTouch(_ sender: UIButton!) {
        print("func onThreadDropdownTouch is running...")
        // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
        view.endEditing(true)
    }
    
    func onThreadNotificationToggleTouch(_ sender: UIButton!) {
        print("func onThreadNotificationToggleTouch is running...")
        // Switch toggle state and update button image for toggle state
        let toggleButton = sender as! MYToggleButton<NotificationsStatus>
        switch toggleButton.toggleState! {
        case .muted:
            toggleButton.toggleState = .active
            toggleButton.setImage(UIImage(named:"notificationsOn")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        case .active:
            toggleButton.toggleState = .muted
            toggleButton.setImage(UIImage(named:"notificationsOff")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        }
        
    }
    
    func onAddFriendsToThreadButtonTouch(_ sender: UIButton!) {
        print("func onAddFriendsToThreadButtonTouch is running...")
        // push select friends view controller onto stack (NOTE: Need this view controller to remain completely the same, no data loss for any reason)
        let moveToInviteViewController: InviteViewController = InviteViewController(mode: .thread)
        //friends = communityMessagingData.community!.members
        guard let community = communityMessagingData.community else {
            return
        }
        // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
        view.endEditing(true)
        moveToInviteViewController.community = community
        self.navigationController!.pushViewController(moveToInviteViewController, animated: true)
    }
    
    func handleRallyThreadButtonTap(_ sender: UITapGestureRecognizer!) {
        print("func handleRallyThreadButtonTap is running...")
        // push select friends view controller onto stack (NOTE: Need this view controller to remain completely the same, no data loss for any reason)
        let detailsViewController: DetailsViewController = DetailsViewController(nibName: nil, bundle: nil)
        guard let community = communityMessagingData.community else {
            return
        }
        let rallyObject: Rally = Rally(members: [])
        rallyObject.associatedCommunity = community
        //rallyObject.userID = User.userID
        detailsViewController.rallyObject = rallyObject
        //communityMessagingData.parentVC.
        returningFromDetailsVC = true
        navigationController?.pushViewController(detailsViewController, animated: false)
        //communityMessagingData.parentVC.navigationController?.isNavigationBarHidden = false
        //centerNavigationControllerCP.pushViewController(detailsViewController, animated: true)
        //self.navigationController!
        // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
        view.endEditing(true)
    }
    
    /*
    func defineSlidableHeaderSectionShadow(_ slidableHeaderSection: UIView) {
        print("func defineSlidableHeaderSectionShadow is running...")
        slidableHeaderSection.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        slidableHeaderSection.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        slidableHeaderSection.layer.shadowOpacity = 1.0
        slidableHeaderSection.layer.shadowRadius = 1.5
    }
    */
    
    // scrollView methods
    func defineScrollView() {
        print("func defineScrollView is running...")
        let slidableHeaderSectionHeight = ScreenDimension.height*9.16/100
        let scrollViewHeight: CGFloat = view.bounds.height-navigationController!.toolbar.frame.height-slidableHeaderSectionHeight // need to reduce height by slidableHeaderSection height, not sure what the exact value will be yet
        print("print scrollview height: \(scrollViewHeight)")
        scrollView.frame = CGRect(x: 0, y: slidableHeaderSectionHeight, width: ScreenDimension.width, height: scrollViewHeight)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.delegate = self
        view.addSubview(scrollView)
        let scrollViewContentHeight = calculateScrollViewContentHeight()
        //if scrollViewContentHeight < scrollViewHeight {
            //scrollView.contentSize.height = scrollViewHeight
        //}
        //else {
          scrollView.contentSize.height = scrollViewContentHeight
        //}
        print("print scrollview content height: \(scrollView.contentSize.height)")
        if scrollView.contentSize.height > scrollView.frame.height {
            let contentYOffset = scrollView.contentSize.height-scrollView.frame.height
            scrollView.contentOffset = CGPoint(x: 0.0, y: contentYOffset)
        }
        
        // add tap gesture recognizer
        let scrollViewTap = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.handleScrollViewTap(_:)))
        scrollView.addGestureRecognizer(scrollViewTap)
    }
    
    func handleScrollViewTap(_ recognizer: UITapGestureRecognizer) {
        print("func handleScrollViewTap is running...")
         // hide keyboard if active
        if keyboardIsActive { // if true, keyboard is active, dismiss keyboard
            view.endEditing(true)
        }
        if let secondaryContainerVC = communityMessagingData.parentVC.secondaryContainerViewController as? ContainerViewController {
            switch secondaryContainerVC.currentState {
            case .bothExpanded:
                secondaryContainerVC.animateLeftPanel(shouldExpand: false)
            default:
                break
            }
        }
    }
    
    func calculateScrollViewContentHeight()-> CGFloat {
        print("func calculateScrollViewContentHeight is running...")
        //let font = UIFont(name: "Helvetica", size: 17)
        var textPlusMessageBlurbPaddingHeight: CGFloat = 0
        var totalSpaceBetweenMessageBlurbs: CGFloat = ScreenDimension.height*0.8/100 // starts at non-zero value to account for top-most padding in scrollView
        if let thread = communityMessagingData.activeThread {
            for messageBlurb in thread.messageBlurbs {
                textPlusMessageBlurbPaddingHeight += messageBlurb.view.frame.height
                totalSpaceBetweenMessageBlurbs += ScreenDimension.height*0.8/100
            }
        }
        let scrollViewContentHeight = textPlusMessageBlurbPaddingHeight + totalSpaceBetweenMessageBlurbs // 0.65/100 offsets textBlurbMessage extra padding (2.15) and intended padding (1.5)
        return scrollViewContentHeight
    }
    // scrollView methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("func scrollViewDidScroll is running...")
        /*
         let offsetY = scrollView.contentOffset.y
         let contentHeight = scrollView.contentSize.height
         if offsetY > contentHeight - scrollView.frame.size.height {
         println("If clause using totalNumberOfAddressBookContacts is running...")
         numberOfContactsToLoad += 12
         self.view.reloadData() // now called reloadUI() (same method)???
         for index in selectedTableDataIndexPaths {
         //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
         self.collectionView.selectItemAtIndexPath(index as? NSIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
         }
         }
         */
    }
    
    // Text box methods
    func defineTextBoxAndSendButtonSuperview() {
        print("func defineTextBoxAndSendButtonSuperview is running...")
        textBoxAndSendButtonSuperview = UIView()
        textBoxAndSendButtonSuperview.tag = 343
        let textBoxAndSendButtonSuperviewYOffset = ScreenDimension.heightMinusStatusAndNavigationBar-navigationController!.toolbar.frame.height
        textBoxAndSendButtonSuperview.frame = CGRect(x: 0,y: textBoxAndSendButtonSuperviewYOffset,width: ScreenDimension.width,height: navigationController!.toolbar.frame.height)
        textBoxAndSendButtonSuperview.backgroundColor = MYColor.rallyBlue
        view.addSubview(textBoxAndSendButtonSuperview)
        view.bringSubview(toFront: textBoxAndSendButtonSuperview)
        textBoxAndSendButtonSuperview.addSubview(textBox)
        textBoxAndSendButtonSuperview.addSubview(sendButton)
    }
    
    func defineTextBox() {
        print("func defineTextBox is running...")
        textBox = messagingTextView() // custom textView
        // initialize public var messagingTextViewSize
        textBox.frame = CGRect(x: ScreenDimension.width*2.8/100, y: 0, width: ScreenDimension.width*78.75/100, height: self.navigationController!.toolbar.frame.height*67.5/100)
        textBox.messagingTextViewHeight = textBox.frame.height
        textBox.messagingTextViewSingleLineHeight = textBox.frame.height
        textBox.center.y = navigationController!.toolbar.frame.height/2
        textBox.layer.cornerRadius = 5
        textBox.font = UIFont.systemFont(ofSize: 17.75)
        textBox.textColor = MYColor.lightBlack
        textBox.textAlignment = .left
        textBox.isScrollEnabled = false
        let textBoxTextContainerHeight = calculateInitialTextBoxTextContainerHeight(textBox.font!, width: textBox.frame.width)
        textBox.messagingTextViewTextContainerSingleLineHeight = textBoxTextContainerHeight
        _ = textBox.caretRect(for: textBox.beginningOfDocument)
        let textBoxYOffset = (textBox.bounds.size.height-textBoxTextContainerHeight)/2
        textBox.textContainerInset.top = textBoxYOffset
        textBox.textContainer.lineFragmentPadding = ScreenDimension.width*0.015
        textBox.delegate = self
        /*
         var fontSize: CGFloat = 20
         while textBox.sizeThatFits(CGSizeMake(textBox.frame.width, CGFloat(FLT_MAX))).height >= textBox.frame.height {
         fontSize -= 1.0
         textBox.font = UIFont.systemFontOfSize(fontSize)
         }
         */
    }
    
    func calculateInitialTextBoxTextContainerHeight(_ font: UIFont, width: CGFloat)-> CGFloat {
        print("func calculateInitialTextBoxTextContainerHeight is running...")
        let sampleText = "HelgjyFo."
        let attributedText = NSMutableAttributedString(string: sampleText)
        attributedText.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, attributedText.length))
        let boundingSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingRect = attributedText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return boundingRect.height
    }
    
    func defineSendButton() {
        print("func defineSendButton is running...")
        sendButton = UIButton(type: .system)
        sendButton.tag = 309
        sendButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 17.25)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            sendButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16)
        default:
            print("Do nothing...")
        }
        sendButton.setTitle("Send", for: UIControlState())
        sendButton.sizeToFit()
        sendButton.frame.origin.x = ScreenDimension.width*85/100
        sendButton.center.y = navigationController!.toolbar.frame.height/2
        sendButton.isUserInteractionEnabled = false
        sendButton.setTitleColor(MYColor.inactiveGrey, for: UIControlState())
        sendButton.addTarget(self, action: #selector(CommunityMessagingViewController.onSendButtonTouch(_:)), for: .touchUpInside)
    }
    
    func onSendButtonTouch(_ sender: UIButton!) {
        print("func onSendButtonTouch is running...")
        // trimmed string is to avoid counting spaces before/after anything user types so user cannot send bodies filled with spaces: "         "
        let trimmedString = textBox.text.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        if /*textBox.text.characters.count*/ trimmedString.characters.count > 0 {
            let commThreadMessage = CommThreadMessage()
            guard let thread = communityMessagingData.activeThread else { return }
            commThreadMessage.commThreadID = Int(thread.threadID)!
            commThreadMessage.commThreadMessageTypeID = 1
            commThreadMessage.messageByteString = textBox.text!
            commThreadMessage.createdByUserID = MYUser.ID
            if let firstName = MYUser.firstName {
                commThreadMessage.firstName = firstName
            }
            if let lastName = MYUser.lastName {
                commThreadMessage.lastName = lastName
            }
            
            let font = UIFont(name: "Helvetica", size: 17)
            // generate new message blurb and add to scrollView
            
            var messageBlurb = self.generateMessageBlurb(commThreadMessage, font: font!)
            // increase scrollView contentsize height to accomodate new message blurb plus inter-message blurb spacing
            // shift scrollView contentOffset up by height of new message blurb plus inter-message blurb spacing
            
            // remove new messageBlurb image to denote message delivery pending status
            messageBlurb.profilePictureView.image = nil
            
            // adjust new messageBlurb y-origin
            let newMessageAndPaddingHeight = ScreenDimension.height*0.8/100+messageBlurb.view.frame.height
            self.scrollView.contentSize.height += newMessageAndPaddingHeight
            messageBlurb.view.frame.origin.y = self.scrollView.contentSize.height-newMessageAndPaddingHeight+ScreenDimension.height*2.15/200
            //messageBlurb.profilePictureView.frame.origin.y += newMessageAndPaddingHeight
            if self.scrollView.contentSize.height > self.scrollView.frame.height {
                self.scrollView.contentOffset.y += newMessageAndPaddingHeight
            }
            
            // reset textBoxandSendButtonSuperview
            textBox.text = ""
            let currentTextInputBarandSendButtonSuperviewHeight = textBoxAndSendButtonSuperview.frame.height
            textBoxAndSendButtonSuperview.frame.origin.y += currentTextInputBarandSendButtonSuperviewHeight-self.navigationController!.toolbar.frame.height
            textBoxAndSendButtonSuperview.frame.size.height = self.navigationController!.toolbar.frame.height
            // re-center sendButton
            sendButton.center.y = textBoxAndSendButtonSuperview.frame.height/2
            textBox.frame.size.height = self.navigationController!.toolbar.frame.height*67.5/100
            textBox.contentSize.height = textBox.frame.height
            textBox.messagingTextViewHeight = textBox.frame.height
            
            // send message to WCS
            let intent = WebServiceCallIntent.postCommThreadMessage(commThreadMessage: commThreadMessage, messageBlurb: messageBlurb)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    if let returnIntent = returnParameters {
                        if case var WebServiceCallReturnParameters.postCommThreadMessage(messageBlurb) = returnIntent {
                            // remove text in textbox
                            DispatchQueue.main.async(execute: { () -> Void in
                                messageBlurb.profilePictureView.image = messageBlurb.messageData.member.getProfileImage()
                            })
                        }
                    }
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    func defineSendButtonInteractionState(_ interactionStateValue: Bool) {
        print("func defineSendButtonInteractionState is running...")
        if interactionStateValue == false { // not sure if this will be updated or not yet, if not change the condition to something else
            sendButton.isUserInteractionEnabled = false
            sendButton.setTitleColor(MYColor.inactiveGrey, for: UIControlState())
        }
        else {
            sendButton.isUserInteractionEnabled = true
            sendButton.setTitleColor(UIColor.white, for: UIControlState())
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        print("textView text equals:\(text)")
        if text != "" {
            defineSendButtonInteractionState(true) // set to true
        }
        let combinedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        // Create attributed version of the text
        let attributedTextIncludingNewText = NSMutableAttributedString(string: combinedText)
        attributedTextIncludingNewText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedTextIncludingNewText.length))
        // may be error if font parameter is nil above
        // Get the padding of the text container
        let padding = textView.textContainer.lineFragmentPadding
        // Create a bounding rect size by subtracting the padding
        // from both sides and allowing for unlimited length
        let boundingSize = CGSize(width: textView.frame.size.width - padding * 2, height: CGFloat.greatestFiniteMagnitude)
        // Get the bounding rect of the attributed text in the
        // given frame
        let boundingRectIncludingNewText = attributedTextIncludingNewText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        // Compare the boundingRect plus the top and bottom padding
        // to the text view height; if the new bounding height would be
        // less than or equal to the text view height, append the text
        // get boundingRect for current text
        let attributedText = NSMutableAttributedString(string: textView.text)
        attributedText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedText.length))
        let boundingRect = attributedText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let numberOfLines = round(boundingRect.height/textView.font!.lineHeight)
        let numberOfLinesUponChangingText = round(boundingRectIncludingNewText.height/textView.font!.lineHeight)
        //if boundingRectIncludingNewText.size.height > textView.frame.size.height { // if true, this means a new line is needed
        if numberOfLines < numberOfLinesUponChangingText { // if true, add new line by updating textView and toolbar size
            //let newMessagingTextViewHeight = boundingRectIncludingNewText.height
            handleMessagingTextViewSizeChange(1)
        }
        else if numberOfLines > numberOfLinesUponChangingText { // if true, remove line by updating textView and toolbar size
            //let newMessagingTextViewHeight = boundingRectIncludingNewText.height
            handleMessagingTextViewSizeChange(-1)
        }
        if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            print("textView:shouldChangeTextInRange text is empty, it's a backspace")
            if combinedText.characters.count == 0 {
                defineSendButtonInteractionState(false) // set to false
            }
            return true
        }
        if combinedText.characters.count < 300 { // 300 is the maximum number of characters per message
            return true
        }
        else { // don't go any further, user already reached max character limit
            print("textView:shouldChangeTextInRange:false reached character limit")
            return false
        }
    }
    
    func handleMessagingTextViewSizeChange(_ numberOfNewLines: CGFloat!) {
        print("func handleMessagingTextViewSizeChange is running...")
        // update textView size
        textBox.frame.size.height += numberOfNewLines*textBox.messagingTextViewTextContainerSingleLineHeight
        let textContainerCorrectionHeight = (textBox.frame.height-textBox.textContainer.size.height)/2
        textBox.textContainer.size.height += 4
        textBox.contentSize.height = textBox.frame.height
        // update toolbar size
        let textBoxAndSendButtonSuperview = view.viewWithTag(343)!
        textBoxAndSendButtonSuperview.frame.size.height += numberOfNewLines*textBox.messagingTextViewTextContainerSingleLineHeight
        textBoxAndSendButtonSuperview.frame.origin.y += -numberOfNewLines*textBox.messagingTextViewTextContainerSingleLineHeight
        let sendButton = textBoxAndSendButtonSuperview.viewWithTag(309) as! UIButton
        sendButton.frame.origin.y += numberOfNewLines*textBox.messagingTextViewTextContainerSingleLineHeight
        // update textView size public variable messagingTextViewSize.messagingTextViewHeight
        textBox.messagingTextViewHeight = textBox.frame.height // I think I use this to calculate cursor/carret height in custom textview method, may not need
    }
    // Text box methods
    
    func defineMessageBlurbs() { // remove any message blurbs already in scrollview and recdefine message blurbs using current data
        print("func defineMessageBlurbs is running...")
        for subview in scrollView.subviews {
            subview.removeFromSuperview()
        }
        // Below doesn't work in architecture where this function is only called when community messaging is expanded bc new active thread is assigned so previous active thread message blurbs aren't being referenced here
        /*if let activeThread = communityMessagingData.activeThread {
        for messageBlurb in communityMessagingData.messageBlurbs {
            messageBlurb.view.removeFromSuperview()
        }
        communityMessagingData.messageBlurbs = []
        }*/
        let font = UIFont(name: "Helvetica", size: 17)
        guard let thread = communityMessagingData.activeThread else { return }
        // reset messageBlurbs
        thread.messageBlurbs = []
        for message in thread.messages {
            let _ = generateMessageBlurb(message, font: font!)
        }
        // define scrollview content height
        let scrollViewContentHeight = calculateScrollViewContentHeight()
        scrollView.contentSize.height = scrollViewContentHeight
        var precedingMessageBlurbYOrigin = scrollViewContentHeight+ScreenDimension.height*2.15/200 // initial value, 2.15/200 center messages for equal padding
        // define y-origins for message blurbs
        for messageBlurb in thread.messageBlurbs.reversed() {
            messageBlurb.view.frame.origin.y = precedingMessageBlurbYOrigin-ScreenDimension.height*0.8/100-messageBlurb.view.frame.height
            precedingMessageBlurbYOrigin = messageBlurb.view.frame.origin.y
        }
        print("number of message blurbs in thread:\(thread.messageBlurbs.count)")
    }
    
    func generateMessageBlurb(_ commThreadMessage: CommThreadMessage, font: UIFont/*, topMessageBlurbYOffset: CGFloat*/)->MYMessageBlurb {
        print("func generateMessageBlurb is running...")
        // initialize messageBlurb
        var messageBlurb = MYMessageBlurb(commThreadMessage, font: font)//, precedingMessageBlurbYOffset: topMessageBlurbYOffset, superviewContentHeight: scrollView.contentSize.height)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.displayLargeProfilePic(_:)))
        messageBlurb.profilePictureView.addGestureRecognizer(tap)

        scrollView.addSubview(messageBlurb.view)
        communityMessagingData.activeThread!.messageBlurbs.append(messageBlurb)
        //communityMessagingData.messageBlurbs.append(messageBlurb)
        return messageBlurb
    }
    // message blurb methods
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard(_ gestureRecognizer: UIPanGestureRecognizer) { // gesture only exists from time that textViewDidBeginEditing and keyboard is dismissed
        print("func dismissKeyboard is running...")
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //http://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
        view.endEditing(true) // looks for text field and IF it finds one, forces it to resign as first responder
        self.scrollView.removeGestureRecognizer(gestureRecognizer)
    }
    
    // textView
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("func textViewDidBeginEditing is running...")
        // allow user to dismiss keyboard by swiping screen, presumably to see out-of-view messages
        /*
        let swipe = UIPanGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.dismissKeyboard(_:)))
        scrollView.addGestureRecognizer(swipe)
        */
    }
    
    func displayLargeProfilePic(_ gestureRecognizer: UITapGestureRecognizer) {
        
        print("func displayLargeProfilePic is running...")
        // get respective profile view reference
        let thumbnailProfileView = gestureRecognizer.view! as! PersonImageView
        // get member associated with the profile view
        let member = thumbnailProfileView.person!
        
        // generate UIWindow to overlay with subviews (dulling and large profile picture view) with a touch to dismiss gesture
        let dullingView = DullingViewClass.defineDullingView(0.47)
        let tap = UITapGestureRecognizer(target: self, action: #selector(CommunityMessagingViewController.dismissLargeProfileImageWindow(_:)))
        dullingView.addGestureRecognizer(tap)
        if let defaultWindow = appDelegateReference.window {
            defaultWindow.makeKeyAndVisible()
        }
        largeProfileImageWindow.makeKeyAndVisible()
        largeProfileImageWindow.rootViewController!.view.addSubview(dullingView)
        let largeProfileImageView = UIImageView()
        largeProfileImageView.frame.size = CGSize(width: ScreenDimension.width*100/100, height: ScreenDimension.height*80/100)
        largeProfileImageView.center = CGPoint(x: ScreenDimension.width*50/100, y: ScreenDimension.height*50/100)
        largeProfileImageView.contentMode = .scaleAspectFit
        largeProfileImageWindow.rootViewController!.view.addSubview(largeProfileImageView)
        if let image = communityMemberLargeProfileImageGlobalDictionary[member.ID!] { // check for member large profile picture in global dictionary. if not there, fetch from WCF
            largeProfileImageView.image = image
        }
        else { // make call to WCF to fetch large profile picture
            //StartupRunTimer.start = Date()
            let intent = WebServiceCallIntent.getLargeProfileImage(memberID: member.ID!, imageView: largeProfileImageView)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        // load any messages
                        if let returnIntent = returnParameters {
                            if case let WebServiceCallReturnParameters.getLargeProfileImage(imageFetched, memberID, imageView) = returnIntent {
                                // update view with new message
                                if imageFetched {
                                    // if successful, display picture
                                    //memberID
                                    if let image = communityMemberLargeProfileImageGlobalDictionary[memberID] { // check for member large profile picture in global dictionary. if not there, fetch from WCF
                                        imageView.image = image
                                    }
                                }
                            }
                        }
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }

    func dismissLargeProfileImageWindow(_ gestureRecognizer: UITapGestureRecognizer) {
        print("dismissLargeProfileImageWindow is running...")
        // remove subviews and dismiss UIWindow
        for subview in gestureRecognizer.view!.superview!.subviews {
            subview.removeFromSuperview()
        }
        if let defaultWindow = appDelegateReference.window {
            defaultWindow.makeKeyAndVisible()
        }
    }
    //StartupRunTimer.end = Date()
    //let totalTimeElapsed = StartupRunTimer.end.timeIntervalSince(StartupRunTimer.start)
}

/*
 class CommunityMessagingViewController: UIViewController, CenterViewController {
 
 @IBOutlet weak private var imageView: UIImageView!
 @IBOutlet weak private var titleLabel: UILabel!
 @IBOutlet weak private var creatorLabel: UILabel!
 
 var delegate: CenterViewControllerDelegate?
 
 // MARK: Button actions
 
 @IBAction func kittiesTapped(sender: AnyObject) {
 delegate?.toggleLeftPanel?()
 }
 
 @IBAction func puppiesTapped(sender: AnyObject) {
 delegate?.toggleBottomPanel?()
 }
 
 func viewDidLoad() {
 
 }
 }
 */


/*
 class CenterViewController: UIViewController {
 
 @IBOutlet weak private var imageView: UIImageView!
 @IBOutlet weak private var titleLabel: UILabel!
 @IBOutlet weak private var creatorLabel: UILabel!
 
 var delegate: CenterViewControllerDelegate?
 
 // MARK: Button actions
 
 @IBAction func kittiesTapped(sender: AnyObject) {
 delegate?.toggleLeftPanel?()
 }
 
 @IBAction func puppiesTapped(sender: AnyObject) {
 delegate?.toggleBottomPanel?()
 }
 
 }
 */


