//
//  CommunityOptionCustomView.swift
//  Rally
//
//  Created by Ian Moses on 10/11/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

class CommunityOptionCustomView: UIView { // used for creating community options (private/public etc) for user to choose
    var optionTitle: InsetLabel = {
        let optionTitle = InsetLabel()
        optionTitle.textColor = MYColor.lightBlack
        optionTitle.font = UIFont.boldSystemFont(ofSize: 17.15)
        optionTitle.numberOfLines = 1
        return optionTitle
    }()
    
    var optionValue: InsetLabel = {
        let optionValue = InsetLabel()
        optionValue.textColor = MYColor.darkGrey
        optionValue.font = UIFont.systemFont(ofSize: 16)
        optionValue.numberOfLines = 1
        return optionValue
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame) // calls designated initializer
        self.backgroundColor = .white
        self.frame.size = CGSize(width: ScreenDimension.width,height: ScreenDimension.height*7/100)
        print("optionView:subview left inset defined next line")
        // set left inset for option title
        let optionTitleLeftInset = ScreenDimension.width*3/100
        optionTitle.contentInset.left = optionTitleLeftInset
        // set left inset for option value
        let optionValueLeftInset = ScreenDimension.width*1.25/100
        optionValue.contentInset.left = optionValueLeftInset
        // add labels as subviews
        self.addSubview(optionTitle)
        self.addSubview(optionValue)
    }
    
    convenience init(yOrigin: CGFloat, optionTitle: String) {
        //self.subViewColor = subViewColor
        //self.subViewMessage = subViewMessage
        let frame: CGRect = CGRect(x:0,y:yOrigin,width: ScreenDimension.width,height: ScreenDimension.height*7/100)
        self.init(frame: frame) // calls the UIView designated initializer above
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class InsetLabel: UILabel {
    var contentInset: UIEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set(newFrame) {
            var insetNewFrame = newFrame
            insetNewFrame.size.width = newFrame.width+contentInset.left
            super.frame = insetNewFrame
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init(insets:UIEdgeInsets) {
        self.init(frame: CGRect.zero)
        //self.setProperties()
        self.contentInset = insets
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setProperties() {
    }
    
    override func drawText(in rect: CGRect) {
        print("CustomLabel:override func drawTextInRect(rect: CGRect)")
        super.drawText(in: UIEdgeInsetsInsetRect(rect, contentInset))
    }
}
