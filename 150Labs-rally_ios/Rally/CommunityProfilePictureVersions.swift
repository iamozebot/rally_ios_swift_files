//
//  CommunityProfilePictureVersions.swift
//  Rally
//
//  Created by Ian Moses on 11/14/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class CommunityProfilePictureVersions {
    var communityMessaging: passThroughView?
    var createCommunity: passThroughView?
    var communityCard: passThroughView?
    var communityMenu: passThroughView?
}
