//
//  ContainerCenterPanelExtensions.swift
//  Rally
//
//  Created by Ian Moses on 8/22/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

extension ContainerViewController: CenterViewControllerDelegate {
    
    func addChildSidePanelControllerHandler(_ sidePanelController: SidePanelViewController) {
        print("func containerViewController:addChildSidePanelControllerHandler is running...")
    //func addChildSidePanelController<T: UIViewController where T: SidePanelViewController>(sidePanelController: T) {
        // generic parameter allows any view controller conforming to SidePanelViewController protocol be passed into method and access parent class (UIViewController) methods/properties
        // sidePanelController delegate is type (protocol) SidePanelViewControllerDelegate. centerViewControllerCP conforms to CenterViewController which inherits SidePanelViewControllerDelegate allowing conformance.
        //sidePanelController.delegate = centerViewControllerCP
        sidePanelController.addChildSidePanelController(self)
    }
    
    func animateLeftPanel(shouldExpand: Bool) {
        print("func animateLeftPanel is running...")
        if (shouldExpand) {
            currentState = .bothExpanded
            animateCenterPanelXPosition(targetPosition: centerPanelExpandedOffset)
        }
        else {
            let primaryContainerVC = self.parent as! ContainerViewController
            primaryContainerVC.displayStatusBar()
            animateCenterPanelXPosition(targetPosition: 0) { finished in
                self.currentState = .bottomPanelExpanded
            }
        }
        // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
        // containerVC is parentView of leftPanelVC so calling endEditing on containerVC view works to dismiss keyboard if active
        view.endEditing(true)
    }
    
    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        print("func animateCenterPanelXPosition is running...")
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
            self.centerNavigationControllerCP.view.frame.origin.x = targetPosition
            }, completion: completion)
    }
    
    func animateCenterPanelYPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        print("func animateCenterPanelYPosition is running...")
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
            self.view.frame.origin.y = targetPosition
            }, completion: completion)
    }
    
    func animateBottomPanel(shouldExpand: Bool) {
        print("func animateBottomPanel is running...")
        if (shouldExpand) {
            animateCenterPanelYPosition(targetPosition: 80) { _ in
                self.currentState = .bottomPanelExpanded
                self.view.frame.origin.y = 0.0
                let primaryContainerVC = self.parent as! ContainerViewController
                primaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = true
                self.centerNavigationControllerCP.isNavigationBarHidden = false
            }
        }
        else {
            animateCenterPanelYPosition(targetPosition: ScreenDimension.height-ScreenDimension.threadBarHeight) { _ in
                self.currentState = .bothCollapsed
            }
        }
    }
}

extension HomeViewController: SidePanelViewControllerDelegate {
}

extension CommunityMessagingViewController: SidePanelViewControllerDelegate {
}

extension ContainerViewController: UIGestureRecognizerDelegate {
    
    func handleThreadBarPanGesture(_ recognizer: UIPanGestureRecognizer) {
        print("func handleThreadBarPanGesture is running...")
            switch recognizer.state {
            case .began:
                let horizontalVelocityMagnitude = abs(recognizer.velocity(in: view).x)
                let verticalVelocityMagnitude = abs(recognizer.velocity(in: view).y)
                panAxis = horizontalVelocityMagnitude > verticalVelocityMagnitude ? .horizontal : .vertical
                if panAxis == .vertical {
                    if centerNavigationControllerCP.view!.frame.origin.x == 0 { // if not at 0, left panel is showing
                        if currentState == .bottomPanelExpanded {
                            if view!.frame.origin.y == 0 {
                                self.view.frame.origin.y = 80
                                let primaryContainerVC = self.parent as! ContainerViewController
                                primaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = false
                                self.centerNavigationControllerCP.isNavigationBarHidden = true
                            }
                        }
                        else if currentState == .bothCollapsed {
                            let communityMessagingVC = centerViewControllerCP as? CommunityMessagingViewController
                            communityMessagingVC?.reloadUI()
                        }
                    }
                }
                else {
                    let gestureHorizontalVelocity: CGFloat = recognizer.velocity(in: view).x
                    let direction: PanDirection = gestureHorizontalVelocity > 0.0 ? .right : .left
                    horizontalPanBegan(panDirection: direction)
                }
                // dismiss keyboard if displayed (case is: leftPanelVC search controller active)
                view.endEditing(true)
            case .changed:
                print(".changed is running...")
                if panAxis == .vertical {
                    if centerNavigationControllerCP.view!.frame.origin.x == 0 { // if not at 0, left panel is showing and
                        let translation = recognizer.translation(in: view).y
                        let proposedPosition: CGFloat = view!.frame.origin.y+translation
                        var finalPosition: CGFloat!
                        if proposedPosition <= ScreenDimension.height-ScreenDimension.threadBarHeight && proposedPosition >= 80 {
                            finalPosition = view!.center.y+translation
                        }
                        else {
                            let gestureIsDraggingFromBottomToTop = (recognizer.velocity(in: view).y < 0)
                            let gestureIsDraggingFromTopToBottom = (recognizer.velocity(in: view).y > 0)
                            if gestureIsDraggingFromTopToBottom {
                                finalPosition = ScreenDimension.height-ScreenDimension.threadBarHeight+view!.frame.height/2
                            }
                            else if gestureIsDraggingFromBottomToTop {
                                finalPosition = 80+view!.frame.height/2
                                
                            }
                            
                        }
                        if finalPosition != nil {
                            view!.center.y = finalPosition
                        }
                        // re-zeroes translation
                        recognizer.setTranslation(CGPoint.zero, in: view)
                    }
                }
                else {
                    let gestureHorizontalVelocity: CGFloat = recognizer.velocity(in: view).x
                    let direction: PanDirection = gestureHorizontalVelocity > 0.0 ? .right : .left
                    horizontalPanChanged(recognizer, panDirection: direction)
                }
            case .ended:
                print(".ended is running")
                if panAxis == .vertical {
                    if centerNavigationControllerCP.view!.frame.origin.x == 0 { // if not at 0, left panel is showing and
                        //secondaryContainerViewController!.view!.center.y
                        let hasMovedGreaterThanHalfway = view!.center.y-40 < ScreenDimension.height
                        animateBottomPanel(shouldExpand: hasMovedGreaterThanHalfway)
                    }
                }
                else {
                    horizontalPanEnded()
                }
            default:
                break
            }
    }
    
    func handleHorizontalPanGesture(_ recognizer: UIPanGestureRecognizer) {
        print("func handleHorizontalPanGesture is running...")
        switch recognizer.state {
        case .began:
            print(".began is running...")
            let horizontalVelocityMagnitude = abs(recognizer.velocity(in: view).x)
            let verticalVelocityMagnitude = abs(recognizer.velocity(in: view).y)
            panAxis = horizontalVelocityMagnitude > verticalVelocityMagnitude ? .horizontal : .vertical
            if panAxis == .horizontal {
                let gestureHorizontalVelocity: CGFloat = recognizer.velocity(in: view).x
                let direction: PanDirection = gestureHorizontalVelocity > 0.0 ? .right : .left
                horizontalPanBegan(panDirection: direction)
            }
        case .changed:
            print(".changed is running...")
            if panAxis == .horizontal {
                let gestureHorizontalVelocity: CGFloat = recognizer.velocity(in: view).x
                let direction: PanDirection = gestureHorizontalVelocity > 0.0 ? .right : .left
                horizontalPanChanged(recognizer, panDirection: direction)
            }
        case .ended:
            print(".ended is running")
            if panAxis == .horizontal {
                horizontalPanEnded()
            }
        default:
            break
        }
    }
    
    func horizontalPanBegan(panDirection: PanDirection) {
        print("func horizontalPanBegan is running...")
        if self.currentState != .bothCollapsed {
            if self.drawerViewController == nil {
                let leftPanel = LeftPanelViewController()
                let communityMessagingVC = centerViewControllerCP as? CommunityMessagingViewController
                if let community = communityMessagingVC?.communityMessagingData.community {
                    leftPanel.leftPanelData.community = community
                }
                drawerViewController = leftPanel
                addChildSidePanelControllerHandler(leftPanel)
            }
            if panDirection == .right {
                let primaryContainerVC = self.parent as! ContainerViewController
                primaryContainerVC.hideStatusBar()
            }
        }
    }
    
    func horizontalPanChanged(_ recognizer: UIPanGestureRecognizer, panDirection: PanDirection) {
        print("func horizontalPanChanged is running...")
        if self.currentState != .bothCollapsed {
            let translation = recognizer.translation(in: view).x
            let proposedPosition: CGFloat = self.centerNavigationControllerCP.view!.frame.origin.x+translation
            var finalPosition: CGFloat!
            if proposedPosition >= 0.0 && proposedPosition <= centerPanelExpandedOffset {
                finalPosition = self.centerNavigationControllerCP.view!.center.x+translation
            }
            else {
                if panDirection == PanDirection.right {
                    finalPosition = centerPanelExpandedOffset+self.centerNavigationControllerCP.view!.frame.width/2//449
                }
                else if panDirection == PanDirection.left {
                    finalPosition = self.centerNavigationControllerCP.view!.frame.width/2//449
                }
                
            }
            if finalPosition != nil { // in some (not well understood) cases, finalPosition was nil
                self.centerNavigationControllerCP.view!.center.x = finalPosition
            }
            // re-zeroes translation
            recognizer.setTranslation(CGPoint.zero, in: view)
        }
    }
    
    func horizontalPanEnded() {
        print("func horizontalPanEnded is running...")
        // animate the side panel open or closed based on whether the (messagingVC) view has moved more or less than halfway. halfway is defined as right screen edge
        if self.currentState != .bothCollapsed {
            let hasMovedGreaterThanHalfway = self.centerNavigationControllerCP.view!.center.x > view.bounds.size.width
            animateLeftPanel(shouldExpand: hasMovedGreaterThanHalfway)
        }
    }
    
    func hideStatusBar() {
        print("func hideStatusBar is running...")
        // hide status bar (while left panel is displayed)
        statusBarDisplayState = true
        statusBarAnimationState = .slide
        UIView.animate(withDuration: 0.35) { () -> Void in
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func displayStatusBar() {
        print("func displayStatusBar is running...")
        statusBarDisplayState = false
        statusBarAnimationState = .slide
        UIView.animate(withDuration: 0.35) { () -> Void in
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
}
