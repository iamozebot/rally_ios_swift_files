//
//  ContainerProtocols.swift
//  Rally
//
//  Created by Ian Moses on 8/22/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation


// NOTE: inherits SidePanelViewControllerDelegate bc the delegate extension contains default function view controllers that conform to CetnerViewController protocol will call (and don't want to write function in each view controller manually as the view controllers that act as CenterViewControllers changes)
// NOTE: initially had it inhereting CenterViewControllerDelegate, couldn't figure out why so deleted
protocol CenterViewController: SidePanelViewControllerDelegate {
    var delegate: CenterViewControllerDelegate? { get set }
}

protocol SidePanelViewController: class { // marking as class enables compiler to know instances conforming to protocol are class types. this way, compiler knows SidePanelViewController as a method parameter is mutable
    var delegate: SidePanelViewControllerDelegate? { get set }
    /*func typedInstance()-> Self*/
    func addChildSidePanelController(_ containerVC: ContainerViewController)
    func removeViewFromSuperView(_ containerVC: ContainerViewController)
}

extension SidePanelViewController {
    /*func typedInstance()->Self {
        return self
    }*/
}

extension ContainerViewController: SidePanelViewController {
    
    /*func typedInstance()->Self {
     return self
     }*/
    
    func addChildSidePanelController(_ containerVC: ContainerViewController) {
        print("func ContainerViewController:addChildSidePanelController is running...")
        //containerVC.view.insertSubview(self.view, aboveSubview: )
        containerVC.view.insertSubview(self.view, at: 0)
        containerVC.view.bringSubview(toFront: self.view)
        print("self.view.frame.origin.y:\(self.view.frame.origin.y)")
        print(" secondaryContainerViewController.view.frame.origin.y:\(String(describing: secondaryContainerViewController?.view.frame.origin.y))")
        //self.centerNavigationControllerCP.view.frame.origin.y = screenDimension.screenHeight-80
        print("self.view.frame.origin.y:\(self.view.frame.origin.y)")
        print(" secondaryContainerViewController.view.frame.origin.y:\(String(describing: secondaryContainerViewController?.view.frame.origin.y))")
        //secondaryContainerViewController.view.frame.origin.y = screenDimension.screenHeight-100
        containerVC.addChildViewController(self)
        self.didMove(toParentViewController: containerVC)
    }
    
    func removeViewFromSuperView(_ containerVC: ContainerViewController) {
        print("func removeViewFromSuperView is running...")
        containerVC.self.view.removeFromSuperview()
    }
}



extension HomeViewController: CenterViewController {
}

extension CommunityMessagingViewController: CenterViewController {
}

extension LeftPanelViewController: SidePanelViewController {
    func addChildSidePanelController(_ containerVC: ContainerViewController) {
        print("func LeftPanelViewController:addChildSidePanelController is running...")
        containerVC.view.insertSubview(self.view, at: 0)
        containerVC.addChildViewController(self)
        self.didMove(toParentViewController: containerVC)
    }
    
    func removeViewFromSuperView(_ containerVC: ContainerViewController) {
        print("func removeViewFromSuperView is running...")
        containerVC.self.view.removeFromSuperview()
    }
}

/*extension UIViewController: CenterViewController { // all view controllers (or subclasses of view controller) that conform to CenterViewController protocol have a default value for delegate
    var delegate =
}*/

@objc
protocol CenterViewControllerDelegate {
    @objc optional func toggleLeftPanel()
    @objc optional func toggleBottomPanel()
    @objc optional func collapseSidePanels()
}

@objc
protocol SidePanelViewControllerDelegate {
    //func animalSelected()
}
