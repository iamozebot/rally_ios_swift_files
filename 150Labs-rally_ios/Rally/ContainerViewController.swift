//
//  ContainerViewController.swift
//  Rally
//
//  Created by Ian Moses on 8/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit
import QuartzCore
import SwiftyJSON

class ContainerViewController: UIViewController {
    
    override var prefersStatusBarHidden : Bool {
        get {
            return self.statusBarDisplayState
        }
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        get {
            return self.statusBarAnimationState
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    var howContainerRelates: Hierarchy = .parent
    var statusBarDisplayState: Bool = false
    var statusBarAnimationState: UIStatusBarAnimation = .none
    var centerNavigationControllerCP: UINavigationController! {
        didSet {
            print("didSet centerNavigationControllerCP is running...")
            centerViewControllerCP = centerNavigationControllerCP.viewControllers.first
        }
    }
    var secondaryContainerViewController: UIViewController?
    var centerViewControllerCP: UIViewController? //CenterViewController? // CenterViewController is a protocol
    var drawerViewController: UIViewController? // SidePanelViewController is a protocol
    var delegate: SidePanelViewControllerDelegate? // containerViewController is parentViewController for home screen and also side panel for itself (to house community messaging). When instance is used as side panel, requires delegate
    var type: containerType
    var currentState: SlideOutState = .bothCollapsed {// when VC loads, both slide-outs start collapsed
        didSet {
            // make sure both containerVCs are updated with correct currentState
            if self.howContainerRelates == .parent {
                if let secondaryContainerVC = secondaryContainerViewController as? ContainerViewController {
                    secondaryContainerVC.currentState = currentState
                }
            }
            else {
                if let containerVC = self.parent as? ContainerViewController {
                    containerVC.currentState = currentState
                }
            }
        }
    }
    // duplicate definition inside leftPanelVC:defineCenterContainer, having trouble calling self.parent so moving on for now
    let centerPanelExpandedOffset: CGFloat = ScreenDimension.width*71.8/100
    var panAxis: PanDirection = .vertical // default value is vertical, hope it helps avoiding any error where .began does not trigger on gesture initialization
    var tagNumber = 1
    var previousViewControllerIsTypeEditProfileViewController = false
    
    // https://thatthinginswift.com/dependency-injection-storyboards-swift/
    // https://theswiftdev.com/2015/08/05/swift-init-patterns/
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        print("override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {")
        //self.example = "Just an example"
        // not doing anything here, can probably delete (test when app is back up and running)
        self.type = .home // this is stupid, not required
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        //        fatalError("init(coder:) has not been implemented")
        //self.example = "Just a coder example"
        print("required init?(coder aDecoder: NSCoder) {")
        self.type = .home // this is stupid, not required
        super.init(coder: aDecoder)
    }
    
    init(type: containerType) {
        // set type
        print("init(type: containerType) {")
        self.type = type
        // initialize view controller
        print("self.type...")
        super.init(nibName: nil, bundle: nil)
        print("super.init...")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // wrap the centerViewController in a navigation controller, so we can push views to it
        // and display bar button items in the navigation bar
        print("class ContainerViewController, func viewDidLoad is running...")
        // initialize center view controller for type
        switch type {
        case .home:
            print(".home case is running...")
            centerNavigationControllerCP = UINavigationController(rootViewController: HomeViewController())
            centerNavigationControllerCP.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: 40)), forKeyPath: "navigationBar")
            // set pulloutVC state
            currentState = .bothCollapsed
        case .community:
            print(".community case is running...")
            howContainerRelates = .child
            let communityMessagingVCInstance = CommunityMessagingViewController()
            centerNavigationControllerCP = UINavigationController(rootViewController: communityMessagingVCInstance)
            centerNavigationControllerCP.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: 40)), forKeyPath: "navigationBar")
            let horizontalPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ContainerViewController.handleHorizontalPanGesture))
            self.centerNavigationControllerCP.view.addGestureRecognizer(horizontalPanGestureRecognizer)
            self.view.frame.origin.y = ScreenDimension.height-ScreenDimension.threadBarHeight
        }
        view.addSubview(centerNavigationControllerCP.view)
        addChildViewController(centerNavigationControllerCP)
        centerNavigationControllerCP.didMove(toParentViewController: self)
        // set center view controller delegate to ContainerViewController
        //centerViewControllerCP!.delegate = self
        // initialize center view controller for type
        switch type {
        case .home:
            print(".home case is running...")
            // initialize bottom view controller (only top bar is visible initially)
            // NOTE: BottomPanelViewController is ContainerViewController(.communityMessaging)
            if drawerViewController == nil {
                let secondaryContainerVC = ContainerViewController(type: .community)
                secondaryContainerViewController = secondaryContainerVC
                drawerViewController = secondaryContainerViewController
                addChildSidePanelControllerHandler(secondaryContainerViewController as! SidePanelViewController)
                let communityMessagingVC = secondaryContainerVC.centerNavigationControllerCP.viewControllers.first as? CommunityMessagingViewController
                communityMessagingVC?.communityMessagingData.parentVC = self
            }
        case .community:
            print(".community case is running...")
            // NOTE: Left side panel not loaded by default (BottomPanelViewController is unique in that it is partially visible within it's centerViewController so needs to be loaded when containerVC is initialized)
        }
        navigationController?.isNavigationBarHidden = true
        
        if !previousViewControllerIsTypeEditProfileViewController {
            // display splash image with duration to control display time and manage status bar appearing late
            StartupRunTimer.mid = Date()
            let splashScreenContinuation = UIImageView(image: UIImage(named:"launchImageContinuation"))
            splashScreenContinuation.sizeToFit()
            navigationController?.view.addSubview(splashScreenContinuation)
            StartupRunTimer.end = Date()
            let totalTimeElapsed = StartupRunTimer.end.timeIntervalSince(StartupRunTimer.start)
            var delayDuration: Double = 0.4
            if totalTimeElapsed < 1.5 {
                delayDuration = 1.5-totalTimeElapsed+0.4
            }
            delay(delayDuration) {
                splashScreenContinuation.removeFromSuperview()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "splashScreenDismissed"), object: nil)
            }
            // reset flag
            previousViewControllerIsTypeEditProfileViewController = false
        }
    }
}
