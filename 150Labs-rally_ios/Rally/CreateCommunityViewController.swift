//
//  CreateCommunityViewController.swift
//  Rally
//
//  Created by Ian Moses on 10/11/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

class CreateCommunityViewControllerDataSource {
    let communityNamePlaceholderText = "Community name"
    let maxMembersTitle = "Max members -"
    let privacyTitle = "Privacy -"
    let locationTitle = "Location -"
    
    let locationDefaultDescription = ""
    var community = Community()
    
    init() {
        if let userAsFriend = MYUser.friendType() {
            community.members.append(userAsFriend)
        }
    }
    
    var maxMembersOptionValues: [Int] = [999, 10, 25, 50, 100, 250]
}

class CreateCommunityViewController: UIViewController, UITextViewDelegate {
    var createCommunityData = CreateCommunityViewControllerDataSource()
    // feature not included in v1.0, use in v1.1
    // var editButton = UIButton() // subview of inviteView
    var profilePictureContainer = passThroughView() // subview of inviteView
    var CameraPhotoLibraryHelper: CameraPhotoLibrary?
    let communityNameTextView = EnterNameTextViewSubclass()
    let communityNamePictureSuperview = UIView()
    let maxMembersOptionView = CommunityOptionCustomView()
    let locationOptionView = CommunityOptionCustomView()
    let privacyOptionView = CommunityOptionCustomView()
    let inviteViewSuperview = UIView() // inviteView superview to inset content from white edge
    let inviteView = UIView()
    let numberInvitedLabel = UILabel() // subview of inviteView
    let invitePicturesScrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("class CreateCommunityViewController, func viewDidLoad is running...")
        WebCallStatus.createCommunityViewControllerLoaded = 1
        // initialize view
        super.viewDidLoad()
        view.backgroundColor = MYColor.lightGrey
        // initialize nav bar
        // hide default navigation controller provided back button
        navigationItem.setHidesBackButton(true, animated: false)
        // set nav bar title
        navigationItem.title = "Create Community"
        // set style so title is white
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        // set left nav bar button
        let leftBarButtonItem = UIBarButtonItem(title:"Cancel", style: .plain, target: self, action: #selector(CreateCommunityViewController.backToHomeViewController(_:)))
        navigationItem.leftBarButtonItem  = leftBarButtonItem
        let cancelButtonAttribute: [String : AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17),NSForegroundColorAttributeName: UIColor.white]
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(cancelButtonAttribute, for: .normal)
        // set right nav bar button
        let rightBarButtonItem = UIBarButtonItem(title:"Create", style: .plain, target: self, action: #selector(CreateCommunityViewController.onCreateButtonTouch(_:)))
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        // initialize subviews
        defineCommunityNamePictureSuperview()
        defineInviteViewSuperview()
        defineMaxMembersOptionView()
        definePrivacyOptionView()
        defineLocationOptionView()
        
        // initialize subviews to subviews
        defineCommunityNameTextView()
        defineInviteView()
        defineProfilePictureContainer()
        // defineEditButton()
        
        // add subviews to view
        view.addSubview(communityNamePictureSuperview)
        view.addSubview(inviteViewSuperview)
        view.addSubview(maxMembersOptionView)
        view.addSubview(privacyOptionView)
        view.addSubview(locationOptionView)
        
        // add subviews to subviews
        inviteViewSuperview.addSubview(inviteView)
        communityNamePictureSuperview.addSubview(communityNameTextView)
        communityNamePictureSuperview.addSubview(profilePictureContainer)
        // communityNameTextView.addSubview(editButton)
        
        // set interaction state and tint color
        updateCreateButtonInteractionState()
        
        // setup for text view placeholder text
        // create the textView
        applyPlaceholderStyle(aTextview: communityNameTextView, placeholderText: createCommunityData.communityNamePlaceholderText)
        // set this class as the delegate so we can handle events from the textView
        communityNameTextView.delegate = self
        // set firstNameTextView as first responder
        communityNameTextView.becomeFirstResponder()
        moveCursorToStart(aTextView: communityNameTextView)
    }
    
    func defineInviteViewSuperview() {
        print("func defineInviteViewSuperview is running...")
        inviteViewSuperview.frame.size = CGSize(width:ScreenDimension.width,height:ScreenDimension.height*15.5/100)
        let yOrigin = communityNamePictureSuperview.frame.height+ScreenDimension.height*2.15/100
        inviteViewSuperview.frame.origin.y = yOrigin
        inviteViewSuperview.backgroundColor = .white
    }
    
    func defineInviteView() {
        print("func defineInviteView is running...")
        inviteView.frame.size = CGSize(width:inviteViewSuperview.frame.width*85/100,height:inviteViewSuperview.frame.height*80/100)
        inviteView.center = CGPoint(x:inviteViewSuperview.bounds.midX,y:inviteViewSuperview.bounds.midY)
        // initialize title label
        let inviteTitleLabel = defineInviteTitleLabel()
        
        // setup number invited label
        defineNumberInvitedLabel()
        
        // setup invite pictures scrollView
        defineInvitePicturesScrollView()
        inviteView.addSubview(invitePicturesScrollView)
        
        inviteView.addSubview(inviteTitleLabel)
        inviteView.addSubview(numberInvitedLabel)
    }
    
    func defineInviteTitleLabel()->UILabel {
        print("func defineInviteTitleLabel is running...")
        let inviteTitleLabel = UILabel()
        inviteTitleLabel.text = "Invite"
        inviteTitleLabel.textColor = MYColor.lightBlack
        inviteTitleLabel.font = UIFont.systemFont(ofSize: 21)
        inviteTitleLabel.sizeToFit()
        return inviteTitleLabel
    }
    
    func defineNumberInvitedLabel() {
        print("func defineNumberInvitedLabel is running...")
        let numberInvited = createCommunityData.community.pendingMembers.count
        numberInvitedLabel.text = "\(numberInvited) invited"
        numberInvitedLabel.textColor = MYColor.darkGrey
        numberInvitedLabel.font = UIFont.systemFont(ofSize: 16)
        numberInvitedLabel.sizeToFit()
        let numberInvitedXOrigin: CGFloat = inviteView.frame.width-numberInvitedLabel.frame.width
        numberInvitedLabel.frame.origin = CGPoint(x: numberInvitedXOrigin, y: 0.0)
    }
    
    func defineInvitePicturesScrollView() {
        print("func defineInvitePicturesScrollView is running...")
        // initialize activeRallyCardsScrollview
        let yOrigin = inviteView.frame.height*40/100
        invitePicturesScrollView.frame = CGRect(x: 0, y: yOrigin, width: inviteView.frame.width, height: inviteView.frame.height-yOrigin)
        invitePicturesScrollView.clipsToBounds = true
        invitePicturesScrollView.delegate = self
        invitePicturesScrollView.showsHorizontalScrollIndicator = false
        // set default to be non-scrollable by making content size same size as frame height
        invitePicturesScrollView.contentSize.height = invitePicturesScrollView.frame.height
    }
    
    func updateInvitePicturesScrollView() {
        print("func updateInvitePicturesScrollView is running...")
        // update pictures
    }
    
    func defineCommunityNamePictureSuperview() {
        print("func defineInviteViewSuperview is running...")
        communityNamePictureSuperview.frame.size = CGSize(width: ScreenDimension.width, height: ScreenDimension.height*10/100)
        communityNamePictureSuperview.backgroundColor = .white
    }
    
    func defineCommunityNameTextView() {
        print("func defineCommunityNameTextView is running...")
        // set communityNameTextView frame
        communityNameTextView.frame.size = CGSize(width: communityNamePictureSuperview.frame.width*85/100, height: communityNamePictureSuperview.frame.height)
        
        // set text
        communityNameTextView.textColor = MYColor.darkGrey
        communityNameTextView.font = UIFont.systemFont(ofSize: 19.5)
    }
    
    func defineMaxMembersOptionView() {
        print("func defineMaxMembersOptionView is running...")
        // set maxMembersOptionView frame
        let yOrigin = inviteViewSuperview.frame.origin.y+inviteViewSuperview.frame.height+ScreenDimension.height*2.15/100
        maxMembersOptionView.frame.origin.y = yOrigin
        //maxMembersOptionView.frame.size = CGSize(width:ScreenDimension.screenWidth,height:ScreenDimension.screenHeight*7/100)
        
        maxMembersOptionView.optionTitle.text = createCommunityData.maxMembersTitle
        maxMembersOptionView.optionTitle.sizeToFit()
        // set initial value
        maxMembersOptionView.optionValue.text = "No maximum set"
        maxMembersOptionView.optionValue.sizeToFit()
        // center y-origin for subviews within max members option views
        maxMembersOptionView.optionTitle.center.y = maxMembersOptionView.frame.height/2
        let optionValueYOrigin = maxMembersOptionView.optionTitle.frame.origin.y+maxMembersOptionView.optionTitle.frame.height-maxMembersOptionView.optionValue.frame.height
        maxMembersOptionView.optionValue.frame.origin.y = optionValueYOrigin
        
        // set optionValue x-origin based on optionTitle width
        maxMembersOptionView.optionValue.frame.origin.x = maxMembersOptionView.optionTitle.frame.origin.x+maxMembersOptionView.optionTitle.frame.width
        
        // add view gesture recognizer
        let maxMembersOptionViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(CreateCommunityViewController.handleMaxMembersOptionViewTap(_:)))
        maxMembersOptionView.addGestureRecognizer(maxMembersOptionViewTap)
    }
    
    func handleMaxMembersOptionViewTap(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func handleMaxMembersOptionViewTap is running...")
        let currentValue = maxMembersOptionView.optionValue.text
        var currentValueAsInt: Int!
        if currentValue == "No maximum set" {
            currentValueAsInt = createCommunityData.maxMembersOptionValues.first!
        }
        else {
            currentValueAsInt = Int(currentValue!)
        }
        let index: Int = createCommunityData.maxMembersOptionValues.index(of:currentValueAsInt)!
        let arrayCount: Int = createCommunityData.maxMembersOptionValues.count
        let indexed: Int = index+1
        let newIndex: Int = indexed < arrayCount ? indexed : 0
        let newValue: Int = createCommunityData.maxMembersOptionValues[newIndex]
        var formattedNewValue: String!
        switch newValue {
        case createCommunityData.maxMembersOptionValues.first!:
            formattedNewValue = "No maximum set"
        default:
            formattedNewValue = String(newValue)
        }
        maxMembersOptionView.optionValue.text = formattedNewValue
    }
    
    func definePrivacyOptionView() {
        print("func definePrivacyOptionView is running...")
        // set privacyOption frame
        let yOrigin = maxMembersOptionView.frame.origin.y+maxMembersOptionView.frame.height+1.5
        privacyOptionView.frame.origin.y = yOrigin
        privacyOptionView.frame.size = CGSize(width:ScreenDimension.width,height:ScreenDimension.height*7/100)
        
        // set title
        privacyOptionView.optionTitle.text = createCommunityData.privacyTitle
        privacyOptionView.optionTitle.sizeToFit()
        
        // set initial value
        privacyOptionView.optionValue.text = "Visible only to people that are invited"
        privacyOptionView.optionValue.sizeToFit()
        
        // center subviews within privacy option view
        privacyOptionView.optionTitle.center.y = privacyOptionView.frame.height/2
        let optionValueYOrigin = privacyOptionView.optionTitle.frame.origin.y+privacyOptionView.optionTitle.frame.height-privacyOptionView.optionValue.frame.height
        privacyOptionView.optionValue.frame.origin.y = optionValueYOrigin
        
        
        // set optionValue x-origin based on optionTitle width
        privacyOptionView.optionValue.frame.origin.x = privacyOptionView.optionTitle.frame.origin.x+privacyOptionView.optionTitle.frame.width
        
        // add view gesture recognizer
        let privacyOptionViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(CreateCommunityViewController.handlePrivacyOptionViewTap(_:)))
        privacyOptionView.addGestureRecognizer(privacyOptionViewTap)
    }
    
    func handlePrivacyOptionViewTap(_ sender:UITapGestureRecognizer) {
        print("func handlePrivacyOptionViewTap is running...")
        let currentText = privacyOptionView.optionValue.text!
        let privateText = "Visible only to people that are invited"
        let publicText = "Visible to anyone nearby to join"
        var newText: String!
        switch currentText {
        case publicText:
            newText = privateText
            createCommunityData.community.isPrivate = true
        case privateText:
            newText = publicText
            createCommunityData.community.isPrivate = false
        default:
            break
        }
        privacyOptionView.optionValue.text = newText
    }
    
    func defineLocationOptionView() {
        print("func defineLocationOptionView is running...")
        // set privacyOption frame
        let yOrigin = privacyOptionView.frame.origin.y+privacyOptionView.frame.height+1.5
        locationOptionView.frame.origin.y = yOrigin
        locationOptionView.frame.size = CGSize(width:ScreenDimension.width,height:ScreenDimension.height*7/100)
        
        // set title
        locationOptionView.optionTitle.text = createCommunityData.locationTitle
        locationOptionView.optionTitle.sizeToFit()
        
        // set initial value
        locationOptionView.optionValue.text = "Current location"
        locationOptionView.optionValue.sizeToFit()
        
        // center y-origin for subviews within location option views
        locationOptionView.optionTitle.center.y = locationOptionView.frame.height/2
        let optionValueYOrigin = locationOptionView.optionTitle.frame.origin.y+locationOptionView.optionTitle.frame.height-locationOptionView.optionValue.frame.height
        locationOptionView.optionValue.frame.origin.y = optionValueYOrigin
        
        // set optionValue x-origin based on optionTitle width
        locationOptionView.optionValue.frame.origin.x = locationOptionView.optionTitle.frame.origin.x+locationOptionView.optionTitle.frame.width
        
        // add view gesture recognizer
        let locationOptionViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(CreateCommunityViewController.handleLocationOptionViewTap(_:)))
        locationOptionView.addGestureRecognizer(locationOptionViewTap)
    }
    
    func handleLocationOptionViewTap(_ sender:UITapGestureRecognizer) {
        print("func handleLocationOptionViewTap is running...")
        /*
        let currentText = privacyOptionView.optionValue.text
        var newText: String!
        switch currentText {
        case publicText:
            newText = privateText
        case privateText:
            newText = publicText
        default:
            break
        }
        privacyOptionView.optionValue.text = newText
        */
        // Do nothing for now, setup location last (along with invite)
    }
    
    func backToHomeViewController(_ sender: UIButton?) {
        print("func backToHomeViewController is running...")
        self.navigationController!.popToRootViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        print("class CreateThreadViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        // for taking pictures to reload new image data when re-entering editProfileVC
        navigationController?.setToolbarHidden(true, animated: animated)
        startObservingKeyboardEvents()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("class CreateThreadViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        stopObservingKeyboardEvents()
    }
    
    func startObservingKeyboardEvents() {
        print("func startObservingKeyboardEvents is running...")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateCommunityViewController.keyboardWillShow),
                                               name:NSNotification.Name.UIKeyboardWillShow,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateCommunityViewController.keyboardWillHide),
                                               name:NSNotification.Name.UIKeyboardWillHide,
                                               object:nil)
    }
    
    func stopObservingKeyboardEvents() {
        print("func stopObservingKeyboardEvents is running...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        if text == "\n" {
            // return false when return key is pressed
            return false
        }
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if textView.text.characters.count == 0 && text == " " {
            return false
        }
        if newLength > 25 {
            return false
        }
        if newLength > 0 {
            if textView == communityNameTextView && textView.text == createCommunityData.communityNamePlaceholderText {
                if text.utf16.count == 0 && textView.textColor == MYColor.darkGrey { // they hit the back button during placeholder style
                    return false // ignore it
                }
                if textView.textColor == MYColor.darkGrey {
                    applyNonPlaceholderStyle(aTextview: textView)
                    textView.text = ""
                }
            }
            return true
        }
        else { // no text, so show the placeholder
            if textView == communityNameTextView {
                applyPlaceholderStyle(aTextview: textView, placeholderText: createCommunityData.communityNamePlaceholderText)
            }
            moveCursorToStart(aTextView: textView)
            updateCreateButtonInteractionState()
            return false
        }
    }
    
    func textViewShouldEndEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldEndEditing is running...")
        if aTextView == communityNameTextView && aTextView.text == "" {
            // move cursor to start
            aTextView.textColor = MYColor.darkGrey
            aTextView.text = createCommunityData.communityNamePlaceholderText
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func moveCursorToStart(aTextView: UITextView) {
        print("func moveCursorToStart is running...")
        DispatchQueue.main.async(execute: { () -> Void in
            aTextView.selectedRange = NSMakeRange(0, 0)
        })
    }
    
    func textViewDidChange(_ atextView: UITextView) {
        print("func textViewDidChange is running...")
        // set interaction state and tint color
        updateCreateButtonInteractionState()
    }
    
    func textViewShouldBeginEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldBeginEditing is running...")
        if aTextView == communityNameTextView && aTextView.text == createCommunityData.communityNamePlaceholderText {
            // move cursor to start
            //aTextView.textColor = .black
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String) {
        print("func applyPlaceholderStyle is running...")
        // make it look (initially) like a placeholder
        aTextview.textColor = MYColor.darkGrey
        aTextview.text = placeholderText
    }
    
    func applyNonPlaceholderStyle(aTextview: UITextView) {
        print("func applyNonPlaceholderStyle is running...")
        // make it look like normal text instead of a placeholder
        aTextview.textColor = .black
        aTextview.alpha = 1.0
    }
    
    func handleProfilePictureViewTap(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func handleProfilePictureViewTap is running...")
        displayCameraOptions()
    }
    
    func onEditButtonTouch(_ sender: UIButton!) {
        print("func onEditButtonTouch is running...")
        displayCameraOptions()
    }
    
    func displayCameraOptions() {
        print("func displayCameraOptions is running...")
        CameraPhotoLibraryHelper = CameraPhotoLibrary()
        CameraPhotoLibraryHelper!.displayCameraAndPhotosActionSheet(self)
    }
    /*
    func defineEditButton() {
        print("func defineEditButton is running...")
        //frame: CGRect(x: ScreenDimension.screenWidth*77.5/100, y: containerViewHeight*4/100, width: ScreenDimension.screenWidth*18.5/100, height: containerViewHeight*17/100)
        // define button y-center as equal to section label y-center
        editButton.tag = 169
        editButton.setTitleColor(MYColor.midGrey, for: UIControlState())
        editButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        editButton.setTitle("Edit", for: UIControlState())
        editButton.addTarget(self, action: #selector(EditProfileViewController.onEditButtonTouch(_:)), for: .touchUpInside)
        editButton.sizeToFit()
        editButton.frame.origin.y = profilePictureContainer.frame.origin.y + profilePictureContainer.frame.height
        editButton.center.x = profilePictureContainer.center.x
    }
    */
    
    func defineProfilePictureContainer() {
        print("func defineProfilePictureContainer is running...")
        let itemSideLength = communityNamePictureSuperview.frame.height*40/100
        let picturesDisplayData: PicturesDisplayDataSource = PicturesDisplayDataSource(nil, createCommunityData.community.members, profilePictureContainer, .createCommunity(nil), itemSideLength)
        profilePictureContainer = PicturesDisplayHelper(picturesDisplayData: picturesDisplayData).defineRallyOrGroupCardPictures()
        for subview in profilePictureContainer.subviews {
            profilePictureContainer.frame.size = CGSize(width:subview.frame.width*2,height:subview.frame.width*2)
            break
        }
        let profilePictureContainerXOrigin: CGFloat = communityNamePictureSuperview.frame.width-profilePictureContainer.frame.width-ScreenDimension.width*3/100
        profilePictureContainer.frame.origin.x = profilePictureContainerXOrigin
        profilePictureContainer.center.y = communityNamePictureSuperview.bounds.midY
        profilePictureContainer.layer.cornerRadius = profilePictureContainer.frame.height/2
        profilePictureContainer.clipsToBounds = true
        //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        // edit community profile picture feature to be added in v1.1
        /*
        let handleProfilePictureViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(EditProfileViewController.onEditButtonTouch(_:)))
        profilePictureContainer.addGestureRecognizer(handleProfilePictureViewTap)
        */
    }

    func updateCreateButtonInteractionState() {
        print("func updateCreateButtonInteractionState is running...")
        var createButtonAttribute: [String : AnyObject] = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)]
        if communityNameTextView.textColor == MYColor.darkGrey {
            createButtonAttribute[NSForegroundColorAttributeName] = MYColor.greyBlue
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        else {
            navigationItem.rightBarButtonItem?.isEnabled = true
            createButtonAttribute[NSForegroundColorAttributeName] = UIColor.white
        }
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(createButtonAttribute, for: .normal)
    }
    
    
    
    
    
    
    
    
    
    
    func onCreateButtonTouch(_ sender: UIButton!) {
        print("func onCreateButtonTouch is running...")
        // make request to create user in database
        createCommunityData.community.name = communityNameTextView.text
        createCommunityData.community.userID = MYUser.ID
        let intent = WebServiceCallIntent.createCommunity(createCommunityData.community)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        backToHomeViewController(nil)
    }
    
        
        
        
        
        
    /*
    func onNewGroupTitleTextViewCreateButtonTouch(_ sender: UIButton!) {
        print("func onNewGroupTitleTextViewCreateButtonTouch is running...")
        // create new community
        if let newGroupTitleTextView = view.viewWithTag(641) as? UITextView {
            if newGroupTitleTextView.text.isEmpty || newGroupTitleTextView.text == "Enter Community Name" {
                // display alert that group name is invalid (not real word, too short, explicit)
                // start with just too short, need to find an efficient, effective way for "real word" and "explicit". also need to see if they're even a problem
                //displayMessageDialogBox("You need to add a Community name before creating")
            }
            else {
                let newCommunity = rally()
                DismissKeyboard(nil)
                newGroupTitleTextView.removeFromSuperview()
                newCommunity.RallyName = newGroupTitleTextView.text
                //newGroup.arrayIndex = userGroupsArray.count
                newCommunity.UserID = userClass.UserID
                newCommunity.IsRallyActive = 0
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .long // date format "November 23, 1937"
                dateFormatter.timeStyle = .long // time format "3:30:32 PM PST"
                let date = Date()
                let dateAsString = dateFormatter.string(from: date)
                newCommunity.CreatedDateTimeStamp = dateAsString // this isn't the same as backend, change if end up being used later on
                newCommunity.type = "userGroupsArray"
                let newFriend = friend() // this is done just to be able to use user picture when creating group card
                newFriend.PhoneNumberList = userClass.PhoneNumber
                newFriend.thumbnailProfilePicture = userClass.thumbnailProfilePicture
                newFriend.firstName = userClass.firstName
                newFriend.lastName = userClass.lastName
                newCommunity.RallyUserList.append(newFriend)
                newlyCreatedCommunityToBeSentToWebService.append(newCommunity)
                updateLocationMethodFlag = "updateLocationThenPostNewRallyGroup"
                permissionsHelperClass.userLocation = nil
                permissionsHelperClass.locationManagerSingleton.startUpdatingLocation()
            }
        }
    }*/
}
