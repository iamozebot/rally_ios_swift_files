//
//  CreateThreadViewController.swift
//  Rally
//
//  Created by Ian Moses on 11/15/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

protocol PassDataToViewControllerDelegate {
    func childViewControllerResponse(returningFromCreateThreadVC: Bool)
}

class CreateThreadViewControllerDataSource {
    let threadNamePlaceholderText = "Thread name"
    //let maxMembersTitle = "Max members -"
    //let privacyTitle = "Privacy -"
    //let locationTitle = "Location -"
    
    let locationDefaultDescription = ""
    var community = Community()
    
    init() {
        if let userAsFriend = MYUser.friendType() {
            community.members.append(userAsFriend)
        }
    }
    
    //var maxMembersOptionValues: [Int] = [999, 10, 25, 50, 100, 250]
}

class CreateThreadViewController: UIViewController, UITextViewDelegate {
    
    override var prefersStatusBarHidden : Bool {
        get {
            return self.statusBarDisplayState
        }
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        get {
            return self.statusBarAnimationState
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    var statusBarDisplayState: Bool = false
    var statusBarAnimationState: UIStatusBarAnimation = .none
 
    var createThreadData = CreateThreadViewControllerDataSource()
    // feature not included in v1.0, use in v1.1
    // var editButton = UIButton() // subview of inviteView
    let threadNameTextView = EnterNameTextViewSubclass()
    let threadNameSuperview = UIView()
    let inviteViewSuperview = UIView() // inviteView superview to inset content from white edge
    let inviteView = UIView()
    let numberInvitedLabel = UILabel() // subview of inviteView
    let invitePicturesScrollView = UIScrollView()
    var delegate: PassDataToViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("class CreateThreadViewController, func viewDidLoad is running...")
        WebCallStatus.createThreadViewControllerLoaded = 1
        // initialize view
        super.viewDidLoad()
        view.backgroundColor = MYColor.lightGrey
        // initialize nav bar
        navigationController?.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: 40)), forKeyPath: "navigationBar")
        navigationController?.isNavigationBarHidden = false
        // hide default navigation controller provided back button
        navigationItem.setHidesBackButton(true, animated: false)
        // set nav bar title
        navigationItem.title = "Create Thread"
        // set style so title is white
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        // set left nav bar button
        let leftBarButtonItem = UIBarButtonItem(title:"Cancel", style: .plain, target: self, action: #selector(CreateThreadViewController.backToLeftPanelViewController(_:)))
        navigationItem.leftBarButtonItem  = leftBarButtonItem
        let cancelButtonAttribute: [String : AnyObject] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17),NSForegroundColorAttributeName: UIColor.white]
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(cancelButtonAttribute, for: .normal)
        // set right nav bar button
        let rightBarButtonItem = UIBarButtonItem(title:"Create", style: .plain, target: self, action: #selector(CreateThreadViewController.onCreateButtonTouch(_:)))
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        // initialize subviews
        defineThreadNameSuperview()
        defineInviteViewSuperview()
        
        // initialize subviews to subviews
        defineThreadNameTextView()
        defineInviteView()
        // defineEditButton()
        
        // add subviews to view
        view.addSubview(threadNameSuperview)
        view.addSubview(inviteViewSuperview)
        
        // add subviews to subviews
        inviteViewSuperview.addSubview(inviteView)
        threadNameSuperview.addSubview(threadNameTextView)
        // threadNameTextView.addSubview(editButton)
        
        // set interaction state and tint color
        updateCreateButtonInteractionState()
        
        // setup for text view placeholder text
        // create the textView
        applyPlaceholderStyle(aTextview: threadNameTextView, placeholderText: createThreadData.threadNamePlaceholderText)
        // set this class as the delegate so we can handle events from the textView
        threadNameTextView.delegate = self
        // set firstNameTextView as first responder
        threadNameTextView.becomeFirstResponder()
        moveCursorToStart(aTextView: threadNameTextView)
    }
    
    func defineInviteViewSuperview() {
        print("func defineInviteViewSuperview is running...")
        inviteViewSuperview.frame.size = CGSize(width: ScreenDimension.width,height: ScreenDimension.height*15.5/100)
        let yOrigin = threadNameSuperview.frame.height+ScreenDimension.height*2.15/100
        inviteViewSuperview.frame.origin.y = yOrigin
        inviteViewSuperview.backgroundColor = .white
    }
    
    func defineInviteView() {
        print("func defineInviteView is running...")
        inviteView.frame.size = CGSize(width:inviteViewSuperview.frame.width*85/100,height:inviteViewSuperview.frame.height*80/100)
        inviteView.center = CGPoint(x:inviteViewSuperview.bounds.midX,y:inviteViewSuperview.bounds.midY)
        // initialize title label
        let inviteTitleLabel = defineInviteTitleLabel()
        
        // setup number invited label
        defineNumberInvitedLabel()
        
        // setup invite pictures scrollView
        defineInvitePicturesScrollView()
        inviteView.addSubview(invitePicturesScrollView)
        
        inviteView.addSubview(inviteTitleLabel)
        inviteView.addSubview(numberInvitedLabel)
    }
    
    func defineInviteTitleLabel()->UILabel {
        print("func defineInviteTitleLabel is running...")
        let inviteTitleLabel = UILabel()
        inviteTitleLabel.text = "Invite"
        inviteTitleLabel.textColor = MYColor.lightBlack
        inviteTitleLabel.font = UIFont.systemFont(ofSize: 21)
        inviteTitleLabel.sizeToFit()
        return inviteTitleLabel
    }
    
    func defineNumberInvitedLabel() {
        print("func defineNumberInvitedLabel is running...")
        let numberInvited = createThreadData.community.pendingMembers.count
        numberInvitedLabel.text = "\(numberInvited) invited"
        numberInvitedLabel.textColor = MYColor.darkGrey
        numberInvitedLabel.font = UIFont.systemFont(ofSize: 16)
        numberInvitedLabel.sizeToFit()
        let numberInvitedXOrigin: CGFloat = inviteView.frame.width-numberInvitedLabel.frame.width
        numberInvitedLabel.frame.origin = CGPoint(x: numberInvitedXOrigin, y: 0.0)
    }
    
    func defineInvitePicturesScrollView() {
        print("func defineInvitePicturesScrollView is running...")
        // initialize activeRallyCardsScrollview
        let yOrigin = inviteView.frame.height*40/100
        invitePicturesScrollView.frame = CGRect(x: 0, y: yOrigin, width: inviteView.frame.width, height: inviteView.frame.height-yOrigin)
        invitePicturesScrollView.clipsToBounds = true
        invitePicturesScrollView.delegate = self
        invitePicturesScrollView.showsHorizontalScrollIndicator = false
        // set default to be non-scrollable by making content size same size as frame height
        invitePicturesScrollView.contentSize.height = invitePicturesScrollView.frame.height
    }
    
    func updateInvitePicturesScrollView() {
        print("func updateInvitePicturesScrollView is running...")
        // update pictures
    }
    
    func defineThreadNameSuperview() {
        print("func defineInviteViewSuperview is running...")
        threadNameSuperview.frame.size = CGSize(width: ScreenDimension.width, height: ScreenDimension.height*10/100)
        threadNameSuperview.backgroundColor = .white
    }
    
    func defineThreadNameTextView() {
        print("func defineThreadNameTextView is running...")
        // set threadNameTextView frame
        threadNameTextView.frame.size = CGSize(width: threadNameSuperview.frame.width*85/100, height: threadNameSuperview.frame.height)
        
        // set text
        threadNameTextView.textColor = MYColor.darkGrey
        threadNameTextView.font = UIFont.systemFont(ofSize: 19.5)
    }
    
    func backToLeftPanelViewController(_ sender: UIButton?) {
        print("func backToLeftPanelViewController is running...")
        //backToLeftPanelViewController
        self.navigationController!.popViewController(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class CreateThreadViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        // for taking pictures to reload new image data when re-entering editProfileVC
        navigationController?.setToolbarHidden(true, animated: animated)
        startObservingKeyboardEvents()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("class CreateThreadViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        stopObservingKeyboardEvents()
        //returningFromCreateThreadVC
        delegate?.childViewControllerResponse(returningFromCreateThreadVC: true)
        navigationController?.isNavigationBarHidden = true
    }
    
    func startObservingKeyboardEvents() {
        print("func startObservingKeyboardEvents is running...")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateThreadViewController.keyboardWillShow),
                                               name:NSNotification.Name.UIKeyboardWillShow,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CreateThreadViewController.keyboardWillHide),
                                               name:NSNotification.Name.UIKeyboardWillHide,
                                               object:nil)
    }
    
    func stopObservingKeyboardEvents() {
        print("func stopObservingKeyboardEvents is running...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        if text == "\n" {
            // return false when return key is pressed
            return false
        }
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if textView.text.characters.count == 0 && text == " " {
            return false
        }
        if newLength > 25 {
            return false
        }
        if newLength > 0 {
            if textView == threadNameTextView && textView.text == createThreadData.threadNamePlaceholderText {
                if text.utf16.count == 0 && textView.textColor == MYColor.darkGrey { // they hit the back button during placeholder style
                    return false // ignore it
                }
                if textView.textColor == MYColor.darkGrey {
                    applyNonPlaceholderStyle(aTextview: textView)
                    textView.text = ""
                }
            }
            return true
        }
        else { // no text, so show the placeholder
            if textView == threadNameTextView {
                applyPlaceholderStyle(aTextview: textView, placeholderText: createThreadData.threadNamePlaceholderText)
            }
            moveCursorToStart(aTextView: textView)
            updateCreateButtonInteractionState()
            return false
        }
    }
    
    func textViewShouldEndEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldEndEditing is running...")
        if aTextView == threadNameTextView && aTextView.text == "" {
            // move cursor to start
            aTextView.textColor = MYColor.darkGrey
            aTextView.text = createThreadData.threadNamePlaceholderText
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func moveCursorToStart(aTextView: UITextView) {
        print("func moveCursorToStart is running...")
        DispatchQueue.main.async(execute: { () -> Void in
            aTextView.selectedRange = NSMakeRange(0, 0)
        })
    }
    
    func textViewDidChange(_ atextView: UITextView) {
        print("func textViewDidChange is running...")
        // set interaction state and tint color
        updateCreateButtonInteractionState()
    }
    
    func textViewShouldBeginEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldBeginEditing is running...")
        if aTextView == threadNameTextView && aTextView.text == createThreadData.threadNamePlaceholderText {
            // move cursor to start
            //aTextView.textColor = .black
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String) {
        print("func applyPlaceholderStyle is running...")
        // make it look (initially) like a placeholder
        aTextview.textColor = MYColor.darkGrey
        aTextview.text = placeholderText
    }
    
    func applyNonPlaceholderStyle(aTextview: UITextView) {
        print("func applyNonPlaceholderStyle is running...")
        // make it look like normal text instead of a placeholder
        aTextview.textColor = .black
        aTextview.alpha = 1.0
    }
    
    func updateCreateButtonInteractionState() {
        print("func updateCreateButtonInteractionState is running...")
        var createButtonAttribute: [String : AnyObject] = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)]
        if threadNameTextView.textColor == MYColor.darkGrey {
            createButtonAttribute[NSForegroundColorAttributeName] = MYColor.greyBlue
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
        else {
            navigationItem.rightBarButtonItem?.isEnabled = true
            createButtonAttribute[NSForegroundColorAttributeName] = UIColor.white
        }
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(createButtonAttribute, for: .normal)
    }
    
    
    
    
    
    
    
    
    
    
    func onCreateButtonTouch(_ sender: UIButton!) {
        print("func onCreateButtonTouch is running...")
        // make request to create user in database
        createThreadData.community.name = threadNameTextView.text
        let intent = WebServiceCallIntent.createCommunity(createThreadData.community)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                print("createCommunity webcall succeeded")
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        backToLeftPanelViewController(nil)
    }
}
