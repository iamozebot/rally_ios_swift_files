//
//  CustomCollectionViewLayout.swift
//  Rally
//
//  Created by Ian Moses on 11/15/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

class StickyHeadersCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    // MARK: - Collection View Flow Layout Methods
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        print("func shouldInvalidateLayout is running...")
        return true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        print("func layoutAttributesForElements is running...")
        guard let layoutAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        
        // Helpers
        let sectionsToAdd = NSMutableIndexSet()
        var newLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for layoutAttributesSet in layoutAttributes {
            if layoutAttributesSet.representedElementCategory == .cell {
                // Add Layout Attributes
                newLayoutAttributes.append(layoutAttributesSet)
                
                // Update Sections to Add
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
                
            } else if layoutAttributesSet.representedElementCategory == .supplementaryView {
                // Update Sections to Add
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
            }
        }
        
        for section in sectionsToAdd {
            let indexPath = IndexPath(item: 0, section: section)
            
            if let sectionAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
                newLayoutAttributes.append(sectionAttributes)
            }
        }
        
        return newLayoutAttributes
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        print("func layoutAttributesForSupplementaryView is running...")
        guard let layoutAttributes = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: indexPath) else { return nil }
        guard let boundaries = boundaries(forSection: indexPath.section) else { return layoutAttributes }
        guard let collectionView = collectionView else { return layoutAttributes }
        
        // Helpers
        let contentOffsetY = collectionView.contentOffset.y
        print("contentOffsetY:\(contentOffsetY)")
        var frameForSupplementaryView = layoutAttributes.frame
        
        let minimum = boundaries.minimum - frameForSupplementaryView.height
        let maximum = boundaries.maximum - frameForSupplementaryView.height
        
        if contentOffsetY < minimum {
            frameForSupplementaryView.origin.y = minimum
        } else if contentOffsetY > maximum {
            frameForSupplementaryView.origin.y = maximum
        } else {
            frameForSupplementaryView.origin.y = contentOffsetY
        }
        
        return layoutAttributes
    }
    
    // MARK: - Helper Methods
    func boundaries(forSection section: Int) -> (minimum: CGFloat, maximum: CGFloat)? {
        print("func boundaries is running...")
        // Helpers
        var result = (minimum: CGFloat(0.0), maximum: CGFloat(0.0))
        
        // Exit Early
        guard let collectionView = collectionView else { return result }
        
        // Fetch Number of Items for Section
        let numberOfItems = collectionView.numberOfItems(inSection: section)
        
        // Exit Early
        guard numberOfItems > 0 else { return result }
        
        if let firstItem = layoutAttributesForItem(at: IndexPath(item: 0, section: section)),
            let lastItem = layoutAttributesForItem(at: IndexPath(item: (numberOfItems - 1), section: section)) {
            result.minimum = firstItem.frame.minY
            result.maximum = lastItem.frame.maxY
            
            // Take Header Size Into Account
            result.minimum -= headerReferenceSize.height
            result.maximum -= headerReferenceSize.height
            
            // Take Section Inset Into Account
            result.minimum -= sectionInset.top
            result.maximum += (sectionInset.top + sectionInset.bottom)
        }
        
        return result
    }
    
}

class CenterContainerCollectionViewCell: UICollectionViewCell {
    
    fileprivate lazy var customTextLabel : UILabel? = {
        let customTextLabel = UILabel()
        customTextLabel.tag = 212
        self.addSubview(customTextLabel)
        return customTextLabel
    }()
    
    func setUpCellWith(text: String) {
        print("func setUpCellWith is running...")
        customTextLabel!.text = text
        customTextLabel!.textColor = MYColor.lightBlack
        customTextLabel!.sizeToFit()
        customTextLabel!.font = UIFont.systemFont(ofSize: 14.5)
    }
}

class StickyHeaderView: UICollectionReusableView {
    
    lazy var headerLabel : UILabel? = {
        let headerLabel = UILabel()
        headerLabel.tag = 213
        self.addSubview(headerLabel)
        return headerLabel
    }()
    
    func setupStickyHeaderView(headerText : String, centerContainer: UIView) {
        print("func setupStickyHeaderView is running...")
        headerLabel!.text = headerText
        headerLabel!.font = UIFont.systemFont(ofSize: 14.5, weight: UIFontWeightBold)
        headerLabel!.textColor = MYColor.lightBlack
        headerLabel!.sizeToFit()
        headerLabel!.frame.origin = CGPoint(x:centerContainer.frame.width*5/100,y:centerContainer.frame.height*3/100)
    }
}
    
