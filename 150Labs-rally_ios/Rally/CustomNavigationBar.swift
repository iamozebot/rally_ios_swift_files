//
//  CustomNavigationBar.swift
//  Rally
//
//  Created by Ian Moses on 12/30/15.
//  Copyright © 2015 Ian Moses. All rights reserved.
// http://stackoverflow.com/questions/28705442/ios-8-swift-xcode-6-set-top-nav-bar-bg-color-and-height

import Foundation
import UIKit

class UINavigationBarTaller: UINavigationBar {
    ///The height you want your navigation bar to be of
    static var navigationBarHeight: CGFloat = 60
    
    ///The difference between new height and default height
    static let heightIncrease:CGFloat = navigationBarHeight - 44
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    fileprivate func initialize() {
        let shift = UINavigationBarTaller.heightIncrease/2
        
        ///Transform all view to shift upward for [shift] point
        self.transform =
            CGAffineTransform(translationX: 0, y: -shift)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let shift = UINavigationBarTaller.heightIncrease/2
        
        ///Move the background down for [shift] point
        // http://stackoverflow.com/questions/39454548/ios-10-custom-navigation-bar-height
        // isIOS10 ? ["_UIBarBackground"] : ["_UINavigationBarBackground"]
        let classNamesToReposition: [String] = ["_UIBarBackground"]
        for view: UIView in self.subviews {
            if classNamesToReposition.contains(NSStringFromClass(type(of: view))) {
                let bounds: CGRect = self.bounds
                var frame: CGRect = view.frame
                frame.origin.y = bounds.origin.y + shift - 20.0
                frame.size.height = bounds.size.height + 20.0
                view.frame = frame
            }
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let amendedSize:CGSize = super.sizeThatFits(size)
        let newSize:CGSize = CGSize(width: amendedSize.width, height: UINavigationBarTaller.navigationBarHeight);
        return newSize;
    }
}
