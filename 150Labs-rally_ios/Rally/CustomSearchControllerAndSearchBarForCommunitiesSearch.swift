//
//  CustomSearchControllerAndSearchBarForCommunitiesSearch.swift
//  Rally
//
//  Created by Ian Moses on 7/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class CustomSearchBar: UISearchBar {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setShowsCancelButton(false, animated: false)
    }
}

class CustomSearchController: UISearchController, UISearchBarDelegate {
    
    lazy var _searchBar: CustomSearchBar = {
        [unowned self] in
        let result = CustomSearchBar(frame: CGRect.zero)
        result.delegate = self
        
        return result
        }()
    
    override var searchBar: UISearchBar {
        get {
            return _searchBar
        }
    }
}
