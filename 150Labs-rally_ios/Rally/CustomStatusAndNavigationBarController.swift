//
//  CustomStatusAndNavigationBarController.swift
//  Rally
//
//  Created by Ian Moses on 12/18/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class NoStatusOrNavigationBarViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}
