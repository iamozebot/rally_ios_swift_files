//
//  CustomTabButton.swift
//  Rally
//
//  Created by Ian Moses on 5/13/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class CustomTabButtonClass: UIButton {
    var focus: Bool!
    // http://stackoverflow.com/questions/32551922/initialize-uibutton-with-parameters

    init(focus: Bool) {
        //self.init()
        // Designed initialiser
        // http://stackoverflow.com/questions/32343198/cannot-subclass-uibutton-must-call-a-designated-initializer-of-the-superclass
        super.init(frame: CGRect.zero)
        self.focus = focus
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
