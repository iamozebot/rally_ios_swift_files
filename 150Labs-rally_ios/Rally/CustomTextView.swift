//
//  CustomTextView.swift
//  Rally
//
//  Created by Ian Moses on 4/1/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit

class messagingTextView: UITextView {
    var messagingTextViewTextContainerSingleLineHeight: CGFloat = 0
    var messagingTextViewSingleLineHeight: CGFloat = 0
    var messagingTextViewHeight: CGFloat = 0
    
    // https://github.com/NoahSaso/SConsole/blob/master/SConsole/ViewController.swift
    override func caretRect(for position: UITextPosition) -> CGRect {
        print("func caretRectForPosition is running...")
        var myRect = super.caretRect(for: position)
        myRect.size.height = messagingTextViewTextContainerSingleLineHeight
        myRect.origin.y = messagingTextViewHeight - myRect.height - (messagingTextViewSingleLineHeight-messagingTextViewTextContainerSingleLineHeight)/2
        return myRect
    }
    /*
    override init(frame: CGRect) {
        super.init(frame: frame)
            addContentSizeObserver() // added to init below since this one is erroring
    }
*/
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        addContentSizeObserver()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func addContentSizeObserver() {
        print("private func addContentSizeObserver is running...")
        self.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }

    fileprivate func removeContentSizeObserver() {
        print("private func removeContentSizeObserver is running...")
        self.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) { // [NSObject: AnyObject] to [String : AnyObject] per http://stackoverflow.com/questions/31308209/error-with-override-public-func-observevalueforkeypath
        print("override func observeValueForKeyPath is running...")
        let textView = object as! UITextView
        var top = (textView.bounds.size.height - textView.contentSize.height * zoomScale) / 2.0
        top = top < 0.0 ? 0.0 : top
        //contentOffset = CGPoint(x: contentOffset.x, y: -top)
        textView.contentInset.top = top
        print("override func observeValueForKeyPath is finished running...")
    }

    deinit {
            removeContentSizeObserver()
    }
    /*
    // Force the text in a UITextView to always center itself.
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        let textView = object as! UITextView
        var topCorrect = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect;
        textView.contentInset.top = topCorrect
    }
    */
}
