//
//  CustomToggleButton.swift
//  Rally
//
//  Created by Ian Moses on 11/16/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class MYCollapseExpandButton: UIButton {
    // var currentState: CollapsedExpandedState = .collapsed // default state is defined as being collapsed
    var thread: Thread!
    
    init(_ thread: Thread) {
        // Designed initialiser
        // http://stackoverflow.com/questions/32343198/cannot-subclass-uibutton-must-call-a-designated-initializer-of-the-superclass
        super.init(frame: CGRect.zero)
        self.thread = thread
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
