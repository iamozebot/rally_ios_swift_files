//
//  CustomUIWindowAlertview.swift
//  Rally
//
//  Created by Ian Moses on 9/4/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class CustomUIWindowAlertview {
    
    init(dataObject: WebServiceCallCheckpointData) {
        checkpointDataObject = dataObject
    }
    
    fileprivate lazy var alert: UIAlertController = {
        let alertController = UIAlertController(title: self.checkpointDataObject.validationTitle!, message: self.checkpointDataObject.validationMessage!, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Got it.", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.checkpointDataObject.validationType!.alertViewHandler()
        }))
        return alertController
    }()
    let appDelegateReference  = AppDelegate.getAppDelegate()
    let viewControllerInstance = UIViewController(nibName: nil, bundle: nil)
    fileprivate lazy var alertControllerNav:UINavigationController = {
        let alertControllerNavigationController = UINavigationController(rootViewController: self.viewControllerInstance)
        alertControllerNavigationController.isNavigationBarHidden = true
        return alertControllerNavigationController
    }()
    fileprivate lazy var alertControllerWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        // CONSIDER INSTEAD: https://github.com/dbettermann/DBAlertController/blob/master/Source/DBAlertController.swift
        window.rootViewController = self.alertControllerNav
        return window
    }()
    
    let checkpointDataObject: WebServiceCallCheckpointData!
    
    func show() {
        //appDelegateReference.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        //UIApplication.sharedApplication().windows[1].makeKeyAndVisible()
        //let controller: UIViewController? = UIApplication.sharedApplication().windows[1].rootViewController
        if let defaultWindow = appDelegateReference.window {
            defaultWindow.makeKeyAndVisible()
        }
        // add UIWindow above current UIWindow
        alertControllerWindow.makeKeyAndVisible()
        alertControllerWindow.rootViewController!.present(alert, animated: true, completion: nil)
    }
}
