//
//  DateHelper.swift
//  Rally
//
//  Created by Ian Moses on 2/2/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class dateHelperClass {
    func formatNSDateForDetailsScreenDateButtonTitle(_ rallyObject: Rally)->String {
        print("func formatNSDateForDetailsScreenDateButtonTitle is running...")
        
        /* Details format
         Today/Tomorrow/Thursday at 7:05 PM
         Aug 31, Wed 7 PM 
         
         4 Going
        */

        var detailsText = ""
        if let rallyStartDateTime = rallyObject.rallyStartDateTime {
            let dateFormatter = DateFormatter()
            let calendar = Calendar.current
            let today = Date()
            var formattedDate: String!
            var dateTimeBridgeText = " \u{00B7} "
            
            // Define daysApart
            let startOfToday = calendar.startOfDay(for: today)
            let startOfDateToFormat: Date!
            startOfDateToFormat = calendar.startOfDay(for: rallyStartDateTime as Date)
            let daysInMonthComponents = (calendar as NSCalendar).components(.day, from: startOfToday, to: startOfDateToFormat, options: NSCalendar.Options(rawValue: 0))
            let daysApart = daysInMonthComponents.day
            
            // format date and set dateTimeBridgeText based on daysApart
            switch daysApart {
            case _ where daysApart! < 0: // Day before today. in future versions these buttons should be userinteractionenabled = false
                dateFormatter.dateFormat = "EEE, MMM d"
                formattedDate = dateFormatter.string(from: rallyStartDateTime as Date)
            case _ where daysApart == 0: // Today
                formattedDate = "Today"
                
            case _ where daysApart! < 2: // Tomorrow
                formattedDate = "Tomorrow"
            case _ where daysApart! < 7:  // Friday
                dateFormatter.dateFormat = "EEEE"
                formattedDate = dateFormatter.string(from: rallyStartDateTime as Date)
            default: // Friday, April 1
                // MMM d, E (Aug 31, Wed)
                dateFormatter.dateFormat = "MMM d, E"
                formattedDate = dateFormatter.string(from: rallyStartDateTime as Date)
                dateTimeBridgeText = ""
            }
            
            // format time
            let formattedTime = formatDetailsScreenTimeButtonTitle(rallyStartDateTime as Date)
            
            // update detailsText with date, dateTimeBridgeText, time, and middle dot
            detailsText = formattedDate+dateTimeBridgeText+formattedTime+" \u{00B7}"+"\n"
        }
        
        // format number going
        let numberGoing = rallyObject.numberOfRallyUsersThatAcceptedInvite()
        let numberGoingFormattedText = "\(numberGoing) Going"
        
        // update detailsText with numberGoingFormattedText
        detailsText += numberGoingFormattedText
        
        // return fully formatted detailsText
        return detailsText
    }
    
    func formatNSDateForDetailsScreenDateButtonTitle(_ dateToFormat: Date?)->String { // button icon title, date planned is date planned, doesn't matter what a person's relative time is. So, UTC vs local doesn't apply here
        print("func defineDetailsDateButtonTitle is running...")
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        let today = Date()
        var formattedDate: String!
        /* This allows Friday vs April 1 to kick in after Saturday versus after 6 days from now
        let weekdayNumberRepresentationComponent = NSCalendar.currentCalendar().components(, fromDate: today)
        let weekdayNumberRepresentation = todaysDateComponents.weekday
        let dateToFormatWeekdayNumberRepresentation = weekdayNumberRepresentation+daysApart
        // Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7
        */
        let startOfToday = calendar.startOfDay(for: today)
        let startOfDateToFormat: Date!
        if let date = dateToFormat {
            startOfDateToFormat = calendar.startOfDay(for: date)
        }
        else {
            startOfDateToFormat = Date()
        }
        let daysInMonthComponents = (calendar as NSCalendar).components(.day, from: startOfToday, to: startOfDateToFormat, options: NSCalendar.Options(rawValue: 0))
        let daysApart = daysInMonthComponents.day
        switch daysApart {
        case _ where daysApart! < 0: // Day before today. in future versions these buttons should be userinteractionenabled = false
            dateFormatter.dateFormat = "EEE. MMM d"
            formattedDate = dateFormatter.string(from: dateToFormat!)
        case _ where daysApart == 0: // Today
            formattedDate = "Today"
        case _ where daysApart! < 2: // Tomorrow
            formattedDate = "Tomorrow"
        case _ where daysApart! < 7:  // Friday
            dateFormatter.dateFormat = "EEEE"
            formattedDate = dateFormatter.string(from: dateToFormat!)
        default: // Friday, April 1
            dateFormatter.dateFormat = "EEE. MMM d"
            formattedDate = dateFormatter.string(from: dateToFormat!)
        }
        return formattedDate
    }
    
    func formatDetailsScreenTimeButtonTitle(_ date: Date)->String { // returns local time
        print("func formatDetailsScreenTimeButtonTitle is running...")
        let dateFormatter = DateFormatter()
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "h:mm a"
        let formattedTime = dateFormatter.string(from: date)
        return formattedTime
    }
    
    func utcStringToLocalTimeNSDate(_ utcDateAsString: String)->Date { // returns local time
        print("func utcStringToLocalTimeNSDate is running...")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let utcNSDate = dateFormatter.date(from: utcDateAsString)!
        let localNSDate = getLocalNSDate(utcNSDate)
        return localNSDate
    }

    func utcString()->String { // returns UTC string
        print("func utcString is running...")
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let utcString = dateFormatter.string(from: date)
        return utcString
    }
    
    func localTimeNSDateToUTCString(_ localTimeAsNSDate: Date?)->String { // returns UTC time
        print("func localTimeNSDateToUTCString is running...")
        let calendar = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let timeZoneOffset = NSTimeZone.local.secondsFromGMT()
        var UTCTimeAsNSDate: Date?
        if let _ = localTimeAsNSDate {
            UTCTimeAsNSDate = (calendar as NSCalendar).date(byAdding: .second, value: -timeZoneOffset, to: localTimeAsNSDate!, options: NSCalendar.Options.init(rawValue: 0))
        }
        var createdDateTimeStampAsString: String?
        if let _ = UTCTimeAsNSDate {
            createdDateTimeStampAsString = dateFormatter.string(from: UTCTimeAsNSDate!)
        }
        if let _ = createdDateTimeStampAsString {
            return createdDateTimeStampAsString!
        }
        else {
            // just setting yesterday as default startDateTime for saved groups using save button on invite screen
            let now = Date()
            let yesteday = now.addingTimeInterval(-24 * 3600)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let utcString = dateFormatter.string(from: yesteday)
            return utcString
        }
    }
    
    func updateTimeInRallyStartDateTime(_ rallyObject: Rally, hour: Int, minute: String, timeUnit: String) {
        print("func updateTimeInrallyStartDateTime is running...")
        // "h:mm a" string format
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.second,.minute,.hour,.day,.month,.year], from: rallyObject.rallyStartDateTime! as Date) // can be force unwrapped bc method only called in detailsVC. all rallys in detailsVC have been assigned defaultstartdatetime upon rally object creation
        components.hour = hour
        if timeUnit == "PM" {
            if components.hour != nil {
                components.hour = components.hour!+12
            }
        }
        components.minute = Int(minute)!
        let updatedRallyStartDateTime = calendar.date(from: components)
        rallyObject.rallyStartDateTime = updatedRallyStartDateTime
    }
    
    func updateDateInRallyStartDateTime(_ rallyObject: Rally, selectedDate: Date) {
        print("func updateDateInRallyStartDateTime is running...")
        let calendar = Calendar.current
        // can be force unwrapped bc method only called in detailsVC. all rallys in detailsVC have been assigned defaultstartdatetime upon rally object creation
        var rallyStartDateTimeComponents = (calendar as NSCalendar).components([.second,.minute,.hour,.day,.month,.year], from: rallyObject.rallyStartDateTime! as Date)
        let selectedDateComponents = (calendar as NSCalendar).components([.day,.month,.year], from: selectedDate)
        rallyStartDateTimeComponents.day = selectedDateComponents.day
        rallyStartDateTimeComponents.month = selectedDateComponents.month
        rallyStartDateTimeComponents.year = selectedDateComponents.year
        let updatedRallyStartDateTime = calendar.date(from: rallyStartDateTimeComponents)
        rallyObject.rallyStartDateTime = updatedRallyStartDateTime
    }
    
    func getLocalNSDate(_ date: Date?)->Date { // returns local time
        print("func getLocalNSDate is running...")
        var utcDate = Date()
        if let dateValue = date {
            utcDate = dateValue
        }
        let calendar = Calendar.current
        let timeZoneOffset = NSTimeZone.local.secondsFromGMT()
        let localTimeAsNSDate = (calendar as NSCalendar).date(byAdding: .second, value: timeZoneOffset, to: utcDate, options: NSCalendar.Options.init(rawValue: 0))
        return localTimeAsNSDate!
    }
    
    func timeFromNowUntilDate(_ rallyStartDateTime: Date)->ComparisonResult {
        print("func timeFromNowUntilDate is running...")
        let calendar = Calendar.current
        let now = Date()
        let order = (calendar as NSCalendar).compare(now, to: rallyStartDateTime,
            toUnitGranularity: .second)
        return order
    }
    
    func defaultRallyStartDateTime()->Date { // returns UTC NSDateTime
        print("func ninetyMinutesFromNowRoundedToNearestFifteenMinutes is running...")
        let calendar = Calendar.current
        let currentTimeInUTC = Date()
        let timeNinetyMinutesFromCurrentTime = (calendar as NSCalendar).date(byAdding: .minute, value: 90, to: currentTimeInUTC, options: NSCalendar.Options.init(rawValue: 0))
        var timeNinteyMinutesFromCurrentTimeComponents = (calendar as NSCalendar).components([.second,.minute,.hour,.day,.month,.year], from:timeNinetyMinutesFromCurrentTime!)
        let roundedMinutes: CGFloat = 15.0*floor(CGFloat(timeNinteyMinutesFromCurrentTimeComponents.minute!/15))+round(CGFloat((timeNinteyMinutesFromCurrentTimeComponents.minute!%15)/15)) // possible values [0,15,30,45,60]
        timeNinteyMinutesFromCurrentTimeComponents.minute = Int(roundedMinutes)
        let date = calendar.date(from: timeNinteyMinutesFromCurrentTimeComponents)
        print("ninetyMinutesFromNowRoundedToNearestFifteenMinutes:date:\(String(describing: date))")
        return date!
    }
}
