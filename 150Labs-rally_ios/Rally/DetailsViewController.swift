//
//  DetailsViewController.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Recreated by Ian Moses on 6/23/15 to rename as DetailsViewController from respondToInviteViewController
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import UIKit
import Darwin
import Foundation
import MapKit // locationViewController library
import SwiftyJSON

class DetailsViewController: UIViewController, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, MKMapViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIGestureRecognizerDelegate { // calendar delegates, // location delegates
    
    override var prefersStatusBarHidden : Bool {
        get {
            return self.statusBarDisplayState
        }
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        get {
            return self.statusBarAnimationState
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    var statusBarDisplayState: Bool = false
    var statusBarAnimationState: UIStatusBarAnimation = .none
    
    var editableRallyDescription = UITextView()
    
    var locationDetailView: RallyDetailView! // [2]
    var timeDetailView: RallyDetailView! // [0]
    var dateDetailView: RallyDetailView! // [3]
    
    var sendRallyButton: UIButton!
    var goToMessagingButton: UIButton!
    var containerView = UIView()
    var detailsButtonsParentView: UIView!
    var containerViewHeight: CGFloat!
    var rallyObject: Rally! // data
    var timerHeaderLabel: UILabel!
    var timeRemainingRallyTimer: Timer? // data
    var rallyMembersContainer = UIScrollView()
    var expandCollapseRallyMemberPicturesCustomView: RallyMemberImageAndNameCustomViewClass!
    fileprivate lazy var spaceBetweenFirstSectioningLineAndTopOfFirstRallyMemberPictureRow: CGFloat = {
        let sectioningLineSpacing = ScreenDimension.heightMinusStatusNavigationAndToolbar*22.05/100
        let rallyMemberPictureCustomViewInstance = RallyMemberImageAndNameCustomViewClass()
        let textViewHeight = rallyMemberPictureCustomViewInstance.rallyMemberNameLabel.font!.lineHeight // only allows one line
        let rallyMemberImageAndNameCustomViewHeight = rallyMemberPictureCustomViewInstance.imageViewSideLength+textViewHeight+2 // +2 is the extra padding added between the rally members picture and name label. the extra 'row spacing' is to add space between each row
        let spaceBetweenFirstSectioningLineAndTopOfFirstRallyMemberPictureRow = (sectioningLineSpacing-rallyMemberImageAndNameCustomViewHeight)/2+2.15
        // added 3 pts (even though first image row is off center by doing so, it looks better bc images look to close to the top line otherwise)
        return spaceBetweenFirstSectioningLineAndTopOfFirstRallyMemberPictureRow
    }()
    var rallyDetailsButtonsAndLastTwoSectioningLinesContainer: UIView!
    var emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton: UIView!
    fileprivate lazy var statusBarBackgroundColorView: UIView = {
        let statusBarBackgroundColorView = UIView(frame: CGRect(x: 0,y: 0,width: ScreenDimension.width,height: 20))
        statusBarBackgroundColorView.backgroundColor = MYColor.rallyBlue
        return statusBarBackgroundColorView
    }()
    var timeSinceMethodLastExecuted: Timer?
    var timeSince: Int = 0
    var keyboardHeight: CGFloat!
    
    // calendarViewController variable declarations
    var collectionView: UICollectionView!
    var flowLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    var calendarTitle: UILabel!
    var dateWithinCurrentMonthToBeDisplayedInCalendar: Date!
    let calendar = Calendar.current
    var dateChosen: Date?
    var indexPathOfDateChosen: IndexPath!
    // calendarViewController variable declarations
    
    // timeViewController variable declarations
    var minuteButtons = [UIButton!]()
    var hourButtons = [UIButton!]()
    var clockButtonsParentView = UIView()
    var hourButtonsArray = [UIButton]()
    var minuteButtonsArray = [UIButton]()
    var hourButtonSelected = 6
    var minuteButtonSelected = "00"
    // timeViewController variable declarations
    
    // locationViewController variable declarations
    var searchController: UISearchController!
    var annotation: MKAnnotation!
    var localSearchRequest: MKLocalSearchRequest!
    var localSearch: MKLocalSearch!
    var localSearchResponse: MKLocalSearchResponse!
    var error: NSError!
    var pointAnnotation: MKPointAnnotation?
    var pinAnnotationView: MKPinAnnotationView!
    var mapView: MKMapView!
    var tableView: UITableView!
    var mapItemArray = [MKMapItem]()
    var updateLocationMethodFlag: String?
    var userLocationInstance: CLLocationCoordinate2D? = nil
    // locationViewController variable declarations
    
    // community data
    var community: Community!
    
    override func viewDidLoad() {
        print("class DetailsViewController,func viewDidLoad is running...")
        super.viewDidLoad()
        WebCallStatus.detailsViewControllerLoaded = 1
        editableRallyDescription.delegate = self
        defineContainerView()
        defineDetailsButtonsParentView()
        defineRallyMembersContainer()
        defineRallyTitleTextView()
        defineRallyDetailsButtonsAndLastTwoSectioningLinesContainer()
        defineRallyMembersPictures()
        defineStartTimeResponseCalendarLocationButtons()
        defineInviteSectioningLines()
        updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable(0)
        defineDateChosen()
        let leftButtonSelector: Selector = #selector(DetailsViewController.onMoveToMainScreenButtonTouch(_:))
        let rightMostButtonSelector: Selector = #selector(DetailsViewController.onMoveToInviteListTouch(_:))
        
        navBarClass.navBar(self, navigationBarItemReference: navigationItem, leftButtonImageReference: "back", rightButtonImageReference: nil, rightMostButtonImageReference: "addMember", leftButtonActionReference: leftButtonSelector, rightButtonActionReference: nil, rightMostButtonActionReference: rightMostButtonSelector, titleReference: "Details")
        // NSNotificationCenter.defaultCenter().addObserver(self, selector: "<#selectorString#>", name: UIApplicationDidBecomeActiveNotification, object: nil)
        // define user longitude latitude and assign to rallyObject properties
        // pull user location from phone
        updateLocationMethodFlag = "initializeUserLatitudeLongitudeForRallyObject"
        permissionsHelperClass.userLocation = nil
        permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocalSearchRequestRegion is listening for within detailsVC which updates localSearchRequest userLocation property
    }

    func updateRallyDetails(_ notification: Notification){
        print("func updateRallyDetails is running...")
        // format message from gcm notification as a string to add the message in a new message blurb
        // commented block below not needed code but may need to access these later on so keeping for now
        /*
        guard let aps = userInfo["aps"] as? NSDictionary  else {
            return
        }
        guard let alert = aps["alert"] as? NSDictionary else {
            return
        }
        guard let message = alert["message"] as? NSString else {
            return
        }
        guard let alert = aps["alert"] as? NSString else {
            return
        }
        */
        guard let userInfo = (notification as NSNotification).userInfo else {
            print("func updateRallyDetails: guard let userInfo = notification.userInfo else {")
            return
        }
        guard let payload = userInfo["payload"] as? NSString  else {
            return
        }
        let optionalDataPackage = payload.data(using: String.Encoding.utf8.rawValue)
        guard let unwrappedDataPackage = optionalDataPackage else {
            return
        }
        let swiftyJSONDataPackage = JSON(data:unwrappedDataPackage)
        let rallyID = swiftyJSONDataPackage["RallyID"].int
        guard let unwrappedRallyID = rallyID else {
            print("inside guard else statement")
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            print("func updateRallyDetails is running: dispatch_async is running...")
            if self.rallyObject.rallyID == unwrappedRallyID {
                self.fetchUpdatedRallyUsingIDAndUpdateUI(self.rallyObject, rallyID: unwrappedRallyID)
            }
        }
    }
    
    func fetchUpdatedRallyUsingIDAndUpdateUI(_ rallyObjectReference: Rally?, rallyID: Int) {
        print("func fetchUpdatedRallyUsingIDAndUpdateUI is running...")
        let getUpdatedRallyPackageintent = WebServiceCallIntent.getUpdatedRallyPackage(rallyObjectReference, rallyID)
        let webServiceCallManagerGetUpdatedRallyPackage = WebServiceCallManager(intent: getUpdatedRallyPackageintent)
        webServiceCallManagerGetUpdatedRallyPackage.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally) = returnIntent {
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.rallyObject = updatedRally
                            let detailType = "title" // switch statement includes fallthrough statements so detailType matches first statement and rest will execute
                            // update all details, changed and unchanged, this way there is no need to check which details changed specifically
                            switch detailType {
                            case "title": // update title
                                self.editableRallyDescription.text = self.rallyObject.rallyName // update editableRallyDescription text to new value
                                if self.editableRallyDescription.text.characters.count > 0 {
                                    self.editableRallyDescription.textColor = UIColor.lightGray
                                    if self.editableRallyDescription.text != "Untitled Rally" && self.editableRallyDescription.text != "Untitled Community" {
                                        self.editableRallyDescription.textColor = MYColor.lightBlack
                                    }
                                    else {
                                        if self.rallyObject.rallyerID != MYUser.ID {
                                            self.editableRallyDescription.text = "Untitled"
                                        }
                                    }
                                }
                                self.updateTextViewSize(self.editableRallyDescription.text) // update editableRallyDescription dimensions
                                fallthrough
                            case "numberJoined": // update number joined
                                print("updateRallyDetails:defineActiveRallyDefaultContainerView is triggering/running...")
                                self.defineActiveRallyDefaultContainerView() // this method clear containerView and re-generates view, the way it is written, when regenerated will check rallyObject for number joined again, so as long as rallyObject.rallyUserList[i].didUserAccept is updated, this will re-populate correctly. may have an issue with timer glitching on update, fix updateTimer method if necessary
                            default:
                                break
                            }
                            for subview in self.rallyDetailsButtonsAndLastTwoSectioningLinesContainer.subviews {
                                //print("subview.isMemberOfClass(UIButton):\(subview.isMember(of: UIButton.self))")
                                if subview.tag == 0 {
                                    subview.removeFromSuperview()
                                }
                                if subview.tag == 5098 {
                                    subview.removeFromSuperview()
                                }
                                if subview.tag == 2 {
                                    subview.removeFromSuperview()
                                }
                                if subview.tag == 3 {
                                    subview.removeFromSuperview()
                                }
                            }
                            self.defineStartTimeResponseCalendarLocationButtons()
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        print("class DetailsViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: animated)
        // determine what buttons to display in toolbar
        rallyMembers: for rallyMember in rallyObject.rallyUserList {
            if rallyMember.ID! == MYUser.ID { // get user reference
                if let didUserAcceptValue = rallyMember.didUserAccept { // if user has accepted (doesn't matter if they created rally)
                    if didUserAcceptValue == 2 {
                        if MYUser.ID == rallyObject.rallyerID { // if user created the rally but hasn't accepted, display "Send Rally!" button
                            defineSendRallyButton()
                        }
                        else { // if user was invited to the rally, display 'decline' and 'join' button so they may respond to invite
                            defineDeclineButton()
                            defineJoinButton()
                        }
                    }
                    else { // else user already accepted (1) or (0). 0 should never get to this screen, 1 can exist before sending rally possibly so adding extra clause below for go to messaging vs send for rally creator. otherwise, should always be showing go to messaging for other rally members
                        if rallyObject.isRallyActive == 1 {
                            if didUserAcceptValue == 1 {
                                if WebCallStatus.messagingViewControllerLoaded == 1 { // avoids running logic on startup
                                    // get updated rallyObject from webservice and update rallyObject reference
                                    //updateSavedGroupsInStartNewSection()
                                    let rallyObjectID = rallyObject.rallyID
                                    fetchUpdatedRallyUsingIDAndUpdateUI(rallyObject, rallyID: rallyObjectID!)
                                }
                                defineGoToMessagingButton()
                                break rallyMembers
                            }
                        }
                        else {
                            defineSendRallyButton()
                        }
                    }
                }
                else { // if rally creator hasn't sent rally yet, this may not have a value so need this else clause
                    if MYUser.ID == rallyObject.rallyerID { // if user created the rally but hasn't accepted, display "Send Rally!" button
                        defineSendRallyButton()
                        break rallyMembers
                    }
                    else { // if user was invited to the rally, display 'decline' and 'join' button so they may respond to invite
                        defineDeclineButton()
                        defineJoinButton()
                        break rallyMembers
                    }
                }
            }
        }
        if rallyObject.isRallyActive == 1 { // if rally has been sent and is active, display joined and timer left/"This rally is happening now label" UI elements. method handles logic for which elements specifically to show
            defineActiveRallyDefaultContainerView()
        }
        // display .howFriendsWithoutAppReceiveInvite tutorial blurb if needed
        //TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.howFriendsWithoutAppReceiveInvite, viewControllerView: view, navigationController: self.navigationController!, referenceCard: nil, VCViewAssociatedWithCard: nil, barButtonItem: nil, barButtonItemImageViewFrame: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DetailsViewController.updateRallyDetails(_:)), name: NSNotification.Name(rawValue: "UpdatedRallyPackage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DetailsViewController.appWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        // only need to be subscribed if user searches locations so not necessary to subscribe within viewWillAppear necessarily but much easier to have same persistence as other observers
        NotificationCenter.default.addObserver(self, selector: #selector(DetailsViewController.updateLocalSearchRequestRegion(_:)), name: NSNotification.Name(rawValue: "locationManagerFetchedUserRegion"), object: nil)
    }
    
    func appWillEnterForeground(_ notification:Notification) {
        print("func DetailsViewController:appWillEnterForeground is running...")
        // this assures user doesn't exit app mid-instance and turn off a permission, then re-enter and attempt to use permission when disabled and crash app
        // checks if any permissions are disabled, if so displays permissions access view handler
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        // check to determine if user should get kicked back to homeVC (rally is expired)
        // fetch updated rally info from webservice
        let rallyID = rallyObject.rallyID
        let getUpdatedRallyPackageintent = WebServiceCallIntent.getUpdatedRallyPackage(rallyObject, rallyID!)
        let webServiceCallManagerGetUpdatedRallyPackage = WebServiceCallManager(intent: getUpdatedRallyPackageintent)
        webServiceCallManagerGetUpdatedRallyPackage.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally) = returnIntent {
                        DispatchQueue.main.async(execute: { () -> Void in
                            // check if rallyisActive and if rallystartDateTime has passed
                            self.rallyObject = updatedRally
                            if self.rallyObject.isRallyActive == 1 {
                                print("do nothing, rally has not expired")
                            }
                            else {
                                if let rallyStartDateTime = self.rallyObject.rallyStartDateTime {
                                    let order = dateHelperClass().timeFromNowUntilDate(rallyStartDateTime)
                                    // switch cases for whether or not current time is past start time
                                    switch order {
                                    case .orderedDescending:
                                        // rally started and is no longer active (it has expired), so kick user back to homeVC
                                        self.navigationController!.popToRootViewController(animated: false)
                                    case .orderedAscending:
                                        // rally has not started yet
                                        print("do nothing")
                                    case .orderedSame:
                                        // rally is starting at this very second
                                        print("do nothing")
                                    }
                                }
                            }
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("class DetailsViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(false)
        // badges managed in viewWillDisappear to avoid badge relevant to screen user is on being added. in this case, need to be able to offset badge since user sees new information update live on screen
        // subtract badges associated with new Rallys
        // UIApplication.sharedApplication().applicationIconBadgeNumber -=
        /*if let _ = timeRemainingRallyTimer {
            timeRemainingRallyTimer!.invalidate()
            timeRemainingRallyTimer = nil
        }*/
        if let _ = timeSinceMethodLastExecuted {
            timeSinceMethodLastExecuted!.invalidate()
            timeSinceMethodLastExecuted = nil
        }
        // remove any tutorial blurbs that exist as subviews of navController view
        //TutorialHelper.removeTutorialBlurbsFromViewControllerView(view)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        // believe name is "UpdateRally" but not sure
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UpdatedRallyPackage"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "locationManagerFetchedUserRegion"), object: nil)
        
        // 
        //navigationController?.isNavigationBarHidden = true
        //navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.setToolbarHidden(true, animated: false)
        view.endEditing(true)
    }
    
    func defineRallyDetailsButtonsAndLastTwoSectioningLinesContainer() {
        print("func defineRallyDetailsButtonsAndLastTwoSectioningLinesContainer is running...")
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer = UIView(frame: CGRect(x: 0, y: ScreenDimension.heightMinusStatusNavigationAndToolbar*35.3/100, width: ScreenDimension.width, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*21.2/100))
        detailsButtonsParentView.addSubview(rallyDetailsButtonsAndLastTwoSectioningLinesContainer)
    }
    
    func defineDetailsButtonsParentView() {
        print("func defineDetailsButtonsParentView is running...")
        detailsButtonsParentView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*56.5/100))
        detailsButtonsParentView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        detailsButtonsParentView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        detailsButtonsParentView.layer.shadowOpacity = 1.0
        detailsButtonsParentView.layer.shadowRadius = 1.5
        detailsButtonsParentView.backgroundColor = UIColor.white
        view.addSubview(detailsButtonsParentView)
    }
    
    func defineRallyMembersContainer() {
        print("func defineRallyMembersContainer is running...")
        rallyMembersContainer.frame.origin = CGPoint(x: 0,y: ScreenDimension.heightMinusStatusNavigationAndToolbar*13.25/100+ScreenDimension.heightMinusStatusNavigationAndToolbar*1/1000)
        rallyMembersContainer.frame.size.height = ScreenDimension.heightMinusStatusNavigationAndToolbar*22.05/100
        rallyMembersContainer.center.x = ScreenDimension.width/2
        rallyMembersContainer.delegate = self
        rallyMembersContainer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        detailsButtonsParentView.addSubview(rallyMembersContainer)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("func scrollViewDidScroll is running...")
        let expandCollapseRallyMemberPicturesCustomViewAsOptional: RallyMemberImageAndNameCustomViewClass? = expandCollapseRallyMemberPicturesCustomView
        if let customView = expandCollapseRallyMemberPicturesCustomViewAsOptional {
            let numberOfHidableRallyMembers: Int = rallyObject.rallyUserList.count - 4
            if customView.plusMembersUILabel.text == "+\(numberOfHidableRallyMembers)" {
            }
            else {  // if friends container is expanded, keep expandCollapseRallyMemberPicturesCustomView in same place
                var fixedFrameOne: CGRect = expandCollapseRallyMemberPicturesCustomView.frame
                fixedFrameOne.origin.y = scrollView.contentOffset.y // initial yOffset plus contentOffset
                expandCollapseRallyMemberPicturesCustomView.frame = fixedFrameOne
                let imageViewSideLength = expandCollapseRallyMemberPicturesCustomView.frame.width
                var fixedFrameTwo: CGRect = emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton.frame
                fixedFrameTwo.origin.y = imageViewSideLength+scrollView.contentOffset.y // initial yOffset plus contentOffset
                emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton.frame = fixedFrameTwo
            }
        }
    }
    
    func defineContainerView() {
        print("func defineContainerView is running...")
        containerView.frame = CGRect(x: 0, y: ScreenDimension.heightMinusStatusNavigationAndToolbar*56.5/100, width: ScreenDimension.width, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*43.5/100)
        containerViewHeight = containerView.frame.height
        containerView.backgroundColor = MYColor.lightGrey
        view.addSubview(containerView)
    }
    
    func defineNumberJoinedHeaderLabel() {
        print("func numberJoinedHeaderLabel is running...")
        let numberJoinedHeaderLabel = UILabel()
        numberJoinedHeaderLabel.text = "Joined"
        numberJoinedHeaderLabel.textColor = MYColor.JoinedAndTimeLeftLabelGrey
        numberJoinedHeaderLabel.textAlignment = .center
        numberJoinedHeaderLabel.font = UIFont.boldSystemFont(ofSize: 25.5)
        numberJoinedHeaderLabel.sizeToFit()
        numberJoinedHeaderLabel.center = CGPoint(x: ScreenDimension.width*50/100, y: containerViewHeight*19/100)
        containerView.addSubview(numberJoinedHeaderLabel)
    }
    
    func defineTimerHeaderLabel() {
        print("func defineTimerHeaderLabel is running...")
        timerHeaderLabel = UILabel()
        timerHeaderLabel.textColor = MYColor.JoinedAndTimeLeftLabelGrey
        timerHeaderLabel.textAlignment = .center
        timerHeaderLabel.font = UIFont.boldSystemFont(ofSize: 25.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            timerHeaderLabel.font = UIFont.boldSystemFont(ofSize: 23.5)
        default:
            print("Do nothing...")
        }
        containerView.addSubview(timerHeaderLabel)
    }
    
    func defineNumberThatHaveJoinedRallyLabel() {
        print("func defineNumberThatHaveJoinedRallyLabel is running...")
        let numberThatHaveJoinedRallyLabel = UILabel()
        let numberOfMembersThatHaveJoinedRally = rallyObject.numberOfRallyUsersThatAcceptedInvite()
        
        numberThatHaveJoinedRallyLabel.text = "\(numberOfMembersThatHaveJoinedRally)"
        numberThatHaveJoinedRallyLabel.textColor = MYColor.JoinedAndTimeLeftViewGrey
        numberThatHaveJoinedRallyLabel.textAlignment = .center
        numberThatHaveJoinedRallyLabel.font = UIFont.boldSystemFont(ofSize: 51.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            numberThatHaveJoinedRallyLabel.font = UIFont.boldSystemFont(ofSize: 41.5)
        default:
            print("Do nothing...")
        }
        numberThatHaveJoinedRallyLabel.sizeToFit()
        numberThatHaveJoinedRallyLabel.center = CGPoint(x: ScreenDimension.width*44/100, y: containerViewHeight*46/100)
        containerView.addSubview(numberThatHaveJoinedRallyLabel)
        
        let solidus = UIView()
        solidus.frame.size = CGSize(width: ScreenDimension.width*24.25/100,height: containerViewHeight*2.6/100)
        solidus.backgroundColor = MYColor.JoinedAndTimeLeftViewGrey
        solidus.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/4*(-1)))
        solidus.center = CGPoint(x: ScreenDimension.width*50/100,y: containerViewHeight*56.5/100)
        containerView.addSubview(solidus)
        
        
        let solidusTopCutout = UIView(frame: CGRect(x: ScreenDimension.width*55.5/100,y: containerViewHeight*41.5/100,width: ScreenDimension.width*5/100,height: containerViewHeight*3/100))
        solidusTopCutout.backgroundColor = MYColor.lightGrey
        containerView.addSubview(solidusTopCutout)
        
        let solidusBottomCutout = UIView(frame: CGRect(x: ScreenDimension.width*40.5/100,y: containerViewHeight*68.5/100,width: ScreenDimension.width*5/100,height: containerViewHeight*3/100))
        solidusBottomCutout.backgroundColor = MYColor.lightGrey
        containerView.addSubview(solidusBottomCutout)
        
        
        let totalNumberOfPeopleInRallyLabel = UILabel()
        totalNumberOfPeopleInRallyLabel.text = "\(rallyObject.rallyUserList.count)"
        totalNumberOfPeopleInRallyLabel.textColor = MYColor.JoinedAndTimeLeftViewGrey
        totalNumberOfPeopleInRallyLabel.textAlignment = .center
        totalNumberOfPeopleInRallyLabel.font = UIFont.boldSystemFont(ofSize: 51.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            totalNumberOfPeopleInRallyLabel.font = UIFont.boldSystemFont(ofSize: 41.5)
        default:
            print("Do nothing...")
        }
        totalNumberOfPeopleInRallyLabel.sizeToFit()
        totalNumberOfPeopleInRallyLabel.center = CGPoint(x: ScreenDimension.width*56/100, y: containerViewHeight*67/100)
        containerView.addSubview(totalNumberOfPeopleInRallyLabel)
    }
    /*
    func defineTimer() {
        print("func defineTimer is running...")
        let timerButton = UIButton()
        timerButton.tag = 572
        timerButton.frame.size = CGSizeMake(ScreenDimension.screenWidth*33.5/100,ScreenDimension.screenWidth*33.5/100)
        timerButton.center = CGPointMake(ScreenDimension.screenWidth*67.1/100, containerViewHeight*56.5/100)
        timerButton.userInteractionEnabled = false
        // http://stackoverflow.com/questions/19274789/how-can-i-change-image-tintcolor-in-ios-and-watchkit
        timerButton.setImage(UIImage(named:"timer")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
        timerButton.imageView!.tintColor = MYColor.JoinedAndTimeLeftViewGrey
        let timerButtonLabel = UILabel()
        timerButtonLabel.tag = 349
        timerButtonLabel.textAlignment = .Center
        timerButtonLabel.font = UIFont.boldSystemFontOfSize(33.5)
        timerButtonLabel.textColor = MYColor.JoinedAndTimeLeftViewGrey
        timerButton.addSubview(timerButtonLabel)
        containerView.addSubview(timerButton)
        //updateTimerLabelValueHeaderAndHandleResponseTimeExpirationUserCases() // set timer label value
    }
    */
    func updateTimer(_ timer: Timer) { // convert format to guard statement
        print("func updateTimer is running...")
        //updateTimerLabelValueHeaderAndHandleResponseTimeExpirationUserCases()
    }
    /*
    func updateTimerLabelValueHeaderAndHandleResponseTimeExpirationUserCases() { // separate function from updateTimer allows method to be used independent of timer firing
        print("func updateTimerLabelValueHeaderAndHandleResponseTimeExpirationUserCases is running...")
        let createdDateTimeStamp: String? = rallyObject.CreatedDateTimeStamp
        if let _ = createdDateTimeStamp {
            var timerButtonLabelText: String! // timerButtonLabelText is used instead of setting directly to timerButtonLabel text bc haven't unwrapped label yet and better to unwrap at same time text value is set and label is re-defined
            let rallyCreatedDateTimeStamp = dateHelperClass().utcStringToLocalTimeNSDate(rallyObject.CreatedDateTimeStamp)
            let responseTimeExpiration = calendar.dateByAddingUnit(.Minute, value: rallyObject.RallyResponseTime, toDate: rallyCreatedDateTimeStamp, options: NSCalendarOptions.init(rawValue: 0))
            let order = dateHelperClass().timeFromNowUntilDate(responseTimeExpiration!)
            // switch cases for whether or not current time is past response time
            switch order {
            case .OrderedDescending:
                // response time has expired
                // Calculate updated timer label time
                timerButtonLabelText = timeDifferenceBetweenCurrentTimeandTimeOfInterest(.OrderedDescending) // time returned is difference between current time to rally start time
            case .OrderedAscending:
                // response time has not expired, just calculate updated timer label time
                timerButtonLabelText = timeDifferenceBetweenCurrentTimeandTimeOfInterest(.OrderedAscending)
            case .OrderedSame:
                // response time has expired (0 seconds left to respond)
                // check if rally tipped, if not, inform user and return to home screen
                if rallyObject.numberOfRallyUsersThatAcceptedInvite() < 3 {
                    displayMessageDialogBox("The time to respond to this Rally has passed. Not enough people joined so no one will be getting together :(")
                    self.navigationController!.popToRootViewControllerAnimated(false)
                }
                // rally tipped, and response time has passed. check whether user accepted and respond accordingly
                rallyMembers: for rallyMember in rallyObject.RallyUserList {
                    if rallyMember.UserID! == User.UserID {
                        if let _ = rallyMember.DidUserAccept {
                        }
                        else { // if user hasn't accepted rally, check whether the user created the rally or was invited to the rally
                            if rallyObject.numberOfRallyUsersThatAcceptedInvite() > 2 {
                                displayMessageDialogBox("The time to respond to this Rally has passed. Your friends decided to get together. You might want to text someone if you want in ;)")
                            }
                            self.navigationController!.popToRootViewControllerAnimated(false)
                        }
                    }
                }
                // Calculate updated timer label time
                timerButtonLabelText = timeDifferenceBetweenCurrentTimeandTimeOfInterest(.OrderedAscending)
                // change timer header label from 'Time to Respond' to 'Starting in'
                timerHeaderLabel.text = "Starting in"
                timerHeaderLabel.sizeToFit()
                timerHeaderLabel.center = CGPointMake(ScreenDimension.screenWidth*67.1/100, containerViewHeight*19/100)
            }
            if let timerButton = containerView.viewWithTag(572) as? UIButton {
                if let timerButtonLabel = timerButton.viewWithTag(349) as? UILabel {
                    timerButtonLabel.text = timerButtonLabelText
                    timerButtonLabel.sizeToFit()
                    timerButtonLabel.center = CGPointMake(timerButton.frame.width/2,timerButton.frame.height/2)
                    timerButtonLabel.frame.origin.y += containerViewHeight*3.25/100
                    // timerButtonLabel.frame.origin.y += (timerButton.frame.size.height-timerButton.frame.size.width) not sure why this line doesn't work, shouldn't add a constant value like 14...the timer icon isn't round either, all needs fixing
                    // stop timer once rally has started, static "started" label is displayed within timer going forward so no need to continually update timer label
                    if timerButtonLabel.text! == "Started" {
                        if let _ = timeRemainingRallyTimer {
                            timeRemainingRallyTimer!.invalidate()
                            timeRemainingRallyTimer = nil
                        }
                    }
                }
            }
        }
    }
    */
    /*
    func timeDifferenceBetweenCurrentTimeandTimeOfInterest(comparisonResult: NSComparisonResult)->String {
        print("func timeDifferenceBetweenCurrentTimeandTimeOfInterest is running...")
        let currentDateTime = NSDate()
        var dateTimeOfInterest: NSDate!
        var timeDifferenceBetweenCurrentTimeandTimeOfInterest: NSDateComponents!
        switch comparisonResult {
        case .OrderedAscending:
            // time of interest is response time
            let rallyCreatedDateTimeStamp = dateHelperClass().utcStringToLocalTimeNSDate(rallyObject.CreatedDateTimeStamp)
            dateTimeOfInterest = calendar.dateByAddingUnit(.Minute, value: rallyObject.RallyResponseTime, toDate: rallyCreatedDateTimeStamp, options: NSCalendarOptions.init(rawValue: 0))
        case.OrderedDescending:
            // time of interest is rally start time
            dateTimeOfInterest = rallyObject.rallyStartDateTime
        default:
            break
        }
        timeDifferenceBetweenCurrentTimeandTimeOfInterest = calendar.components([.Second,.Minute,.Hour,.Day,.Month,.Year], fromDate: currentDateTime, toDate: dateTimeOfInterest, options: NSCalendarOptions(rawValue: 0))
        if timeDifferenceBetweenCurrentTimeandTimeOfInterest.year > 0 {
            if timeDifferenceBetweenCurrentTimeandTimeOfInterest.month > 6 {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.year+1)+"y"
            }
            else {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.year)+"y"
            }
        }
        if timeDifferenceBetweenCurrentTimeandTimeOfInterest.month > 0 {
            if timeDifferenceBetweenCurrentTimeandTimeOfInterest.day > 15 {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.month+1)+"m"
            }
            else {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.month)+"m"
            }
        }
        if timeDifferenceBetweenCurrentTimeandTimeOfInterest.day > 0 {
            if timeDifferenceBetweenCurrentTimeandTimeOfInterest.hour > 0 || timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute > 0 {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.day+1)+"d"
            }
            else {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.day)+"d"
            }
        }
        else if timeDifferenceBetweenCurrentTimeandTimeOfInterest.hour > 0 {
            if timeDifferenceBetweenCurrentTimeandTimeOfInterest.hour == 1 {
                if timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute > 30 {
                    print("this is when 2h text label is determined in logic path")
                    return "2h" // somewhere between 120 and 100 minutes, don't want to put 3 digit time
                }
                else {
                    return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute)+"m"
                }
            }
            else {
                if timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute > 0 {
                    return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.hour+1)+"h"
                }
                else {
                    return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.hour)+"h"
                }
            }
        }
        else if timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute > 0 {
            if timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute > 0 {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute+1)+"m"
            }
            else {
                return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.minute)+"m"
            }
        }
        else if timeDifferenceBetweenCurrentTimeandTimeOfInterest.second > 0 {
            return String(timeDifferenceBetweenCurrentTimeandTimeOfInterest.second)+"s"
        }
        else { // time remaining is 0
            return "Started"
        }
    }
    */
    
    func displayMessageDialogBox(_ message: String) {
        print("func displayMessageDialogBox is running...")
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil) // Should this be done using navigationController?
        let delay = 0.5 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) { () -> Void in
            self.dismissAlertController()
        }
    }
    
    func dismissAlertController() {
        print("func dismissPopover is running...")
        self.dismiss(animated: true, completion: nil)
    }
    
    func defineRallyTitleTextView() {
        print("func defineRallyTitleTextView is running...")
        editableRallyDescription.text = rallyObject.rallyName
        self.editableRallyDescription.textColor = UIColor.lightGray
        if self.editableRallyDescription.text != "Untitled Rally" && self.editableRallyDescription.text != "Untitled Community" {
            self.editableRallyDescription.textColor = MYColor.lightBlack
        }
        else {
            if rallyObject.rallyerID != MYUser.ID {
                self.editableRallyDescription.text = "Untitled Rally"
            }
        }
        editableRallyDescription.tag = 419
        editableRallyDescription.font = UIFont.systemFont(ofSize: 25.25)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            editableRallyDescription.font = UIFont.systemFont(ofSize: 21.25)
        default:
            print("Do nothing...")
        }
        let size = editableRallyDescription.sizeThatFits(CGSize(width: ScreenDimension.width*78/100,  height: CGFloat(Float.greatestFiniteMagnitude)))
        editableRallyDescription.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        editableRallyDescription.center.y = ScreenDimension.heightMinusStatusNavigationAndToolbar*6.75/100
        editableRallyDescription.center.x = ScreenDimension.width/2
        editableRallyDescription.textAlignment = .center
        editableRallyDescription.isScrollEnabled = false
        if MYUser.ID != rallyObject.rallyerID {
            editableRallyDescription.isUserInteractionEnabled = false
        }
        detailsButtonsParentView.addSubview(editableRallyDescription)
    }
    
    func defineInviteSectioningLines() {
        print("func defineInviteSectioningLines is running...")
        let firstSectioningLine = UIView(frame: CGRect(x: ScreenDimension.width*3.95/32, y: ScreenDimension.heightMinusStatusNavigationAndToolbar*13.25/100, width: ScreenDimension.width-ScreenDimension.width*3.95/16, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*1/1000))
        firstSectioningLine.center.x = ScreenDimension.width/2
        firstSectioningLine.backgroundColor = MYColor.secondVeryDarkGrey
        detailsButtonsParentView.addSubview(firstSectioningLine)
        detailsButtonsParentView.bringSubview(toFront: firstSectioningLine)
        let secondSectioningLine = UIView(frame: CGRect(x: ScreenDimension.width*1.935/84, y: 0, width: ScreenDimension.width-ScreenDimension.width*1.935/42, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*1/1000))
        secondSectioningLine.backgroundColor = MYColor.secondVeryDarkGrey
        secondSectioningLine.tag = 307
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(secondSectioningLine)
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.bringSubview(toFront: secondSectioningLine)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            let thirdSectioningLine = UIView(frame: CGRect(x: ScreenDimension.width*1.935/84, y: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height/2, width: ScreenDimension.width-ScreenDimension.width*1.935/42, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*2/1000))
            thirdSectioningLine.backgroundColor = MYColor.secondVeryDarkGrey
            thirdSectioningLine.tag = 308
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(thirdSectioningLine)
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.bringSubview(toFront: thirdSectioningLine)
        case .iPhone6, .iPhone6S, .iPhone6plus, .iPhone6Splus, .iPhone7, .iPhone7plus:
            let thirdSectioningLine = UIView(frame: CGRect(x: ScreenDimension.width*1.935/84, y: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height/2, width: ScreenDimension.width-ScreenDimension.width*1.935/42, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*1/1000))
            thirdSectioningLine.backgroundColor = MYColor.secondVeryDarkGrey
            thirdSectioningLine.tag = 308
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(thirdSectioningLine)
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.bringSubview(toFront: thirdSectioningLine)
        default:
            print("Do nothing...")
        }
    }
    
    func defineRallyMembersPictures() {
        print("func defineRallyMembersPictures is running...")
        // remove and insert rally creator at front of rallyUserList array
        for (index, rallyMember) in rallyObject.rallyUserList.enumerated() {
            if rallyMember.ID == rallyObject.rallyerID {
                rallyObject.rallyUserList.remove(at: index)
                rallyObject.rallyUserList.insert(rallyMember, at: 0)
                break
            }
        }
        // create pictures
        for (index, rallyMember) in rallyObject.rallyUserList.enumerated() {
            let rallyMemberImageAndNameCustomView = RallyMemberImageAndNameCustomViewClass()
            if index == 0 {
                rallyMemberImageAndNameCustomView.rallyMemberImageView.layer.borderColor = MYColor.rallyBlue.cgColor
                rallyMemberImageAndNameCustomView.rallyMemberImageView.layer.borderWidth = 4.25
            }
            var numberOfRallyMemberPicturesInFirstRow = 4
            if rallyObject.rallyUserList.count == 5 {
                numberOfRallyMemberPicturesInFirstRow = 5
            }
            if index < numberOfRallyMemberPicturesInFirstRow {
                rallyMemberImageAndNameCustomView.frame = CGRect(x: rallyMemberImageAndNameCustomView.xOffSet(index),y: rallyMemberImageAndNameCustomView.yOffSet(index),width: rallyMemberImageAndNameCustomView.imageViewSideLength,height: rallyMemberImageAndNameCustomView.imageViewSideLength)
                rallyMemberImageAndNameCustomView.tag = index
            }
            else { // offset rally member pictures after blue button an extra index to skip putting a rally member in that row/column
                rallyMemberImageAndNameCustomView.frame = CGRect(x: rallyMemberImageAndNameCustomView.xOffSet(index+1),y: rallyMemberImageAndNameCustomView.yOffSet(index+1),width: rallyMemberImageAndNameCustomView.imageViewSideLength,height: rallyMemberImageAndNameCustomView.imageViewSideLength)
                rallyMemberImageAndNameCustomView.tag = index+1
                rallyMemberImageAndNameCustomView.isHidden = true
            }
            rallyMemberImageAndNameCustomView.rallyMemberImageView.image = rallyMember.getProfileImage()
            if let value = rallyMember.firstName {
                rallyMemberImageAndNameCustomView.rallyMemberNameLabel.text = value
            }
            let textViewHeight = rallyMemberImageAndNameCustomView.rallyMemberNameLabel.font!.lineHeight // only allows one line
            rallyMemberImageAndNameCustomView.rallyMemberNameLabel.frame = CGRect(x: 0,y: rallyMemberImageAndNameCustomView.imageViewSideLength+2,width: rallyMemberImageAndNameCustomView.imageViewSideLength,height: textViewHeight)
            let rowSpacing: CGFloat = 11.0 // adds space in between the rows
            rallyMemberImageAndNameCustomView.frame.size.height += rallyMemberImageAndNameCustomView.rallyMemberNameLabel.frame.height+2+rowSpacing // +2 is the extra padding added between the rally members picture and name label. the extra 'row spacing' is to add space between each row
            if index == 4 {
                if rallyObject.rallyUserList.count != 5 {
                    expandCollapseRallyMemberPicturesCustomView = RallyMemberImageAndNameCustomViewClass()
                    expandCollapseRallyMemberPicturesCustomView.frame = CGRect(x: expandCollapseRallyMemberPicturesCustomView.xOffSet(index),y: 0,width: expandCollapseRallyMemberPicturesCustomView.imageViewSideLength,height: expandCollapseRallyMemberPicturesCustomView.imageViewSideLength)
                    expandCollapseRallyMemberPicturesCustomView.tag = index
                    let numberOfHidableRallyMembers: Int = rallyObject.rallyUserList.count - index
                    expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.text = "+\(numberOfHidableRallyMembers)"
                    expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.tag = 822
                    let onShowMoreRallyMembersButtonTouch = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.onShowMoreRallyMembersButtonTouch(_:)))
                    expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.addGestureRecognizer(onShowMoreRallyMembersButtonTouch)
                    rallyMembersContainer.addSubview(expandCollapseRallyMemberPicturesCustomView)
                    // only done so expandCollapseRallyMemberPicturesCustomView is the same size as all rallyMemberPictures for calculating layout in other funcs since expandCollapseRallyMemberPicturesCustomView is used for convenience as it is a class variable
                    expandCollapseRallyMemberPicturesCustomView.frame.size.height = rallyMemberImageAndNameCustomView.frame.size.height
                }
            }
            rallyMembersContainer.addSubview(rallyMemberImageAndNameCustomView)
        }
        rallyMembersContainer.frame.origin.y += spaceBetweenFirstSectioningLineAndTopOfFirstRallyMemberPictureRow
        // added 5/2 pts (even though first image row is off center by doing so, it looks better bc images look too close to the top line otherwise)
        rallyMembersContainer.center.x = ScreenDimension.width/2
        defineRallyMembersContainerContentSize(.collapsed)
        if rallyObject.rallyUserList.count > 5 {
            rallyMembersContainer.bringSubview(toFront: self.expandCollapseRallyMemberPicturesCustomView) // keeps expand/collapse button in front of rally member pictures when scrolling
            defineEmptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton()
        }
    }
    
    func defineEmptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton() {
        print("func defineEmptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton is running...")
        let imageViewSideLength = expandCollapseRallyMemberPicturesCustomView.frame.width
        emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton = UIView(frame: CGRect(x: expandCollapseRallyMemberPicturesCustomView.frame.origin.x,y: imageViewSideLength,width: imageViewSideLength,height: (expandCollapseRallyMemberPicturesCustomView.frame.size.height-imageViewSideLength)*65/100))
        emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton.backgroundColor = UIColor.white
        rallyMembersContainer.addSubview(emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton)
    }
    
    func defineRallyMembersContainerContentSize(_ rallyMemberPictureViewCaseValue: CollapsedExpandedState) {
        print("func defineRallyMembersContainerContentSize is running...")
        var rallyMembersContainerWidthMultiplier: CGFloat = 5
        if rallyObject.rallyUserList.count < 5 {
            rallyMembersContainerWidthMultiplier = CGFloat(rallyObject.rallyUserList.count)
        }
        let rallyMemberPictureCustomViewInstance = RallyMemberImageAndNameCustomViewClass()
        rallyMembersContainer.frame.size.width = rallyMemberPictureCustomViewInstance.imageViewSideLength*rallyMembersContainerWidthMultiplier+rallyMemberPictureCustomViewInstance.imageViewSpacing*(rallyMembersContainerWidthMultiplier-1)+rallyMemberPictureCustomViewInstance.xOffSet(0)*2
        switch rallyMemberPictureViewCaseValue {
        case .collapsed:
            // set height for single row
            let rallyMemberPictureCustomViewInstance = RallyMemberImageAndNameCustomViewClass()
            let textViewHeight = rallyMemberPictureCustomViewInstance.rallyMemberNameLabel.font!.lineHeight // only allows one line
            let rallyMemberImageAndNameCustomViewHeight = rallyMemberPictureCustomViewInstance.imageViewSideLength+textViewHeight+2 // +2 is the extra padding added between the rally members picture and name label. the extra 'row spacing' is to add space between each row
            rallyMembersContainer.frame.size.height = rallyMemberImageAndNameCustomViewHeight
            // setting container height equal to content size, there is no extra content to scroll. this effectively locks the uiscrollview
            rallyMembersContainer.contentSize = rallyMembersContainer.bounds.size
            rallyMembersContainer.center.x = ScreenDimension.width/2
        case .expanded:
            // set the height up to a max of three rows depending on number of rally members. expand contentsize to full size
            let numberOfRallyMembers: Double = Double(rallyObject.rallyUserList.count)
            let numberOfRowsFraction: Double = (numberOfRallyMembers+1.0)/5.0
            let numberOfRows: CGFloat = CGFloat(ceil(numberOfRowsFraction))
            // set rally member pictures scrollview contentsize to fit all rally member pictures
            rallyMembersContainer.contentSize.height = (expandCollapseRallyMemberPicturesCustomView.frame.height)*numberOfRows
        }
    }
    
    func numberOfRallyMembersPicturesRows(_ numberOfRallyMembers: Double)->CGFloat {
        print("func numberOfRallyMembersPicturesRows is running...")
        var numberOfRows: CGFloat!
        switch numberOfRallyMembers {
        case  _ where numberOfRallyMembers < 6:
            numberOfRows = 1
        case  _ where numberOfRallyMembers < 10:
            numberOfRows = 2
        default:
            numberOfRows = 3
        }
        return numberOfRows
    }
    
    func defineDetailsButtonsParentViewAndReleventSubviewsPositionsForDynamicRallyMemberPicturesContainerResizing(_ rallyMemberPictureViewCaseValue: CollapsedExpandedState) {
        print("func defineDetailsButtonsParentViewAndReleventSubviewsPositionsForDynamicRallyMemberPicturesContainerResizing is running...")
        let numberOfRallyMembers: Double = Double(rallyObject.rallyUserList.count)
        let numberOfRows = numberOfRallyMembersPicturesRows(numberOfRallyMembers)
        let verticalSpaceToAddOrSubtractForSubviews = (expandCollapseRallyMemberPicturesCustomView.frame.height)*(numberOfRows-1)
        switch rallyMemberPictureViewCaseValue {
        case .collapsed:
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.origin.y = ScreenDimension.heightMinusStatusNavigationAndToolbar*35.3/100
            detailsButtonsParentView.frame.size.height = ScreenDimension.heightMinusStatusNavigationAndToolbar*56.5/100
            rallyMembersContainer.frame.size.height = expandCollapseRallyMemberPicturesCustomView.frame.height
        case .expanded:
            rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.origin.y += verticalSpaceToAddOrSubtractForSubviews
            detailsButtonsParentView.frame.size.height = detailsButtonsParentView.frame.size.height+verticalSpaceToAddOrSubtractForSubviews
            rallyMembersContainer.frame.size.height = (expandCollapseRallyMemberPicturesCustomView.frame.height)*numberOfRows
        }
    }
    
    func onShowMoreRallyMembersButtonTouch(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func onShowMoreRallyMembersButtonTouch is running...")
        let parentView = sender.view?.superview as! RallyMemberImageAndNameCustomViewClass
        let numberOfHidableRallyMembers: Int = rallyObject.rallyUserList.count - 4
        print("parentView.rallyMemberNameTextView.text:\(String(describing: parentView.rallyMemberNameLabel.text))")
        if editableRallyDescription.isFirstResponder {
            dismissKeyboard()
        }
        if parentView.plusMembersUILabel.text == "+\(numberOfHidableRallyMembers)" {
            removeContainerViewSubviews()
            parentView.rallyMemberImageView.isHidden = false
            parentView.plusMembersUILabel.text = ""
            parentView.rallyMemberImageView.frame = CGRect(x: parentView.imageViewSideLength*15/100,y: parentView.imageViewSideLength*15/100,width: parentView.imageViewSideLength*70/100,height: parentView.imageViewSideLength*70/100)
            parentView.rallyMemberImageView.image = UIImage(named:"remove")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            parentView.bringSubview(toFront: parentView.rallyMemberImageView)
            for subview in rallyMembersContainer.subviews { // -1 makes i the cards index value within rallyCardArray, displays all rally cards
                if subview.tag > 4 {
                    subview.isHidden = false
                }
            }
            defineRallyMembersContainerContentSize(.expanded)
            defineDetailsButtonsParentViewAndReleventSubviewsPositionsForDynamicRallyMemberPicturesContainerResizing(.expanded)
        }
        else {
            // in case where expandCollapseRallyMemberPicturesCustomView is pressed and friends pictures container view is not in default position (it is scrolled), expandCollapseRallyMemberPicturesCustomView and emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton y-origin is non-zero in both cases so must be reset back to zero upon collapsing
            parentView.frame.origin.y = 0
            let imageViewSideLength = parentView.frame.width
            emptyViewToHideRightMostColumnPicturesBeneathExpandCollapseButton.frame.origin.y = imageViewSideLength
            parentView.rallyMemberImageView.isHidden = true
            parentView.plusMembersUILabel.font = UIFont.systemFont(ofSize: 30)
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                parentView.plusMembersUILabel.font = UIFont.systemFont(ofSize: 26)
            default:
                print("Do nothing...")
            }
            parentView.plusMembersUILabel.text = "+\(numberOfHidableRallyMembers)"
            for subview in rallyMembersContainer.subviews { // -1 makes i the cards index value within rallyCardArray, displays all rally cards
                if subview.tag > 4 {
                    subview.isHidden = true
                }
            }
            defineRallyMembersContainerContentSize(.collapsed)
            defineDetailsButtonsParentViewAndReleventSubviewsPositionsForDynamicRallyMemberPicturesContainerResizing(.collapsed)
            if rallyObject.isRallyActive == 1 { // if rally is active redisplay number joined and time to respond subviews within containerView
                defineActiveRallyDefaultContainerView()
            }
        }
    }
    
    func defineStartTimeResponseCalendarLocationButtons() {
        print("func defineStartTimeResponseCalendarLocationButtons is running...")
        
        // reference rallyDetailsButtonsAndLastTwoSectioningLinesContainer height w shorter name
        let superviewHeight = rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height
        
        // define timeDetailView
        timeDetailView = RallyDetailView(detailType: .time, rally: rallyObject, superviewHeight: superviewHeight)
        // set gesture recognizer
        let timeDetailViewTap: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(DetailsViewController.switchContainerView(_:)))
        timeDetailViewTap.minimumPressDuration = 0.0
        timeDetailView.addGestureRecognizer(timeDetailViewTap)
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(timeDetailView)
        
        // define locationDetailView
        locationDetailView = RallyDetailView(detailType: .location, rally: rallyObject, superviewHeight: superviewHeight)
        // set gesture recognizer
        let locationDetailViewTap: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(DetailsViewController.switchContainerView(_:)))
        locationDetailViewTap.minimumPressDuration = 0.0
        locationDetailView.addGestureRecognizer(locationDetailViewTap)
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(locationDetailView)
        
        // define dateDetailView
        dateDetailView = RallyDetailView(detailType: .date, rally: rallyObject, superviewHeight: superviewHeight)
        // set gesture recognizer
        let dateDetailViewTap: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(DetailsViewController.switchContainerView(_:)))
        dateDetailViewTap.minimumPressDuration = 0.0
        dateDetailView.addGestureRecognizer(dateDetailViewTap)
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(dateDetailView)
        
        self.updateLocationButtonSubviewLayoutWhenActive()
        if rallyObject.rallyerID == MYUser.ID && rallyObject.isRallyActive == 1 {
            self.rallyerEditRallyAfterSendingButton()
        }
    }
    
    func switchContainerView(_ recognizer: UILongPressGestureRecognizer) {
        print("func switchContainerView is running...")
        let detailView = recognizer.view! as! RallyDetailView
        let location = recognizer.location(in: recognizer.view)
        let locationRelativeToSuperview = detailView.convert(location, to:rallyDetailsButtonsAndLastTwoSectioningLinesContainer)
        
        if recognizer.state == .began {
            print("UIGestureRecognizerState.began")
            // set subviews to highlight color
            detailView.imageView.tintColor = MYColor.extremelyDarkGrey
            detailView.textLabel.textColor = MYColor.midDarkGrey
        }
        if detailView.frame.contains(locationRelativeToSuperview) {
            // press happened. Do stuff!
            if recognizer.state == .ended && detailView.imageView.tintColor != MYColor.veryDarkGrey {
                print("UIGestureRecognizerState.ended")
                detailView.imageView.tintColor = MYColor.veryDarkGrey
                detailView.textLabel.textColor = MYColor.detailButtonTitleGrey
                if editableRallyDescription.isFirstResponder {
                    dismissKeyboard()
                }
                // check if detailsButtonsParentView is sized correctly. if not, collapse rally members pictures view and resize detailsButtonsParentView so containerView can properly display ui element displaying as a result of button press
                // alternatively, could check if expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.text == "+\(numberOfHidableRallyMembers)"
                if detailsButtonsParentView.frame.height != ScreenDimension.heightMinusStatusNavigationAndToolbar*56.5/100 {
                    expandCollapseRallyMemberPicturesCustomView.rallyMemberImageView.isHidden = true
                    expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.font = UIFont.systemFont(ofSize: 30)
                    switch UIDevice().type {
                    case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                        print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                        expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.font = UIFont.systemFont(ofSize: 26)
                    default:
                        print("Do nothing...")
                    }
                    let numberOfHidableRallyMembers: Int = rallyObject.rallyUserList.count - 4
                    expandCollapseRallyMemberPicturesCustomView.plusMembersUILabel.text = "+\(numberOfHidableRallyMembers)"
                    for subview in rallyMembersContainer.subviews { // -1 makes i the cards index value within rallyCardArray, displays all rally cards
                        if subview.tag > 4 {
                            subview.isHidden = true
                        }
                    }
                    defineRallyMembersContainerContentSize(.collapsed)
                    defineDetailsButtonsParentViewAndReleventSubviewsPositionsForDynamicRallyMemberPicturesContainerResizing(.collapsed)
                }
                switch (recognizer.view as! RallyDetailView).tag {
                case 0: // startTime
                    removeContainerViewSubviews() // cannot be atop all cases bc it isn't removed for location button when touched when rally is active
                    defineClockButtons()
                    let nowButton = defineNowButton()
                    containerView.addSubview(nowButton)
                case 1: // responseTime
                    removeContainerViewSubviews() // cannot be atop all cases bc it isn't removed for location button when touched when rally is active
                //defineResponseTimeButtons()
                case 2: // location
                    if Reachability.isConnectedToNetwork() {
                        print("Internet connection OK")
                        if rallyObject.isRallyActive == 1 {
                            displayMapsApp()
                        }
                        else {
                            displayContainerViewMapViewSubview()
                        }
                    }
                    else {
                        print("Internet connection FAILED")
                        // initialize alertview data model
                        let errorData = WebServiceCallCheckpointData(customValidationType: .NetworkReachability)
                        // initialize alertview
                        let alertView = CustomUIWindowAlertview(dataObject: errorData)
                        // display alertview
                        alertView.show()
                    }
                default: // calendar
                    // made this calendar since most people will use this for day of, so least used. Also, more efficient code than including as extra case
                    removeContainerViewSubviews() // cannot be atop all cases bc it isn't removed for location button when touched when rally is active
                    updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable(0)
                    // collectionView cell width determined by collectionView.frame.width/7. need collectionWidth to be divisible by 7 to be an integer so that each cell is exactly the same width and the cumulitive cell width adds up precisely to the collectionView width to avoid gaps between cells. these gaps show the background color (lightGrey); they look like grey vertical lines separating some of the weekdays
                    let collectionViewWidth = round(Double(ScreenDimension.width-containerViewHeight*5.25/100*2))
                    let collectionViewWidthDivisibleBySeven = CGFloat(round(collectionViewWidth/7)*7)
                    let collectionViewXOffset = (ScreenDimension.width-collectionViewWidthDivisibleBySeven)/2
                    collectionView = UICollectionView(frame: CGRect(x: collectionViewXOffset, y: containerViewHeight*5/100, width: collectionViewWidthDivisibleBySeven, height: containerViewHeight*90.25/100),collectionViewLayout: flowLayout)
                    collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "collectionCell")
                    collectionView.delegate = self
                    collectionView.dataSource = self
                    collectionView.backgroundColor = MYColor.lightGrey
                    collectionView.layer.cornerRadius = 5.0
                    collectionView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
                    collectionView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
                    collectionView.layer.shadowOpacity = 1.0
                    collectionView.layer.shadowRadius = 1.5
                    collectionView.allowsMultipleSelection = false // not allowing user to select multiple dates yet, will likely change in future. For now, focusing app as being used for a hangout, not planning a multiday vacation
                    flowLayout.minimumInteritemSpacing = 0
                    flowLayout.minimumLineSpacing = 0.9
                    containerView.addSubview(collectionView)
                    calendarTitle = UILabel(frame: CGRect(x: 0, y: 0, width: collectionViewWidthDivisibleBySeven, height: (containerViewHeight*90.25/100)/6.75))
                    calendarTitle.font = UIFont.systemFont(ofSize: 18.25)
                    switch UIDevice().type {
                    case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                        print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                        calendarTitle.font = UIFont.systemFont(ofSize: 16.25)
                    case .iPhone6, .iPhone6S:
                        calendarTitle.font = UIFont.systemFont(ofSize: 17)
                    default:
                        print("Do nothing...")
                    }
                    calendarTitle.textAlignment = .center
                    collectionView.isScrollEnabled = false
                    updateCalendarTitle()
                    collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
                }
            }
        }
        else {
            if recognizer.state == .changed {
                print("UIGestureRecognizerState.changed")
                detailView.imageView.tintColor = MYColor.veryDarkGrey
                detailView.textLabel.textColor = MYColor.detailButtonTitleGrey
            }
            if recognizer.state == .cancelled {
                print("UIGestureRecognizerState.cancelled")
                detailView.imageView.tintColor = MYColor.veryDarkGrey
                detailView.textLabel.textColor = MYColor.detailButtonTitleGrey
            }
        }
    }
    
    func updateLocalSearchRequestRegion(_ notification: Notification) {
        print("func updateLocalSearchRequestRegion is running...")
        print("permissionsHelperClass.userLocation:\(String(describing: permissionsHelperClass.userLocation))")
        let updatedUserLocation: CLLocationCoordinate2D = permissionsHelperClass.userLocation!
        // update local class property for user location with phone-returned location
        userLocationInstance = updatedUserLocation
        // reset timer since last user location update to class property
        timeSince = 0
        // run case
        print("func updateLocalSearchRequestRegion: entering if statement")
        if let _ = updateLocationMethodFlag {
            if updateLocationMethodFlag! == "mapViewInitialization" {
                print("inside updateLocalSearchRequestRegion:mapViewInitialization case")
                if rallyObject.rallyLocation == "Current Location" {
                    pointAnnotation!.coordinate = updatedUserLocation
                    let pt = MKMapPointForCoordinate(updatedUserLocation)
                    let w = MKMapPointsPerMeterAtLatitude(updatedUserLocation.latitude)*1200 // multiply for meters
                    self.mapView.visibleMapRect = MKMapRectMake(pt.x - w/2.0, pt.y - w/2.0, w*10, w*10)
                    self.mapView.centerCoordinate = updatedUserLocation
                    self.mapView.addAnnotation(self.pointAnnotation!)
                    self.mapView.selectAnnotation(self.pointAnnotation!, animated: true)
                    mapView.frame.origin.y = searchController.searchBar.frame.height
                    // initialize localSearch
                    let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
                    localSearchRequest.region = MKCoordinateRegion(
                            center: updatedUserLocation,
                            span: span)
                    localSearch = MKLocalSearch(request: self.localSearchRequest)
                    NotificationCenter.default.addObserver(self, selector: #selector(DetailsViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                }
                else { // if not current location, location may exist in latitude longitude values
                    let latitude = CLLocationDegrees(rallyObject.latitude)
                    let longitude = CLLocationDegrees(rallyObject.longitude)
                    let coordinatesForLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    pointAnnotation!.coordinate = coordinatesForLocation
                    let pt = MKMapPointForCoordinate(coordinatesForLocation)
                    let w = MKMapPointsPerMeterAtLatitude(coordinatesForLocation.latitude)*1200 // multiply for meters
                    self.mapView.visibleMapRect = MKMapRectMake(pt.x - w/2.0, pt.y - w/2.0, w*10, w*10)
                    self.mapView.centerCoordinate = coordinatesForLocation
                    self.mapView.addAnnotation(self.pointAnnotation!)
                    self.mapView.selectAnnotation(self.pointAnnotation!, animated: true)
                    mapView.frame.origin.y = searchController.searchBar.frame.height
                    // initialize localSearch
                    let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
                    localSearchRequest.region = MKCoordinateRegion(
                        center: coordinatesForLocation,
                        span: span)
                    localSearch = MKLocalSearch(request: self.localSearchRequest)
                    NotificationCenter.default.addObserver(self, selector: #selector(DetailsViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                }
            }
            if updateLocationMethodFlag! == "withSearchBarSearchTerm" {
                print("inside updateLocalSearchRequestRegion:withSearchBarSearchTerm case")
                // one degree of longitude spans a distance of approximately 111 kilometers (69 miles) at the equator but shrinks to 0 kilometers at the poles, may need MKCoordinateSpanMake instead
                    // using coordinate to update localSearchRequest region property to give search engine hint where to check, stored in instance coordinates
                let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
                /*if rallyObject.RallyLocation != "Current Location" || rallyObject.RallyLocation != "Dropped Pin" { // if neither default, means user selected term from search, find latitude/longitude
                    print("searchController.searchBar.text:\(searchController.searchBar.text)")
                    self.localSearchRequest.naturalLanguageQuery = rallyObject.RallyLocation
                    let geocoder = CLGeocoder()
                    self.localSearchRequest.region = MKCoordinateRegion(
                        center: regionCenter,
                        span: span)
                    if let searchTerm = self.searchController.searchBar.text { // if there is text, use as search term for localSearch call
                        self.localSearchRequest.naturalLanguageQuery = searchTerm
                    }
                    self.localSearch = MKLocalSearch(request: self.localSearchRequest)
                    self.updateBestMatches()
                }*/
                //else {
                    if let searchTerm = self.searchController.searchBar.text { // if there is text, use as search term for localSearch call
                        self.localSearchRequest.naturalLanguageQuery = searchTerm
                    }
                    self.localSearchRequest.region = MKCoordinateRegion(
                        center: updatedUserLocation,
                        span: span)
                    self.localSearch = MKLocalSearch(request: self.localSearchRequest)
                    self.updateBestMatches()
                //}
                
                
                
                /*
                let geocoder = CLGeocoder()
                var coordinates: CLLocationCoordinate2D?
                // http://martianwabbit.com/2014/07/29/using-mapkit-with-swift.html
                let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0) // one degree of longitude spans a distance of approximately 111 kilometers (69 miles) at the equator but shrinks to 0 kilometers at the poles, may need MKCoordinateSpanMake instead
                geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                    if((error) != nil){
                        print("Error", error)
                    }
                    if let placemark = placemarks?.first {
                        coordinates = placemark.location!.coordinate
                        if let regionCenter = coordinates as CLLocationCoordinate2D! { // search for search term best matches within this region
                            // geocodeAddressString returned non nil placemark.location!.coordinate value,  updating localSearchRequest region property to give search engine hint where to check, stored in instance coordinates
                            self.localSearchRequest.region = MKCoordinateRegion(
                                center: regionCenter,
                                span: span)
                        }
                        if let searchTerm = self.searchController.searchBar.text { // if there is text, use as search term for localSearch call
                            self.localSearchRequest.naturalLanguageQuery = searchTerm
                        }
                        self.localSearch = MKLocalSearch(request: self.localSearchRequest)
                        self.updateBestMatches()
                    }
                })*/
            }
            if updateLocationMethodFlag! == "openAppleMaps" {
                // send user current location and destination location coordinates to apple maps app
                let scheme: String = "http://maps.apple.com/?saddr=\(updatedUserLocation.latitude),\(updatedUserLocation.longitude)&daddr=\(rallyObject.latitude),\(rallyObject.longitude)"
                if schemeAvailable(scheme) {
                    UIApplication.shared.openURL(URL(string: scheme)!)
                }
            }
            if updateLocationMethodFlag! == "initializeUserLatitudeLongitudeForRallyObject" {
                print("initializeUserLatitudeLongitudeForRallyObject")
                rallyObject.latitude = CGFloat(updatedUserLocation.latitude)
                rallyObject.longitude = CGFloat(updatedUserLocation.longitude)
                
                // NOTE: Commented out bc it seems rally shouldn't be created on backend until sent. In either case, until sent it doesn't need to be updated so that invited members can be synced since invite isn't even sent to those members until rallyer sends rally out
                /*
                // make request to update rally details on web service
                let intent = WebServiceCallIntent.updateRallyDetails(rallyObject)
                let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
                webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                    if succeeded {
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                    else { // post message saying could not join group. try again later
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                })
                */
            }
        }
    }
    
    func displayContainerViewMapViewSubview() {
        print("func displayContainerViewSubview is running...")
        removeContainerViewSubviews() // cannot be atop all cases bc it isn't removed for location button when touched when rally is active
        mapView = MKMapView()
        mapView.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: containerViewHeight*2/*+(self.navigationController?.toolbar.frame.height)!*/)
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.mapType = .standard
        mapView.delegate = self
        mapView.tag = 57
        if let _ = view.viewWithTag(83) {
        }
        else {
            showSearchBar() // searchBar is subview within bestMatchesTableView header
        }
        if let _ = containerView.viewWithTag(57) { // if mapView is  subview of containerView do nothing
        }
        else { // if mapView is not a subview of containerView, add as subview
            containerView.addSubview(mapView)
            defineBestMatchesTableView()
        }
        // pull user location from phone
        updateLocationMethodFlag = "mapViewInitialization"
        permissionsHelperClass.userLocation = nil
        permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocalSearchRequestRegion is listening for within detailsVC which updates localSearchRequest userLocation property
    }
    
    func schemeAvailable(_ scheme: String) -> Bool {
        print("func schemeAvailable is running...")
        if let url = URL.init(string: scheme) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    func displayMapsApp() {
        print("func displayMapsApp is running...")
        updateLocationMethodFlag = "openAppleMaps"
        permissionsHelperClass.userLocation = nil
        permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocalSearchRequestRegion is listening for within detailsVC which updates localSearchRequest userLocation property
    }
    
    func defineSendRallyButton() {
        print("func defineSendRallyButton is running...")
        if let subviews = self.navigationController?.toolbar.subviews {
            for subview in subviews { // keep toolbar empty for future any future view loads using toolbar
                subview.removeFromSuperview()
            }
        }
        sendRallyButton = UIButton(type: .custom)
        sendRallyButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            sendRallyButton.titleLabel!.font = UIFont.systemFont(ofSize: 16)
        case .iPhone6, .iPhone6S:
            sendRallyButton.titleLabel!.font = UIFont.systemFont(ofSize: 17)
        default:
            print("Do nothing...")
        }
        sendRallyButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: (self.navigationController?.toolbar.frame.height)!)
        sendRallyButton.backgroundColor = MYColor.rallyBlue
        sendRallyButton.layer.masksToBounds = true
        sendRallyButton.setTitle("Send Rally!", for: UIControlState())
        sendRallyButton.setTitleColor(UIColor.white, for: UIControlState())
        sendRallyButton.addTarget(self, action: #selector(DetailsViewController.onSendRallyButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        //Selector(onSendRallyButtonTouch(sendRallyButton))
        self.navigationController?.toolbar.addSubview(sendRallyButton)
        print("func defineSendRallyButton is complete...")
    }
    
    func changeContainerViewSize(_ xOffset: CGFloat, height: CGFloat) { /*30/100, 70/100, 2/5, 3/5*/
        print("func enlargeContainerViewSize is running...")
        containerView.frame = CGRect(x: 0, y: ScreenDimension.heightMinusStatusNavigationAndToolbar*xOffset, width: ScreenDimension.width, height: ScreenDimension.heightMinusStatusNavigationAndToolbar*height)
        containerViewHeight = containerView.frame.height
        clockButtonsParentView.center = CGPoint(x: self.view.bounds.width/2.0, y: containerView.frame.height/2.0)
    }

    func onSendRallyButtonTouch(_ sender: UIButton!) { // no addTarget command added to class yet. Need to add
        //return to main screen, cache selected friends
        print("func onSendRallyButtonTouch is running...")
        // update rally object's location, date, name, and start time info before sending
        sender.isUserInteractionEnabled = false
        rallyObject.isRallyActive = 1 // 1 means active. always active when sent from details screen. starts as 0 in case user presses back button which aborts sending rally
        // mark user didUserAcceptRally property to true (1)
        rallyMembers: for rallyMember in rallyObject.rallyUserList {
            if MYUser.ID == rallyMember.ID {
                rallyMember.didUserAccept = 1
            }
        }
        // post new rally to webservice
        // update is active property doesn't update other details user customized on screen. need to make call: UpdateRallyDetails
        let intent = WebServiceCallIntent.createRally(rallyObject)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    // ADD LOGIC TO CHANGE UI TO JOIN SCREEN
                    //self.rallyDetailsButtons[1].removeFromSuperview() // remove responseButton from menu and add to container view
                    self.updateLocationButtonSubviewLayoutWhenActive()
                    self.rallyerEditRallyAfterSendingButton()
                    self.defineActiveRallyDefaultContainerView()
                    // update toolbar button to say 'go to messaging' and define button interaction state
                    self.defineGoToMessagingButton()
                })
            }
            else { // post message saying could not join group. try again later
                sender.isUserInteractionEnabled = true
                self.rallyObject.isRallyActive = 0
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func rallyerEditRallyAfterSendingButton() {
        print("func rallyerEditRallyAfterSendingButton is running...")
        let rallyerEditRallyOnceActiveButton = UIButton(type: .system)
        rallyerEditRallyOnceActiveButton.tag = 5030
        rallyerEditRallyOnceActiveButton.frame = CGRect(x: 0, y: 0, width: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height*20/100, height: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height*20/100)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            rallyerEditRallyOnceActiveButton.frame = CGRect(x: 0, y: 0, width: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height*25/100, height: rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height*25/100)
        default:
            print("Do nothing...")
        }
        rallyerEditRallyOnceActiveButton.setImage(UIImage(named:"edit"), for: UIControlState())
        //rallyerEditRallyOnceActiveButton.imageView!.frame.size = CGSizeMake(rallyerEditRallyOnceActiveButton.imageView!.frame.width*5/100,rallyerEditRallyOnceActiveButton.imageView!.frame.height*5/100)
        rallyerEditRallyOnceActiveButton.imageView!.contentMode = UIViewContentMode.scaleAspectFit

        //rallyerEditRallyOnceActiveButton.sizeToFit()
        let lastTwoSectioningLinesContainerHeight = rallyDetailsButtonsAndLastTwoSectioningLinesContainer.frame.height
        rallyerEditRallyOnceActiveButton.center = CGPoint(x: ScreenDimension.width*91/100, y: lastTwoSectioningLinesContainerHeight*3/4)
        rallyerEditRallyOnceActiveButton.tintColor = MYColor.veryDarkGrey
        rallyerEditRallyOnceActiveButton.addTarget(self, action: #selector(DetailsViewController.onRallyerEditRallyAfterSendingButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        rallyDetailsButtonsAndLastTwoSectioningLinesContainer.addSubview(rallyerEditRallyOnceActiveButton)
    }
    
    func onRallyerEditRallyAfterSendingButtonTouch(_ sender: UIButton!) {
        print("func onRallyerEditRallyAfterSendingButtonTouch is running...")
        displayContainerViewMapViewSubview()
    }
    
    func defineActiveRallyDefaultContainerView() {
        print("func defineActiveRallyDefaultContainerView is running...")
        // for rally creator, in case any buttons were clicked and are currently displayed (start time, calendar, response time, location)
        removeContainerViewSubviews()
        defineNumberThatHaveJoinedRallyLabel()
        defineNumberJoinedHeaderLabel()
        //let order = dateHelperClass().timeFromNowUntilDate(rallyObject.rallyStartDateTime!)
        /*
        switch order {
        case .OrderedDescending:
            // ascending means date rallystartdatetime is after current time, so rally has not started yet
            print("DESCENDING")
            // rally start time has passed, display started label inside timer
            //defineTimer()
            //defineTimerHeaderLabel()
            //timerHeaderLabel.text = "Starting in"
        case .OrderedAscending:
            // ascending means date rallystartdatetime is after current time, so rally has not started yet
            print("ASCENDING")
            // define timer for active rally to be display in container view
            //defineTimer()
            if let _ = timeRemainingRallyTimer {
            }
            else { // if timeRemainingRallyTimer doesn't exist, schedule timer
                //timeRemainingRallyTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
            }
            //defineTimerHeaderLabel()
            timerHeaderLabel.text = "Time to Respond"
        case .OrderedSame:
            print("SAME")
            //defineTimer()
            //timeRemainingRallyTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
            //defineTimerHeaderLabel()
            //timerHeaderLabel.text = "Time to Respond"
        }*/
        //timerHeaderLabel.sizeToFit()
        //timerHeaderLabel.center = CGPointMake(ScreenDimension.screenWidth*67.1/100, containerViewHeight*19/100)
    }
    
    func onMoveToMainScreenButtonTouch(_ sender: UIButton!) { // no addTarget command added to class yet. Need to add
        //return to main screen, cache selected friends
        print("func onMoveToMainScreenButtonTouch is running...")
        _ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    func onMoveToInviteListTouch(_ sender: UIButton!) {
        print("func onMoveToInviteListTouch is running...")
        let moveToInviteViewController: InviteViewController = InviteViewController(nibName: nil, bundle: nil)
        moveToInviteViewController.invitablePeople = rallyObject.associatedCommunity!.members
        // dismiss keyboard if displayed
        moveToInviteViewController.rallyObject = rallyObject
        moveToInviteViewController.community = community
        self.navigationController!.pushViewController(moveToInviteViewController, animated: true)
    }
    
    func backToHomeViewController(_ sender: UIButton!) {
        print("func backToHomeViewController is running...")
        self.navigationController!.popToRootViewController(animated: false)
    }
    
// calendar methods
    func updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable(_ adjustMonthToBeDisplayedByThisValue: Int) {
        print("func updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable is running...")
        if let _ = dateWithinCurrentMonthToBeDisplayedInCalendar {
            var components = DateComponents()
            components.month = adjustMonthToBeDisplayedByThisValue // should this be += instead of = ?
            dateWithinCurrentMonthToBeDisplayedInCalendar = (calendar as NSCalendar).date(byAdding: components, to: dateWithinCurrentMonthToBeDisplayedInCalendar, options: NSCalendar.Options(rawValue: 0))!
        }
        else {
            dateWithinCurrentMonthToBeDisplayedInCalendar = Date()
            var monthToBeDisplayedComponents = (calendar as NSCalendar).components([.month, .year, .weekday, .day], from: dateWithinCurrentMonthToBeDisplayedInCalendar)
            monthToBeDisplayedComponents.day = 1
            let monthDateAsNSDate: Date = calendar.date(from: monthToBeDisplayedComponents)!
            dateWithinCurrentMonthToBeDisplayedInCalendar = monthDateAsNSDate
        }
    }
    
    func defineDateChosen() {
        print("func defineDateChosen is running...")
        dateChosen = calendar.startOfDay(for: rallyObject.rallyStartDateTime! as Date)
        let dateChosenComponentDayComponents = (calendar as NSCalendar).components(.day, from: dateChosen!)
        let weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday = [0, 1, 2, 3, 4, 5, 6, 7]
        let firstDayOfMonthAsInt: Int = weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday[dayOfWeek()]+5
        let dayOfMonthChosen = dateChosenComponentDayComponents.day
        indexPathOfDateChosen = IndexPath(item: firstDayOfMonthAsInt+dayOfMonthChosen!, section: 0)
    }
    
    func dayOfWeek()-> Int {
        print("func dayOfWeek is running...")
        let currentMonthComponent = (calendar as NSCalendar).components(.weekday, from: dateWithinCurrentMonthToBeDisplayedInCalendar)
        return currentMonthComponent.weekday!
        // Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6, Saturday = 7
    }
    
    func daysInMonth()-> Int {
        print("func daysInMonth is running...")
        // https://gist.github.com/benjamintanweihao/63c302852c0444189be1
        var currentMonthComponent = (calendar as NSCalendar).components(NSCalendar.Unit.month, from: dateWithinCurrentMonthToBeDisplayedInCalendar)
        currentMonthComponent.day = 1 // getting the first date of the month
        let firstDateOfMonth: Date = calendar.date(from: currentMonthComponent)!
        if let month = currentMonthComponent.month {
            currentMonthComponent.month = month + 1 // getting the first date of the next month
        }
        currentMonthComponent.day = 1
        let lastDateOfMonth: Date = calendar.date(from: currentMonthComponent)!
        let daysInMonthComponents = (calendar as NSCalendar).components(.day, from: firstDateOfMonth, to: lastDateOfMonth, options: NSCalendar.Options(rawValue: 0))
        return Int(daysInMonthComponents.day!)  // This will return the number of day(s) between dates, in this case, the first day of consecutive months to ascertain number of days in month of first date
    }
    
    func updateCalendarTitle() {
        print("func updateCalendarTitle is running...")
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM, yyyy"
        let dateAsString = dayTimePeriodFormatter.string(from: dateWithinCurrentMonthToBeDisplayedInCalendar)
        calendarTitle.text = dateAsString
        calendarTitle.textColor = MYColor.extremelyDarkGrey
    }
    
    func onChangeCalendarMonthButtonTouch(_ sender: UIButton!) {
        print("func onNextMonthButtonTouch is running...")
        if sender.tag == 69 {
            updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable(-1) // move to previous month
        }
        else {
            updateDateWithinCurrentMonthToBeDisplayedInCalendarVariable(1) // display next month
        }
        updateCalendarTitle()
        for subview in containerView.subviews {
            if subview.tag > 200 && subview.tag < 207 {
                subview.removeFromSuperview()
            }
        }
        collectionView.reloadData()
    }
    
    func addBackAndForwardButtons(_ parentView: UIView, headerViewHeight: CGFloat) {
        print("func addBackAndForwardButtons is running...")
        let previousMonthButton = UIButton(type: .system)
        previousMonthButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width/8, height: ScreenDimension.height/14)
        let previousMonthButtonXOffset = previousMonthButton.frame.width/2
        previousMonthButton.center = CGPoint(x: previousMonthButtonXOffset, y: headerViewHeight/2)
        previousMonthButton.setImage(UIImage(named:"navigateBefore"), for: UIControlState())
        previousMonthButton.tag = 69 // to determine on button touch if calendar should move to previous or next month
        parentView.addSubview(previousMonthButton)
        previousMonthButton.addTarget(self, action: #selector(DetailsViewController.onChangeCalendarMonthButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        let nextMonthButton = UIButton(type: .system) // doesn't need tag since it is the "else" logic
        nextMonthButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width/8, height: ScreenDimension.height/14)
        let nextMonthButtonXOffset = collectionView.frame.width-nextMonthButton.frame.width/2
        nextMonthButton.center = CGPoint(x: nextMonthButtonXOffset, y: headerViewHeight/2)
        nextMonthButton.setImage((UIImage(named:"navigateNext")), for: UIControlState())
        parentView.addSubview(nextMonthButton)
        nextMonthButton.addTarget(self, action: #selector(DetailsViewController.onChangeCalendarMonthButtonTouch(_:)), for: UIControlEvents.touchUpInside)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("func collectionView:viewForSupplementaryElementOfKind is running...")
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
        for subview in headerView.subviews {
            subview.removeFromSuperview()
        }
        headerView.addSubview(calendarTitle)
        collectionView.frame.size.height = collectionView.contentSize.height
        // collectionView.setNeedsDisplay()
        headerView.tintColor = MYColor.extremelyDarkGrey
        headerView.backgroundColor = UIColor.white
        let headerViewHeight = headerView.frame.height
        addBackAndForwardButtons(headerView, headerViewHeight: headerViewHeight)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        print("func collectionView:referenceSizeForHeaderInSection is running...")
        return CGSize(width: collectionView.frame.width, height: (containerViewHeight*90.25/100)/6.25)  // Header size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        print("func collectionView:sizeForItemAtIndexPath is running...")
        if (indexPath as NSIndexPath).row < 7 {
            return CGSize(width: collectionView.frame.width/7, height: (containerViewHeight*90.25/100)/12) // should be 20/7, rounded to 2.86 because sunday was bleeding into next row. ultimately, need to get rid of all these hard coded numbers...
        }
        else {
            let collectionViewHeight = containerViewHeight*90.25/100
            return CGSize(width: collectionView.frame.width/7, height: (collectionViewHeight-collectionViewHeight/6.75-collectionViewHeight/12)/6) // should be 20/7, rounded to 2.86 because sunday was bleeding into next row. ultimately, need to get rid of all these hard coded numbers...
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("func collectionView:numberOfItemsInSection is running...")
        let numberOfDaysInMonth = daysInMonth()
        let weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday = [0, 1, 2, 3, 4, 5, 6, 7]
        let firstDayOfMonthAsInt: Int = weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday[dayOfWeek()]-1
        var lastDayOfMonthCell = Int(numberOfDaysInMonth + firstDayOfMonthAsInt)+7 // 1 headerView cell, 7 weekday titles (8)
        switch lastDayOfMonthCell {
        // don't do anything if lastDayOfMonthCell = 28, already goes to end of row (4 rows)
        case _ where lastDayOfMonthCell > 35 && lastDayOfMonthCell <= 42: // 5 rows
            lastDayOfMonthCell = 42
        case _ where lastDayOfMonthCell > 42 && lastDayOfMonthCell <= 50: // 6 rows
            lastDayOfMonthCell = 49
        default:
            break
        }
        return lastDayOfMonthCell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("func collectionView:cellForItemAtIndexPath is running...")
        print("collectionView cell indexPath:\(indexPath)")
        let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        //see page 436 for reusable cell test, not that hard looking
        for subview in cell.contentView.subviews {
            subview.removeFromSuperview()
        }
        /*when a new cell is requested for the view (for initial page loading or scrolling to new cells), a cell that may have already been populated with subviews (in the case of scrolling) may be pulled from the cell queue for use. This assures all subviews are removed prior to populating with correct data (friend name and picture*/
        cell.backgroundColor = UIColor.white
        cell.isUserInteractionEnabled = false // do not allow day of week or cells used to format past month numbering to be clicked because text changes color on click
        let daysInMonthInstance = daysInMonth()
        let weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday = [0, 1, 2, 3, 4, 5, 6, 7]
        let firstDayOfMonthAsInt: Int = weekDayConvertedToIndexPathCalendarCellOrderStartingAtMonday[dayOfWeek()]
        let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: cell.frame.size.width, height: cell.frame.size.height))
        textLabel.tag = 500
        textLabel.font = UIFont.systemFont(ofSize: 17.5)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            textLabel.font = UIFont.systemFont(ofSize: 15)
        case .iPhone6, .iPhone6S:
            textLabel.font = UIFont.systemFont(ofSize: 16)
        default:
            print("Do nothing...")
        }
        textLabel.textAlignment = .center
        textLabel.textColor = MYColor.extremelyDarkGrey
        // create selected/highlighted cell backView
        let myBackView = UIView(frame:cell.frame)
        myBackView.tag = 350
        myBackView.frame = CGRect(x: 0, y: 0, width: cell.frame.width*5.35/10, height: cell.frame.width*5.35/10)
        
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            myBackView.frame = CGRect(x: 0, y: 0, width: cell.frame.width*5.0/10, height: cell.frame.width*5.0/10)
        case .iPhone6, .iPhone6S:
            myBackView.frame = CGRect(x: 0, y: 0, width: cell.frame.width*5.15/10, height: cell.frame.width*5.15/10)
        default:
            print("Do nothing...")
        }
        myBackView.frame.origin.x = (cell.bounds.width - myBackView.frame.width) / 2.0
        myBackView.frame.origin.y = (cell.bounds.height - myBackView.frame.height) / 2.0
        myBackView.layer.cornerRadius = myBackView.frame.width/2
        if (indexPath as NSIndexPath).row < 7 && (indexPath as NSIndexPath).row <= daysInMonthInstance + 5 + firstDayOfMonthAsInt {
            print("making weekday header cell...")
            // firstDayOfMonthAsInt is not "zeroed out" the way I wrote it so I subtract -1 to even up with index starting at zero, also zero daysInMonthInstance for same reason
            let weekday = ["SUN","MON", "TUE", "WED", "THU", "FRI", "SAT"]
            textLabel.text = weekday[(indexPath as NSIndexPath).row]
            cell.contentView.addSubview(textLabel)
            textLabel.textColor = MYColor.midDarkGrey
        }
        else if (indexPath as NSIndexPath).row <= daysInMonthInstance + 5 + firstDayOfMonthAsInt && (indexPath as NSIndexPath).row - 6 - firstDayOfMonthAsInt >= 0 {
            print("making cell, adding text start...")
            textLabel.text = String((indexPath as NSIndexPath).row - 5 - firstDayOfMonthAsInt)
            // when cell is selected or highlighted, backview is the view shown. Adjust frame to be circle centered around number in cell
            cell.contentView.addSubview(myBackView)
            let today = Date()
            var timeComponentsForUserTimeInputsToBeAppendedTo = (calendar as NSCalendar).components([.day,.month,.year], from: dateWithinCurrentMonthToBeDisplayedInCalendar)
            timeComponentsForUserTimeInputsToBeAppendedTo.day = (indexPath as NSIndexPath).row - 5 - firstDayOfMonthAsInt
            let rallyResponseDeadlineTimeComponent: Date = calendar.date(from: timeComponentsForUserTimeInputsToBeAppendedTo)!
            let timeUsersHaveToRespondComponent = (calendar as NSCalendar).components(.day, from: today, to: rallyResponseDeadlineTimeComponent, options: NSCalendar.Options(rawValue: 0))
            if timeUsersHaveToRespondComponent.day! > -1 {
                cell.isUserInteractionEnabled = true
            }
            else {
                cell.isUserInteractionEnabled = false
                textLabel.textColor = MYColor.midDarkGrey
            }
            cell.contentView.addSubview(textLabel)
            print("making cell, adding text end...")
        }
        if let date = dateChosen {
            var timeComponentsForUserTimeInputsToBeAppendedTo = (calendar as NSCalendar).components([.year, .month], from: dateWithinCurrentMonthToBeDisplayedInCalendar)
            timeComponentsForUserTimeInputsToBeAppendedTo.day = (indexPath as NSIndexPath).row - 5 - firstDayOfMonthAsInt
            let rallyResponseDeadlineTimeComponent: Date = calendar.date(from: timeComponentsForUserTimeInputsToBeAppendedTo)!
            let date2 = calendar.startOfDay(for: rallyResponseDeadlineTimeComponent)
            let timeUsersHaveToRespondComponent = (calendar as NSCalendar).components(.hour, from: date, to: date2, options: NSCalendar.Options(rawValue: 0))
            if timeUsersHaveToRespondComponent.hour == 0 {
                if let previousSelectedCellBackView = cell.contentView.viewWithTag(350) as UIView! {
                    previousSelectedCellBackView.backgroundColor = MYColor.rallyBlue
                    if let previousCellUILabel = cell.contentView.viewWithTag(500) as? UILabel {
                        previousCellUILabel.textColor = UIColor.white
                    }
                }
            }
        }
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(DetailsViewController.handleLongPress(_:)))
        lpgr.minimumPressDuration = 0.0
        lpgr.delaysTouchesBegan = false
        lpgr.delegate = self
        cell.addGestureRecognizer(lpgr)
        print("cell textLabel:\(String(describing: textLabel.text))")
        return cell
    }
    
    func handleLongPress(_ recognizer: UILongPressGestureRecognizer) {
        print("func dateSelected is running...")
        let swipeLocation = recognizer.location(in: self.collectionView)
        if let indexPath = collectionView.indexPathForItem(at: swipeLocation) {
            if let cell = self.collectionView.cellForItem(at: indexPath) {
                // Swipe happened. Do stuff!
                if cell.isUserInteractionEnabled == true { // do not allow week day titles or past days to be highlighted when touched
                    if recognizer.state == UIGestureRecognizerState.began {
                        if let previouslySelectedCellIndexPath = indexPathOfDateChosen as IndexPath! {
                            print("previouslyChosenCellExists:\(previouslySelectedCellIndexPath)")
                            let previouslySelectedCell = self.collectionView.cellForItem(at: previouslySelectedCellIndexPath)
                            print("previouslySelectedCell:\(String(describing: previouslySelectedCell))")
                            if let previouslySelectedCellsExists = previouslySelectedCell {
                                if let previousSelectedCellBackView = previouslySelectedCellsExists.contentView.viewWithTag(350) as UIView! {
                                    previousSelectedCellBackView.backgroundColor = UIColor.white
                                    if let previousSelectedCellUILabel = previouslySelectedCellsExists.contentView.viewWithTag(500) as? UILabel {
                                        previousSelectedCellUILabel.textColor = MYColor.extremelyDarkGrey
                                    }
                                }
                            }
                        }
                        var timeComponentsForUserTimeInputsToBeAppendedTo = (calendar as NSCalendar).components([.day, .year, .month], from: dateWithinCurrentMonthToBeDisplayedInCalendar) // to create an NSDate() for date chosen within calendar
                        let dayTimePeriodFormatter = DateFormatter()
                        dayTimePeriodFormatter.dateFormat = "MMM"
                        let dateAsString = dayTimePeriodFormatter.string(from: dateWithinCurrentMonthToBeDisplayedInCalendar)
                        var calendarButtonTitle = dateAsString // to get current month date is being selected for within calendar
                        let newSelectedCell = self.collectionView.cellForItem(at: indexPath)
                        if let newSelectedCellBackView = newSelectedCell!.contentView.viewWithTag(350) as UIView! {
                            newSelectedCellBackView.backgroundColor = MYColor.rallyBlue
                            if let calendarDateChosenByUser = newSelectedCell!.contentView.viewWithTag(500) as? UILabel {
                                calendarButtonTitle += " \(calendarDateChosenByUser.text!)"
                                timeComponentsForUserTimeInputsToBeAppendedTo.day = Int(calendarDateChosenByUser.text!)!
                                calendarDateChosenByUser.textColor = UIColor.white
                            }
                        }
                        let rallyResponseDeadlineTimeComponent: Date = calendar.date(from: timeComponentsForUserTimeInputsToBeAppendedTo)!
                        let date2 = calendar.startOfDay(for: rallyResponseDeadlineTimeComponent)
                        let calendarButtonFormattedTitle = dateHelperClass().formatNSDateForDetailsScreenDateButtonTitle(rallyResponseDeadlineTimeComponent)
                        dateDetailView.textLabel.text = calendarButtonFormattedTitle
                        if dateDetailView.textLabel.text!.characters.count > 10 || dateDetailView.textLabel.text! == "Wednesday" {
                            print("case1")
                            dateDetailView.textLabel.font = UIFont.systemFont(ofSize: 21)
                            switch UIDevice().type {
                            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                                dateDetailView.textLabel.font = UIFont.systemFont(ofSize: 16)
                            case .iPhone6, .iPhone6S:
                                dateDetailView.textLabel.font = UIFont.systemFont(ofSize: 19)
                            default:
                                print("Do nothing...")
                            }
                        }
                        else {
                            print("case2")
                            dateDetailView.textLabel.font = UIFont.systemFont(ofSize: 22)
                            switch UIDevice().type {
                            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                                dateDetailView.textLabel.font = UIFont.systemFont(ofSize: 20)
                            default:
                                print("Do nothing...")
                            }
                        }
                        dateDetailView.textLabel.sizeToFit()
                        dateHelperClass().updateDateInRallyStartDateTime(rallyObject, selectedDate: rallyResponseDeadlineTimeComponent)
                        dateChosen = date2
                        indexPathOfDateChosen = indexPath
                    }
                    if recognizer.state == UIGestureRecognizerState.ended {
                        if rallyObject.isRallyActive == 1 { // re-add number joined and response time subviews if rally is active
                            defineActiveRallyDefaultContainerView() // switchcontainerview auto-removes subview when any rally button is placed, so needs to be re-added
                            // make request to update rally details on web service
                            let intent = WebServiceCallIntent.updateRallyDetails(rallyObject)
                            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
                            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                                if succeeded {
                                    DispatchQueue.main.async(execute: { () -> Void in
                                    })
                                }
                                else { // post message saying could not join group. try again later
                                    DispatchQueue.main.async(execute: { () -> Void in
                                    })
                                }
                            })
                        }
                        else { // in either case, calendar view is always removed after selection
                            removeContainerViewSubviews()
                        }
                    }
                }
            }
        }
    }
// calendar methods
    
//time methods
    func defineClockButtons() {
        print("func defineClockButtons is running...")
        clockButtonsParentView = UIView(frame: CGRect(x: 0, y: 0, width: containerViewHeight*88/100, height: containerViewHeight*88/100))
        containerView.addSubview(clockButtonsParentView)
        clockButtonsParentView.backgroundColor = UIColor.white
        clockButtonsParentView.center = CGPoint(x: self.view.bounds.width/2.0, y: containerView.frame.height/2.0)
        clockButtonsParentView.layer.cornerRadius = clockButtonsParentView.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
        let hourCircleRadius = containerViewHeight*3.45/10
        let minuteCircleRadius = containerViewHeight*2.95/10
        // three variables below are generically defined so they may be used for all 12 clockHourButtons
        let clockButtonDiameter = ScreenDimension.width/9.5
        let hourButtonSquarerootComponentSolved = CGFloat(sqrt(3)/2.0)
        let XYComponentTrigValueDictionaryPairs: Dictionary<Int, [CGFloat]> = [1: [0.5, hourButtonSquarerootComponentSolved], 2: [hourButtonSquarerootComponentSolved, 0.5], 3: [1, 0], 4: [hourButtonSquarerootComponentSolved, -0.5], 5: [0.5, -hourButtonSquarerootComponentSolved], 6: [0, -1], 7: [-0.5, -hourButtonSquarerootComponentSolved], 8: [-hourButtonSquarerootComponentSolved, -0.5], 9: [-1, 0], 10: [-hourButtonSquarerootComponentSolved, 0.5], 11: [-0.5, hourButtonSquarerootComponentSolved], 12: [0, 1]] // key is x, value is y. other option is to make key, value with array value like [x, y]
        for i in 0..<12 {
            let hourButton = UIButton()
            hourButton.titleLabel!.font = UIFont.systemFont(ofSize: 22)
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                hourButton.titleLabel!.font = UIFont.systemFont(ofSize: 20)
            default:
                print("Do nothing...")
            }
            hourButton.tag = i + 1
            // hourButton.backgroundColor = MYColor.midGrey
            hourButton.frame = CGRect(x: 0, y: 0, width: clockButtonDiameter, height: clockButtonDiameter) // 30 degrees from 12 o'clock, 60 degrees as trig circle reference
            var hourButtonXComponentOffsetFromCenter: CGFloat!
            var hourButtonYComponentOffsetFromCenter: CGFloat!
            hourButtonXComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[i+1]![0]
            hourButtonYComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[i+1]![1]
            hourButton.center = CGPoint(x: clockButtonsParentView.bounds.size.width/2.0 + hourCircleRadius*hourButtonXComponentOffsetFromCenter, y: clockButtonsParentView.bounds.size.width/2.0 - hourCircleRadius*hourButtonYComponentOffsetFromCenter) // may use same value for centering for X and Y axis because it's a circle ;)
            hourButton.layer.cornerRadius = hourButton.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
            hourButton.clipsToBounds = true
            hourButton.setTitle("\(hourButton.tag)", for: UIControlState())
            hourButton.setTitleColor(MYColor.midGrey, for: UIControlState())
            hourButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonTouch(_:)), for: UIControlEvents.touchDown)
            hourButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonEndTouchOutside(_:)), for: UIControlEvents.touchDragOutside)
            hourButton.addTarget(self, action: #selector(DetailsViewController.onClockHourButtonEndTouch(_:)), for: UIControlEvents.touchUpInside)
            clockButtonsParentView.addSubview(hourButton)
            hourButtonsArray.append(hourButton)
        }
        for i in 0..<4 {
            let minuteButton = UIButton()
            // minuteButton.backgroundColor = MYColor.midGrey
            minuteButton.frame = CGRect(x: 0, y: 0, width: clockButtonDiameter*1.2, height: clockButtonDiameter*1.2) // 30 degrees from 12 o'clock, 60 degrees as trig circle reference
            var hourButtonXComponentOffsetFromCenter: CGFloat!
            var hourButtonYComponentOffsetFromCenter: CGFloat!
            hourButtonXComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[(i+1)*3]![0]
            hourButtonYComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[(i+1)*3]![1]
            minuteButton.center = CGPoint(x: clockButtonsParentView.bounds.size.width/2.0 + minuteCircleRadius*hourButtonXComponentOffsetFromCenter, y: clockButtonsParentView.bounds.size.width/2.0 - minuteCircleRadius*hourButtonYComponentOffsetFromCenter) // may use same value for centering for X and Y axis because it's a circle ;)
            minuteButton.layer.cornerRadius = minuteButton.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
            let buttonText = (i + 1)*15
            if buttonText != 60 {
                minuteButton.setTitle("\(buttonText)", for: UIControlState())
            }
            else {
                minuteButton.setTitle("00", for: UIControlState())
            }
            minuteButton.isHidden = true
            minuteButton.setTitleColor(MYColor.midGrey, for: UIControlState())
            minuteButton.titleLabel!.font = UIFont.systemFont(ofSize: 25.0)
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                minuteButton.titleLabel!.font = UIFont.systemFont(ofSize: 22.5)
            default:
                print("Do nothing...")
            }
            minuteButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonTouch(_:)), for: UIControlEvents.touchDown)
            minuteButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonEndTouchOutside(_:)), for: UIControlEvents.touchDragOutside)
            minuteButton.addTarget(self, action: #selector(DetailsViewController.onClockMinuteButtonEndTouch(_:)), for: UIControlEvents.touchUpInside)
            clockButtonsParentView.addSubview(minuteButton)
            minuteButtonsArray.append(minuteButton)
        }
        let clockUnitButton = UIButton()
        clockUnitButton.titleLabel!.font = UIFont.systemFont(ofSize: 36)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            clockUnitButton.titleLabel!.font = UIFont.systemFont(ofSize: 32)
        default:
            print("Do nothing...")
        }
        clockUnitButton.tag = 508
        // clockUnitButton.backgroundColor = MYColor.rallyBlue
        clockUnitButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width/8.45*1.78, height: ScreenDimension.width/8.45*1.78) // 30 degrees from 12 o'clock, 60 degrees as trig circle reference
        clockUnitButton.center = CGPoint(x: clockButtonsParentView.frame.width/2,y: clockButtonsParentView.frame.height/2)
        clockUnitButton.layer.cornerRadius = clockUnitButton.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
        clockUnitButton.setTitle("PM", for: UIControlState())
        clockUnitButton.setTitleColor(MYColor.veryDarkGrey, for: UIControlState())
        clockUnitButton.addTarget(self, action: #selector(DetailsViewController.onClockUnitButtonTouch(_:)), for: UIControlEvents.touchDown)
        // clockUnitButton.addTarget(self, action: "onMinuteHourDayButtonEndTouchOutside:", forControlEvents: UIControlEvents.TouchDragOutside)
        // clockUnitButton.addTarget(self, action: "onResponseTimeButtonEndTouch:", forControlEvents: UIControlEvents.TouchUpInside)
        clockButtonsParentView.addSubview(clockUnitButton)
    }
    /*
    func defineResponseTimeButtons() {
        print("func defineResponseTimeButtons is running...")
        clockButtonsParentView = UIView(frame: CGRect(x: 0, y: 0, width: containerViewHeight*88/100, height: containerViewHeight*88/100))
        containerView.addSubview(clockButtonsParentView)
        clockButtonsParentView.backgroundColor = UIColor.white
        clockButtonsParentView.center = CGPoint(x: self.view.bounds.width/2.0, y: containerView.frame.height/2.0)
        clockButtonsParentView.layer.cornerRadius = clockButtonsParentView.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
        let minuteValuesArray = ["20","25","30","40","45","50"]
        let hourCircleRadius = containerViewHeight*2.85/10
        // let minuteCircleRadius = containerViewHeight*2/10
        // three variables below are generically defined so they may be used for all 12 clockHourButtons
        let clockButtonDiameter = ScreenDimension.screenWidth/9.5
        let hourButtonSquarerootComponentSolved = CGFloat(sqrt(3)/2.0)
        let XYComponentTrigValueDictionaryPairs: Dictionary<Int, [CGFloat]> = [1: [0.5, hourButtonSquarerootComponentSolved], 2: [1, 0], 3: [0.5, -hourButtonSquarerootComponentSolved], 4: [-0.5, -hourButtonSquarerootComponentSolved], 5: [-1, 0], 6: [-0.5, hourButtonSquarerootComponentSolved]] // key is x, value is y. other option is to make key, value with array value like [x, y]
        for i in 0..<6 {
            let minuteHourDayButton = UIButton()
            minuteHourDayButton.titleLabel!.font = UIFont.systemFont(ofSize: 23.75)
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                minuteHourDayButton.titleLabel!.font = UIFont.systemFont(ofSize: 21.5)
            default:
                print("Do nothing...")
            }
            minuteHourDayButton.tag = i + 50
            // minuteHourDayButton.backgroundColor = MYColor.midGrey
            minuteHourDayButton.frame = CGRect(x: 0, y: 0, width: clockButtonDiameter*1.2, height: clockButtonDiameter*1.2) // 30 degrees from 12 o'clock, 60 degrees as trig circle reference
            var minuteHourDayButtonXComponentOffsetFromCenter: CGFloat!
            var minuteHourDayButtonYComponentOffsetFromCenter: CGFloat!
            minuteHourDayButtonXComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[i+1]![0]
            minuteHourDayButtonYComponentOffsetFromCenter = XYComponentTrigValueDictionaryPairs[i+1]![1]
            minuteHourDayButton.center = CGPoint(x: clockButtonsParentView.bounds.size.width/2.0 + hourCircleRadius*minuteHourDayButtonXComponentOffsetFromCenter, y: clockButtonsParentView.bounds.size.width/2.0 - hourCircleRadius*minuteHourDayButtonYComponentOffsetFromCenter) // may use same value for centering for X and Y axis because it's a circle ;)
            minuteHourDayButton.layer.cornerRadius = minuteHourDayButton.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
            minuteHourDayButton.clipsToBounds = true
            minuteHourDayButton.setTitle("\(minuteValuesArray[i])", for: UIControlState())
            minuteHourDayButton.setTitleColor(MYColor.midGrey, for: UIControlState())
            minuteHourDayButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonTouch(_:)), for: UIControlEvents.touchDown)
            minuteHourDayButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayValueButtonEndTouchOutside(_:)), for: UIControlEvents.touchDragOutside)
            minuteHourDayButton.addTarget(self, action: #selector(DetailsViewController.onResponseTimeButtonEndTouch(_:)), for: UIControlEvents.touchUpInside)
            clockButtonsParentView.addSubview(minuteHourDayButton)
        }
        let responseTimeUnitsButton = UIButton()
        responseTimeUnitsButton.titleLabel!.font = UIFont.systemFont(ofSize: 48)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            responseTimeUnitsButton.titleLabel!.font = UIFont.systemFont(ofSize: 43)
        default:
            print("Do nothing...")
        }
        responseTimeUnitsButton.tag = 506
        // responseTimeUnitsButton.backgroundColor = MYColor.rallyBlue
        responseTimeUnitsButton.frame = CGRect(x: 0, y: 0, width: clockButtonDiameter*2.1, height: clockButtonDiameter*2.1) // 30 degrees from 12 o'clock, 60 degrees as trig circle reference
        responseTimeUnitsButton.center = CGPoint(x: clockButtonsParentView.frame.width/2,y: clockButtonsParentView.frame.height/2)
        responseTimeUnitsButton.layer.cornerRadius = responseTimeUnitsButton.frame.size.width/2 // Defines clockButtonsParentView frame to be circular
        responseTimeUnitsButton.setTitle("m", for: UIControlState())
        responseTimeUnitsButton.titleEdgeInsets = UIEdgeInsetsMake((responseTimeUnitsButton.frame.height-responseTimeUnitsButton.titleLabel!.frame.height)/2,0,(responseTimeUnitsButton.frame.height-responseTimeUnitsButton.titleLabel!.frame.height)/2+4,0)
        responseTimeUnitsButton.setTitleColor(MYColor.veryDarkGrey, for: UIControlState())
        responseTimeUnitsButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayUnitButtonTouch(_:)), for: UIControlEvents.touchDown)
        responseTimeUnitsButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayUnitButtonEndTouchOutside(_:)), for: UIControlEvents.touchDragOutside)
        responseTimeUnitsButton.addTarget(self, action: #selector(DetailsViewController.onMinuteHourDayUnitButtonEndTouch(_:)), for: UIControlEvents.touchUpInside)
        clockButtonsParentView.addSubview(responseTimeUnitsButton)
    }*/
    
    func defineNowButton()-> UIButton {
        print("func defineNowButton is running...")
        let nowButton = UIButton(frame: CGRect(x: ScreenDimension.width*77.5/100, y: containerViewHeight*4/100, width: ScreenDimension.width*18.5/100, height: containerViewHeight*17/100))
        // define button y-center as equal to section label y-center
        nowButton.backgroundColor = UIColor.white
        nowButton.tag = 345
        nowButton.setTitleColor(UIColor.white, for: UIControlState())
        nowButton.setTitleColor(MYColor.midGrey, for: UIControlState())
        nowButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        nowButton.layer.cornerRadius = 2.25
        nowButton.setTitle("NOW", for: UIControlState())
        nowButton.addTarget(self, action: #selector(DetailsViewController.onNowButtonTouch(_:)), for: .touchUpInside)
        return nowButton
    }
    
    func onNowButtonTouch(_ sender: UIButton!) {
        print("func onNowButtonTouch is running...")
        let now = Date()
        let formatter = DateFormatter()
        //let calendar = Calendar.current
        formatter.dateFormat = "hh"
        hourButtonSelected = Int(formatter.string(from: now))!
        formatter.dateFormat = "mm"
        minuteButtonSelected = formatter.string(from: now) //"\(components.minute)"
        formatter.dateFormat = "a"
        let timeUnit = formatter.string(from: now)
        for minuteButton in minuteButtonsArray {
            minuteButton.isHidden = true
        }
        timeSelected(hourButtonSelected, minuteButtonSelected: minuteButtonSelected, timeUnit: timeUnit)
        sender.removeFromSuperview()
        clockButtonsParentView.removeFromSuperview()
        for subview in clockButtonsParentView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    func onClockUnitButtonTouch(_ sender: UIButton!) {
        print("func onClockUnitButtonTouch is running...")
        print(sender.titleLabel!.text!)
        if sender.titleLabel!.text! == "PM" {
            sender.setTitle("AM", for: UIControlState())
        }
        else {
            sender.setTitle("PM", for: UIControlState())
        }
    }
    
    func onClockHourButtonEndTouch(_ sender: UIButton!) {
        print("func onClockHourButtonEndTouch is running...")
        sender.setTitleColor(UIColor.black, for: UIControlState())
        hourButtonSelected = Int((sender.titleLabel?.text)!)!
        for hourButton in hourButtonsArray {
            hourButton.isHidden = true
        }
        for minuteButton in minuteButtonsArray {
            minuteButton.isHidden = false
        }
    }
    
    func onClockMinuteButtonEndTouch(_ sender: UIButton!) {
        print("func onClockMinuteButtonEndTouch is running...")
        sender.setTitleColor(UIColor.black, for: UIControlState())
        minuteButtonSelected = (sender.titleLabel?.text!)!
        let clockUnitButton = clockButtonsParentView.viewWithTag(508) as! UIButton
        let clockUnitButtonValue = clockUnitButton.titleLabel!.text!
        for minuteButton in minuteButtonsArray {
            minuteButton.isHidden = true
        }
        timeSelected(hourButtonSelected, minuteButtonSelected: minuteButtonSelected, timeUnit: clockUnitButtonValue)
        clockButtonsParentView.removeFromSuperview()
        if let nowButton = containerView.viewWithTag(345) as? UIButton {
            nowButton.removeFromSuperview()
        }
        for subview in clockButtonsParentView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    func onMinuteHourDayValueButtonTouch(_ sender: UIButton!) {
        print("func onMinuteHourDayValueButtonTouch is running...")
        sender.backgroundColor = MYColor.rallyBlue
        sender.setTitleColor(UIColor.white, for: UIControlState())
    }
    
    func onMinuteHourDayValueButtonEndTouchOutside(_ sender: UIButton!) {
        print("func onClockButtonEndTouchOutside is running...")
        sender.backgroundColor = UIColor.white
        sender.setTitleColor(MYColor.midGrey, for: UIControlState())
    }
    
    /*
    func onResponseTimeButtonEndTouch(_ sender: UIButton!) {
        print("func onResponseTimeButtonEndTouch is running...")
        sender.setTitleColor(UIColor.black, for: UIControlState())
        //var responseTime: Int!
        let responseTimeUnitsButton = clockButtonsParentView.viewWithTag(506) as! UIButton
        var responseTimeUnit = responseTimeUnitsButton.titleLabel!.text!
        switch responseTimeUnit {
        case "m":
            responseTimeUnit = "minutes"
            //responseTime = Int(sender.titleLabel!.text!)
        case "h":
            if sender.titleLabel!.text! != "1" {
                responseTimeUnit = "hours"
            }
            else {
                responseTimeUnit = "hour"
            }
            //responseTime = Int(sender.titleLabel!.text!)!*60
        case "d":
            if sender.titleLabel!.text! != "1" {
                responseTimeUnit = "days"
            }
            else {
                responseTimeUnit = "day"
            }
            //responseTime = Int(sender.titleLabel!.text!)!*60*24
        default:
            break
        }
        let responseTimeTitle = sender.titleLabel!.text!+" "+responseTimeUnit
        rallyDetailsButtons[1].setTitle(responseTimeTitle, for:  UIControlState())
        rallyDetailsButtons[1].titleLabel!.text! = responseTimeTitle // setting titleLabel text property is necessary for titleLabel frame to update for calculation below for redrawing button subviews
        //rallyObject.RallyResponseTime = responseTime // webservice expects rallyResponseTime in minutes unit
        clockButtonsParentView.removeFromSuperview()
        for subview in clockButtonsParentView.subviews {
            subview.removeFromSuperview()
        }
    }
    */
    
    func onMinuteHourDayUnitButtonTouch(_ sender: UIButton!) {
        print("func onMinuteHourDayUnitButtonTouch is running...")
        //sender.backgroundColor = MYColor.rallyBlue
        //sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }
    
    func onMinuteHourDayUnitButtonEndTouchOutside(_ sender: UIButton!) {
        print("func onClockButtonEndTouchOutside is running...")
        //sender.backgroundColor = UIColor.whiteColor()
        //sender.setTitleColor(MYColor.rallyBlue, forState: .Normal)
    }
    
    func onMinuteHourDayUnitButtonEndTouch(_ sender: UIButton!) {
        print("func onMinuteHourDayUnitButtonEndTouch is running...")
        //sender.setTitleColor(MYColor.rallyBlue, forState: .Normal)
        //sender.backgroundColor = UIColor.whiteColor()
        let minuteHourDayUnitButtonTitle = sender.titleLabel!.text!
        var responseTimeValuesArray: [String]!
        let responseTimeUnitsButton = clockButtonsParentView.viewWithTag(506) as! UIButton
        switch minuteHourDayUnitButtonTitle {
        case "m":
            sender.setTitle("h", for:  UIControlState())
            responseTimeValuesArray = ["1","2","4","6","8","12"]
            responseTimeUnitsButton.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0)
        case "h":
            sender.setTitle("d", for:  UIControlState())
            responseTimeValuesArray = ["1","2","3","5","7","14"]
            responseTimeUnitsButton.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0)
        case "d":
            sender.setTitle("m", for:  UIControlState())
            responseTimeValuesArray = ["20","25","30","40","45","50"]
            responseTimeUnitsButton.titleEdgeInsets = UIEdgeInsetsMake((responseTimeUnitsButton.frame.height-responseTimeUnitsButton.titleLabel!.frame.height)/2,0,(responseTimeUnitsButton.frame.height-responseTimeUnitsButton.titleLabel!.frame.height)/2+4,0)
        default:
            break
        }
        for i in 0..<6 { // response time button value options have tag values 50-55
            let responseTimeUnitsButton = clockButtonsParentView.viewWithTag(i+50) as! UIButton
            let responseTimeUnitsButtonTitle = responseTimeValuesArray[i]
            responseTimeUnitsButton.setTitle(responseTimeUnitsButtonTitle, for:  UIControlState())
        }
    }
    
    func timeSelected(_ hourButtonSelected: Int, minuteButtonSelected: String, timeUnit: String) { // clock time, not response time
        print("func timeSelected is running...")
        timeDetailView.textLabel.text = "\(hourButtonSelected):\(minuteButtonSelected) \(timeUnit)"
        timeDetailView.textLabel.sizeToFit()
        // this is the local start time in "h:mm a" string format
        dateHelperClass().updateTimeInRallyStartDateTime(rallyObject, hour: hourButtonSelected, minute: minuteButtonSelected, timeUnit: timeUnit)
        if rallyObject.isRallyActive == 1 {
            defineActiveRallyDefaultContainerView() // switchcontainerview auto-removes subview when any rally button is placed, so needs to be re-added
            // send rally details update to backend to update rally start time
            let intent = WebServiceCallIntent.updateRallyDetails(rallyObject)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
//time methods
    
    func updateLocationButtonSubviewLayoutWhenActive() {
        print("func updateLocationButtonSubviewLayoutWhenActive is running...")
        locationDetailView.frame.size.width = ScreenDimension.width*73/100
    }
    
// location methods
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("func searchBarShouldBeginEditing is running...")
        tableView.isHidden = false
        containerView.bringSubview(toFront: tableView)
        containerView.backgroundColor = UIColor.white
        AppDelegate.getAppDelegate().window!.addSubview(statusBarBackgroundColorView)
        view.bringSubview(toFront: searchController.searchBar)
        // tableView needs to be resized so best matches tableView extends all the way down to toolbar during animation
        let tableViewHeight = ScreenDimension.height-UIApplication.shared.statusBarFrame.height-UINavigationController().toolbar.frame.height-searchController.searchBar.frame.height
        tableView.frame.size.height = tableViewHeight
        self.navigationController?.navigationBar.layer.zPosition = -1
        UIView.animate(withDuration: 0.3,
            delay: 0.0,
            options: UIViewAnimationOptions.curveLinear,
            animations: {
                self.detailsButtonsParentView.frame.origin.y += UIApplication.shared.statusBarFrame.height-self.detailsButtonsParentView.frame.height
                self.containerView.frame.origin.y += UIApplication.shared.statusBarFrame.height-self.detailsButtonsParentView.frame.height
                searchBar.frame.origin.y += -self.detailsButtonsParentView.frame.height
            },
            completion: { finished in
                print("searchBarShouldBeginEditing:animateWithDuration:completion")
                self.containerView.frame.size.height += self.detailsButtonsParentView.frame.height
                        self.containerView.backgroundColor = MYColor.lightGrey
        })
        return true
    }
    
    func showSearchBar() {
        print("func showSearchBar is running...")
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.tag = 83
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.frame.origin.y = self.detailsButtonsParentView.frame.height
        self.definesPresentationContext = true // makes it so searchBar doesn't jump off page when clicked, provides layout context for it I believe
        view.addSubview(searchController.searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("func searchBarCancelButtonClicked is running...")
        mapItemArray.removeAll() // empty so there is no row text when best matches table appears prior to user typing in textfield to generate best matches
        tableView.reloadData()
        moveFromBestMatchesToDisplayMapInContainer()
    }
    
    func moveFromBestMatchesToDisplayMapInContainer() {
        print("func moveFromBestMatchesToDisplayMapInContainer is running...")
        searchController.hidesNavigationBarDuringPresentation = false
        view.frame.origin.y += 20
        self.detailsButtonsParentView.frame.origin.y += -20
        self.containerView.frame.origin.y += -20
        self.navigationController?.navigationBar.layer.zPosition = 0
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        delay(0.05) { // prevents uisearchBar from getting hung up and stay directly underneath navigation bar
            self.tableView.isHidden = true
            UIView.animate(withDuration: 0.3,
                delay: 0.0,
                options: UIViewAnimationOptions.curveLinear,
                animations: {
                    self.detailsButtonsParentView.frame.origin.y += self.detailsButtonsParentView.frame.height
                    self.containerView.frame.origin.y += self.detailsButtonsParentView.frame.height
                    self.searchController.searchBar.frame.origin.y += self.detailsButtonsParentView.frame.height
                },
                completion: { finished in
                    print("searchBarCancelButtonClicked:animateWithDuration:completion")
                    self.containerView.frame.size.height = self.containerViewHeight
                    self.searchController.hidesNavigationBarDuringPresentation = true
                    self.statusBarBackgroundColorView.removeFromSuperview()
                    self.searchController.searchBar.removeFromSuperview()
                    self.showSearchBar()
            })
        }
    }
    
    func searchResultHandler(_ mapItemArrayIndex: Int) {
        print("func searchResultHandler is running...")
        // matt neuburg pg 809
        // in future versions, add rallyLocation title property (eg Chipotle Mexican Grill)
        if mapItemArrayIndex > 0 { // offset by one bc first bestMatches table row is searchBar text rather than a fetched item from Apple maps API
            locationDetailView.textLabel.text = mapItemArray[mapItemArrayIndex-1].placemark.title!
            if locationDetailView.textLabel.text!.characters.count > 8 {
                locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 21)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 18)
                default:
                    print("Do nothing...")
                }
            }
            else {
                locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 22)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 20)
                default:
                    print("Do nothing...")
                }
            }
            updateLocationButtonSubviewLayoutWhenActive()
            pointAnnotation!.title = mapItemArray[mapItemArrayIndex-1].name // displays map item title overtop pin on mapView
            let coordinate: CLLocationCoordinate2D = mapItemArray[mapItemArrayIndex-1].placemark.location!.coordinate
            rallyObject.rallyLocation = mapItemArray[mapItemArrayIndex-1].placemark.title! // eg 123 Alphabet Lane, Yahoo, Iowa
            print("searchResultHandler:rallyObject.RallyLocation:\(rallyObject.rallyLocation)")
            pointAnnotation?.coordinate = coordinate
            let loc = coordinate
            let pt = MKMapPointForCoordinate(loc)
            let w = MKMapPointsPerMeterAtLatitude(loc.latitude)*1200 // multiply to get in meters
            mapView.visibleMapRect = MKMapRectMake(pt.x - w/2.0, pt.y - w/2.0, w*10, w*10)
            mapView.centerCoordinate = loc
            mapView.addAnnotation(pointAnnotation!)
            mapView.selectAnnotation(pointAnnotation!, animated: true)
        }
        else { // do not add map pin if first cell is selected (mapItemArrayIndex == 0), it is only a text row, there is no associated location
            locationDetailView.textLabel.text = "Current Location" // Take note that this line is different from above. it is adding search text as title instead of item title
            if locationDetailView.textLabel.text!.characters.count > 8 {
                locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 21)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 18)
                default:
                    print("Do nothing...")
                }
            }
            else {
                locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 22)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    locationDetailView.textLabel.font = UIFont.systemFont(ofSize: 20)
                default:
                    print("Do nothing...")
                }
            }
            updateLocationButtonSubviewLayoutWhenActive()
            pointAnnotation!.title = "Current Location" // displays map item title overtop pin on mapView
            
            if let updatedUserLocation = userLocationInstance { // saved class property instance exists for use
                rallyObject.rallyLocation = "Current Location" // eg 123 Alphabet Lane, Yahoo, Iowa
                print("searchResultHandler:rallyObject.rallyLocation:\(rallyObject.rallyLocation)")
                pointAnnotation?.coordinate = updatedUserLocation
                let pt = MKMapPointForCoordinate(updatedUserLocation)
                let w = MKMapPointsPerMeterAtLatitude(updatedUserLocation.latitude)*1200 // multiply to get in meters
                mapView.visibleMapRect = MKMapRectMake(pt.x - w/2.0, pt.y - w/2.0, w*10, w*10)
                mapView.centerCoordinate = updatedUserLocation
                mapView.addAnnotation(pointAnnotation!)
                mapView.selectAnnotation(pointAnnotation!, animated: true)
            }
        }
        //locationDetailView.textLabel.sizeToFit()
        rallyObject.latitude = CGFloat(pointAnnotation!.coordinate.latitude)
        rallyObject.longitude = CGFloat(pointAnnotation!.coordinate.longitude)
        if rallyObject.isRallyActive == 1 { // send rally details update to backend to update rally location
            let intent = WebServiceCallIntent.updateRallyDetails(rallyObject)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    // https://github.com/sachinkesiraju/SwiftExample/blob/master/SwiftExample/MapViewController.swift
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print("func mapView is running...")
        let pinView: MKPinAnnotationView = MKPinAnnotationView()
        pinView.annotation = annotation
        pinView.pinTintColor = UIColor.red
        pinView.animatesDrop = true
        pinView.canShowCallout = true
        return pinView
    }
    
    // UISearchController Delegate method
    func updateSearchResults(for searchController: UISearchController) {
        print("func updateSearchResultsForSearchController is running...")
        if Reachability.isConnectedToNetwork() {
            print("Internet connection OK")
            updateLocationForSearchBarValue()
        }
        else {
            print("Internet connection FAILED")
            // initialize alertview data model
            let errorData = WebServiceCallCheckpointData(customValidationType: .NetworkReachability)
            // initialize alertview
            let alertView = CustomUIWindowAlertview(dataObject: errorData)
            // display alertview
            alertView.show()
        }
    }
    
    // UISearchBar Delegate method
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("func searchBarSearchButtonClicked is running...")
        if tableView.isHidden == false { // this case is if user presses search after completing a term, this resultHandler will use the first result in the mapItemArray as value for map
            searchController.isActive = false
            mapItemArray.removeAll() // empty so there is no row text when best matches table appears prior to user typing in textfield to generate best matches
            tableView.reloadData()
            moveFromBestMatchesToDisplayMapInContainer()
        }
    }
    
    func countTime(_ timer: Timer) {
        print("func countTime is running...")
        timeSince += 1
    }
    
    func updateLocationForSearchBarValue() { // updateLocationForSearchBarValue fetches a placemark(s) which is a different representation of the address the user has typed. this is used to define the region in (localSearchRequest userLocation). So, when updateBestMatches executes localSearch.startWithCompletionHandler, it has the searchTerm and the region to search for the search term within
        // can easily improve this method in future http://novaera.fr/wordpress/a-concrete-example-of-the-guard-statement-in-swift-2/
        print("func updateLocationForSearchBarValue is running...")
        let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
        if let _ = timeSinceMethodLastExecuted {
        }
        else { // if timeRemainingRallyTimer doesn't exist, schedule timer
            timeSinceMethodLastExecuted = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetailsViewController.countTime(_:)), userInfo: nil, repeats: true)
        }
        if timeSince < 5 { // if been less than 5 seconds, don't update location since user is likely still just typing in search bar, bc updating location is slow and creates lag in updating table of results for user search term
            if let searchTerm = self.searchController.searchBar.text { // if there is text, use as search term for localSearch call
                self.localSearchRequest.naturalLanguageQuery = searchTerm
            }
            // check if location has already been initialized, if not go create
            if let updatedUserLocation = userLocationInstance { // saved class property instance exists for use
                self.localSearchRequest.region = MKCoordinateRegion(
                center: updatedUserLocation,
                span: span)
                self.localSearch = MKLocalSearch(request: self.localSearchRequest)
                self.updateBestMatches()
            }
            else { // if no saved temp user location, need to find user location and then search around user for locations best matching search bar text
                updateLocationMethodFlag = "withSearchBarSearchTerm"
                permissionsHelperClass.userLocation = nil
                permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocalSearchRequestRegion is listening for within detailsVC which updates localSearchRequest userLocation property
            }
        }
        else { // been more than 5 seconds, don't use temp location, update temp location
            updateLocationMethodFlag = "withSearchBarSearchTerm"
            permissionsHelperClass.userLocation = nil
            permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocalSearchRequestRegion is listening for within detailsVC which updates localSearchRequest userLocation property
        }
    }
    
    func updateBestMatches() {
        print("func updateBestMatches is running...")
        // fetch new best matches using search term and user's location. these properties of localSearch are updated in updateLocationForSearchBarValue method
        localSearch.start { (localSearchResponse, error) -> Void in
            if let _ = localSearchResponse {
                self.pointAnnotation!.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
                // print("updateBestMatches:self.mapItemArray.count:\(self.mapItemArray.count)")
                let numberOfCurrentItems = self.mapItemArray.count+1 // +1 is for first row representing searchBar text instead of a fetched item from Apple maps API
                print("updateBestMatches:numberOfCurrentItems:\(numberOfCurrentItems)")
                let numberOfNewItems = localSearchResponse!.mapItems.count
                print("updateBestMatches:numberOfNewItems:\(numberOfNewItems)")
                var numberOfItemsToRemove = 0 // default 0
                if numberOfCurrentItems+numberOfNewItems > 11 {
                    numberOfItemsToRemove = numberOfCurrentItems+numberOfNewItems-11
                }
                print("updateBestMatches:numberOfItemsToRemove:\(numberOfItemsToRemove)")
                for _ in 0..<numberOfItemsToRemove {
                    let removeAtIndex = numberOfCurrentItems-numberOfItemsToRemove-1 // off +1 added in counting the first row
                    self.mapItemArray.remove(at: removeAtIndex)
                }
                //print("updateBestMatches:localSearchResponse!.mapItems:\(localSearchResponse!.mapItems)")
                var currentMapItemsToKeep: [MKMapItem] = localSearchResponse!.mapItems
                //print("mapItemArray.count:\(mapItemArray.count)")
                currentMapItemsToKeep.append(contentsOf: self.mapItemArray)
                self.mapItemArray = currentMapItemsToKeep
                print("updateBestMatches:self.mapItemArray.count:\(self.mapItemArray.count)")
                self.tableView.reloadData()
                // set tableView height to meet with keyboard height for easy scrolling calculations, need to do here bc keyboardHeight is set in keyboardWillShow and doesn't run reliably on searchBar click so must be done on second pass
                let tableViewHeight = ScreenDimension.height-UIApplication.shared.statusBarFrame.height-self.searchController.searchBar.frame.height
                self.tableView.frame.size.height = tableViewHeight-self.keyboardHeight
                self.tableView.contentSize.height = CGFloat(self.mapItemArray.count+1)*self.tableView.rowHeight // update tableView content size, +1 accounts for first row being searchBar text as opposed to an item in mapItemArray
            }/*
            else if self.searchController.searchBar.text!.characters.count == 0 { // if searchBar text is cleared, need to clear table and move scrollView back to top
                print("self.searchController.searchBar.text?.characters.count:\(self.searchController.searchBar.text!.characters.count)")
                self.mapItemArray.removeAll()
                let indexPath = NSIndexPath(forItem: 0, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: false)
                self.tableView.reloadData()
                self.tableView.contentSize.height = self.tableView.frame.height // set to match, so the user cannot scroll since there are no matches in table to scroll
            }*/
        }
    }
    
    func defineBestMatchesTableView() {
        print("func defineBestMatchesTableView is running...")
        tableView = UITableView()
        tableView.tag = 78
        let tableViewHeight = ScreenDimension.height-UIApplication.shared.statusBarFrame.height-UINavigationController().toolbar.frame.height-searchController.searchBar.frame.height
        // initial height fits only searchBar. on searchBar click, height is changed to show "best matches" row options
        tableView.frame = CGRect(x: 0, y: searchController.searchBar.frame.height, width: ScreenDimension.width, height: tableViewHeight)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.rowHeight = tableViewHeight/8
        containerView.addSubview(tableView)
        tableView.isHidden = true
        self.localSearchRequest = MKLocalSearchRequest()
        self.pointAnnotation = MKPointAnnotation()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("func tableView:numberOfRowsInSection is running...")
        return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("func tableView:cellForRowAtIndexPath is running...")
        // https://gist.github.com/jquave/8ca03aad33490a2ffa73
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        cell.backgroundColor = UIColor.white
        cell.textLabel?.font = UIFont.systemFont(ofSize: 19)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
        default:
            print("Do nothing...")
        }
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13)
        default:
            print("Do nothing...")
        }
        cell.detailTextLabel?.textColor = MYColor.darkGrey
        print("indexPath.row:\((indexPath as NSIndexPath).row)")

        print("mapItemArray.count:\(mapItemArray.count)")

        if mapItemArray.count>=(indexPath as NSIndexPath).row { // are there any items to display in best matches. if any text has been typed, there will always be at least one option meaning there is no need to also check for searchBar text for displaying searchBar text in first row
            if (indexPath as NSIndexPath).row<11 && (indexPath as NSIndexPath).row>0 { // no more than 10 items. first row is not a best match. instead, it is the text typed itself
                cell.textLabel?.text = self.mapItemArray[(indexPath as NSIndexPath).row-1].name!
                cell.detailTextLabel?.text = self.mapItemArray[(indexPath as NSIndexPath).row-1].placemark.title!
                cell.isUserInteractionEnabled = true
            }
        }
        else { // if cell does not contain a location to choose, do not allow user to click the empty cell
            cell.isUserInteractionEnabled = false
            cell.textLabel?.text = nil // in case where user refocuses on searchbar, initial cell title value should be empty
            cell.detailTextLabel?.text = nil
        }
        if (indexPath as NSIndexPath).row == 0 { // add searchBar text as first row's title
            cell.textLabel?.text = "Current Location"
            cell.detailTextLabel?.text = nil
            cell.isUserInteractionEnabled = true
        }
        print("func tableView:cellForRowAtIndexPath is finished...")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("func tableView:didSelectRowAtIndexPath is running...")
        print("self.mapView.annotations.count:\(self.mapView.annotations.count)")
        if Reachability.isConnectedToNetwork() {
            print("Internet connection OK")
            if self.mapView.annotations.count != 0 { // remove old pin on map so new pin of selected location may be added
                annotation = self.mapView.annotations[0]
                self.mapView.removeAnnotation(annotation)
            }
            searchResultHandler((indexPath as NSIndexPath).row) // this tells the searchResultHandler in the case of user selecting a best match suggestion for location, which item in mapItemArray to place on map
            searchController.isActive = false
            mapItemArray.removeAll() // empty so there is no row text when best matches table appears prior to user typing in textfield to generate best matches
            tableView.reloadData()
            moveFromBestMatchesToDisplayMapInContainer()
        }
        else {
            print("Internet connection FAILED")
            // initialize alertview data model
            let errorData = WebServiceCallCheckpointData(customValidationType: .NetworkReachability)
            // initialize alertview
            let alertView = CustomUIWindowAlertview(dataObject: errorData)
            // display alertview
            alertView.show()
        }
    }
// location methods
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        print("func dismissKeyboard is running...")
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //http://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
        view.endEditing(true)
    }
    
    // textView
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("func textViewDidBeginEditing is running...")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        RallyTitleTextView.textViewDidBeginEditing(textView)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("func textViewDidEndEditing is running...")
        if let rallyName = editableRallyDescription.text { // i think this can be updated on didSet for editableRallyDescription, look into later
            rallyObject.rallyName = rallyName
        }
        for recognizer in self.view.gestureRecognizers! {
            self.view.removeGestureRecognizer(recognizer)
        }
        RallyTitleTextView.textViewDidEndEditing(textView)
        let currentText = editableRallyDescription.text
        let size = editableRallyDescription.sizeThatFits(CGSize(width: ScreenDimension.width*78/100,  height: CGFloat(Float.greatestFiniteMagnitude)))
        editableRallyDescription.frame.size = size
        editableRallyDescription.text = currentText
        editableRallyDescription.frame.size.width = ScreenDimension.width*78/100
        editableRallyDescription.center.y = ScreenDimension.heightMinusStatusNavigationAndToolbar*6.75/100
        editableRallyDescription.center.x = ScreenDimension.width/2
        if rallyObject.isRallyActive == 1 { // make request to update rally details
            let intent = WebServiceCallIntent.updateRallyDetails(rallyObject)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        // determine whether group name is too long. if characters<45, user may add additional or delete characters. If 45 characters, user may delete characters
        // replacementText = text
        let combinedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            // could return true right here since backspacing is always fine
            print("textView:shouldChangeTextInRange text is empty, it's a backspace")
            updateTextViewSize(combinedText)
            return true
        }
        if combinedText.characters.count < 45 { // 45 is maximum number of characters group name may be
            // still need to check if new text would create a third line
        }
        else if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            // could return true right here since backspacing is always fine
            print("textView:shouldChangeTextInRange text is empty, it's a backspace")
            updateTextViewSize(combinedText)
            return true
        }
        else { // if user tries entering 26th digit, returns false
            print("textView:shouldChangeTextInRange:false reached character limit")
            return false // don't go any further, user already reached max character limit
        }
        // Create attributed version of the text
        let attributedText = NSMutableAttributedString(string: combinedText)
        attributedText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedText.length))
        // may be error if font parameter is nil above
        // Get the padding of the text container
        let padding = textView.textContainer.lineFragmentPadding
        // Create a bounding rect size by subtracting the padding
        // from both sides and allowing for unlimited length
        let boundingSize = CGSize(width: textView.frame.size.width - padding * 2, height: CGFloat.greatestFiniteMagnitude)
        // Get the bounding rect of the attributed text in the
        // given frame
        let boundingRect = attributedText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        // Compare the boundingRect plus the top and bottom padding
        // to the text view height; if the new bounding height would be
        // less than or equal to the text view height, append the text
        if(text == "\n") {
            //Dismiss keyboard when return key is pressed
            textView.resignFirstResponder()
            return false // if someone presses next row button, quick-easy solution is just to end editing. may consider just making it do nothing in the future if going to third line and allowing it to work going to a second line but that's for a later discussion
        }
        if (boundingRect.size.height + padding * 2 > textView.frame.size.height){ // if true, this means a new line is needed
            print("textView:shouldChangeTextInRange: true boundingRect.size.height + padding * 2 > textView.frame.size.height")
            if textView.font!.lineHeight*2>=boundingRect.size.height { // if true, this means it's not a third line
                updateTextViewSize(combinedText)
                return true
            }
            else { // a third line is being asked for but don't allow a third line
                return false
            }
        }
        else { // a new line is not needed, so allow new characters
            return true
        }
    }
    // textView
    
    func updateTextViewSize(_ combinedText: String) {
        print("func updateTextViewSize is running...")
        let currentText = editableRallyDescription.text
        let size = editableRallyDescription.sizeThatFits(CGSize(width: ScreenDimension.width*78/100,  height: CGFloat(Float.greatestFiniteMagnitude)))
        editableRallyDescription.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        editableRallyDescription.text = combinedText
        editableRallyDescription.frame.size.width = ScreenDimension.width*78/100
        editableRallyDescription.center.y = ScreenDimension.heightMinusStatusNavigationAndToolbar*6.75/100
        editableRallyDescription.center.x = ScreenDimension.width/2
        editableRallyDescription.text = currentText
    }
    
    func defineUpperToolbarBorder(_ respondToInviteButton: UIButton)->CALayer {
        print("func defineUpperToolbarBorder is running...")
        // define upper border sublayer
        let border2 = CALayer()
        let width2 = CGFloat(3.5)
        border2.borderColor = MYColor.darkGrey.cgColor
        border2.frame = CGRect(x: 0, y: 0, width:  respondToInviteButton.frame.size.width, height: respondToInviteButton.frame.size.height*1/100)
        border2.borderWidth = width2
        return border2
        // sets blue background color using layers, don't use until proven necessary
        /*
        // define background sublayer
        let border3 = CALayer()
        border3.borderColor = MYColor.rallyBlue.CGColor
        border3.backgroundColor = MYColor.rallyBlue.CGColor
        border3.frame = CGRect(x: 0, y: border2.frame.height, width:  respondToInviteButton.frame.size.width, height: respondToInviteButton.frame.size.height-border2.frame.height)
        respondToInviteButton.layer.addSublayer(border3)
        // re-order sublayers so textlabel is on top
        let textLabelLayerReference = respondToInviteButton.titleLabel!.layer
        textLabelLayerReference.removeFromSuperlayer()
        respondToInviteButton.layer.insertSublayer(textLabelLayerReference, above: border3)
        respondToInviteButton.layer.addSublayer(border2)*/
    }
    
    func defineDeclineButton() {
        print("func defineDeclineButton is running...")
        for childView in (self.navigationController?.toolbar.subviews)! { // remove send rally now button to add decline and join button
            childView.removeFromSuperview()
        }
        let declineButton = UIButton(type: .custom) // .System depending on button click animation preference
        declineButton.titleLabel!.font = UIFont.systemFont(ofSize: 15)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            declineButton.titleLabel!.font = UIFont.systemFont(ofSize: 13)
            case .iPhone6, .iPhone6S:
            declineButton.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        default:
            print("Do nothing...")
        }
        declineButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width/2, height: (self.navigationController?.toolbar.frame.height)!)
        declineButton.backgroundColor = MYColor.rallyBlue
        declineButton.setTitle("No Thanks", for: UIControlState())
        declineButton.setTitleColor(UIColor.white, for: UIControlState())
        declineButton.addTarget(self, action: #selector(DetailsViewController.onDeclineButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        self.navigationController?.toolbar.addSubview(declineButton)
        //rallyButton.setTitle("Send Now!", forState: UIControlState.Normal)
        //rallyButton.addSubview(defineTimerLabel())
        let border = CALayer()
        let width = CGFloat(0.3125)
        border.borderColor = MYColor.lightGrey.cgColor
        border.frame = CGRect(x: declineButton.frame.size.width - width, y: 0, width:  declineButton.frame.size.width, height: declineButton.frame.size.height)
        border.borderWidth = width
        declineButton.layer.addSublayer(border)
        declineButton.layer.masksToBounds = true
        // add top border
        let topBorder = defineUpperToolbarBorder(declineButton)
        declineButton.layer.addSublayer(topBorder)
    }
    
    func onDeclineButtonTouch(_ sender: UIButton!) {
        //return to main screen, cache selected friends
        print("func onDeclineButtonTouch is running...")
        let intent = WebServiceCallIntent.updateDidUserAccept(rallyObject, 0)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController!.popToRootViewController(animated: false)
                })
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func defineJoinButton() {
        print("func defineJoinButton is running...")
        let joinButton = UIButton(type: .custom) //.System depending on button click animation preference
        joinButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            joinButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        case .iPhone6, .iPhone6S:
            joinButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        default:
            print("Do nothing...")
        }
        joinButton.titleLabel?.lineBreakMode = .byTruncatingTail
        joinButton.frame = CGRect(x: ScreenDimension.width/2, y: 0, width: ScreenDimension.width/2, height: (self.navigationController?.toolbar.frame.height)!)
        joinButton.backgroundColor = MYColor.rallyBlue
        joinButton.layer.masksToBounds = true
        joinButton.setTitle("Join", for: UIControlState())
        joinButton.setTitleColor(UIColor.white, for: UIControlState())
        joinButton.addTarget(self, action: #selector(DetailsViewController.onJoinButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        self.navigationController?.toolbar.addSubview(joinButton)
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = MYColor.lightGrey.cgColor
        border.frame = CGRect(x: width - joinButton.frame.size.width, y: 0, width:  joinButton.frame.size.width, height: joinButton.frame.size.height)
        border.borderWidth = width
        joinButton.layer.addSublayer(border)
        joinButton.layer.masksToBounds = true
        // add top border
        let topBorder = defineUpperToolbarBorder(joinButton)
        joinButton.layer.addSublayer(topBorder)
    }
    
    func onJoinButtonTouch(_ sender: UIButton!) {
        print("func onJoinButtonTouch is running...")
        let intent = WebServiceCallIntent.updateDidUserAccept(rallyObject, 1)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    let rallyHasTippedValue = self.rallyObject.rallyHasTipped()
                    if rallyHasTippedValue {
                        let moveToMessagingViewController: MessagingViewController = MessagingViewController(nibName: nil, bundle: nil)
                        moveToMessagingViewController.rallyObject = self.rallyObject
                        self.navigationController!.pushViewController(moveToMessagingViewController, animated: true)
                    }
                    else { // update toolbar button to 'go to messaging' and interaction state. container view starts with timer for anyone joining rally so no need to add
                        self.defineGoToMessagingButton()
                    }
                })
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func defineGoToMessagingButton() {
        print("func defineGoToMessagingButton is running...")
        for childView in (self.navigationController?.toolbar.subviews)! { // keep toolbar empty for future any future view loads using toolbar
            childView.removeFromSuperview()
        }
        goToMessagingButton = UIButton(type: .custom)
        goToMessagingButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            goToMessagingButton.titleLabel!.font = UIFont.systemFont(ofSize: 16)
        case .iPhone6, .iPhone6S:
            goToMessagingButton.titleLabel!.font = UIFont.systemFont(ofSize: 17)
        default:
            print("Do nothing...")
        }
        goToMessagingButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: (self.navigationController?.toolbar.frame.height)!)
        goToMessagingButton.backgroundColor = MYColor.rallyBlue
        goToMessagingButton.setTitle("Go to Messaging", for: UIControlState())
        goToMessagingButton.setTitleColor(UIColor.white, for: UIControlState())
        goToMessagingButton.addTarget(self, action: #selector(DetailsViewController.onGoToMessagingButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        self.navigationController?.toolbar.addSubview(goToMessagingButton)
    }
    
    func onGoToMessagingButtonTouch(_ sender: UIButton!) {
        print("func onJoinButtonTouch is running...")
        let moveToMessagingViewController: MessagingViewController = MessagingViewController(nibName: nil, bundle: nil)
        moveToMessagingViewController.rallyObject = self.rallyObject
        self.navigationController!.pushViewController(moveToMessagingViewController, animated: true)
    }
    
    func removeContainerViewSubviews() { // use generic to pass in parentView to use this as a global func in future to remove all subviews from any superview
        print("func removeContainerViewSubviews is running...")
        for childView in containerView.subviews {
            childView.removeFromSuperview()
        }
        if let searchBar = view.viewWithTag(83) { // removes searchBar which is supposed to be a subview of containerView from a UX perspective but to get searchBar textfield focus animation working properly, required making subview of view
            searchBar.removeFromSuperview()
        }
        // for case of moving off mapview to another view, nils out timer counting to avoid fetching user location over and over so it isn't counting in the background while not on mapview
        if let _ = timeSinceMethodLastExecuted {
            timeSinceMethodLastExecuted!.invalidate()
            timeSinceMethodLastExecuted = nil
        }
    }
}
