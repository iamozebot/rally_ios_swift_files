//
//  DullingView.swift
//  Rally
//
//  Created by Ian Moses on 5/7/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class DullingViewClass {
    static func defineDullingView(_ alphaValue: CGFloat)->UIView {
        print("func defineDullingView is running...")
        let dullingViewForViewControllerView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: ScreenDimension.height))
        
        //let dullingViewForNavBar = UIView(frame: CGRectMake(0, 0, screenDimension.screenWidth, UINavigationBarTaller.navigationBarHeight+UIApplication.sharedApplication().statusBarFrame.height))
        //let toolBarHeight = UINavigationController().toolbar.frame.height
        //let dullingViewForToolbar = UIView(frame: CGRectMake(0, screenDimension.screenHeight-toolBarHeight, screenDimension.screenWidth, toolBarHeight))
        dullingViewForViewControllerView.tag = 165
        let rgb: RGB = (red: 0.0, green: 0.0, blue: 0.0, alpha: alphaValue)
        var hsb = colorHelperClass().rgb2hsv(rgb)
        hsb.saturation = 0.5
        let rgb2 = colorHelperClass().hsv2rgb(hsb)
        dullingViewForViewControllerView.backgroundColor = UIColor(red: rgb2.red, green: rgb2.green, blue: rgb2.blue, alpha: rgb2.alpha)
        return (dullingViewForViewControllerView)
    }
    
    /*
    static func defineTapToDismissDullingView()->(UIView,UIView) {
        print("func defineTapToDismissDullingView is running...")
        let dullingViewForViewControllerView = UIView(frame: CGRectMake(0, 0, screenDimension.screenWidth, screenDimension.screenHeightMinusStatusAndNavigationBar))
        let dullingViewForNavBar = UIView(frame: CGRectMake(0, 0, screenDimension.screenWidth, UINavigationBarTaller.navigationBarHeight+UIApplication.sharedApplication().statusBarFrame.height))
        dullingViewForViewControllerView.tag = 165
        dullingViewForNavBar.tag = 165
        let rgb: RGB = (red: 0.0, green: 0.0, blue: 0.0, alpha: 0.47)
        var hsb = colorHelperClass().rgb2hsv(rgb)
        hsb.saturation = 0.5
        let rgb2 = colorHelperClass().hsv2rgb(hsb)
        dullingViewForViewControllerView.backgroundColor = UIColor(red: rgb2.red, green: rgb2.green, blue: rgb2.rallyBlue, alpha: rgb2.alpha)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissMenu")
        dullingViewForViewControllerView.addGestureRecognizer(tap)
        dullingViewForNavBar.addGestureRecognizer(tap)
        return (dullingViewForViewControllerView,dullingViewForNavBar)
        
        
        view.addSubview(dullingViewForViewControllerView)
    }
    
    static func dismissMenu() {
        print("func defineNewGroupTitleTextViewDullingBackView is running...")

    }*/
}
