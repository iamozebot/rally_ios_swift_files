//
//  EditProfileViewController.swift
//  Rally
//
//  Created by Ian Moses on 9/28/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class EditProfileViewControllerDataSource {
    let instructionText: String = "Enter your name and add an optional profile picture"
    let firstNamePlaceholderText = "First name (for invites)"
    let lastNamePlaceholderText = "Last name"
}

class EditProfileViewController: UIViewController, UITextViewDelegate {
    var editProfileData = EditProfileViewControllerDataSource()
    var editButton = UIButton()
    var profilePictureImageView = UIImageView()
    var headerInstructionLabel = UILabel()
    var CameraPhotoLibraryHelper: CameraPhotoLibrary?
    var firstNameTextView = EnterNameTextViewSubclass()
    var lastNameTextView = EnterNameTextViewSubclass()
    var firstNameUISuperview = UIView()
    var lastNameUISuperview = UIView()
    
    override func viewDidLoad() {
        print("class EditProfileViewController, func viewDidLoad is running...")
        WebCallStatus.editProfileViewControllerLoaded = 1
        // initialize view
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white

        // initialize nav bar
        // hide default navigation controller provided back button
        navigationItem.setHidesBackButton(true, animated: false)
        // set nav bar title
        navigationItem.title = "Edit Profile"
        // set right nav bar button
        let rightBarButtonItem = UIBarButtonItem(title:"Done", style: .plain, target: self, action: #selector(EditProfileViewController.onDoneButtonTouch(_:)))
        navigationItem.rightBarButtonItem = rightBarButtonItem
        // http://stackoverflow.com/questions/11572372/modifying-uisearchbar-cancel-button-font-text-color-and-style
        let doneButtonAttributes: NSDictionary = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(doneButtonAttributes as? [String : AnyObject], for: .normal)
        
        //navigationItem.rightBarButtonItem.title.
        // set style so title is white
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        // initialize subviews
        defineProfilePictureImageView()
        defineEditButton()
        defineHeaderInstructionLabel()
        defineFirstNameTextView()
        defineLastNameTextView()
        defineFirstNameUISuperview()
        defineLastNameUISuperview()

        // add subviews to view
        view.addSubview(profilePictureImageView)
        view.addSubview(editButton)
        view.addSubview(headerInstructionLabel)
        view.addSubview(firstNameUISuperview)
        view.addSubview(lastNameUISuperview)
        firstNameUISuperview.addSubview(firstNameTextView)
        lastNameUISuperview.addSubview(lastNameTextView)
        
        // set interaction state and tint color
        updateDoneButtonInteractionState()
        
        // setup for text view placeholder text
        // create the textView
        applyPlaceholderStyle(aTextview: firstNameTextView, placeholderText: editProfileData.firstNamePlaceholderText)
        applyPlaceholderStyle(aTextview: lastNameTextView, placeholderText: editProfileData.lastNamePlaceholderText)
        // set this class as the delegate so we can handle events from the textView
        firstNameTextView.delegate = self
        lastNameTextView.delegate = self
        // set firstNameTextView as first responder
        firstNameTextView.becomeFirstResponder()
        moveCursorToStart(aTextView: firstNameTextView)
    }
    
    func updateDoneButtonInteractionState() {
        print("func updateDoneButtonInteractionState is running...")
        var isButtonActive = true
        for textView in [firstNameTextView, lastNameTextView] {
            if textView.textColor == MYColor.darkGrey {
                isButtonActive = false
            }
        }
        var doneButtonAttributes: NSDictionary!
        if isButtonActive {
            navigationItem.rightBarButtonItem?.tintColor = .white
            doneButtonAttributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        }
        else {
            doneButtonAttributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18), NSForegroundColorAttributeName: MYColor.greyBlue]
        }
        navigationItem.rightBarButtonItem?.isEnabled = isButtonActive
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(doneButtonAttributes as? [String : AnyObject], for: .normal)
    }
    
    func defineHeaderInstructionLabel() {
        print("func defineHeaderInstructionLabel is running...")
        let xOffset: CGFloat = profilePictureImageView.frame.origin.x+profilePictureImageView.frame.width+ScreenDimension.width*4/100
        headerInstructionLabel.frame.size = CGSize(width: ScreenDimension.width-xOffset,height: CGFloat.greatestFiniteMagnitude)
        headerInstructionLabel.text = editProfileData.instructionText
        headerInstructionLabel.lineBreakMode = .byWordWrapping
        headerInstructionLabel.numberOfLines = 2
        headerInstructionLabel.textColor = MYColor.lightBlack
        headerInstructionLabel.font = .systemFont(ofSize: 16.15)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            headerInstructionLabel.font = .systemFont(ofSize: 14.75)
        default:
            print("I am not equipped to handle this device")
        }
        headerInstructionLabel.sizeToFit()
        let yOrigin = profilePictureImageView.frame.origin.y+profilePictureImageView.frame.height/4
        headerInstructionLabel.frame.origin = CGPoint(x: xOffset,y: yOrigin)
    }
    
    func defineProfilePictureImageView() {
        print("func defineProfilePictureImageView is running...")
        let itemSideLength = ScreenDimension.width*17.7/100
        profilePictureImageView.frame = CGRect(x: ScreenDimension.width*6/100, y: ScreenDimension.width*7/100, width: itemSideLength, height: itemSideLength)
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.frame.height/2
        //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        profilePictureImageView.clipsToBounds = true
        profilePictureImageView.tag = 159
        let profilePicture: UIImage? = MYUser.getProfileImage()
        profilePictureImageView.image = profilePicture
        let handleProfilePictureViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(EditProfileViewController.onEditButtonTouch(_:)))
        profilePictureImageView.addGestureRecognizer(handleProfilePictureViewTap)
    }
    
    
    func defineFirstNameUISuperview() {
        print("func defineFirstNameUISuperview is running...")
        // define borders
        // define top border
        firstNameUISuperview.frame.size.width = firstNameTextView.frame.width
        firstNameUISuperview.frame.size.height = firstNameTextView.frame.height*103/100
        let topBorder = nameTextViewBorder(withYOffset: 0)
        firstNameUISuperview.layer.addSublayer(topBorder)
        // define bottom border
        let bottomBorder = nameTextViewBorder(withYOffset: firstNameUISuperview.frame.height*98.5/100)
        firstNameUISuperview.layer.addSublayer(bottomBorder)
        // set lastNameTextView y-origin
        firstNameUISuperview.frame.origin = CGPoint(x: ScreenDimension.width*7/100,y: ScreenDimension.height*22/100)
        firstNameTextView.frame.origin.y += topBorder.frame.height
    }
    
    func defineLastNameUISuperview() {
        print("func defineLastNameUISuperview is running...")
        lastNameUISuperview.frame.size = firstNameUISuperview.frame.size
        // define bottom border
        let bottomBorder = nameTextViewBorder(withYOffset: lastNameUISuperview.frame.height*98.5/100)
        lastNameUISuperview.layer.addSublayer(bottomBorder)
        lastNameUISuperview.frame.size.height = lastNameTextView.frame.height+bottomBorder.frame.height
        // set lastNameTextView y-origin
        let yOrigin = firstNameUISuperview.frame.origin.y+firstNameUISuperview.frame.height
        lastNameUISuperview.frame.origin = CGPoint(x:firstNameUISuperview.frame.origin.x,y:yOrigin)
    }
    
    func defineFirstNameTextView() {
        print("func defineFirstNameTextView is running...")
        // initialize textview
        firstNameTextView.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width*86/100, height: ScreenDimension.height*9.25/100)
        
        // set text
        firstNameTextView.textColor = MYColor.darkGrey
        firstNameTextView.font = UIFont.systemFont(ofSize: 18)
    }
    
    func defineLastNameTextView() {
        print("func defineLastNameTextView is running...")
        // initialize textview
        lastNameTextView.frame = firstNameTextView.frame
        
        // set text
        lastNameTextView.textColor = MYColor.darkGrey
        lastNameTextView.font = UIFont.systemFont(ofSize: 18)
    }
    
    func nameTextViewBorder(withYOffset yOffset: CGFloat)-> CALayer {
        print("func nameTextViewBorder is running...")
        let borderWidth = firstNameUISuperview.frame.width
        let borderHeight = firstNameUISuperview.frame.height*1.5/100
        let borderXOffset: CGFloat = 0.0
        let borderColor = MYColor.lightGrey.cgColor
        let border = CALayer()
        border.backgroundColor = borderColor
        border.frame = CGRect(x: borderXOffset, y: yOffset, width:  borderWidth, height: borderHeight)
        return border
    }
    
    func onDoneButtonTouch(_ sender: UIButton!) {
        print("func onEditButtonTouch is running...")
        if let _ = MYUser.GCMToken {
            // make request to create user in database
            guard let firstName = firstNameTextView.text else {
                return
            }
            guard let lastName = lastNameTextView.text else {
                return
            }
            MYUser.firstName = firstName
            MYUser.lastName = lastName
            let intent = WebServiceCallIntent.updateUserNames(firstName: firstName, lastName: lastName)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    // update local file for user first and last name
                    NSUserDefaultsClass.updateUser(firstNameKey: firstName)
                    NSUserDefaultsClass.updateUser(lastNameKey: lastName)
                    NSUserDefaultsClass.updateUserNameKey() // sets NSUserDefaults key "userNameKey" to true
                    let homeContainerViewControllerInstance: ContainerViewController = ContainerViewController(type: .home)
                    homeContainerViewControllerInstance.previousViewControllerIsTypeEditProfileViewController = true
                    self.navigationController!.setViewControllers([homeContainerViewControllerInstance], animated: false)
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    func defineEditButton() {
        print("func onEditButtonTouch is running...")
        //frame: CGRect(x: ScreenDimension.screenWidth*77.5/100, y: containerViewHeight*4/100, width: ScreenDimension.screenWidth*18.5/100, height: containerViewHeight*17/100)
        // define button y-center as equal to section label y-center
        editButton.tag = 169
        editButton.setTitleColor(MYColor.midGrey, for: UIControlState())
        editButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        editButton.setTitle("Edit", for: UIControlState())
        editButton.addTarget(self, action: #selector(EditProfileViewController.onEditButtonTouch(_:)), for: .touchUpInside)
        editButton.sizeToFit()
        editButton.frame.origin.y = profilePictureImageView.frame.origin.y + profilePictureImageView.frame.height
        editButton.center.x = profilePictureImageView.center.x
    }
    
    func handleProfilePictureViewTap(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func handleProfilePictureViewTap is running...")
        displayCameraOptions()
    }
    
    func onEditButtonTouch(_ sender: UIButton!) {
        print("func onEditButtonTouch is running...")
        displayCameraOptions()
    }
    
    func displayCameraOptions() {
        print("func displayCameraOptions is running...")
        CameraPhotoLibraryHelper = CameraPhotoLibrary()
        CameraPhotoLibraryHelper!.displayCameraAndPhotosActionSheet(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class EditProfileViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        // for taking pictures to reload new image data when re-entering editProfileVC
        reloadView()
        navigationController?.setToolbarHidden(true, animated: animated)
        startObservingKeyboardEvents()
    }
    
    func startObservingKeyboardEvents() {
        print("func startObservingKeyboardEvents is running...")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(EditProfileViewController.keyboardWillShow),
                                               name:NSNotification.Name.UIKeyboardWillShow,
                                               object:nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(EditProfileViewController.keyboardWillHide),
                                               name:NSNotification.Name.UIKeyboardWillHide,
                                               object:nil)
    }
    
    func stopObservingKeyboardEvents() {
        print("func stopObservingKeyboardEvents is running...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func reloadView() {
        print("func reloadView is running...")
        // update profilePictureView with updated profile picture
        profilePictureImageView.image = MYUser.getProfileImage()
        // reload display to reflect updated profile picture
        view.setNeedsDisplay()
    }

    override func viewWillDisappear(_ animated: Bool) {
        print("class EditProfileViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        stopObservingKeyboardEvents()
    }
    
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String) {
        print("func applyPlaceholderStyle is running...")
        // make it look (initially) like a placeholder
        aTextview.textColor = MYColor.darkGrey
        aTextview.text = placeholderText
    }
    
    func applyNonPlaceholderStyle(aTextview: UITextView) {
        print("func applyNonPlaceholderStyle is running...")
        // make it look like normal text instead of a placeholder
        aTextview.textColor = .black
        aTextview.alpha = 1.0
    }
    
    func moveCursorToStart(aTextView: UITextView) {
        print("func moveCursorToStart is running...")
        DispatchQueue.main.async(execute: { () -> Void in
            aTextView.selectedRange = NSMakeRange(0, 0)
        })
    }
    
    func textViewShouldBeginEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldBeginEditing is running...")
        if aTextView == firstNameTextView && aTextView.text == editProfileData.firstNamePlaceholderText {
            // move cursor to start
            //aTextView.textColor = .black
            moveCursorToStart(aTextView: aTextView)
        }
        else if aTextView == lastNameTextView && aTextView.text == editProfileData.lastNamePlaceholderText {
            // move cursor to start
            //aTextView.textColor = .black
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func textViewShouldEndEditing(_ aTextView: UITextView) -> Bool {
        print("func textViewShouldEndEditing is running...")
        if aTextView == firstNameTextView && aTextView.text == "" {
            // move cursor to start
            aTextView.textColor = MYColor.darkGrey
            aTextView.text = editProfileData.firstNamePlaceholderText
            moveCursorToStart(aTextView: aTextView)
        }
        else if aTextView == lastNameTextView && aTextView.text == "" {
            // move cursor to start
            aTextView.textColor = MYColor.darkGrey
            aTextView.text = editProfileData.lastNamePlaceholderText
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("func textViewDidChange is running...")
        // set interaction state and tint color
        updateDoneButtonInteractionState()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        if text == "\n" {
            // return false when return key is pressed
            return false
        }
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if textView.text.characters.count == 0 && text == " " {
            return false
        }
        if newLength > 25 {
            return false
        }
        if newLength > 0 {
            if textView == firstNameTextView && textView.text == editProfileData.firstNamePlaceholderText {
                if text.utf16.count == 0 && textView.textColor == MYColor.darkGrey { // they hit the back button during placeholder style
                    return false // ignore it
                }
                if textView.textColor == MYColor.darkGrey {
                    applyNonPlaceholderStyle(aTextview: textView)
                    textView.text = ""
                }
            }
            else if textView == lastNameTextView && textView.text == editProfileData.lastNamePlaceholderText {
                if text.utf16.count == 0 && textView.textColor == MYColor.darkGrey { // they hit the back button during placeholder style
                    return false // ignore it
                }
                if textView.textColor == MYColor.darkGrey {
                    applyNonPlaceholderStyle(aTextview: textView)
                    textView.text = ""
                }
            }
            return true
        }
        else { // no text, so show the placeholder
            if textView == firstNameTextView {
                applyPlaceholderStyle(aTextview: textView, placeholderText: editProfileData.firstNamePlaceholderText)
            }
            else if textView == lastNameTextView {
                applyPlaceholderStyle(aTextview: textView, placeholderText: editProfileData.lastNamePlaceholderText)
            }
            moveCursorToStart(aTextView: textView)
            updateDoneButtonInteractionState()
            return false
        }
    }
}


