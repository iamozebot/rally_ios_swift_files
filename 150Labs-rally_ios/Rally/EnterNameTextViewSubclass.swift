//
//  EnterNameTextViewSubclass.swift
//  Rally
//
//  Created by Ian Moses on 10/9/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class EnterNameTextViewSubclass: UITextView {
    var placeHolderXOrigin: CGFloat = 2.0
    
    // https://github.com/NoahSaso/SConsole/blob/master/SConsole/ViewController.swift
    override func caretRect(for position: UITextPosition) -> CGRect {
        print("func caretRectForPosition is running...")
        var myRect = super.caretRect(for: position)
        // if placeholder text is there, return x-origin
        if self.textColor == MYColor.darkGrey {
            myRect.origin.x = placeHolderXOrigin
        }
        return myRect
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        addContentSizeObserver()
        contentInset.left = ScreenDimension.width*5/100
        let menu = UIMenuController.shared
        menu.isMenuVisible = false
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func addContentSizeObserver() {
        print("private func addContentSizeObserver is running...")
        self.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    fileprivate func removeContentSizeObserver() {
        print("private func removeContentSizeObserver is running...")
        self.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) { // [NSObject: AnyObject] to [String : AnyObject] per http://stackoverflow.com/questions/31308209/error-with-override-public-func-observevalueforkeypath
        print("override func observeValueForKeyPath is running...")
        let textView = object as! UITextView
        let top = (textView.bounds.size.height - textView.contentSize.height * zoomScale) / 2.0
        contentInset.top = top
    }
    
    deinit {
        removeContentSizeObserver()
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // You need to only return true for the actions you want, otherwise you get the whole range of
        //  iOS actions. You can see this by just removing the if statement here.
        print("override func canPerformAction is running...")
        return false
    }
}
