//
//  Enumerations.swift
//  Rally
//
//  Created by Ian Moses on 2/14/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

enum CardType {  // associated value type parameters used in payload construction
    case upcomingRally(Rally?) // upcoming rallys for active section
    case userJoinedCommunity(Community?) // communities user has joined
    case createCommunity(Community?) // case is used to create card user presses card to create a new community
    case joinCommunity(Community?) // case is used to create card that user presses card to search for new communities to join
    case communityNearby(Community?) // communities nearby user is not a part of but has searched for
    case publicCommunity(Community?)
    case privateCommunity(Community?)
}

enum tutorialBlurbType: String/*, CustomStringConvertible*/ {
    case howToUseSavedGroupCards = "tutorialStartRallyWithGroupBlurbDisplayStatus" // how to use saved group cards
    case howToUseRallyCommunityCard = "tutorialStartRallyWithCommunityBlurbDisplayStatus" // how to use rally community card
    case whatAreCommunities = "tutorialCommunityIntroBlurbDisplayStatus" // what are communities
    case howToUseSaveButtonToSaveGroups = "tutorialSaveRallyGroupBlurbDisplayStatus" // how to use save button to save groups to use for starting new rallies
    case howFriendsWithoutAppReceiveInvite = "tutorialInviteFriendsWithoutAppBlurbDisplayStatus" // how friends without app receive invite
    
    var description: String {
        switch self {
        case .howToUseSavedGroupCards:
            return "Use past Rally groups or create custom groups to start a new rally, tapping automatically pre-selects these people in friends selection."
            // Easy way to start a new rally, automatically pre-selects these people in friends selection
        case .whatAreCommunities:
            return "Communities are public. Join to send and be notified when a rally is sent to the community."
        case .howToUseSaveButtonToSaveGroups:
            return "Select friends and tap to save group."
            // . Use to start new rallies with friends already pre-selected.  // second half to what's already written
            // "Save group to quickly start rallies from (going forward) (on home screen)"
            // create your own friends groups
        case .howToUseRallyCommunityCard:
            return "Tap to rally your community."
        case .howFriendsWithoutAppReceiveInvite:
            return "Don't worry, friends not on Rally yet will receive a text invite."
        }
    }
    
    var side: adjoiningLabelSide {
        switch self {
        case .howToUseSavedGroupCards:
            return .bottom
        case .whatAreCommunities:
            return .top
        case .howToUseSaveButtonToSaveGroups:
            return .top
        case .howToUseRallyCommunityCard:
            return .bottom
        case .howFriendsWithoutAppReceiveInvite:
            return .bottom
        }
    }
}

enum homeScreenSection: Int {
    case rallyView
    case communitiesView
    case searchCommunitiesNearbyView
}

enum permissionCase: Int {
    case contacts
    case location
    case remoteNotifications
    case locationEnabled // generic permission for phone
}

enum adjoiningLabelSide: Int {
    case top
    case bottom
}

/*enum CryptoAlgorithm {
    case md5, sha1, sha224, sha256, sha384, sha512
    
    var HMACAlgorithm: CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .md5:		result = kCCHmacAlgMD5
        case .sha1:		result = kCCHmacAlgSHA1
        case .sha224:	result = kCCHmacAlgSHA224
        case .sha256:	result = kCCHmacAlgSHA256
        case .sha384:	result = kCCHmacAlgSHA384
        case .sha512:	result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    typealias DigestAlgorithm = (UnsafeRawPointer, CC_LONG, UnsafeMutablePointer<CUnsignedChar>) -> UnsafeMutablePointer<CUnsignedChar>
    
    var digestAlgorithm: DigestAlgorithm {
        switch self {
        case .md5:      return CC_MD5
        case .sha1:     return CC_SHA1
        case .sha224:   return CC_SHA224
        case .sha256:   return CC_SHA256
        case .sha384:   return CC_SHA384
        case .sha512:   return CC_SHA512
        }
    }
    
    var digestLength: Int {
        var result: Int32 = 0
        switch self {
        case .md5:		result = CC_MD5_DIGEST_LENGTH
        case .sha1:		result = CC_SHA1_DIGEST_LENGTH
        case .sha224:	result = CC_SHA224_DIGEST_LENGTH
        case .sha256:	result = CC_SHA256_DIGEST_LENGTH
        case .sha384:	result = CC_SHA384_DIGEST_LENGTH
        case .sha512:	result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}*/

enum containerType {
    case home
    case community
}

enum SlideOutState {
    case bothCollapsed
    case bottomPanelExpanded
    case bothExpanded
}

enum CollapsedExpandedState {
    case collapsed
    case expanded
}

enum WebServiceValidationType: String {
    case WebServiceInMaintenanceMode
    case NetworkReachability
    case IsAppVersionSupported
    case IsUserBanned
    case IsPhoneNumberVerified
    case IsUserAuthenticated
    case IsPayloadClean
    
    // title is for iOS (not coming from web service), for error message display above the message (which is what web service passes back to be displayed), here only for that display to make error message more readable
    var title: String {
        switch self {
        case .WebServiceInMaintenanceMode:
            return "Server Under Maintenance."
        // Easy way to start a new rally, automatically pre-selects these people in friends selection
        case .NetworkReachability:
            return "Cannot Reach Network."
        case .IsAppVersionSupported:
            return "A new App Is Available."
        case .IsUserBanned:
            return "Account Suspended."
        case .IsPhoneNumberVerified:
            return "Phone Number Requires Verification."
        case .IsUserAuthenticated:
            return "Authentication Error."
        case .IsPayloadClean:
            return "Authentication Error."
        }
    }
    
    // from webservice
    var message: String? {
        switch self {
        case .WebServiceInMaintenanceMode:
            return nil
        // Easy way to start a new rally, automatically pre-selects these people in friends selection
        case .NetworkReachability:
            return "An internet connection is required to use Rally. Please connect to the internet and try again."
        case .IsAppVersionSupported:
            return nil
        case .IsUserBanned:
            return nil
        case .IsPhoneNumberVerified:
            return nil
        case .IsUserAuthenticated:
            return nil
        case .IsPayloadClean:
            return nil
        }
    }
    
    func alertViewHandler() {
        switch self {
        case .WebServiceInMaintenanceMode:
            inMaintenanceModeHandler()
        case .NetworkReachability:
            networkReachabilityHandler()
        case .IsAppVersionSupported:
            appVersionSupportedHandler()
        case .IsUserBanned:
            isUserBannedHandler()
        case .IsPhoneNumberVerified:
            isPhoneNumberVerifiedHandler()
        case .IsUserAuthenticated:
            isUserAuthenticatedHandler()
        case .IsPayloadClean:
            isPayloadCleanHandlerHandler()
        }
    }
    
    // Error handler methods
    func inMaintenanceModeHandler() {
        print("func inMaintenanceModeHandler is running...")
        // display error message, allow user to dismiss message to continue using app offline
        // leave empty, no additional code required to work as intended
        // in future, consider preventing user from dismissing alert
    }
    
    func networkReachabilityHandler() {
        print("func networkReachabilityHandler is running...")
        // display error message, allow user to dismiss message to continue using app offline
        // leave empty, no additional code required to work as intended
    }
    
    func appVersionSupportedHandler() {
        print("func appVersionSupportedHandler is running...")
        // force users to update app on appVersionSupported = false, button sends user to app store. do not need to worry about dismissing alert ever, newly updated version will be fresh instance of app.
        if let info = Bundle.main.infoDictionary {
            let appName = info[kCFBundleNameKey as String] as! String
            let appStoreAppID = info[kCFBundleIdentifierKey as String] as? String ?? "Unknown"
            let urlString = "itms-apps://geo.itunes.apple.com/us/app/"+appName+"/id"+appStoreAppID
            let url  = URL(string: urlString)!
            // go to updated app in app store
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func isUserBannedHandler() {
        print("func isUserBannedHandler is running...")
        // display error message, do not allow user to dismiss alert (because user is banned from using app)
        // for now, not going to add anything here to prevent alert from being dismissed since banning probably won't happen much until later updates and not being able to successfully make webcalls is essentially the same thing.
    }
    
    func isPhoneNumberVerifiedHandler() {
        print("func isPhoneNumberVerified is running...")
        // phone number requiring verification should only occur on new app update
        // display error message, pressing button takes user to verify phone number screen
        // let verifyPhoneNumberViewControllerInstance = VerifyPhoneNumberViewController(nibName: nil, bundle: nil)
        // self.navigationController!.setViewControllers([verifyPhoneNumberViewControllerInstance], animated: true) // user must go enter phone number
    }
    
    func isUserAuthenticatedHandler() {
        print("func isUserAuthenticated is running...")
        // display error message, allow user to dismiss message to continue using app offline
        // leave empty, no additional code required to work as intended
    }
    
    func isPayloadCleanHandlerHandler() {
        print("func isPayloadCleanHandler is running...")
        // display error message, allow user to dismiss message to continue using app offline
        // leave empty, no additional code required to work as intended
    }
}

enum WebServiceCallIntent { // associated value type parameters used in payload construction
    case createRally(Rally)
    case createCommunity(Community)
    case getRallys
    case getCommunities
    case postRallyMessage(Rally, message: String)
    case getNewStockImages
    case createUser
    case updateIsRallyActiveFlag(Rally)
    case updateRallyDetails(Rally)
    case getNearbyCommunitySearchResults(String)
    case getRallyMessages(Rally) // addt'l completion handler parameter- rally?
    case getUpdatedRallyPackage(Rally?, Int) // addt'l completion handler parameter- rally
    case joinCommunity(Community)
    case updateDidUserAccept(Rally, Int)
    case createUsersWithPhoneContacts
    case getSingleRallyMessage(Int) // addt'l completion handler parameter- rallyMessage?
    case addSupportRequest(String)
    case confirmValidationCode(Int)
    case uploadLargeProfileImage(Data)
    case getThumbnailProfileImage(userID: Int, membersArrayMemberIndex: Int?)
    case updateUserNames(firstName: String, lastName: String) // userClass.(firstName: String, lastName: String) executed in method
    case leaveCommunity(communityID: Int)
    case getCommThreadMessages(for: Thread)
    case getCommThreadMessage(commThreadMessageID: Int)
    case checkForUpdatedThumbnailProfileImage(userID: Int, thumbnailProfileImageID: Int)
    case postCommThreadMessage(commThreadMessage: CommThreadMessage, messageBlurb: MYMessageBlurb) // need messageBlurb reference to mark message delivery success in profilePicture message subview
    case getLargeProfileImage(memberID: Int, imageView: UIImageView)
}

enum WebServiceCallReturnParameters {
    case createRally
    case createCommunity
    case getRallys
    case getCommunities
    case postRallyMessage
    case getNewStockImages
    case createUser
    case updateIsRallyActiveFlag
    case updateRallyDetails
    case getNearbyCommunitySearchResults
    case getRallyMessages(updatedRally: Rally) // addt'l completion handler parameter- rally?
    case getUpdatedRallyPackage(updatedRally: Rally) // addt'l completion handler parameter- rally
    case joinCommunity
    case updateDidUserAccept
    case createUsersWithPhoneContacts
    case getSingleRallyMessage(rallyMessage: RallyMessage) // addt'l completion handler parameter- rallyMessage?
    case addSupportRequest
    case confirmValidationCode
    case uploadLargeProfileImage
    case getThumbnailProfileImage(membersArrayMemberIndex: Int?, imageFetched: Bool, thumbnailProfileImageID: Int)
    case updateUserNames
    case leaveCommunity
    case getCommThreadMessages // for thread but not needed; no required return parameter
    case getCommThreadMessage (threadID: Int, commThreadMessage: CommThreadMessage)
    case checkForUpdatedThumbnailProfileImage(userID: Int, thumbnailUpdated: Bool)
    case postCommThreadMessage(messageBlurb: MYMessageBlurb)
    case getLargeProfileImage(imageFetched: Bool, memberID: Int, imageView: UIImageView)
}

enum WebServiceCallRequestType {
    case request
    case upload
    case stream
}

enum VerifyPhoneNumberMode {
    case phoneNumber
    case confirmationCode
}

enum PanDirection {
    case vertical
    case horizontal
    case left
    case right
    case up
    case down
}

enum NotificationsStatus {
    case muted
    case active
}

enum Edge {
    case top
    case bottom
    case left
    case right
}

enum Hierarchy {
    case parent
    case child
}

enum ImageType {
    case stockImage
    case customPicture
}

enum UnderlineScrollDirection {
    case left
    case right
    case none
}

enum CardContainerScrollDirection {
    case vertical
    case horizontal
    case left
    case right
    case up
    case down
    case none
}

enum CardContainerSide {
    case left
    case right
}

enum VerticalAlignment: Int {
    case top = 0, middle, bottom
}

enum RallyDetailType {
    case time
    case location
    case date
}

enum InviteMode {
    case rally
    case thread
    case community
}

enum ProfileImageSize {
    case thumbnail
    case large
}
