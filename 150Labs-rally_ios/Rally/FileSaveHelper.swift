//
//  FileSaveHelper.swift
//  Rally
//
//  Created by Ian Moses on 12/14/15.
//  Copyright © 2015 Ian Moses. All rights reserved.
//
// https://github.com/jjessel/FileSaveHelper/blob/master/FileSaveHelper.swift
//  FileSaveHelper.swift
//  SavingFiles
//
//  Created by Jeremiah Jessel on 9/14/15.
//  Copyright © 2015 JCubedApps. All rights reserved.
//
/*  Copyright (c) 2015 Jeremiah

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

import Foundation
import UIKit

class fileSaveHelper {
    
    // MARK:- Error Types
    
    fileprivate enum FileErrors:Error {
        case jsonNotSerialized
        case fileNotSaved
        case imageNotConvertedToData
        case fileNotRead
        case fileNotFound
    }
    
    // MARK:- File Extension Types
    enum FileExtension:String {
        case TXT = ".txt"
        case JPG = ".jpg"
        case JSON = ".json"
        case PNG = ".png" // I added this in, didn't want jpeg
    }
    
    // MARK:- Private Properties
    fileprivate let directory: FileManager.SearchPathDirectory
    fileprivate let directoryPath: String
    fileprivate let fileManager = FileManager.default
    fileprivate let fileName: String
    fileprivate let filePath: String
    fileprivate let fullyQualifiedPath: String
    fileprivate let subDirectory: String
    
    // MARK:- Public Properties
    var fileExists:Bool {
        get {
            return fileManager.fileExists(atPath: fullyQualifiedPath)
        }
    }
    
    var directoryExists:Bool {
        get {
            var isDir = ObjCBool(true)
            return fileManager.fileExists(atPath: filePath, isDirectory: &isDir )
        }
    }
    
    // MARK:- Initializers
    convenience init(fileName:String, fileExtension:FileExtension){
        self.init(fileName:fileName, fileExtension:fileExtension, subDirectory:"", directory:.documentDirectory)
    }
    
    convenience init(fileName:String, fileExtension:FileExtension, subDirectory:String){
        self.init(fileName:fileName, fileExtension:fileExtension, subDirectory:subDirectory, directory:.documentDirectory)
    }
    
    /**
     Initialize the FileSaveHelper Object with parameters
     
     :param: fileName      The name of the file
     :param: fileExtension The file Extension
     :param: directory     The desired sub directory
     :param: saveDirectory Specify the NSSearchPathDirectory to save the file to
     
     */
    init(fileName:String, fileExtension:FileExtension, subDirectory:String, directory:FileManager.SearchPathDirectory){
        self.fileName = fileName + fileExtension.rawValue
        self.subDirectory = "/\(subDirectory)"
        self.directory = directory
        self.directoryPath = NSSearchPathForDirectoriesInDomains(directory, .userDomainMask, true)[0]
        self.filePath = directoryPath + self.subDirectory // i feel like there ought to be a / after subDirectory for createDirectory use of filePath in below method
        self.fullyQualifiedPath = "\(filePath)/\(self.fileName)"
        createDirectory()
    }
    
    /**
     If the desired directory does not exist, then create it.
     */
    fileprivate func createDirectory(){
        if !directoryExists {
            do {
                try fileManager.createDirectory(atPath: filePath, withIntermediateDirectories: false, attributes: nil) // Library directory already exists so withIntermediateDirectories may be set to false. if not working, try true
            }
            catch {
                print("An Error was generated creating directory")
            }
        }
    }
    
    // MARK:- File saving methods
    
    /**
    Save the contents to file
    
    :param: fileContents A String that will be saved in the file
    */
    func saveFile(string fileContents:String) throws{
        
        do {
            try fileContents.write(toFile: fullyQualifiedPath, atomically: true, encoding: String.Encoding.utf8) //
        }
        catch  {
            throw error
        }
    }
    
    /**
     Save the image to file.
     
     :param: image UIImage
     */
    func saveFile(image:UIImage) throws {
        guard let data = UIImagePNGRepresentation(image) else { // changed from UIImageJPEGRepresentation to UIImagePNGRepresentation
            throw FileErrors.imageNotConvertedToData
        }
        if !fileManager.createFile(atPath: fullyQualifiedPath, contents: data, attributes: nil) { // if file exists, overwrites if permissions are set correctly. I do not know if they are but we shouldn't be overwriting files per the current architecture
            throw FileErrors.fileNotSaved
        }
    }
    
    /**
     Save a JSON file
     
     :param: dataForJson NSData
     */
    func saveFile(dataForJson:AnyObject) throws{
        do {
            let jsonData = try convertObjectToData(dataForJson)
            if !fileManager.createFile(atPath: fullyQualifiedPath, contents: jsonData, attributes: nil){
                throw FileErrors.fileNotSaved
            }
        } catch {
            print(error)
            throw FileErrors.fileNotSaved
        }
        
    }
    
    func getContentsOfFile() throws -> String {
        guard fileExists else {
            throw FileErrors.fileNotFound
        }
        
        var returnString:String
        do {
            returnString = try String(contentsOfFile: fullyQualifiedPath, encoding: String.Encoding.utf8)
        } catch {
            throw FileErrors.fileNotRead
        }
        return returnString
    }
    
    func getImage() throws -> UIImage {
        guard fileExists else {
            throw FileErrors.fileNotFound
        }
        
        guard let image = UIImage(contentsOfFile: fullyQualifiedPath) else {
            throw FileErrors.fileNotRead
        }
        
        return image
        
    }
    
    func getJSONData() throws -> NSDictionary {
        guard fileExists else {
            throw FileErrors.fileNotFound
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: fullyQualifiedPath), options: NSData.ReadingOptions.mappedIfSafe)
            let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
            return jsonData
        } catch {
            throw FileErrors.fileNotRead
        }
        
    }
    
    // MARK:- Json Converting
    
    /**
    Convert the NSData to Json Data
    
    :param: data NSData
    
    :returns: Json Serialized NSData
    */
    fileprivate func convertObjectToData(_ data:AnyObject) throws -> Data {
        
        do {
            let newData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            return newData
        }
        catch {
            print("Error writing data: \(error)")
        }
        throw FileErrors.jsonNotSerialized
    }
    /*
    func updateUserProfilePictureLocalFile(newProfilePicture: UIImage) {
        print("func updateUserProfilePictureLocalFile is running...")
        let stockImageFile = fileSaveHelper(fileName: "profilePicture", fileExtension: .PNG, subDirectory: "userProfilePicture", directory: .libraryDirectory)
        do {
            try stockImageFile.saveFile(image: newProfilePicture) // overwrites file if file already exists, otherwise creates new file
            print("successfully saved new profile picture")
        }
        catch {
            print("There was an error saving the file: \(error)")
        }
    }
    */
}
