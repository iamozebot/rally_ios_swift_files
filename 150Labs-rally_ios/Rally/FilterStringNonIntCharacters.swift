//
//  FilterStringNonIntCharacters.swift
//  Rally
//
//  Created by Ian Moses on 12/22/15.
//  Copyright © 2015 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class stringHelperClass {
    // http://stackoverflow.com/questions/29971505/leave-only-numbers-in-a-phone-number-in-swift
    // http://stackoverflow.com/questions/24200888/any-way-to-replace-characters-on-swift-string
    func filterStringNonIntCharacters(_ phoneNumberString: String)->String {
        var phoneNumber = phoneNumberString
        phoneNumber = phoneNumber.replacingOccurrences(
        of: "\\D", with: "")
        return phoneNumber
    }
    
    func removeSpecialCharacters(_ str: String) -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ".characters)
        return String(str.characters.filter { chars.contains($0) })
    }
    
    func removeNonNumericCharacters(_ str: String) -> String {
        let chars = Set("0123456789".characters)
        return String(str.characters.filter { chars.contains($0) })
    }
}
