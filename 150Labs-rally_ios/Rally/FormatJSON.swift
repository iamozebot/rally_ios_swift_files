//
//  FormatJSON.swift
//  Rally
//
//  Created by Ian Moses on 9/15/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class formatJSONClass {
    class func rallyMembersIdentifiersArrayConcatonation(_ rallyID: Int, rallyObject: Rally) {
        // this function works for storing member identifiers for both rallys and groups
        print("func rallyMembersIdentifiersArrayConcatonation is running...")
        var JSONStringConcatonationMethodFriendIndex = 1
        var  jsonStringRallyPackage = ""
        var rallyMembersIdentifiersArrayValue = ""
        let numberOfRallyMembers = rallyObject.rallyUserList.count
        let defaults = UserDefaults.standard
        var localFileKey = "communityMembersIdentifiersResultKey"
        if rallyObject.type == "upcomingRallysGlobalArray" {
            print("rallyObject.type: \(rallyObject.type)")
            localFileKey = "rallyMembersIdentifiersResultKey"
        }
        if let rallyMembersIdentifiersJSON = defaults.value(forKey: "\(localFileKey)") as? Data { // if doesn't work, try "as? NSData"
            let rallyMembersIdentifiersJSONAsNSString = NSString(data: rallyMembersIdentifiersJSON, encoding: String.Encoding.utf8.rawValue)!
            let rallyMembersIdentifiersJSONAsString = rallyMembersIdentifiersJSONAsNSString as String
            jsonStringRallyPackage = rallyMembersIdentifiersJSONAsString // assigns existing rally's rallyMembersIdentifiers array dictionary JSON to jsonStringRallyPackage
            // http://stackoverflow.com/questions/32237243/advancedby-api-in-swift-2-0
            let endIndex = jsonStringRallyPackage.characters.index(jsonStringRallyPackage.endIndex, offsetBy: -2)
            jsonStringRallyPackage = jsonStringRallyPackage.substring(to: endIndex) // removes }] from jsonStringRallyPackage to open JSON to append new rally's ID and rallyMembersIdentifiers array dictionary pair. Will reclose at end of this method
            jsonStringRallyPackage += ",\"\(rallyID)\":{"
        }
        else { // if else is true, rallyMembersIdentifiersResultKey is nil and array structure needs to be created
            jsonStringRallyPackage = "[{\"\(rallyID)\":{"
        }
        for friend in rallyObject.rallyUserList {
            let unformattedFriendPhoneNumber = Array(friend.phoneNumberList.values) // all friend objects should have phone numbers so unwrapping is safe
            if let value = unformattedFriendPhoneNumber[0] {
                let formattedFriendPhoneNumber = stringHelperClass().filterStringNonIntCharacters(value) // filters phone number to this format: "5555555555"
                if JSONStringConcatonationMethodFriendIndex<numberOfRallyMembers {
                    if let rallyMemberIdentifier = friend.identifier {
                        rallyMembersIdentifiersArrayValue += "\"\(formattedFriendPhoneNumber)\":\"\(rallyMemberIdentifier)\","
                    }
                    else {
                        rallyMembersIdentifiersArrayValue += "\"nil\":\"nil\","
                    }
                }
                else {
                    if let rallyMemberIdentifier = friend.identifier { // last rallyMemberIdentifier, close up NSData package
                        rallyMembersIdentifiersArrayValue += "\"\(formattedFriendPhoneNumber)\":\"\(rallyMemberIdentifier)\"}}]"
                    }
                    else { // if last rallyMemberIdentifier doesn't have identifier
                        rallyMembersIdentifiersArrayValue += "\"nil\":\"nil\"}}]"
                    }
                }
            }
            JSONStringConcatonationMethodFriendIndex += 1
        }
        // findnerd.com/list/view/How-to-convert-NSData-to-NSString-or-vice-versa-in-Objective-C-and-Swift/4137/#sthash.JqlgAINx.dpuf
        jsonStringRallyPackage += rallyMembersIdentifiersArrayValue // combine "[{\"\(rallyID)\":[" with array of identifier values and package closure
        let data = jsonStringRallyPackage.data(using: String.Encoding.utf8, allowLossyConversion: true) // convert to NSData to store as local file. NSData allows for convenient swiftyJSON parsing for profile pictures on rallyObject serialization upon startup
        if rallyObject.type == "upcomingRallysGlobalArray" {
            if let JSONAsNSDataValue = data {
                NSUserDefaultsClass.updateRallyMembersIdentifiersResultKey(JSONAsNSDataValue)
            }
        }
        else {
            if let JSONAsNSDataValue = data {
                print("rallyMembersIdentifiersArrayConcatonation:jsonStringRallyPackage being saved in updateGroupsMembersIdentifiersResultKey")
                NSUserDefaultsClass.updateCommunityMemberIdentifiersResultKey(JSONAsNSDataValue)
            }
        }
    }
    
    /*
    [
        {
            "rallyID1":
                        {
                            "6306745840":"rallyMemberIdentifier1",
                            "6306745841":"rallyMemberIdentifier2",
                            "6306745842":"rallyMemberIdentifier3"
                        },
            "rallyID2":
                        {
                            "6306745843":"rallyMemberIdentifier4",
                            "6306745844":"rallyMemberIdentifier5",
                            "6306745845":"rallyMemberIdentifier6"
                        }
        }
    ]
    */
    
    // Need to write a function for every time groups are pulled from webservice to match phone numbers with contacts. if there are any matches, save the member identifiers in group local file. So, if groups are rallied, any pictures and friends whose numbers you have will be included in rally. Need to store all friend's stock image numbers as well for this
    /*
    [
        {
            "EmailAddressList":
                [
                    {
                        "Address":"acuriarte@petrobras.com",
                        "Type": "TYPE_MOBILE"
                    }
                ],
            "Name": "ADRIANA URIARTE",
            "PhoneNumberList":
                [
                    {
                        "Number": "+17133147881",
                        "Type": "TYPE_MOBILE"
                    },
                    {
                        "Number": "+17138082391",
                        "Type": "TYPE_WORK"
                    }
                ]
        },
        {
            "EmailAddressList":
                [
                    {
                        "Address": "abdamm1187@gmail.com",
                        "Type": "TYPE_OTHER"
                    }
                ],
            "Name": "Aaron Damm",
            "PhoneNumberList":
                [
                    {
                        "Number": "+18326187881",
                        "Type": "TYPE_MOBILE"
                    }
                ]
        },
        {
            "EmailAddressList":
                [
                    {
                        "Address": "ak103n@att.com",
                        "Type": "TYPE_MOBILE"
                    }
                ]
            "Name": "Abdul Khandwalla",
            "PhoneNumberList":
                [
                    {
                        "Number": "7132329070",
                        "Type": "TYPE_MOBILE"
                    }
                ]
        },
        {
            "EmailAddressList":
                [
                    {
                        "Address": "a.fletcher@nigelfrank.com",
                        "Type": "TYPE_HOME"
                    }
                ],
            "Name": "Adam Fletcher",
            "PhoneNumberList":
                [
                    {
                        "Number": "07817681278",
                        "Type": "TYPE_MOBILE"
                    }
                ]
        },
        {
            "EmailAddressList":
                [
                    {
                        "Address": "a.fletcher@nigelfrank.com",
                        "Type": "TYPE_HOME"
                    }
                ],
            "Name": "Adam Fletcher",
            "PhoneNumberList":
                [
                    {
                        "Number": "07817681278",
                        "Type": "TYPE_MOBILE"
                    }
                ]
        }
    ]
    */
}
