//
//  FriendClass.swift
//  Rally
//
//  Created by Ian Moses on 9/8/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class Friend {
    var fullName: String?
    var firstName: String? {
        didSet {
            var lastName = ""
            if let value = MYUser.lastName {
                lastName += value
            }
            if let value = MYUser.firstName {
                self.fullName = value+" "+lastName
            }
            else {
                self.fullName = self.lastName
            }
        }
    }
    var lastName: String? {
        didSet {
            var lastName = ""
            if let value = MYUser.lastName {
                lastName += value
            }
            if let value = MYUser.firstName {
                self.fullName = value+" "+lastName
            }
            else {
                self.fullName = self.lastName
            }
        }
    }
    var phoneNumberList: [String: String?] = [:]
    var thumbnailProfileImage: UIImage? {
        get {
            if let userID = self.ID {
                if let value = communityMemberThumbnailsGlobalDictionary[userID] {
                    return value
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
    }
    var stockImageID: Int!
    var identifier: String? // webcall calls this PhoneContactID
    var ID: Int?
    var didUserAccept: Int?
    var imageDataAvailable: Bool? // iOS addressBook Contacts framework available property
    var hasThumbnailProfileImage: Int? // 0 is no, 1 is yes
    //UIImage is implicitly unwrapped because we will add stock images to this variable if user doesn't have an image
    
    func getProfileImage(size: ProfileImageSize = .thumbnail)-> UIImage { // default profile picture is thumbnail for method
        print("func getProfilePicture is running...")
        if let value = self.thumbnailProfileImage {
            //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            return value
        }
        if let stockImageID = self.stockImageID {
            return stockImageGlobalDictionary[stockImageID]!
        }
        else {
            let image = UIImage(color: .white)!
            return image
        }
    }
}
