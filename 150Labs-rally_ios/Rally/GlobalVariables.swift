//
//  GlobalVariables.swift
//  Rally
//
//  Created by Ian Moses on 9/8/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

var upcomingRallysGlobalArray: [Rally] = [] // Request data on opening app to populate main screen rallys, so this will be up to date (populated) upon app startup
//var userJoinedCommunitiesGlobalArray: [Community] = []
var publicCommunitiesGlobalArray: [Community] = []
var privateCommunitiesGlobalArray: [Community] = []
var communitiesNearbyGlobalArray: [Community] = []
var stockImageGlobalDictionary: [Int: UIImage] = [:] // populated from local file on app startup. Prior to populating array, database is checked for any new images. If so, images are added to local file
var communityMemberThumbnailUserIDsGlobalDictionary: [Int: Int] = [:] // userID: profileImageID //[String.UTF16View] = []
var communityMemberThumbnailsGlobalDictionary: [Int: UIImage] = [:] // populated with all members apart of user's communities and any other members being viewed (within non-joined communities, etc)
var communityMemberLargeProfileImageGlobalDictionary: [Int: UIImage] = [:]
