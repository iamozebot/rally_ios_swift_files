//
//  HSVColorPicker.swift
//  Rally
//
//  Created by Ian Moses on 1/10/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//
//  ColorUtils.swift
//  SwiftHSVColorPicker
//
//  Created by johankasperi on 2015-08-20.
// https://github.com/johankasperi/SwiftHSVColorPicker/blob/master/Source/ColorUtils.swift

import UIKit

// Typealias for RGB color values
typealias RGB = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)

// Typealias for HSV color values
typealias HSV = (hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat)

class colorHelperClass {
    func hsv2rgb(_ hsv: HSV) -> RGB {
        // Converts HSV to a RGB color
        var rgb: RGB = (red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        var r: CGFloat
        var g: CGFloat
        var b: CGFloat
        
        let i = Int(hsv.hue * 6)
        let f = hsv.hue * 6 - CGFloat(i)
        let p = hsv.brightness * (1 - hsv.saturation)
        let q = hsv.brightness * (1 - f * hsv.saturation)
        let t = hsv.brightness * (1 - (1 - f) * hsv.saturation)
        switch (i % 6) {
        case 0: r = hsv.brightness; g = t; b = p; break;
            
        case 1: r = q; g = hsv.brightness; b = p; break;
            
        case 2: r = p; g = hsv.brightness; b = t; break;
            
        case 3: r = p; g = q; b = hsv.brightness; break;
            
        case 4: r = t; g = p; b = hsv.brightness; break;
            
        case 5: r = hsv.brightness; g = p; b = q; break;
            
        default: r = hsv.brightness; g = t; b = p;
        }
        
        rgb.red = r
        rgb.green = g
        rgb.blue = b
        rgb.alpha = hsv.alpha
        return rgb
    }

    func rgb2hsv(_ rgb: RGB) -> HSV {
        // Converts RGB to a HSV color
        var hsb: HSV = (hue: 0.0, saturation: 0.0, brightness: 0.0, alpha: 0.0)
        
        let rd: CGFloat = rgb.red
        let gd: CGFloat = rgb.green
        let bd: CGFloat = rgb.blue
        
        let maxV: CGFloat = max(rd, max(gd, bd))
        let minV: CGFloat = min(rd, min(gd, bd))
        var h: CGFloat = 0
        var s: CGFloat = 0
        let b: CGFloat = maxV
        
        let d: CGFloat = maxV - minV
        
        s = maxV == 0 ? 0 : d / minV;
        
        if (maxV == minV) {
            h = 0
        } else {
            if (maxV == rd) {
                h = (gd - bd) / d + (gd < bd ? 6 : 0)
            } else if (maxV == gd) {
                h = (bd - rd) / d + 2
            } else if (maxV == bd) {
                h = (rd - gd) / d + 4
            }
            
            h /= 6;
        }
        
        hsb.hue = h
        hsb.saturation = s
        hsb.brightness = b
        hsb.alpha = rgb.alpha
        return hsb
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}


/*
 
 // calculate textLabel highlight color
 let textLabelColorComponents = detailView.textLabel.textColor.components
 var hsb1 = colorHelperClass().rgb2hsv(textLabelColorComponents)
 hsb1.saturation = 0.25
 let rgb = colorHelperClass().hsv2rgb(hsb1)
 let textLabelHighlightColor = UIColor(red: rgb.red, green: rgb.green, blue: rgb.blue, alpha: rgb.alpha)
 detailView.textLabel.textColor = textLabelHighlightColor
 
 // calculate imageView highlight color
 let colorComponents = detailView.imageView.tintColor.components
 var hsb2 = colorHelperClass().rgb2hsv(colorComponents)
 hsb2.saturation = 0.25
 let rgb2 = colorHelperClass().hsv2rgb(hsb2)
 let imageViewHighlightColor = UIColor(red: rgb2.red, green: rgb2.green, blue: rgb2.blue, alpha: rgb2.alpha)
 detailView.imageView.tintColor = imageViewHighlightColor
 
 */
