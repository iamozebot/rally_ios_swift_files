//
//  HomeViewController.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Recreated by Ian Moses on 5/18/15 to rename as HomeViewController from ViewController
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import UIKit
import AddressBook
import Contacts
import MapKit
import Darwin
import SwiftyJSON

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class HomeViewControllerDataSource {
    var returningFromCreateThreadVC: Bool = false
    var myCommunitiesManager: MyCommunitiesManager?
    var panAxis: PanDirection = .vertical // default value is vertical, hope it helps avoiding any error where .began does not trigger on gesture initialization
    var nearbyCardsAlreadyInitialized = false
    var community: Community?
    var updateLocationMethodFlag: String?
    var activeRallysCardsArray = [MYCard]()
    var publicCommunitiesCardsArray = [MYCard]()
    var privateCommunitiesCardsArray = [MYCard]()
    var userQueryCommunitiesNearbyResultsArray = [Community]()
    var communitiesNearbyCardsArray = [MYCard]() // may need to empty array on cancel button press
    init() {
        print("Do something...")
    }
}

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate, UITextViewDelegate, PassDataToViewControllerDelegate {
    
    var delegate: CenterViewControllerDelegate?
    var homeData = HomeViewControllerDataSource()
    fileprivate lazy var parentMenuView: UIView = {
        // initialize parentMenuView
        print("start fileprivate lazy initialization of parentMenuView")
        let parentMenuView = passThroughView()
        self.navigationController!.view.addSubview(parentMenuView)
        // define tail
        // http://stackoverflow.com/questions/28991100/get-the-frame-of-uibarbuttonitem-in-swift
        let barButtonItem = self.navigationItem.leftBarButtonItem!
        let buttonItemView = barButtonItem.value(forKey: "view")
        let leftButtonItemPositionOfBottom = (buttonItemView! as AnyObject).frame.origin.y+(buttonItemView! as AnyObject).frame.height
        let navBarBottom = UINavigationBarTaller.navigationBarHeight+UIApplication.shared.statusBarFrame.height
        let distanceBetwenLeftButtonItemPositionOfBottomAndNavBarBottom = navBarBottom-leftButtonItemPositionOfBottom
        let triangleHeight = distanceBetwenLeftButtonItemPositionOfBottomAndNavBarBottom - UINavigationBarTaller.navigationBarHeight*6.6/100
        var colorValues = [String:CGFloat]()
        let colorComponents = UIColor.white.components
        colorValues["redValue"] = colorComponents.red
        colorValues["greenValue"] = colorComponents.green
        colorValues["blueValue"] = colorComponents.blue
        colorValues["alphaValue"] = colorComponents.alpha
        let triangle = TriangeViewClass(frame: CGRect(x: 0, y: 0, width: triangleHeight*2, height: triangleHeight), colorValues: colorValues)
        triangle.tag = 506
        triangle.backgroundColor = UIColor(red: 1.0,green: 1.0,blue:1.0, alpha: 0.0)
        parentMenuView.addSubview(triangle)
        // define rounded rectangle
        let roundedRectangle = UIView()
        roundedRectangle.clipsToBounds = true
        parentMenuView.clipsToBounds = true
        roundedRectangle.tag = 507
        let tableViewHeight = ScreenDimension.height*3/10.6+ScreenDimension.height/6.25+ScreenDimension.height/95
        roundedRectangle.frame = CGRect(x: 0,y: triangle.frame.height-1,width: ScreenDimension.width*69/100, height: tableViewHeight)
        roundedRectangle.backgroundColor = UIColor.white
        roundedRectangle.layer.cornerRadius = 2.4
        roundedRectangle.addSubview(self.menuTableView) // reference to menuTableView initializes it since it's private lazy var
        parentMenuView.addSubview(roundedRectangle)
        // define parentMenuView frame
        let xOffset = ScreenDimension.width*1/100 // offset from edge by a small constant padding
        let yOffset = leftButtonItemPositionOfBottom + UINavigationBarTaller.navigationBarHeight*13/100 // offset from button bottom by a small constant padding
        parentMenuView.frame = CGRect(x: xOffset,y: yOffset,width: roundedRectangle.frame.width,height: 0/*triangle.frame.height+roundedRectangle.frame.height*/)
        //parentMenuView.layoutMargins.bottom = parentMenuView.frame.height
        // update triangle x-offset for subview of parentMenuView
        let triangleXOffset = (buttonItemView! as AnyObject).frame.origin.x + (buttonItemView! as AnyObject).frame.width/2 - self.navigationItem.leftBarButtonItem!.imageInsets.right/2 - triangleHeight - parentMenuView.frame.origin.x
        triangle.frame.origin.x = triangleXOffset
        print("finish fileprivate lazy initialization of parentMenuView")
        return parentMenuView
    }()
    fileprivate lazy var menuTableView: UITableView = {
        let menuTableView = UITableView()
        let tableViewHeight = ScreenDimension.height*3/10.6+ScreenDimension.height/6.25
        let yOffset = ScreenDimension.height/95 // constant is to provide small padding value
        menuTableView.frame = CGRect(x: 0, y: yOffset, width: ScreenDimension.width*69/100, height: tableViewHeight)
        menuTableView.delegate = self
        menuTableView.canCancelContentTouches = true
        menuTableView.dataSource = self
        menuTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        menuTableView.isScrollEnabled = false
        menuTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        return menuTableView
    }()
    let rallyViewSuperView = UIView() // subview of view, used so that (parent)view is a scrollview for this screen (changed to UIView to tie all gesture recognition to this view (was struggling to implement infiniteScrollView))
    var activeRallyCardsScrollView = UIScrollView()
    let activeRallyCardsAndHeaderViewsSuperview = UIView()
    let myCommunitiesSuperview = UIView()
    fileprivate lazy var userQueryCommunitiesNearbyResultsSuperview: UIScrollView  = {
        let userQueryCommunitiesNearbyResultsSuperview = UIScrollView()
        userQueryCommunitiesNearbyResultsSuperview.delegate = self
        userQueryCommunitiesNearbyResultsSuperview.frame = CGRect(x: 0,y: 0,width: ScreenDimension.width,height: ScreenDimension.heightMinusStatusAndNavigationBar) // height isn't exact but it shouldn't make a difference. measure it out if there's a problem
        return userQueryCommunitiesNearbyResultsSuperview
    }()
    fileprivate lazy var communitiesNearbyCardsSuperview: UIView  = {
        let communitiesNearbyCardsSuperview = UIView()
        communitiesNearbyCardsSuperview.frame = CGRect(x: 0,y: 0,width: ScreenDimension.width,height: ScreenDimension.heightMinusStatusAndNavigationBar) // height isn't exact but it shouldn't make a difference. measure it out if there's a problem
        return communitiesNearbyCardsSuperview
    }()
    var menuSublayerArray: [CALayer] = [] // maintains instance to cell border layers to avoid re-adding duplicates to cells
    // define y-padding for section labels from headers
    let sectionLabelPadding = ScreenDimension.height*0.5/100
    // define y-padding for cardSuperviews from section labels
    let cardSuperviewYPaddingFromSectionLabel = ScreenDimension.height*0.9/100 // also added to community view cards, they're not in a separate superview that may be moved
    var CameraPhotoLibraryHelper: CameraPhotoLibrary?
    var customSearchController: UISearchController?
    
// myCommunities public/private button and infinite scrolling variables
    var leftMostXOrigin: CGFloat!
    var underlineWidth: CGFloat = ScreenDimension.width*16/100
    var publicPrivateCommunitiesCardContainerInfiniteView = UIView() // parent for all user's community cards, myCommunitiesSuperview is the parent
    var publicCommunitySectionButton: MYButton!
    var privateCommunitySectionButton: MYButton!
    var underlineOne: MYWrapAroundUnderline = MYWrapAroundUnderline(quadrant: 1.0)
    var underlineTwo: MYWrapAroundUnderline = MYWrapAroundUnderline(quadrant: 3.0)
    var quadrantZeroXOrigin: CGFloat = 0.0 // not visible to user
    var quadrantOneXOrigin: CGFloat { // visible to user
        get {
            return self.quadrantZeroXOrigin+publicPrivateButtonCentersDistance
        }
    }
    var quadrantTwoXOrigin: CGFloat { // visible to user
        get {
            return self.quadrantOneXOrigin+publicPrivateButtonCentersDistance
        }
    }
    var quadrantThreeXOrigin: CGFloat { // not visible to user
        get {
            return self.quadrantTwoXOrigin+publicPrivateButtonCentersDistance
        }
    }
    var quadrantFourXOrigin: CGFloat { // not visible to user
        get {
            return self.quadrantThreeXOrigin+publicPrivateButtonCentersDistance
        }
    }
    var underlineContainer: UIView!
    var publicPrivateButtonCentersDistance: CGFloat {
        get {
            return self.privateCommunitySectionButton.center.x-self.publicCommunitySectionButton.center.x
        }
    }
    var publicCardContainer: CardContainer = CardContainer(cardType: .publicCommunity(nil))
    var privateCardContainer: CardContainer = CardContainer(cardType: .privateCommunity(nil))
    var activeCardContainer: CardType {
        get {
            if publicCardContainer.center.x >= ScreenDimension.width && publicCardContainer.frame.origin.x < ScreenDimension.width*2 {
                return .publicCommunity(nil)
            }
            else {
                return .privateCommunity(nil)
            }
        }
    }
// myCommunities public/private button and infinite scrolling variables
    var secondaryContainerVC: ContainerViewController {
        get {
            let asdf = self.primaryContainerVC
            return asdf.secondaryContainerViewController as! ContainerViewController
        }
        
    }
    var primaryContainerVC: ContainerViewController {
        get {
            let navigationController = self.parent as! UINavigationController
            return navigationController.parent as! ContainerViewController
        }
    }
    
    override func viewDidLoad() {
        print("class homeViewController, func viewDidLoad is running...")
        super.viewDidLoad()
        WebCallStatus.homeViewControllerLoaded = 1
        
        // setup rallyViewSuperView
        view.backgroundColor = MYColor.lightGrey
        rallyViewSuperView.frame = CGRect(x: 0,y: 0,width: ScreenDimension.width,height: ScreenDimension.height-ScreenDimension.height*9.16/100)
        view.addSubview(rallyViewSuperView)
        // add pan gesture to handle all touch events in rallyViewSuperView
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(HomeViewController.handlePanGestureOnHomeScreen))
        //panGesture.delegate = self
        rallyViewSuperView.addGestureRecognizer(panGesture)
        //publicPrivateCommunitiesCardContainerInfiniteView.addGestureRecognizer(swipeRight)
        
        //activeRallyCardsAndHeaderViewsSuperview
        activeRallyCardsAndHeaderViewsSuperview.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: ScreenDimension.height) // placeholder height, sized in updateViews method
        rallyViewSuperView.addSubview(activeRallyCardsAndHeaderViewsSuperview)
        
        // initialize activeRallyCardsScrollview
        //activeRallyCardsScrollView.frame = CGRect(x: 0, y: 0, width: ScreenDimension.screenWidth, height: self.cardHeight)
        activeRallyCardsScrollView.clipsToBounds = false
        activeRallyCardsScrollView.delegate = self
        activeRallyCardsScrollView.showsHorizontalScrollIndicator = false
        activeRallyCardsAndHeaderViewsSuperview.addSubview(activeRallyCardsScrollView)
        // set default to be non-scrollable by making content size same size as frame height
        activeRallyCardsScrollView.contentSize.height = activeRallyCardsScrollView.frame.height
        activeRallyCardsScrollView.frame.size.width = ScreenDimension.width
        
        // initialize myCommunitiesSuperview
        //myCommunitiesSuperview.frame = CGRect(x: 0, y: activeRallyCardsScrollView.frame.origin.y+activeRallyCardsScrollView.frame.height, width: ScreenDimension.screenWidth, height: ScreenDimension.screenHeight)
        myCommunitiesSuperview.frame.size.width = ScreenDimension.width
        //myCommunitiesSuperview.clipsToBounds = true
        rallyViewSuperView.addSubview(myCommunitiesSuperview)
        
        // initialize publicPrivateCommunitiesCardContainerInfiniteScrollView
        //publicPrivateCommunitiesCardContainerInfiniteScrollView.delegate = self
        publicPrivateCommunitiesCardContainerInfiniteView.frame.size.width = ScreenDimension.width*3
        publicPrivateCommunitiesCardContainerInfiniteView.frame.origin.x = -ScreenDimension.width
        publicPrivateCommunitiesCardContainerInfiniteView.clipsToBounds = true
        //publicPrivateCardsSuperview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        myCommunitiesSuperview.addSubview(publicPrivateCommunitiesCardContainerInfiniteView)
        
        // initialize publicCardContainer and privateCardContainer
        // size of containers will update in func updateRallyViewViewsSizes
        publicCardContainer.frame.origin.x = ScreenDimension.width
        publicCardContainer.frame.size.width = ScreenDimension.width
        publicPrivateCommunitiesCardContainerInfiniteView.addSubview(publicCardContainer)
        privateCardContainer.frame.origin.x = ScreenDimension.width*2
        privateCardContainer.frame.size.width = ScreenDimension.width
        publicPrivateCommunitiesCardContainerInfiniteView.addSubview(privateCardContainer)
        
        // initialize communitiesNearbyCardsSuperview
        userQueryCommunitiesNearbyResultsSuperview.addSubview(communitiesNearbyCardsSuperview)
        
        // define activity indicator
        defineActivityIndicator()
        
        if WebCallStatus.createUserWebCall == 0 { // if call hasn't already been made; only use case this occurs is first app run before user enables remote notifications and if FCM is ever not working. very rare
            let webServiceCallManagerInstance = WebServiceCallManager(intent: .createUser)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    func reloadUIForUpdatedProfilePicture(_ notification:Notification) {
        print("func reloadUIForUpdatedProfilePicture is running...")
        
        // reload cards
        updateActiveSection()
        updateUserJoinedCommunitiesSection()
        
        // reload communityMessagingUI to update profile pics there too
        // set this as active community in communityMessagingVC
        let communityMessagingVC = secondaryContainerVC.centerViewControllerCP as! CommunityMessagingViewController
        communityMessagingVC.reloadUI()
        if secondaryContainerVC.drawerViewController != nil {
            let leftPanelVC = secondaryContainerVC.drawerViewController as! LeftPanelViewController
            leftPanelVC.reloadUI()
        }
    }
    
    func updateDataAndUIForLeavingCommunity(_ notification:Notification) {
        print("func updateDataAndUIForLeavingCommunity is running...")
        
        // reload cards
        updateActiveSection()
        updateUserJoinedCommunitiesSection()
        
    // update data & reload communityMessagingUI/sidePanelVC UI
        
        // get reference to communityMessagingVC
        let communityMessagingVC = secondaryContainerVC.centerViewControllerCP as! CommunityMessagingViewController
        
        // update data
        communityMessagingVC.communityMessagingData.community = nil
        communityMessagingVC.communityMessagingData.activeThread = nil
        
        
        // reload UI for community messaging
        communityMessagingVC.reloadUI()
        
        // get reference to leftPanelVC
        if secondaryContainerVC.drawerViewController != nil {
            let leftPanelVC = secondaryContainerVC.drawerViewController as! LeftPanelViewController
            
            // update data
            leftPanelVC.leftPanelData.community = nil
            
            // reload UI for leftPanelVC
            leftPanelVC.reloadUI()
        }
    }
    
    func childViewControllerResponse(returningFromCreateThreadVC: Bool) {
        print("func childViewControllerResponse is running...")
        homeData.returningFromCreateThreadVC = returningFromCreateThreadVC
    }
    
    func defineActivityIndicator() {
        print("func defineActivityIndicator is running...")
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicator.tag = 409
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle  = UIActivityIndicatorViewStyle.gray // could conversely style white, WhiteLarge etc
        // activityIndicator.color = MYColor.rallyBlue
        activityIndicator.center.x = view.center.x
        activityIndicator.frame.origin.y = view.frame.origin.y+activityIndicator.frame.height*35/100
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func updateRallyCards(_ notification: Notification) { // notifications posted from appDelegate, external notifications
        print("func updateRallyCards is running...")
        // http://stackoverflow.com/questions/28597295/swift-read-userinfo-of-remote-notification
        guard let userInfo = (notification as NSNotification).userInfo else {
            return
        }
        guard let payload = userInfo["payload"] as? NSString  else {
            return
        }
        let optionalDataPackage = payload.data(using: String.Encoding.utf8.rawValue)
        guard let unwrappedDataPackage = optionalDataPackage else {
            return
        }
        let swiftyJSONDataPackage = JSON(data:unwrappedDataPackage)
        let rallyID = swiftyJSONDataPackage["RallyID"].int
        guard let unwrappedRallyID = rallyID else {
            return
        }
        let getUpdatedRallyPackageintent = WebServiceCallIntent.getUpdatedRallyPackage(nil, unwrappedRallyID)
        let webServiceCallManagerGetUpdatedRallyPackage = WebServiceCallManager(intent: getUpdatedRallyPackageintent)
        webServiceCallManagerGetUpdatedRallyPackage.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally) = returnIntent {
                        if updatedRally.isRallyActive == 1 {
                            upcomingRallysGlobalArray.append(updatedRally)
                            //updatedRallyObject!.arrayIndex = 0
                            DispatchQueue.main.async(execute: { () -> Void in
                                //self.defineCards(.anyRemainingGroupCards) // creates rally card to add to "My Rallys" section on home screen. appends to end of userRallyCardsArray
                                self.updateActiveSection()
                            })
                        }
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class HomeViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        
        // configure nav bar
        defineNavBar()
        
        // if not loading page (ie coming from farther up the stack), need to re-load tutorial, rally and community objects
        if WebCallStatus.inviteViewControllerLoaded == 1 || WebCallStatus.detailsViewControllerLoaded == 1 || WebCallStatus.messagingViewControllerLoaded == 1 { // avoids running logic on startup
            print("inside at start of viewWillAppear:webCallStatus.inviteViewControllerLoaded == 1 || webCallStatus.detailsViewControllerLoaded == 1")
            // adds any saved groups to start new section
            //updateSavedGroupsInStartNewSection()
            reloadHomeScreenUIAndUserRallyAndCommunityArrays()
        }
        
        // subscribe to notifications
        subscribeToNotifications()
        
        primaryContainerVC.view.bringSubview(toFront: secondaryContainerVC.view)
        print("func viewWillAppear is finished running...")
    }
    
    func subscribeToNotifications() {
        print("func subscribeToNotifications is running...")

        // updates UI as needed for upcoming rally data returned from web service
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateUIForReturnedUpcomingRallyDataFromWebService(_:)), name: NSNotification.Name(rawValue: "fetchUpcomingRallyDataFromWebServiceClassResponseCompleted"), object: nil)
        
        // updates UI as needed for user joined communities data returned from web service
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateUIForReturnedUserJoinedCommunitiesDataFromWebService(_:)), name: NSNotification.Name(rawValue: "fetchUserJoinedCommunitiesDataFromWebServiceClassResponseCompleted"), object: nil)
        
        // updates UI as needed for stock image data returned from web service
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateUIForReturnedStockImageDataFromWebService(_:)), name: NSNotification.Name(rawValue: "fetchStockImageDataFromWebServiceClassResponseCompleted"), object: nil)
        
        // NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.homePageLoadUI(_:)), name: NSNotification.Name(rawValue: "fetchRallyAndGroupUserInfoFromWebServiceClassResponseCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateRallyCards(_:)), name: NSNotification.Name(rawValue: "RallyPackage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.appWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateUserLatitudeAndLongitude(_:)), name: NSNotification.Name(rawValue: "locationManagerFetchedUserRegion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateRallyViewSuperviewHeight(_:)), name: NSNotification.Name(rawValue: "whatAreCommunitiesTutorialBlurbMarkedRead"), object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "respondToUserRemoteNotificationDialogSelection:", name: "UserRespondedToRemoteNotificationsDialog", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.reloadView(_:)), name: NSNotification.Name(rawValue: "updatedUserProfilePicture"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.checkPermissionsStatus(_:)), name: NSNotification.Name(rawValue: "splashScreenDismissed"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.reloadUIForUpdatedProfilePicture(_:)), name: NSNotification.Name(rawValue: "profilePictureUploadResultReturnedFromWCF"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateDataAndUIForLeavingCommunity(_:)), name: NSNotification.Name(rawValue: "leaveCommunityResultReturnedFromWCF"), object: nil)
        
    }
    
    func checkPermissionsStatus(_ notification:Notification) {
        print("func checkPermissionsStatus is running...")
        // check if all permissions are enabled
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location, navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        if let permissionsViewDullingView = self.navigationController!.view.viewWithTag(567) {
            navigationController!.view.bringSubview(toFront: permissionsViewDullingView)
        }
        if let permissionsView = self.navigationController!.view.viewWithTag(890) {
            navigationController!.view.bringSubview(toFront: permissionsView)
        }
    }
    
    func defineNavBar() {
        print("func defineNavBar is running...")
        navigationController?.setNavigationBarHidden(false, animated: true)
        defineNavigationBarShadow()
        navigationController?.setToolbarHidden(true, animated: true)
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        navigationController?.setValue(UINavigationBarTaller(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: 40)), forKeyPath: "navigationBar")
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 18)!, NSForegroundColorAttributeName: UIColor.white]
        navigationItem.title = "Home"
        // setup left bar button item
        let menuImage = UIImage(named: "menu")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let imageInsetValue = menuImage.size.height*15/100
        let menuButton = UIBarButtonItem(image: menuImage,  style: .plain, target: self, action: #selector(HomeViewController.onMenuButtonTouch(_:)))
        navigationItem.leftBarButtonItem = menuButton
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(imageInsetValue, 0, imageInsetValue, imageInsetValue) // left inset set to zero for transition from login screen to homeVC. Non-zero value causes animation jumping at end
        
        // setup right bar button items
        // initialize images
        let searchImage = UIImage(named: "search")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let createCommunityImage   = UIImage(named: "add")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        // initialize nearby community search button
        let searchButton: UIButton = UIButton(type: .custom)
        searchButton.setImage(searchImage, for: .normal)
        searchButton.addTarget(self, action: #selector(HomeViewController.onJoinCommunityButtonTouch(_:)), for: .touchUpInside)
        //let searchButtonSideLength = ScreenDimension.screenWidth*6.8/100
        searchButton.frame = CGRect(x:0,y: 0,width: 28,height: 28)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
        // initialize create community button
        let createCommunityButton: UIButton = UIButton(type: .custom)
        createCommunityButton.setImage(createCommunityImage, for: .normal)
        createCommunityButton.addTarget(self, action: #selector(HomeViewController.onCreateCommunityTouch(_:)), for: .touchUpInside)
        //let createCommunityButtonSideLength = ScreenDimension.screenWidth*7.5/100
        createCommunityButton.frame.size = CGSize(width: 31,height: 31)
        let createCommunityButtonInset = ScreenDimension.width*1.1/100
        createCommunityButton.contentEdgeInsets.right = createCommunityButtonInset
        createCommunityButton.contentEdgeInsets.left = -createCommunityButtonInset
        let createCommunityBarButton = UIBarButtonItem(customView: createCommunityButton)
        navigationItem.rightBarButtonItems = [searchBarButton, createCommunityBarButton]
    }
    
    func onCreateCommunityTouch(_ sender: UIButton!) {
        print("func onCreateCommunityTouch() is running...")
        let createCommunityViewControllerInstance: CreateCommunityViewController = CreateCommunityViewController(nibName: nil, bundle: nil)
        dismissMenu(nil)
        navigationController!.pushViewController(createCommunityViewControllerInstance, animated: true)
    }
    
    func reloadView(_ notification:Notification) {
        print("func reloadView() is running...")
        view.setNeedsDisplay()
        menuTableView.reloadData()
    }
    
    func reloadHomeScreenUIAndUserRallyAndCommunityArrays() {
        print("func reloadHomeScreenUIAndUserRallyAndCommunityArrays() is running...")
        if Reachability.isConnectedToNetwork() {
            // remove all cards from superview
            /*
            if let _ = rallyViewSuperView.superview {
                rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            }
             */
            for subview in homeData.activeRallysCardsArray {
                subview.removeFromSuperview()
            }
            for subview in homeData.publicCommunitiesCardsArray {
                subview.removeFromSuperview()
            }
            for subview in homeData.privateCommunitiesCardsArray {
                subview.removeFromSuperview()
            }
            for subview in homeData.communitiesNearbyCardsArray {
                subview.removeFromSuperview()
            }
            // empty all card arrays
            homeData.activeRallysCardsArray.removeAll()
            homeData.publicCommunitiesCardsArray.removeAll()
            homeData.privateCommunitiesCardsArray.removeAll()
            homeData.communitiesNearbyCardsArray.removeAll()
            // set flags to calls being made to not finished
            WebCallStatus.getCommunitiesFromWebService = 0
            WebCallStatus.fetchRallyUserInfoFromWebService = 0
            // fetch user rally and community info from webservice (these functions will trigger update UI on completion via postNotification)
            let webServiceCallManagerGetRallys = WebServiceCallManager(intent: .getRallys)
            webServiceCallManagerGetRallys.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
            let webServiceCallManagerGetCommunities = WebServiceCallManager(intent: .getCommunities)
            webServiceCallManagerGetCommunities.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        // home view controller has successfully updated user communities to phone, post notification so any class needing default community info can update data (before user has manually pressed a community card to select a community focus)
                        
                        // post notification
                        print("posting userCommunitiesReturnedFromBackend")
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "userCommunitiesReturnedFromBackend"), object: nil)
                    })
                }
                else { // post message saying could not join community. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
            // check if tutorial blurbs need display and display if needed
            var referenceCard: UIButton?
            if homeData.publicCommunitiesCardsArray.count > 0 {
                referenceCard = homeData.publicCommunitiesCardsArray[0]
            }
            TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.howToUseRallyCommunityCard, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
            referenceCard = nil // reset for next cardsArray
            // determine and if necessary define and display .whatAreCommunities tutorial blurb
            //REENABLE
            //TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.whatAreCommunities, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
        }
    }
    
    func appWillEnterForeground(_ notification:Notification) {
        print("func HomeViewController:appWillEnterForeground is running...")
        // this assures user doesn't exit app mid-instance and turn off a permission, then re-enter and attempt to use permission when disabled and crash app
        /*
        if let _ = rallyViewSuperView.superview {
            rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            //TutorialHelper.removeTutorialBlurbsFromViewControllerView(self.rallyViewSuperView)
        }
         */
        // checks if any permissions are disabled, if so displays permissions access view handler
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        if WebCallStatus.createUserWebCall == 1 {
            reloadHomeScreenUIAndUserRallyAndCommunityArrays()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("class homeViewController, func viewDidAppear is running...")
        /*webCallStatus.testVariable = 1
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        webCallStatus.testVariable = 0*/
    }
    
    func updateActiveSection() {
        print("func updateActiveSection is running...")
        // first, remove old active rally cards before generating up-to-date cards (needed when called from updateRallyCards, but not from homePageLoadUI. doesn't break it though in second case, there just won't be any cards yet)
        for card in homeData.activeRallysCardsArray {
            card.removeFromSuperview()
        }
        homeData.activeRallysCardsArray.removeAll()
        print("upcomingRallysGlobalArray.count:\(upcomingRallysGlobalArray.count)")
        if upcomingRallysGlobalArray.count != 0 { // don't execute any more code if no new upcoming rallys to add
            defineCards(.upcomingRally(nil), numberOfCardsToDefine: upcomingRallysGlobalArray.count)
            defineSectionHeadersLabels(.rallyView) // call to update 'upcoming rallys' section sublabel: 'You have ... number of upcoming rallys'. no duplicate labels are created on call
            updateRallyViewViewsSizes()
        }
    }
    
    func updateUserJoinedCommunitiesSection() {
        print("func updateUserJoinedCommunitiesSection is running...")
        for card in homeData.publicCommunitiesCardsArray {
            card.removeFromSuperview()
        }
        for card in homeData.privateCommunitiesCardsArray {
            card.removeFromSuperview()
        }
        homeData.publicCommunitiesCardsArray.removeAll()
        homeData.privateCommunitiesCardsArray.removeAll()
        // update public community cards
        let publicCommunityCount = publicCommunitiesGlobalArray.count
        defineCards(.publicCommunity(nil), numberOfCardsToDefine: publicCommunityCount)
        // update private community cards
        let privateCommunityCount = privateCommunitiesGlobalArray.count
        defineCards(.privateCommunity(nil), numberOfCardsToDefine: privateCommunityCount)
        updateRallyViewViewsSizes()
    }
    
    func defineNavigationBarShadow() {
        print("func defineNavigationBarShadow is running...")
        navigationController?.navigationBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        navigationController?.navigationBar.layer.shadowOpacity = 1.0
        navigationController?.navigationBar.layer.shadowRadius = 1.5
    }
    
    func defineSectionHeadersLabels(_ sectionHeader: homeScreenSection) {
        print("func defineSectionHeadersLabels is running...")
        var sectionHeaderTextSize: CGFloat = 21.0
        var sublabelTextSize: CGFloat = 16.25
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            sectionHeaderTextSize = 19.5
            sublabelTextSize = 15.0
        default:
            print("Do nothing...")
        }
        switch sectionHeader {
        case .rallyView: // rally tab
            // do nothing if header already exists
            if let _ = activeRallyCardsAndHeaderViewsSuperview.viewWithTag(591) {
            }
            else { // otherwise, define active rally header
                let activeRallysHeader = defineHomeLabels("Upcoming Rallys", textColor: UIColor.black, fontSize: UIFont.boldSystemFont(ofSize: sectionHeaderTextSize), associatedHeader: nil)
                activeRallysHeader.tag = 591
                activeRallyCardsAndHeaderViewsSuperview.addSubview(activeRallysHeader)
            }
            
            // define active rally section sublabel
            // if a section label already exists, remove in favor of a newly updated label to keep number of upcoming rallys up-to-date
            if let rallysActiveSectionLabel = activeRallyCardsAndHeaderViewsSuperview.viewWithTag(592) {
                rallysActiveSectionLabel.removeFromSuperview()
            }
            var labelText: String = "You have no upcoming Rallys"
            if upcomingRallysGlobalArray.count > 0 {
                labelText = "You have \(upcomingRallysGlobalArray.count) Upcoming Rallys"
            }
            if let associatedHeader = activeRallyCardsAndHeaderViewsSuperview.viewWithTag(591) as? UILabel { // unwrap header to use as parameter in method, while it doesn't need to be unwrapped technically, that is not intentional, just coincidence, so this is more readable. was made optional bc most labels don't have associated headers, not because associated header may not have been initialized yet like in this case
                let rallysActiveSectionLabel = defineHomeLabels(labelText, textColor: MYColor.homeSectionSublabelGrey, fontSize: UIFont.systemFont(ofSize: sublabelTextSize), associatedHeader: associatedHeader)
                rallysActiveSectionLabel.tag = 592
                activeRallyCardsAndHeaderViewsSuperview.addSubview(rallysActiveSectionLabel)
            }
            // IF header already exists, do nothing
            // ELSE, define 'My Communities' header
            if let _ = myCommunitiesSuperview.viewWithTag(593) {
            }
            else {
                let savedUsersCommunitiesHeader = defineHomeLabels("My Communities", textColor: UIColor.black, fontSize: UIFont.boldSystemFont(ofSize: sectionHeaderTextSize), associatedHeader: nil)
                savedUsersCommunitiesHeader.tag = 593
                myCommunitiesSuperview.addSubview(savedUsersCommunitiesHeader)
            }
        case .searchCommunitiesNearbyView: // this case is only called for initializing, isn't called again so no need to add logic here to avoid duplicates
            // define communities nearby header
            // do nothing if section label already exists
            
            // do nothing if header already exists
            if let _ = userQueryCommunitiesNearbyResultsSuperview.viewWithTag(599) {
            }
            else { // otherwise, define communities nearby header
                let searchCommunitiesNearbyHeader = defineHomeLabels("Communities Nearby", textColor: UIColor.black, fontSize: UIFont.boldSystemFont(ofSize: sectionHeaderTextSize), associatedHeader: nil)
                searchCommunitiesNearbyHeader.tag = 599
                userQueryCommunitiesNearbyResultsSuperview.addSubview(searchCommunitiesNearbyHeader)
            }
            if let _ = userQueryCommunitiesNearbyResultsSuperview.viewWithTag(600) {
            }
            else { // otherwise, define communities nearby header
                // define communities nearby section sublabel
                if let unwrappedSearchCommunitiesNearbyHeader = userQueryCommunitiesNearbyResultsSuperview.viewWithTag(599) as? UILabel {
                    let searchCommunitiesNearbySectionLabel = defineHomeLabels("Tap a Community to Join", textColor: UIColor.darkGray, fontSize: UIFont.systemFont(ofSize: sublabelTextSize), associatedHeader: unwrappedSearchCommunitiesNearbyHeader)
                    searchCommunitiesNearbySectionLabel.tag = 600
                    userQueryCommunitiesNearbyResultsSuperview.addSubview(searchCommunitiesNearbySectionLabel)
                }
            }
        default:
            break
        }
    }
    
    func defineHomeLabels(_ text: String, textColor: UIColor, fontSize: UIFont, associatedHeader: UILabel?)->UILabel {
        print("func defineHomeLabels is running...")
        let label = UILabel()
        let xOriginScaler: CGFloat = 3.9/100 // width applies to titles and subtitles
        let yOriginScaler: CGFloat = 2.9/100
        label.frame.origin = CGPoint(x: ScreenDimension.width*xOriginScaler, y: ScreenDimension.height*yOriginScaler)
        if let associatedHeaderInstance = associatedHeader {
            label.frame.origin.y = associatedHeaderInstance.frame.height + associatedHeaderInstance.frame.origin.y + sectionLabelPadding
        }
        label.text = text
        label.textColor = textColor
        label.font = fontSize
        label.sizeToFit()
        return label
    }
    
    func homePageLoadUI(_ notification:Notification?) {
        print("func homePageLoadUI is running...")
        if WebCallStatus.homePageLoadUIHandler != 1 { // assure logic only runs on startup
            WebCallStatus.homePageLoadUIHandler = 1
            defineSectionHeadersLabels(.rallyView) // function uses user rallys response, homePageLoadUIHandler called upon response return
            // stop/hide activity indicator because view is finished loading
            defineMyCommunitiesPrivatePublicCommunitySectionButtonAndUnderlineHandler()
            if let activityIndicator = view.viewWithTag(409) as? UIActivityIndicatorView {
                // activityIndicator hides when stopped animating is set to true. activity indicator is still subview of view
                print("stop activity indicator animating")
                activityIndicator.stopAnimating()
                //activityIndicator.removeFromSuperview()
            }
        }
        print("func homePageLoadUI is finished running...")
    }
    
    func updateUIForReturnedUpcomingRallyDataFromWebService(_ notification:Notification) {
        print("func updateUIForReturnedUpcomingRallyDataFromWebService is running...")
        homePageLoadUI(nil)
        updateActiveSection()
    }
    
    func updateUIForReturnedUserJoinedCommunitiesDataFromWebService(_ notification:Notification) {
        // will update UI as needed (if first call of notification type to return)
        print("func updateUIForReturnedUserJoinedCommunitiesDataFromWebService is running...")
        homePageLoadUI(nil)
        updateUserJoinedCommunitiesSection()
    }
    
    func updateUIForReturnedStockImageDataFromWebService(_ notification:Notification) {
        print("func updateUIForReturnedStockImageDataFromWebService is running...")
        if UserDefaults.standard.value(forKey: "firstTimeAppRunKey") == nil { // for first time app run, if new stock images are fetched and call completes when home screen is up, this will re-load cards. re-loaded cards will then display stock images
            updateActiveSection()
            updateUserJoinedCommunitiesSection()
            NSUserDefaultsClass.updateAppHasRunOnDeviceKey()
        }
    }
 
    func defineCards(_ cardTypeValue: CardType, numberOfCardsToDefine: Int) { // numberOfCardsToDefine relevant index to reference relevant rally/community is respective rally/community array and starts at index N-numberOfCards for array of count N
        print("func defineCards is running...")
        var startingCardPositionIndex = 0 // this is intended to make startingCardPositionIndex 0 on first call, and the number of shown cards on future calls
        func defineCardsForLoop() {
            print("func defineCardsForLoop() is running...")
            for i in startingCardPositionIndex..<startingCardPositionIndex+numberOfCardsToDefine {
                print("cardTypeValue:\(cardTypeValue)")
                // assign associated community/rally type to CardType enumeration
                var cardTypeWithAssociatedValue: CardType!
                switch cardTypeValue {
                case .upcomingRally:
                    cardTypeWithAssociatedValue = CardType.upcomingRally(upcomingRallysGlobalArray[i])
                case .publicCommunity:
                    cardTypeWithAssociatedValue = CardType.publicCommunity(publicCommunitiesGlobalArray[i])
                case .privateCommunity:
                    cardTypeWithAssociatedValue = CardType.privateCommunity(privateCommunitiesGlobalArray[i])
                case .communityNearby:
                    cardTypeWithAssociatedValue = CardType.communityNearby(communitiesNearbyGlobalArray[i])
                default:
                    cardTypeWithAssociatedValue = CardType.upcomingRally(upcomingRallysGlobalArray[i]) // no other card types created so this case will never run but must satisfy compiler
                }
                defineCardHandler(cardPositionIndex: i, cardTypeValue: cardTypeWithAssociatedValue)
            }
        }
        switch cardTypeValue {
        case .upcomingRally: // done
            print("func defineCards:.active case is running...")
            startingCardPositionIndex = homeData.activeRallysCardsArray.count // value will be 0 or 1 depending on whether viewDidLoad or homePageLoadUIHandler calls defineCards respectively. the possible card in question is the new rally card
        case .publicCommunity: // communities the user is a part of listed in the rally tab under communities section
            print("func defineCards:.publicCommunity case is running...")
            startingCardPositionIndex = homeData.publicCommunitiesCardsArray.count
        case .privateCommunity: // communities the user is a part of listed in the rally tab under communities section
            print("func defineCards: .privateCommunity case is running...")
            startingCardPositionIndex = homeData.privateCommunitiesCardsArray.count
        default:
            break
        }
        defineCardsForLoop()
        switch cardTypeValue {
        case .publicCommunity:
            // now that user main thread saved groups and user joined community initial cards have been created (background may not have completed, only first card is referenced if existing), call tutorial
            // defines tutorial blurbs if cards exist to display above and should display is true
            updateRallyViewViewsSizes() // tutorial frames depend on updated card frames
            let cardsArrayArray = [homeData.publicCommunitiesCardsArray]
            var referenceCard: UIButton?
            var caseType: [tutorialBlurbType] = [.howToUseRallyCommunityCard]
            for (index, cardsArray) in cardsArrayArray.enumerated() {
                if cardsArray.count > 0 {
                    referenceCard = cardsArray[0]
                }
                TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(caseType[index], viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
                referenceCard = nil // reset for next cardsArray
            }
            //REENABLE
            // determine and if necessary define and display .whatAreCommunities tutorial blurb
        //TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.whatAreCommunities, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
        default:
            break
        }
    }
    
    func updateRallyViewViewsSizes() { // updates y-origin and height of: activeRallyCardsAndHeaderViewsSuperview, savedGroupsSuperview, publicPrivateCommunitiesCardContainerInfiniteScrollView, myCommunitiesSuperview, and rallyViewSuperView
        print("func updateRallyViewViewsSizes is running...")
        // NOTE: active RallyCardsScrollView and activeRallyCardsAndHeaderViewsSuperview width is constant, activeRallyCardsAndHeaderViewsSuperview origin is constant
        // update activeRallyCardsScrollView y-origin
        if let activeRallySectionLabel = activeRallyCardsAndHeaderViewsSuperview.viewWithTag(592) {
            activeRallyCardsScrollView.frame.origin.y = activeRallySectionLabel.frame.origin.y + activeRallySectionLabel.frame.height+cardSuperviewYPaddingFromSectionLabel
        }
        // update activeRallyCardsAndHeaderViewsSuperview height
        // update activeRallyCardsScrollView height
        let numberOfActiveRallys = calculateNumberOfCardsInViewWithTag(homeData.activeRallysCardsArray)
        print("numberOfActiveRallys:\(numberOfActiveRallys)")
        if numberOfActiveRallys > 0 { // check assures there is at least one element in activeRallysCardsArray
            activeRallyCardsScrollView.frame.size.height = homeData.activeRallysCardsArray.last!.frame.origin.y+homeData.activeRallysCardsArray.last!.frame.height
            activeRallyCardsAndHeaderViewsSuperview.frame.size.height = activeRallyCardsScrollView.frame.origin.y + activeRallyCardsScrollView.frame.height
        }
        else { // if no cards, size superview to fit header and section label
            if let activeRallysSectionLabel = activeRallyCardsAndHeaderViewsSuperview.viewWithTag(592) {
                activeRallyCardsAndHeaderViewsSuperview.frame.size.height = activeRallysSectionLabel.frame.origin.y + activeRallysSectionLabel.frame.height
                activeRallyCardsScrollView.frame.size.height = activeRallysSectionLabel.frame.origin.y + activeRallysSectionLabel.frame.height
            }
        }
        // update activeRallyCardsScrollView contentsize.width
        let width = CGFloat(homeData.activeRallysCardsArray.count)*(2.0*MYCard.horizontalCardsSpacingUnit+MYCard.upcomingRallyCardWidth)
        if width > ScreenDimension.width {
            activeRallyCardsScrollView.contentSize.width = width
            print("activeRallyCardsScrollView.contentSize.width:\(activeRallyCardsScrollView.contentSize.width)")
            print("ScreenDimension.screenWidth:\(ScreenDimension.width)")
        }
        else {
            activeRallyCardsScrollView.contentSize.width = activeRallyCardsScrollView.frame.width
        }
        // update users communities section label (public & private button) y-origin NOTE: This is the only label that requires updating. Requires cards to use to position it properly
        // update publicPrivateCommunitiesCardContainerInfiniteScrollView y-origin
        // NOTE: publicPrivateCommunitiesCardContainerInfiniteScrollView width and contentsize (it's a view, not a scrollview) are constant
        if let startNewRallySectionLabel = myCommunitiesSuperview.viewWithTag(593) {
            if publicCommunitySectionButton != nil {
                // set publicCommunitySectionButton y-origin
                publicCommunitySectionButton.frame.origin.y = startNewRallySectionLabel.frame.origin.y + startNewRallySectionLabel.frame.height+cardSuperviewYPaddingFromSectionLabel
                let underlineContainerYOrigin = publicCommunitySectionButton.frame.origin.y+publicCommunitySectionButton.frame.height+1.0
                // set underlineContainer y-origin
                underlineContainer.frame.origin.y = underlineContainerYOrigin
                if privateCommunitySectionButton != nil {
                    // set privateCommunitySectionButton y-origin
                    privateCommunitySectionButton.frame.origin.y = startNewRallySectionLabel.frame.origin.y + startNewRallySectionLabel.frame.height+cardSuperviewYPaddingFromSectionLabel
                    // set infinite scroll y-origin
                    publicPrivateCommunitiesCardContainerInfiniteView.frame.origin.y = privateCommunitySectionButton.frame.origin.y + privateCommunitySectionButton.frame.height + cardSuperviewYPaddingFromSectionLabel
                }
                // set underlineOne origin
                // set underlineTwo origin
                updateUnderlineXorigin() // does all three
            }
        }
    
    // update publicCardContainer height (width is constant, set at initialization)
    // update privateCardContainer height (width is constant, set at initialization)
    if let lastCard = homeData.publicCommunitiesCardsArray.last { // check assures there is at least one element in usersCommunitiesCardsArray
        publicCardContainer.frame.size.height = lastCard.frame.origin.y+lastCard.frame.height+lastCard.layer.shadowOffset.height
    }
    else {
        publicCardContainer.frame.size.height = 0
    }
    if let lastCard = homeData.privateCommunitiesCardsArray.last {
        privateCardContainer.frame.size.height = lastCard.frame.origin.y+lastCard.frame.height+lastCard.layer.shadowOffset.height
    }
    else {
        privateCardContainer.frame.size.height = 0
    }
    // update publicPrivateCommunitiesCardContainerInfiniteScrollView height (width is constant, set at initialization)
        publicPrivateCommunitiesCardContainerInfiniteView.frame.size.height = publicCardContainer.frame.height > privateCardContainer.frame.height ? publicCardContainer.frame.height : privateCardContainer.frame.height
    // NOTE: myCommunitiesSuperview width and contentsize (it's a view, not a scrollview) is constant
    // update myCommunitiesSuperview y-origin
    myCommunitiesSuperview.frame.origin.y = activeRallyCardsAndHeaderViewsSuperview.frame.origin.y+activeRallyCardsAndHeaderViewsSuperview.frame.height
    // update myCommunitiesSuperview height
    myCommunitiesSuperview.frame.size.height = publicPrivateCommunitiesCardContainerInfiniteView.frame.origin.y+publicPrivateCommunitiesCardContainerInfiniteView.frame.height
        
        // update communitiesNearbyCardsSuperview y-origin
        // NOTE: communitiesNearbyCardsSuperview width and contentsize (it's a view, not a scrollview) are constant
        if let communitiesNearbySectionLabel = userQueryCommunitiesNearbyResultsSuperview.viewWithTag(600) { // 600 is users communities section label tag value
            communitiesNearbyCardsSuperview.frame.origin.y = communitiesNearbySectionLabel.frame.origin.y + communitiesNearbySectionLabel.frame.height + cardSuperviewYPaddingFromSectionLabel
            // update communitiesnearbycardssuperview height
            if homeData.communitiesNearbyCardsArray.count > 0 { // check assures there is at least one element in usersCommunitiesCardsArray
                // update userQueryCommunitiesNearbyResultsSuperview scrollview contentSize if subview of VC view
                communitiesNearbyCardsSuperview.frame.size.height = homeData.communitiesNearbyCardsArray.last!.frame.origin.y+homeData.communitiesNearbyCardsArray.last!.frame.height
            }
            else {
                communitiesNearbyCardsSuperview.frame.size.height = 0
            }
            // update userQueryCommunitiesNearbyResultsSuperview content size, height and width frame is a constant (equals view)
            userQueryCommunitiesNearbyResultsSuperview.contentSize.height = view.frame.height*110/100 // constant value to assure view can always be scrolled to trigger dismiss keyboard, even if content isn't off page
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                userQueryCommunitiesNearbyResultsSuperview.contentSize.height = view.frame.height*115/100 // constant value to assure view can always be scrolled to trigger dismiss keyboard, even if content isn't off page
            default:
                print("Do nothing...")
            }
        }
        // update tutorial blurb y-origin
        if let _ = rallyViewSuperView.superview {
            let (viewsToRemoveFromSuperview,blurbTypes):([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType]) = TutorialHelper.tutorialViewsInViewControllerView(rallyViewSuperView)
            for tutorialSuperview in viewsToRemoveFromSuperview {
                let label = tutorialSuperview.viewWithTag(5000) as! CustomLabel
                let labelTail = tutorialSuperview.viewWithTag(5001) as! TriangeViewClass
                let labelAndLabelTailSuperview = tutorialSuperview
                let gotItButton = tutorialSuperview.viewWithTag(5002) as! UIButton
                var referenceCard: UIButton!
                //REENABLE
                /*switch tutorialSuperview.tutorialType {
                case .whatAreCommunities:
                    if let startACommunityButton = manageCommunitiesSuperview.viewWithTag(224) as? UIButton {
                        referenceCard = startACommunityButton
                    }
                case .howToUseRallyCommunityCard:
                    if usersCommunitiesCardsArray.count > 0 {
                        referenceCard = usersCommunitiesCardsArray[0]
                    }
                default:
                    break
                }*/
                //TutorialHelper.tutorialHelperClassSingleton.updateXAndYOriginForViews(label, labelTail: labelTail, labelAndLabelTailSuperview: labelAndLabelTailSuperview, gotItButton: gotItButton, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
            }
            // update rallyViewSuperView contentsize height

            if blurbTypes.contains(.whatAreCommunities) {
                let superView = viewsToRemoveFromSuperview.filter({$0.tutorialType == .whatAreCommunities})
                //rallyViewSuperView.contentSize.height = superView[0].frame.origin.y+superView[0].frame.height+ScreenDimension.screenHeight*4/100 // constant value at end adds appropriate padding
            }
            else {
                //rallyViewSuperView.contentSize.height = myCommunitiesSuperview.frame.origin.y+myCommunitiesSuperview.frame.size.height+ScreenDimension.screenHeight*4/100 // constant value at end adds appropriate padding
            }
        }
        let (viewsToBringToFront,_):([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType]) = TutorialHelper.tutorialViewsInViewControllerView(rallyViewSuperView)
        for view in viewsToBringToFront {
            print("viewsToBringToFront.description:\(view.description)")
            rallyViewSuperView.bringSubview(toFront: view)
        }
    }
    
    func updateRallyViewSuperviewHeight(_ notification: Notification) {
        print("func updateRallyViewSuperviewHeight is running...")
        /*
        rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        updateRallyViewViewsSizes()
        if rallyViewSuperView.contentSize.height>=view.frame.height {
            let finalYOffset = rallyViewSuperView.contentSize.height-view.frame.height
            print("finalYOffset:\(finalYOffset)")
            rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: finalYOffset), animated: false)
        }
         */
    }
    
    func calculateSuperViewHeightToFitSubviews(_ superView: UIView)-> CGFloat { // not used yet, less efficient but is a more general form, consider using in updateRallyViewViewsSizes()
        print("func calculateSuperViewHeightToFitSubviews is running...")
        var largestYOffsetPlusHeightValue: CGFloat = 0
        for subview in superView.subviews {
            let subviewYOffsetPlusHeightValue: CGFloat = subview.frame.origin.y+subview.frame.height
            if subviewYOffsetPlusHeightValue>largestYOffsetPlusHeightValue {
                largestYOffsetPlusHeightValue = subviewYOffsetPlusHeightValue
            }
        }
        return largestYOffsetPlusHeightValue
    }
    
    func calculateNumberOfCardsInViewWithTag(_ cardArray: [UIButton])->Int {
        print("func calculateNumberOfCardsInViewWithTag is running...")
        var numberOfCards = 0
        for rallyCard in cardArray {
            if rallyCard.superview != nil {
                numberOfCards += 1
            }
        }
        return numberOfCards
    }
    
    func defineCardHandler(cardPositionIndex: Int, cardTypeValue: CardType) {
        print("func defineCardHandler is running...")
        // initialize/generate card
        let card = MYCard(cardTypeValue: cardTypeValue, cardPositionIndex: cardPositionIndex)
        // make request for member profile pictures from database
        card.liveLoadThumbnailsHandler(precedingMemberIndex: nil)
        // set card type specific values/properties
        switch cardTypeValue {
        case .upcomingRally:
            // set card:onTouch method
            card.addTarget(self, action: #selector(HomeViewController.onRallyCardTouch(_:)), for: .touchUpInside)
            // add card as subview of scrollview
            activeRallyCardsScrollView.addSubview(card)
            // add card to upcoming rallys array
            homeData.activeRallysCardsArray.append(card)
        case .publicCommunity:
            card.addTarget(self, action: #selector(HomeViewController.onPublicCommunityCardTouch(_:)), for: .touchUpInside)
            publicCardContainer.addSubview(card)
            homeData.publicCommunitiesCardsArray.append(card)
        case .privateCommunity:
            card.addTarget(self, action: #selector(HomeViewController.onPrivateCommunityCardTouch(_:)), for: .touchUpInside)
            privateCardContainer.addSubview(card)
            homeData.privateCommunitiesCardsArray.append(card)
        case .communityNearby:
            card.addTarget(self, action: #selector(HomeViewController.onNearbyCommunityCardTouch(_:)), for: .touchUpInside)
            communitiesNearbyCardsSuperview.addSubview(card)
            homeData.communitiesNearbyCardsArray.append(card)
        default:
            break
        }
    }
    
// tableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("tableView:numberOfRowsInSection is running..")
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var rowHeight = ScreenDimension.height/10.6
        switch (indexPath as NSIndexPath).row {
        case 0:
            rowHeight = ScreenDimension.height/6.25
        default:
            break
        }
        return rowHeight // custom row height based on row (1st row has different height than other rows)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("func tableView:cellForRowAtIndexPath is running...")
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        //cell.contentView.backgroundColor = UIColor.whiteColor()
        cell.textLabel?.textColor = MYColor.menuGreyBlack
        //cell.selectionStyle = .none
        //cell.contentView.
        //cell.imageView?.tintColor = MYColor.menuGreyBlack
        cell.layoutMargins.left = cell.frame.width*2.8/100 // add left padding for content in cells
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16.25)
        // image size to display next to words
        var itemSideLength = ScreenDimension.width*6.4/100
        let row = (indexPath as NSIndexPath).row
        switch row {
        case 0:
            itemSideLength = itemSideLength*3.25
            if let firstName = MYUser.firstName {
                if let lastName = MYUser.lastName {
                    let fullName = firstName+" "+lastName
                    cell.textLabel?.text = fullName
                    //cell.isUserInteractionEnabled = false
                }
            }
            cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            cell.imageView?.image = MYUser.getProfileImage()
            cell.imageView?.layer.cornerRadius = itemSideLength/2 // Defines clockButtonsParentView frame to be circular
            cell.imageView?.clipsToBounds = true
            cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold)
            // NOTE: loop through all sublayers in cell and search each sublayer for key value pair to locate correct layer to see if it exists. if so, do not re-create line layer. otherwise, create and add. only do if flag doesn't work as hoped
            // border.setValue("", forKey: "cell zero")
            if menuSublayerArray.count == 0 {
                let border = defineTableViewRowSublayer(cell.frame.width*34/100, cellWidth: cell.frame.width, cellHeight: ScreenDimension.height/6.25)
                cell.contentView.layer.addSublayer(border)
                menuSublayerArray.append(border)
            }
        case 1:
            cell.textLabel?.text = "Reset Tutorial"
            cell.imageView?.image = UIImage(named:"reset")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.imageView?.transform = CGAffineTransform(scaleX: -1, y: 1)
            if menuSublayerArray.count == 1 {
                let border = defineTableViewRowSublayer(cell.frame.width*5/100, cellWidth: cell.frame.width, cellHeight: ScreenDimension.height/10.6)
                cell.contentView.layer.addSublayer(border)
                menuSublayerArray.append(border)
            }
        case 2:
            cell.textLabel?.text = "Report a Problem"
            cell.imageView?.image = UIImage(named:"report")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            if menuSublayerArray.count == 2 {
                let border = defineTableViewRowSublayer(cell.frame.width*5/100, cellWidth: cell.frame.width, cellHeight: ScreenDimension.height/10.6)
                cell.contentView.layer.addSublayer(border)
                menuSublayerArray.append(border)
            }
        case 3:
            cell.textLabel?.text = "Terms & Policy"
            cell.imageView?.image = UIImage(named:"legal")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            
            //let border = defineTableViewRowSublayer(cell.frame.width*20/100, cellWidth: cell.frame.width)
            //cell.contentView.layer.addSublayer(border)
            // below is not required since it would sit along edge of parent menu edge
            /*
            if menuSublayerArray.count == 3 {
            let border = defineTableViewRowSublayer(cell.frame.width*5/100, cellWidth: cell.frame.width, cellHeight: ScreenDimension.screenHeight/10.6)
            cell.contentView.layer.addSublayer(border)
                menuSublayerArray.append(border)
            }*/
            /*
        case 3:
            cell.textLabel?.text = "Logout"
            cell.imageView?.image = UIImage(named:"quill")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            */
        default:
            break
        }
        // inset imageview to set smaller image
        if let image = cell.imageView?.image {
            let itemSize = CGSize(width: itemSideLength, height: itemSideLength)
            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
            MYColor.menuGreyBlack.set()
            let imageRect = CGRect(x: 0.0, y: 0.0, width: itemSize.width, height: itemSize.height)
            image.draw(in: imageRect)
            cell.imageView!.image! = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        return cell
    }
    
    func defineTableViewRowSublayer(_ xOrigin: CGFloat, cellWidth: CGFloat, cellHeight: CGFloat)->CALayer {
        print("func defineTableViewRowSublayer is running...")
        let border = CALayer()
        let width = CGFloat(0.3)
        border.borderColor = MYColor.darkGrey.cgColor
        border.frame = CGRect(x: xOrigin, y: cellHeight-width, width:  cellWidth, height: width)
        border.borderWidth = width
       return border
    }
    
    func schemeAvailable(_ scheme: String) -> Bool {
        print("func schemeAvailable is running...")
        if let url = URL.init(string: scheme) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("func tableView:didSelectRowAtIndexPath is running...")
        let row = (indexPath as NSIndexPath).row
        dismissMenu(nil)
        switch row {
        case 0: // user name and profile picture
            print("edit profile picture")
            CameraPhotoLibraryHelper = CameraPhotoLibrary()
            CameraPhotoLibraryHelper!.displayCameraAndPhotosActionSheet(self)
        case 1: // reset tutorial
            print("reset tutorial")
            TutorialHelper.tutorialHelperClassSingleton.markMessageBlurbAsUnread()
            if let _ = rallyViewSuperView.superview {
                //  scroll to top of rallyViewSuperView before calculating message blurbs
                //rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                // add any tutorial blurbs that should be displayed on home screen
                let cardsArrayArray = [homeData.publicCommunitiesCardsArray]
                var referenceCard: UIButton?
                var caseType: [tutorialBlurbType] = [.howToUseRallyCommunityCard]
                for (index, cardsArray) in cardsArrayArray.enumerated() {
                    if cardsArray.count > 0 { //
                        referenceCard = cardsArray[0]
                    }
                    TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(caseType[index], viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
                    referenceCard = nil // reset for next cardsArray
                }
                //REENABLE
                // determine and if necessary define and display .whatAreCommunities tutorial blurb
                TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.whatAreCommunities, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
            }
            updateRallyViewViewsSizes()
        case 2:
            // pop-up text-box for user to type report and a send and cancel button for the text-box
            ReportAProblemClass(title: "Report a Problem").reportAProblemViewsHandler(navigationController!)
        case 3: // display Terms & Policy
            // re-direct to 150labs website, terms and privacy policy sections
            displayTermsAndPolicyActionSheet()
        default:
            break
        }
    }
    
    func displayTermsAndPolicyActionSheet() {
        print("func displayTermsAndPolicyActionSheet is running...")
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Terms of Service", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let scheme: String = "https://150labs.net" // change URL for terms
            if self.schemeAvailable(scheme) {
                UIApplication.shared.openURL(URL(string: scheme)!)
            }
        })
        let saveAction = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let scheme: String = "https://150labs.net"
            if self.schemeAvailable(scheme) {
                UIApplication.shared.openURL(URL(string: scheme)!)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            optionMenu.dismiss(animated: true, completion: nil)
        })
        
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
// tableView methods
    
// menu methods
    func displayMenu() {
        print("func displayMenu is running...")
        //parentMenuView.hidden = false
        // calculate parentMenuView visible height
        let roundedRectangleMenuViewComponent = self.parentMenuView.viewWithTag(507)!
        let roundedRectangleYOrigin = roundedRectangleMenuViewComponent.frame.origin.y
        let roundedRectangleHeight = roundedRectangleMenuViewComponent.frame.height
        let fullHeight = roundedRectangleYOrigin + roundedRectangleHeight
        // define dullingView
        let yOffset = UIApplication.shared.statusBarFrame.height+UINavigationBarTaller.navigationBarHeight
        let dullingView = UIView(frame: CGRect(x: 0, y: yOffset, width: view.bounds.width, height: view.bounds.height))
        dullingView.tag = 567
        let rgb: RGB = (red: 0.0, green: 0.0, blue: 0.0, alpha: 0.28)
        var hsb = colorHelperClass().rgb2hsv(rgb)
        hsb.saturation = 0.5
        let rgb2 = colorHelperClass().hsv2rgb(hsb)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissMenu(_:)))
        dullingView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.0)
        dullingView.addGestureRecognizer(tap)
        self.navigationController!.view.addSubview(dullingView)
        self.navigationController!.view.bringSubview(toFront: parentMenuView)
        // display parentMenuView and dullingView on screen with an animation effect
        parentMenuView.isHidden = false
        UIView.animate(withDuration: 0.11, delay: 0.05, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.parentMenuView.frame.size.height = fullHeight
            },
                                   completion: { finished in
                                    print("searchBarShouldBeginEditing:animateWithDuration:completion")
        })
        UIView.animate(withDuration: 0.14, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            // animate dulling view color to semi-transparent dark grey
            dullingView.backgroundColor = UIColor(red: rgb2.red, green: rgb2.green, blue: rgb2.blue, alpha: rgb2.alpha)
            },
                                   completion: { finished in
                                    print("searchBarShouldBeginEditing:animateWithDuration:completion")
        })
        // need to update profile picture, can't reload data without handling clearing duplicate data
        menuTableView.reloadData()
    }
    
    func dismissMenu(_ sender:UITapGestureRecognizer?) {
        print("func dismissMenu is running...")
        //if parentMenuView.hidden == false {
            if let dullingView = self.navigationController!.view.viewWithTag(567) {
                /*let triangleMenuViewComponent = self.parentMenuView.viewWithTag(506)!
                let triangleHeight = triangleMenuViewComponent.frame.height
                let roundedRectangleMenuViewComponent = self.parentMenuView.viewWithTag(507)!
                let roundedRectangleHeight = roundedRectangleMenuViewComponent.frame.height
                let fullHeight = triangleHeight + roundedRectangleHeight*/
                UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
                    //self.parentMenuView.layoutMargins.bottom = fullHeight
                    // animate parentMenuView height to zero (which in effect, hides view)
                    self.parentMenuView.frame.size.height = 0
                    // animate dullingView color to transparent
                    dullingView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.0)
                    },
                                           completion: { finished in
                                            print("searchBarShouldBeginEditing:animateWithDuration:completion")
                                            // update hide parentMenuView state
                                            self.parentMenuView.isHidden = true // needs to be hidden, making height zero is still leaving a ghost box of sorts thats not allowing for proper touching of screens where menu was, even after closing menu
                                            // remove dulling view
                                            dullingView.removeFromSuperview()
                                            for recognizer in dullingView.gestureRecognizers! {
                                                dullingView.removeGestureRecognizer(recognizer)
                                            }
                })
            }
        //}
    }
// menu methods
    
// onTouch button action methods
    func onMenuButtonTouch(_ sender: UIButton!) {
        print("func onMenuButtonTouch is running...")
        if parentMenuView.frame.height == 0 {
            // and if menu is not subview of navcontroller.view
            displayMenu()
        }
        else {
            dismissMenu(nil)
        }
    }

    func onJoinCommunityButtonTouch(_ sender: UIButton!) {
        print("func onJoinCommunityButtonTouch is running...")
        TutorialHelper.removeTutorialBlurbsFromViewControllerView(self.rallyViewSuperView)
        self.rallyViewSuperView.removeFromSuperview() // removes communitiesViewSuperview from view. must re-add upon exiting search
        
        
        self.defineSearchableGroupsSearchBar()
        self.defineSectionHeadersLabels(.searchCommunitiesNearbyView)
        self.view.addSubview(self.userQueryCommunitiesNearbyResultsSuperview)
        homeData.updateLocationMethodFlag = "updateLocationToFetchCommunitiesNearby"
        permissionsHelperClass.userLocation = nil
        permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateUserLatitudeAndLongitude is listening for within homeVC which updates User.Latitude, User.Longitude, and then runs webservice method to fetch nearby communities array within 300 miles (Default value defined in database)
        //view.isUserInteractionEnabled = false
    }
    
    func onRallyCardTouch(_ sender: UIButton!) {
        print("func onRallyCardTouch is running...")
        let rallyCardTag = sender.tag // rally index value is assigned on creation of rally card button to tag value
        let superViewTag = sender.superview!.tag
        var rallysArray: [Rally] = upcomingRallysGlobalArray // default to avoid assigning in else clause below
        let rallyObject = rallysArray[rallyCardTag]
        // handle transition to correct view controller
        transitionToRelevantViewController(rallyObject)
    }
    
    func transitionToRelevantViewController(_ rallyObject: Rally) {
        print("func transitionToRelevantViewController is running...")
        // transition to relevant view controller (invite, detail/join, or messaging)
        let inviteViewControllerInstance: InviteViewController = InviteViewController(nibName: nil, bundle: nil)
        // pass rallyObject to new viewController
        inviteViewControllerInstance.rallyObject = rallyObject
        if rallyObject.isRallyActive == 0 { // if true, rally is being used as template
            /*rallyObject.matchRallyMembersWithPhoneContacts(completionHandler: {() -> Void in
                self.navigationController!.pushViewController(inviteViewControllerInstance, animated: true)
            })*/
        }
        else { // rally is active, has rally tipped, move to details screen if not tipped, move to messaging screen if tipped
            let detailsViewControllerInstance: DetailsViewController = DetailsViewController(nibName: nil, bundle: nil)
            // pass rallyObject to new viewController
            detailsViewControllerInstance.rallyObject = rallyObject
            if rallyObject.rallyHasTipped() {
                print("transitionToRelevantViewController: inside rallyHasTipped")
                let messagingViewControllerInstance: MessagingViewController = MessagingViewController(nibName: nil, bundle: nil)
                // pass rallyObject to new viewController
                messagingViewControllerInstance.rallyObject = rallyObject
                //self.navigationController!.pushViewController(inviteViewControllerInstance, animated: false)
                self.navigationController!.pushViewController(detailsViewControllerInstance, animated: false)
                self.navigationController!.pushViewController(messagingViewControllerInstance, animated: true)
            }
            else {
                print("transitionToRelevantViewController: inside rallyHasTipped else function")
                //self.navigationController!.pushViewController(inviteViewControllerInstance, animated: false)
                //view.bringSubview(toFront: detailsViewControllerInstance.navigationController!.view)
                
                // secondaryContainerVC
                // primaryContainerVC
                //navigationController?.view.bringSubview(toFront: self.view)
                primaryContainerVC.view.bringSubview(toFront: self.navigationController!.view) //self.view)
                //self.navigationController?.view
                self.navigationController!.pushViewController(detailsViewControllerInstance, animated: false)
                
                
            }
        }
    }
    
    func onPublicCommunityCardTouch(_ sender: MYCard!) {
        print("func onPublicCommunityCardTouch is running...")
        // append rally object from global groups array to [rallys] global array as inactive rally
        // refresh displayed rallys (displayed first in last requires all rally card position to be udpated for move back one spot, should be a single function call looping through entire userRallyCardsArray and if necessary, removing then readding as subviews or something more clever to refresh UI) (might be better to do upon returning to screen)
        // create rally card for the rally and add to userRallyCardsArray
        
        
        
        // set this as active community in communityMessagingVC
        let communityMessagingVC = secondaryContainerVC.centerViewControllerCP as! CommunityMessagingViewController
        communityMessagingVC.communityMessagingData.community = sender.publicCommunity
        
        // update active thread in communityMessagingVC
        communityMessagingVC.communityMessagingData.activeThread = sender.publicCommunity?.defaultThreadForFocus()
        
        // fetch messages for active thread
        communityMessagingVC.fetchActiveThreadMessagesFromWebService()
        // communityMessagingVC.communityMessagingData.messageBlurbs = []
        
        // reload UI for community messaging
        communityMessagingVC.reloadUI()

        // set this as active community in leftPanelVC
        if secondaryContainerVC.drawerViewController != nil {
            let leftPanelVC = secondaryContainerVC.drawerViewController as! LeftPanelViewController

            leftPanelVC.leftPanelData.community = sender.publicCommunity

            // reload UI for leftPanel
            leftPanelVC.reloadUI()
        }
        
        // animate community messaging section up
        secondaryContainerVC.animateBottomPanel(shouldExpand: true)
        
        
        
        
        /*
        
        homeData.community = sender.publicCommunity
         
        homeData.community = Community(homeData.community!.members) // creates new rally
        // update location to user location, once updated post rally
        homeData.updateLocationMethodFlag = "updateNewRallyDefaultLocation"
        self.view.isUserInteractionEnabled = false
        permissionsHelperClass.userLocation = nil
        permissionsHelperClass.locationManagerSingleton.startUpdatingLocation() // delegate method in apppermissionshelper will receive when location updates and post notification which method updateLocationForNewRallyThenMoveToDetailsVC is listening for within homeVC which updates localSearchRequest userLocation property
 
 
         */
        
        
    }
    
    func onPrivateCommunityCardTouch(_ sender: MYCard!) {
        print("func onPrivateCommunityCardTouch is running...")
        // update community property in communityMessagingVC
        let communityMessagingVC = secondaryContainerVC.centerViewControllerCP as! CommunityMessagingViewController
        communityMessagingVC.communityMessagingData.community = sender.privateCommunity
        
        // update active thread in communityMessagingVC
        communityMessagingVC.communityMessagingData.activeThread = sender.privateCommunity?.defaultThreadForFocus()
        
        // reload UI for community messaging
        communityMessagingVC.reloadUI()
        
        if secondaryContainerVC.drawerViewController != nil {
            // set this as active community in leftPanelVC
            let leftPanelVC = secondaryContainerVC.drawerViewController as! LeftPanelViewController
            leftPanelVC.leftPanelData.community = sender.privateCommunity
            
            // reload UI for leftPanel
            leftPanelVC.reloadUI()
        }
        
        // animate community messaging section up
        secondaryContainerVC.animateBottomPanel(shouldExpand: true)
    }
    
    func onNearbyCommunityCardTouch(_ sender: UIButton!) {
        print("func onNearbyCommunityCardTouch is running...")
        let publicGroupTitle: String = "Community"
        let card = sender as! MYCard
        card.titleTextView.text = publicGroupTitle
        var presentingViewController: UIViewController = self
        if let searchController = self.presentedViewController {
            // do nothing, something is already presented
            presentingViewController = searchController
        }
        // do something, present alert
        // http://stackoverflow.com/questions/25511945/swift-alert-view-ios8-with-ok-and-cancel-button-which-button-tapped
        let alert = UIAlertController(title: "Join \(publicGroupTitle)?", message: "Joining a community lets you Rally everyone in this community and notifies you when any other member send a Rally.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Join Community", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            // add group to user's groups
            self.joinGroupHelper(sender)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            sender.isUserInteractionEnabled = true
            alert.dismiss(animated: true, completion: nil)
        }))
        presentingViewController.present(alert, animated: true, completion: nil) // Should this be done using navigationController?
    }
// onTouch button action methods

// search & join new group methods (may also include onNearbyCommunityCardTouch method directly above part of these methods)
    func joinGroupHelper(_ publicGroupCard: UIButton!) {
        print("func joinGroupHelper is running...")
        // disable all nearby community cards while webcall resolves
        for subview in homeData.communitiesNearbyCardsArray {
            subview.isUserInteractionEnabled = false
        }
        let newFriend = Friend()
        newFriend.phoneNumberList = MYUser.phoneNumberList
        communityMemberThumbnailsGlobalDictionary[MYUser.ID] = MYUser.thumbnailProfileImage
        newFriend.firstName = MYUser.firstName
        newFriend.lastName = MYUser.lastName
        newFriend.ID = MYUser.ID
        newFriend.stockImageID = MYUser.stockImageID
        let publicGroupCardTag = publicGroupCard.tag // rally index value is assigned on creation of rally card button to tag value
        let newlyJoinedCommunity = homeData.userQueryCommunitiesNearbyResultsArray[publicGroupCardTag]
        let intent = WebServiceCallIntent.joinCommunity(newlyJoinedCommunity)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    newlyJoinedCommunity.members.append(newFriend)
                    self.homeData.updateLocationMethodFlag = "updateDisplayAfterUserJoinedCommunityNearby"
                    permissionsHelperClass.userLocation = nil
                    permissionsHelperClass.locationManagerSingleton.startUpdatingLocation()
                })
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func defineSearchableGroupsSearchBar() {
        print("func defineSearchableGroupsSearchBar is running...")
        customSearchController = CustomSearchController(searchResultsController: nil)
        customSearchController!.searchResultsUpdater = self
        customSearchController!.dimsBackgroundDuringPresentation = false
        customSearchController!.hidesNavigationBarDuringPresentation = false
        customSearchController!.delegate = self
        customSearchController!.searchBar.delegate = self
        customSearchController!.searchBar.placeholder = "Search all communities within 5 miles!"
        let searchBarParentView = UIView(frame: CGRect(x: ScreenDimension.width*13.5/100,y: ScreenDimension.width*4/100,width: ScreenDimension.width*80/100,height: UINavigationBarTaller.navigationBarHeight-ScreenDimension.width*4/100))
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            searchBarParentView.frame = CGRect(x: ScreenDimension.width*15/100,y: ScreenDimension.width*4.5/100,width: ScreenDimension.width*80/100,height: UINavigationBarTaller.navigationBarHeight-ScreenDimension.width*4/100)
        default:
            print("Do nothing...")
        }
        customSearchController!.searchBar.frame = CGRect(x: 0,y: 0,width: searchBarParentView.frame.width,height: customSearchController!.searchBar.frame.height)
        searchBarParentView.backgroundColor = MYColor.rallyBlue
        // http://stackoverflow.com/questions/26542035/create-uiimage-with-solid-color-in-swift
        let rect = CGRect(x: 0, y: 0, width: customSearchController!.searchBar.frame.width, height: customSearchController!.searchBar.frame.height)
        UIGraphicsBeginImageContextWithOptions(customSearchController!.searchBar.frame.size, false, 0)
        MYColor.rallyBlue.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        customSearchController!.searchBar.setBackgroundImage(image, for: .any, barMetrics: .default)
        customSearchController!.searchBar.backgroundColor = UIColor.clear
        navigationController?.navigationBar.addSubview(searchBarParentView)
        searchBarParentView.addSubview(customSearchController!.searchBar)
        // http://stackoverflow.com/questions/11572372/modifying-uisearchbar-cancel-button-font-text-color-and-style
        //let cancelButtonAttributes: NSDictionary = [NSFontAttributeName: UIFont.systemFontOfSize(16), NSForegroundColorAttributeName: UIColor.whiteColor()]
        //UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [String : AnyObject], forState: UIControlState.Normal)
        customSearchController!.searchBar.frame.origin.x = 4.0
        // define back button
        navigationItem.setHidesBackButton(true, animated: false)
        let leftButtonSelector: Selector = #selector(HomeViewController.backToHomeViewController(_:))
        let leftButtonImageReference = "back"
        let myCustomLeftButtonItem = UIBarButtonItem(image: UIImage(named: "\(leftButtonImageReference)")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .plain, target: self, action: leftButtonSelector)
        navigationItem.leftBarButtonItem  = myCustomLeftButtonItem
        let imageInsetValue = (navigationItem.leftBarButtonItem?.image!.size.height)!*15/100
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(imageInsetValue, 0, imageInsetValue, imageInsetValue) // left inset set to zero for transition from login screen to homeVC. Non-zero value causes animation jumping at end
        
        //navigationItem.leftBarButtonItem = nil
        navigationItem.title = nil
        navigationItem.rightBarButtonItems = nil
        customSearchController!.searchBar.becomeFirstResponder()
        //customSearchController!.searchBar.showsCancelButton = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("func scrollViewDidScroll is running...")
        // for communities nearby superview
        if let _ = customSearchController {
            if customSearchController!.searchBar.isFirstResponder {
                view.endEditing(true)
                customSearchController!.searchBar.resignFirstResponder()
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print("func updateSearchResultsForSearchController running...")
        // add gesture recognizer ot userQueryCommunitiesNearbyResultsSuperview on startup of searchController (delegate calls this method on startup), add if not added yet (this method keeps getting called throughout life of searchbar, don't want to re-add)
        if homeData.nearbyCardsAlreadyInitialized {
            // remove communities nearby cards from superview
            defineNearbyCommunitiesToDisplayArray()
            let cardType = CardType.communityNearby(nil)
            self.defineCards(cardType, numberOfCardsToDefine: homeData.userQueryCommunitiesNearbyResultsArray.count)
            self.updateRallyViewViewsSizes()
        }
    }
    
    func defineNearbyCommunitiesToDisplayArray() {
        print("func defineNearbyCommunitiesToDisplayArray is running...")
        if let _ = customSearchController {
            print("communitiesNearbyCardsArray.count:\(homeData.communitiesNearbyCardsArray.count)")
            view.isUserInteractionEnabled = true
            for subview in homeData.communitiesNearbyCardsArray {
                subview.isUserInteractionEnabled = true
                subview.removeFromSuperview()
            }
            /* for card in communitiesNearbyCardsSuperview {
             card.removeFromSuperview()
             }*/
            // empty communityObject array and card array
            homeData.communitiesNearbyCardsArray.removeAll()
            homeData.userQueryCommunitiesNearbyResultsArray.removeAll()
            if customSearchController!.searchBar.text!.isEmpty == true {
                // add first nine communities in array to display for empty search field case
                for community in communitiesNearbyGlobalArray {
                    if homeData.userQueryCommunitiesNearbyResultsArray.count < 9 {
                        homeData.userQueryCommunitiesNearbyResultsArray.append(community)
                    }
                    else {
                        break // end loops, nine queried communities already added to userQueryCommunitiesNearbyResultsArray
                    }
                }
            }
            else {
                // filter communitiesNearbyGlobalArray by user query keyword for first nine matching results, or all matching results if there are less than nine
                for community in communitiesNearbyGlobalArray {
                    // truncate larger of two terms so term character length is equal
                    //let smallerTerm = min(customSearchController!.searchBar.text!.characters.count,community.RallyName.characters.count)
                    let searchTextCharacterCount = customSearchController!.searchBar.text!.characters.count
                    let communityNameCharacterCount = community.name.characters.count
                    //var endIndex: NSIndex
                    /*if searchController.searchBar.text! == community.RallyName.substringToIndex(searchTextCharacterCount) {
                     
                     let range: Range<String.Index> = searchController.searchBar.text!.rangeOfString("b")!
                     let index: Int = text.startIndex.distanceTo(range.startIndex) //will call successor/predecessor several times until the indices match
                     }*/
                    //let endIndex = "".startIndex.advancedBy(smallerTerm)
                    // compare terms, if equal add to array
                    if homeData.userQueryCommunitiesNearbyResultsArray.count < 9 {
                        if searchTextCharacterCount<=communityNameCharacterCount { // if search term is larger or equal to community name, do something. Otherwise, do nothing
                            // match by start of title going through spaces i.e.: "pokemon gro" matches to "pokemon group"
                            if community.name.lowercased().hasPrefix(customSearchController!.searchBar.text!.lowercased()) {
                                homeData.userQueryCommunitiesNearbyResultsArray.append(community)
                                continue // move to next community in app array
                            }
                            /*
                             for index in 0..<searchTextCharacterCount {
                             let searchText = Array(arrayLiteral: searchController.searchBar.text!)
                             let communityName = Array(arrayLiteral: community.RallyName)
                             if searchText[index] == communityName[index] {
                             break
                             }
                             if index == searchTextCharacterCount {
                             userQueryCommunitiesNearbyResultsArray.append(communitiesNearbyGlobalArray[index])
                             /*if community.RallyName.substringToIndex(endIndex) == searchController.searchBar.text!.substringToIndex(endIndex) {*/
                             }
                             }*/
                            // match by start of any word in search i.e.: "gro" matches to "pokemon group"
                            let communityNameSplitByWordsArray = community.name.characters.split{$0 == " "}.map(String.init)
                            for word in communityNameSplitByWordsArray {
                                if word.lowercased().hasPrefix(customSearchController!.searchBar.text!.lowercased()) {
                                    homeData.userQueryCommunitiesNearbyResultsArray.append(community)
                                    break // move to next community in communitiesNearbyGlobalArray
                                }
                            }
                        }
                    }
                    else {
                        break
                    }
                }
            }
        }
    }
    
    func updateUserLatitudeAndLongitude(_ notification: Notification) {
        print("func updateUserLatitudeAndLongitude is running...")
        // update User.Latitude and User.Longitude
        if let userLocationValue = permissionsHelperClass.userLocation {
            MYUser.latitude = userLocationValue.latitude
            MYUser.longitude = userLocationValue.longitude
            print("User.latitude:\(MYUser.latitude)")
            print("User.longitude:\(MYUser.longitude)")
        }
        // load communitiesNearbyCardsArray with all non-user joined communities within 300 miles of user  
        // // empty string requests all communities within 300 miles
        if let _ = homeData.updateLocationMethodFlag {
            if homeData.updateLocationMethodFlag! == "updateLocationToFetchCommunitiesNearby" {
                let intent = WebServiceCallIntent.getNearbyCommunitySearchResults("")
                let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
                webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                    if succeeded {
                        if !self.homeData.nearbyCardsAlreadyInitialized { // if true, updateSearchResultsForSearchController didn't initialize cards bc call hadn't finished yet so initialize here
                            for community in communitiesNearbyGlobalArray {
                                if self.homeData.userQueryCommunitiesNearbyResultsArray.count < 9 {
                                    self.homeData.userQueryCommunitiesNearbyResultsArray.append(community)
                                }
                                else {
                                    break // end loops, nine queried communities already added to userQueryCommunitiesNearbyResultsArray
                                }
                            }
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.defineCards(.communityNearby(nil), numberOfCardsToDefine: self.homeData.userQueryCommunitiesNearbyResultsArray.count)
                                self.userQueryCommunitiesNearbyResultsSuperview.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                                self.updateRallyViewViewsSizes()
                            })
                        }
                        self.homeData.nearbyCardsAlreadyInitialized = true // reset flag for next user press on 'join community' card
                        self.view.isUserInteractionEnabled = true
                    }
                    else { // post message saying could not join group. try again later
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                })
            }
            if homeData.updateLocationMethodFlag! == "updateNewRallyDefaultLocation" {
                homeData.community!.latitude = CGFloat(MYUser.latitude)
                homeData.community!.longitude = CGFloat(MYUser.longitude)
                let moveToDetailsViewController: DetailsViewController = DetailsViewController(nibName: nil, bundle: nil)
                // pass rallyObject to new viewController
                moveToDetailsViewController.rallyObject = Rally(members: homeData.community!.members)
                homeData.community = nil // reset rallyObject before moving to detailsVC
                // transition to details screen
                self.view.isUserInteractionEnabled = true
                self.navigationController!.pushViewController(moveToDetailsViewController, animated: true)
            }
            if homeData.updateLocationMethodFlag! == "updateDisplayAfterUserJoinedCommunityNearby" {
                if let _ = self.userQueryCommunitiesNearbyResultsSuperview.superview {
                    let intent = WebServiceCallIntent.getNearbyCommunitySearchResults("")
                    let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
                    webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                        if succeeded {
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.defineNearbyCommunitiesToDisplayArray()
                                self.defineCards(.communityNearby(nil), numberOfCardsToDefine: self.homeData.userQueryCommunitiesNearbyResultsArray.count)
                                for subview in self.homeData.communitiesNearbyCardsArray {
                                    subview.isUserInteractionEnabled = true
                                }
                            })
                        }
                        else { // post message saying could not join group. try again later
                            DispatchQueue.main.async(execute: { () -> Void in
                            })
                        }
                    })
                }
                // .privateCommunity cannot be searched in nearby communities, must be invited
                let cardType = CardType.publicCommunity(nil)
                self.defineCards(cardType, numberOfCardsToDefine: 1) // creates card to add to "My Communities- from my communities" section on home screen. appends community to userJoinedCommunitiesGlobalArray
                self.updateRallyViewViewsSizes()
            }
        }
        homeData.updateLocationMethodFlag = nil
    }
    
    func backToHomeViewController(_ sender: UIButton!) {
        print("func backToHomeViewController is running...")
        // may need to empty userQueryCommunitiesNearbyResultsArray and communitiesNearbyCardsArray array on cancel button press
        if let unwrappedSearchController = customSearchController {
            if let searchBarSuperview = unwrappedSearchController.searchBar.superview {
                UIView.animate(withDuration: 0.3,
                                           animations:
                    {
                        // http://www.petersimpson.me/blog/static-nav-bar/
                        let navTransition = CATransition()
                        navTransition.duration = 0.3
                        navTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        navTransition.type = kCATransitionPush
                        navTransition.subtype = kCATransitionPush
                        self.navigationController?.navigationBar.layer.add(navTransition, forKey: nil)
                        unwrappedSearchController.searchBar.alpha = 0.0
                    },
                       completion: {(value: Bool) in
                        self.customSearchController!.searchBar.resignFirstResponder()
                        unwrappedSearchController.searchBar.removeFromSuperview()
                        searchBarSuperview.removeFromSuperview()
                        UIView.animate(withDuration: 0.85,
                            animations:
                            {
                            },
                            completion: {(value: Bool) in
                                searchBarSuperview.removeFromSuperview()
                                self.customSearchController!.searchBar.removeFromSuperview()
                                self.customSearchController!.isActive = false
                                self.customSearchController = nil
                                // configure nav bar
                                self.defineNavBar()
                        })
                })
            }
        }
        // remove communities nearby cards
        for subview in homeData.communitiesNearbyCardsArray {
            subview.removeFromSuperview()
        }
        // empty out arrays used for communities nearby search
        homeData.communitiesNearbyCardsArray.removeAll()
        homeData.userQueryCommunitiesNearbyResultsArray.removeAll()
        //REENABLE
        // determine and if necessary define and display .whatAreCommunities tutorial blurb
        //TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.whatAreCommunities, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: referenceCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
        // remove communities nearby cards superview
        userQueryCommunitiesNearbyResultsSuperview.removeFromSuperview()
        // reset nearbyCardsAlreadyInitialized flag for initializing nearby communities array from webservice
        homeData.nearbyCardsAlreadyInitialized = false
        // add any tutorial blurbs that should be displayed on home screen
        var savedCard: MYCard?
        if homeData.publicCommunitiesCardsArray.count > 0 { //
            savedCard = homeData.publicCommunitiesCardsArray[0]
        }
        TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.howToUseRallyCommunityCard, viewControllerView: rallyViewSuperView, navigationController: self.navigationController!, referenceCard: savedCard, VCViewAssociatedWithCard: view, barButtonItem: nil, barButtonItemImageViewFrame: nil)
        // display community tab superview
        if rallyViewSuperView.superview == nil {
            view.addSubview(rallyViewSuperView)
        }
        //rallyViewSuperView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        updateRallyViewViewsSizes()
    }
    
    /*func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        print("func searchBarCancelButtonClicked is running...")
        
    }*/
// search & join new group methods
    
    // generic Alert box object from UIKit library
    func displayMessageDialogBox(_ message: String) {
        print("func displayMessageDialogBox is running...")
        /*
         if let presentedViewController = self.presentedViewController { presentedViewController.dismissViewControllerAnimated(false, completion: nil) }
        */
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil) // Should this be done using navigationController?
    }
    
    override func viewWillDisappear(_ animated: Bool) { // always return to rally screen with 6 cards
        print("class HomeViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        // subtract badges associated with new Rallys
        // UIApplication.sharedApplication().applicationIconBadgeNumber -=
        // remove any tutorial blurbs in this view before moving to new screen
        TutorialHelper.removeTutorialBlurbsFromViewControllerView(rallyViewSuperView)
        updateRallyViewViewsSizes()
        unsubscribeToNotifications()
    }
    
    func unsubscribeToNotifications() {
        print("func unsubscribeToNotifications is running...")
        
        // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "fetchRallyAndGroupUserInfoFromWebServiceClassResponseCompleted"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "fetchUpcomingRallyDataFromWebServiceClassResponseCompleted"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "fetchUserJoinedCommunitiesDataFromWebServiceClassResponseCompleted"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "fetchStockImageDataFromWebServiceClassResponseCompleted"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RallyPackage"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "locationManagerFetchedUserRegion"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "whatAreCommunitiesTutorialBlurbMarkedRead"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updatedUserProfilePicture"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "splashScreenDismissed"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "profilePictureUploadResultReturnedFromWCF"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "leaveCommunityResultReturnedFromWCF"), object: nil)
        
        // on WCF return result, reload all relevant data
    }
}

extension HomeViewController: UIGestureRecognizerDelegate {
    
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("func gestureRecognizer: shouldReceiveTouch is running...")
        // https://stackoverflow.com/questions/28355041/swift-how-to-add-the-shouldreceivetouch-on-switch
        return false
    }
     */
    
    func handlePanGestureOnHomeScreen(_ recognizer: UIPanGestureRecognizer) {
        print("func handlePanGestureOnHomeScreen is running...")
        var point = recognizer.location(in: rallyViewSuperView)
        point = publicPrivateCommunitiesCardContainerInfiniteView.convert(point, from: rallyViewSuperView)
        if publicPrivateCommunitiesCardContainerInfiniteView.bounds.contains(point) {
            handlePanGesture(recognizer)
        }
        point = publicPrivateCommunitiesCardContainerInfiniteView.convert(point, from: rallyViewSuperView)
    }
    
    func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        print("func handlePanGesture is running...")
        switch recognizer.state {
        case .began:
            print(".began is running...")
            let horizontalVelocityMagnitude = abs(recognizer.velocity(in: view).x)
            let verticalVelocityMagnitude = abs(recognizer.velocity(in: view).y)
            homeData.panAxis = horizontalVelocityMagnitude > verticalVelocityMagnitude ? .horizontal : .vertical
        case .changed:
            print(".changed is running...")
            if homeData.panAxis == .horizontal {
                // get reference to x-translation
                let translation = recognizer.translation(in: view).x
                
                // update card container x-origins
                self.publicCardContainer.center.x = self.publicCardContainer.center.x+translation
                self.privateCardContainer.center.x = self.privateCardContainer.center.x+translation
                
                // calculate equivalent translation points for underlines
                let translationUnderlineEquivalency = translation*publicPrivateButtonCentersDistance/ScreenDimension.width
                
                // update underline x-origins
                underlineOne.center.x = self.underlineOne.center.x-translationUnderlineEquivalency
                underlineTwo.center.x = self.underlineTwo.center.x-translationUnderlineEquivalency
                
                // wrap underlines/cardContainers around container if necessary
                wraparoundUnderlineOrCardContainerIfNecessary()
                
                // re-zeroes translation
                recognizer.setTranslation(CGPoint.zero, in: view)
            }
            if homeData.panAxis == .vertical {
                // get reference to x-translation
                let translation = recognizer.translation(in: view).y
                
                // update card container x-origins
                var activeCardContainer: CardContainer!
                switch self.activeCardContainer {
                case .publicCommunity:
                    activeCardContainer = publicCardContainer
                case .privateCommunity:
                    activeCardContainer = privateCardContainer
                default:
                    break
                }
                let proposedYOriginPosition = activeCardContainer.frame.origin.y+translation
                let minYOrigin = -activeCardContainer.frame.height+MYCard.cardHeight+MYCard.horizontalCardsSpacingUnit
                if proposedYOriginPosition <= 0.0 && proposedYOriginPosition > minYOrigin {
                    activeCardContainer.frame.origin.y = proposedYOriginPosition
                }
                else if proposedYOriginPosition > 0.0 {
                    activeCardContainer.frame.origin.y = 0.0
                }
                else if proposedYOriginPosition < minYOrigin {
                    activeCardContainer.frame.origin.y = minYOrigin
                }
                // re-zeroes translation
                recognizer.setTranslation(CGPoint.zero, in: view)
            }
        case .ended:
            print(".ended is running")
            if homeData.panAxis == .horizontal {
                // update quadrants
                updateCardContainerQuadrants()
                updateUnderlineQuadrants()
                
                // animate underlines and containers to center of quadrants
                print("underlineOne.quadrant:\(underlineOne.quadrant)")
                print("underlineTwo.quadrant:\(underlineTwo.quadrant)")
                animateUnderlinesToQuadrantCenters()
                animateCardContainersToQuadrantCenters()
            }
        default:
            break
        }
    }
    
    func updateUnderlineXorigin() { // updates left/right underline x-origin
        print("func updateUnderlineXorigin is running...")
        // get a reference to active card container and determine left/right underline quadrants
        var activeCardContainer: CardContainer!
        var (leftUnderlineQuadrant, rightUnderlineQuadrant): (CGFloat, CGFloat) = (0.0,0.0)
        switch self.activeCardContainer {
        case .publicCommunity:
            activeCardContainer = publicCardContainer
            (leftUnderlineQuadrant, rightUnderlineQuadrant) = (1.0, 3.0)
        case .privateCommunity:
            activeCardContainer = privateCardContainer
            (leftUnderlineQuadrant, rightUnderlineQuadrant) = (0.0, 2.0)
        default:
            break
        }
        
        // calculate active card container x position
        let activeCardContainerCenterPositionAsPercent = (activeCardContainer.center.x-ScreenDimension.width)/ScreenDimension.width
        
        // set left/right underlines
        let (leftUnderline, rightUnderline): (MYWrapAroundUnderline, MYWrapAroundUnderline) = underlineOne.center.x <= underlineTwo.center.x ? (underlineOne,underlineTwo) : (underlineTwo, underlineOne)
        
        // update left/right underline center-x (height & y-origin is always constant)
        leftUnderline.center.x = publicPrivateButtonCentersDistance*(leftUnderlineQuadrant-activeCardContainerCenterPositionAsPercent)
        rightUnderline.center.x = publicPrivateButtonCentersDistance*(rightUnderlineQuadrant-activeCardContainerCenterPositionAsPercent)
    }
    
    func wraparoundUnderlineOrCardContainerIfNecessary() {
        print("func wraparoundUnderlineOrCardContainerIfNecessary is running...")
        // wrap-around underlineOne if necessary
        if underlineOne.center.x < -publicPrivateButtonCentersDistance {
            underlineOne.center.x = publicPrivateButtonCentersDistance*3
        }
        else if underlineOne.center.x > publicPrivateButtonCentersDistance*3 {
            underlineOne.center.x = -publicPrivateButtonCentersDistance
        }
        
        // wrap-around underlineTwo if necessary
        if underlineTwo.center.x < -publicPrivateButtonCentersDistance {
            underlineTwo.center.x = publicPrivateButtonCentersDistance*3
        }
        else if underlineTwo.center.x > publicPrivateButtonCentersDistance*3 {
            underlineTwo.center.x = -publicPrivateButtonCentersDistance
        }
        
        // wrap-around publicCardContainer if necessary
        if publicCardContainer.center.x < ScreenDimension.width*0.5 {
            publicCardContainer.center.x = privateCardContainer.frame.origin.x+privateCardContainer.frame.width+publicCardContainer.frame.width/2
        }
        else if publicCardContainer.center.x > ScreenDimension.width*2.5 {
            publicCardContainer.center.x = privateCardContainer.frame.origin.x-publicCardContainer.frame.width/2
        }
        
        // wrap-around privateCardContainer if necessary
        if privateCardContainer.center.x < ScreenDimension.width*0.5 {
            privateCardContainer.center.x = publicCardContainer.frame.origin.x+publicCardContainer.frame.width+privateCardContainer.frame.width/2
        }
        else if privateCardContainer.center.x > ScreenDimension.width*2.5 {
            privateCardContainer.center.x = publicCardContainer.frame.origin.x-privateCardContainer.frame.width/2
        }
    }
    
    func updateCardContainerQuadrants() {
        print("func updateCardContainerQuadrants is running...")
        
        // set left/right card container
        let (leftCardContainer, rightCardContainer): (CardContainer, CardContainer) = publicCardContainer.center.x < privateCardContainer.center.x ? (publicCardContainer,privateCardContainer) : (privateCardContainer, publicCardContainer)
        
        // leftCardContainer is always either in quadrant 0 or 1; correspondingly rightCardContainer is in 1 or 2
        if leftCardContainer.center.x < ScreenDimension.width {
            leftCardContainer.quadrant = 0
            rightCardContainer.quadrant = 1
        }
        else {
            leftCardContainer.quadrant = 1
            rightCardContainer.quadrant = 2
        }
    }

    func updateUnderlineQuadrants() {
        print("func updateUnderlineQuadrants is running...")
        // NOTE: method only gets called once underline reaches new quadrant. also, only one underline is tracked, other just moves accordingly. for example, underlineOne is at quadrant 1. user is scrolling right, pushing underlineOne to wrap around and 'appear' from right side (really underlineTwo appearing but user doesn't know there are two underlines). once underlineOne is completely off-screen, it is now in quadrant 0. accordingly underlineTwo is now in quadrant 2. the underline quadrant properties are updated using this function.
        
        // determine left and right underline
        let (leftUnderline, rightUnderline): (MYWrapAroundUnderline, MYWrapAroundUnderline) = underlineOne.center.x < underlineTwo.center.x ? (underlineOne,underlineTwo) : (underlineTwo, underlineOne)
        
        // leftUnderline is only ever in quadrant 0 or 1; correspondingly rightUnderline is in 2 or 3
        if leftUnderline.center.x < 0.0 {
            leftUnderline.quadrant = 0
            rightUnderline.quadrant = 2
        }
        else {
            leftUnderline.quadrant = 1
            rightUnderline.quadrant = 3
        }
    }
    
    func animateUnderlinesToQuadrantCenters() {
        print("func animateUnderlinesToQuadrantCenters is running...")
        // NOTE: no wrap-arounds occur in .ended(gesture recognizer). wrap-around is resolved in .change
        // NOTE: no underlines cross from non-visible through visible parts of view, they remain within their current quadrant (movement is only for centering within the quadrant)
        
        // animate underlineOne into center of current quadrant
        underlineOne.animate(toPosition: underlineCenterPosition(for: underlineOne.quadrant))
        
        // animate underlineTwo into center of current quadrant
        underlineTwo.animate(toPosition: underlineCenterPosition(for: underlineTwo.quadrant))
    }
    
    func underlineCenterPosition(for quadrant: CGFloat)-> CGFloat {
        print("func underlineQuadrantPosition is running...")
        switch quadrant {
        case 0, 4: // 4 should never happen
            return -0.5*publicPrivateButtonCentersDistance
        case 1: // 4 should never happen
            return 0.5*publicPrivateButtonCentersDistance
        case 2:
            return 1.5*publicPrivateButtonCentersDistance
        case 3:
            return 2.5*publicPrivateButtonCentersDistance
        default:
            return 0.0 // to satisfy compiler, this should never happen
        }
    }
    
    func animateCardContainersToQuadrantCenters() {
        print("func animateCardContainersToQuadrantCenters is running...")
        // animate publicCardContainer into center of current quadrant
        publicCardContainer.animate(toPosition: cardContainerCenterPosition(for: publicCardContainer.quadrant))
        
        // animate privateCardContainer into center of current quadrant
        privateCardContainer.animate(toPosition: cardContainerCenterPosition(for: privateCardContainer.quadrant))
    }
    
    func cardContainerCenterPosition(for quadrant: CGFloat)-> CGFloat {
        print("func underlineQuadrantPosition is running...")
        switch quadrant {
        case 0:
            return 0.5*ScreenDimension.width
        case 1:
            return 1.5*ScreenDimension.width
        case 2:
            return 2.5*ScreenDimension.width
        default:
            return 0.0 // to satisfy compiler, this should never happen
        }
    }
}

extension HomeViewController { // separate extension used for organization to centralize public/private button, underline and infinite scrollview UI methods (in homeviewcontroller)
    func defineMyCommunitiesPrivatePublicCommunitySectionButtonAndUnderlineHandler() {
        print("func defineMyCommunitiesPrivatePublicCommunitySectionButtonAndUnderlineHandler is running...")
        // define public/private buttons if not already initialized
        if publicCommunitySectionButton == nil {
            // define Public button, add as subview to myCommunitiesSuperview
            publicCommunitySectionButton = definePublicCommunitySectionButton()
            myCommunitiesSuperview.addSubview(publicCommunitySectionButton)
        }
        if privateCommunitySectionButton == nil {
            // define Private button, add as subview to myCommunitiesSuperview
            privateCommunitySectionButton = definePrivateCommunitySectionButton()
            myCommunitiesSuperview.addSubview(privateCommunitySectionButton)
        }
        // define underline container if not already initialized
        if underlineContainer == nil {
            underlineContainer = defineMyCommunitiesUnderlineContainer()
        }
        // define underlines if not already initialized
        if underlineOne.superview == nil {
            underlineOne = defineMyCommunitiesActiveSectionUnderline(in: 1.0)
            // define underlineOne, add as subview to myCommunitiesSuperview
        }
        if underlineTwo.superview == nil {
            // define underlineTwo, add as subview to myCommunitiesSuperview
            underlineTwo = defineMyCommunitiesActiveSectionUnderline(in: 3.0)
        }
        // set container and underlines as subviews
        myCommunitiesSuperview.addSubview(underlineContainer)
        underlineContainer.addSubview(underlineOne)
        underlineContainer.addSubview(underlineTwo)
    }
    
    func defineMyCommunitiesUnderlineContainer()-> UIView {
        print("func defineMyCommunitiesUnderlineContainer is running...")
        // update underlineContainer frame
        let underlineContainer = UIView()
        underlineContainer.clipsToBounds = true
        let underlineContainerWidth = publicPrivateButtonCentersDistance*2
        let underlineContainerHeight: CGFloat = 1.75
        underlineContainer.frame.size = CGSize(width: underlineContainerWidth, height: underlineContainerHeight)
        let underlineContainerXorigin = publicCommunitySectionButton.center.x-publicPrivateButtonCentersDistance*0.5 // NOTE: This is a negative number
        underlineContainer.frame.origin.x = underlineContainerXorigin
        return underlineContainer
    }
    
    func defineMyCommunitiesActiveSectionUnderline(in quadrant: CGFloat)->MYWrapAroundUnderline {
        print("func defineMyCommunitiesActiveSectionUnderline is running...")
        let activeSectionUnderline = MYWrapAroundUnderline(quadrant: quadrant)
        activeSectionUnderline.backgroundColor = MYColor.homeSectionSublabelGrey
        // initialize frame (height is constant)
        let width = underlineWidth
        activeSectionUnderline.frame.size = CGSize(width:width,height:1.75)
        return activeSectionUnderline
    }
    
    func definePublicCommunitySectionButton()-> MYButton {
        print("func definePublicCommunitySectionButton is running...")
        let publicCommunitySectionButton = MYButton(labelTitle: "Public")
        // set button size
        let width = underlineWidth
        let height = ScreenDimension.height*3.13/100
        publicCommunitySectionButton.frame.size = CGSize(width:width,height:height)
        // set button x-origin
        publicCommunitySectionButton.frame.origin.x = ScreenDimension.width*2.1/100
        // set label origin
        publicCommunitySectionButton.label.center.x = publicCommunitySectionButton.frame.width/2
        publicCommunitySectionButton.label.center.y = publicCommunitySectionButton.frame.height*48/100
        // set button target
        publicCommunitySectionButton.addTarget(self, action: #selector(HomeViewController.onPublicCommunitySectionButtonTouch(_:)), for: .touchUpInside)
        return publicCommunitySectionButton
    }
    
    func definePrivateCommunitySectionButton()-> MYButton {
        print("func definePrivateCommunitySectionButton is running...")
        let privateCommunitySectionButton = MYButton(labelTitle: "Private")
        // set button size
        let width = underlineWidth
        let height = ScreenDimension.height*3.13/100
        privateCommunitySectionButton.frame.size = CGSize(width:width,height:height)
        // set button x-origin
        let publicCommunitySectionButtonXOrigin = publicCommunitySectionButton.frame.origin.x+(publicCommunitySectionButton.frame.width-publicCommunitySectionButton.label.frame.width)/2
        let cardXOrigin = MYCard.horizontalCardsSpacingUnit
        let publicCommunitySectionButtonCardXOriginDiff = publicCommunitySectionButtonXOrigin - cardXOrigin
        let adjustBy = 2.3*publicCommunitySectionButtonCardXOriginDiff - privateCommunitySectionButton.label.frame.origin.x
        privateCommunitySectionButton.frame.origin.x = publicCommunitySectionButton.frame.origin.x + publicCommunitySectionButton.frame.width + adjustBy
        // set label origin
        privateCommunitySectionButton.label.center.x = privateCommunitySectionButton.frame.width/2
        privateCommunitySectionButton.label.center.y = privateCommunitySectionButton.frame.height*48/100
        // set button target
        privateCommunitySectionButton.addTarget(self, action: #selector(HomeViewController.onPrivateCommunitySectionButtonTouch(_:)), for: .touchUpInside)
        return privateCommunitySectionButton
    }
    
    func onPublicCommunitySectionButtonTouch(_ sender: UIButton!) {
        print("func onPublicCommunitySectionButtonTouch is running...")
        // check active container
        switch activeCardContainer {
        case .publicCommunity:
        // if public do nothing
            break
        case .privateCommunity:
            // if private:
            // move shift private/public containers
            // move shift private/public card containers
            // always move card containers (and thus underlines accordingly) according to side of inactive card container directly into center. for example, if public is active and private is to the right, animate private from right into the center (and accordingly, underline straight from left to right (no wrap-around))
            // check side of secondary card container
            let secondaryCardContainerSide = publicCardContainer.containerSide // currently not the active container
            let (leftUnderline, rightUnderline): (MYWrapAroundUnderline, MYWrapAroundUnderline) = underlineOne.center.x <= underlineTwo.center.x ? (underlineOne,underlineTwo) : (underlineTwo, underlineOne)
            switch secondaryCardContainerSide {
            case _ where secondaryCardContainerSide == .left:
                print("secondaryCardContainerSide == .left")
                // animate underlineOne (left-most) (wrap-around to right-most (no animation))
                leftUnderline.animate(toPosition: publicPrivateButtonCentersDistance*2.5, animated: false)
                // animate underlineTwo (right-most) (right to left)
                rightUnderline.animate(toPosition: publicPrivateButtonCentersDistance*0.5)
                // animate publicCardContainer (left to right)
                publicCardContainer.animate(toPosition: ScreenDimension.width*1.5)
                // animate privateCardContainer (left to right)
                privateCardContainer.animate(toPosition: ScreenDimension.width*2.5)
            case _ where secondaryCardContainerSide == .right:
                print("secondaryCardContainerSide == .right")
                // NOTE: no wrapping around required (so everything can use default animations)
                // animate underlineOne (left-most) (left to right)
                leftUnderline.animate(toPosition: publicPrivateButtonCentersDistance*0.5)
                // animate underlineTwo (right-most)
                rightUnderline.animate(toPosition: publicPrivateButtonCentersDistance*2.5)
                // animate publicCardContainer (left to right)
                publicCardContainer.animate(toPosition: ScreenDimension.width*1.5)
                // animate privateCardContainer (left to right)
                privateCardContainer.animate(toPosition: ScreenDimension.width*0.5)
            default:
                break
            }
        default:
            break
        }
    }
    
    func onPrivateCommunitySectionButtonTouch(_ sender: UIButton!) {
        print("func onPrivateCommunitySectionButtonTouch is running...")
        // check active container
        switch activeCardContainer {
        case .publicCommunity:
            // if public:
            // move shift private/public card containers
            // always move card containers (and thus underlines accordingly) according to side of inactive card container directly into center. for example, if public is active and private is to the right, animate private from right into the center (and accordingly, underline straight from left to right (no wrap-around))
            // check side of secondary card container
            let secondaryCardContainerSide = privateCardContainer.containerSide // currently not the active container
            print("check check 1 2")
            let (leftUnderline, rightUnderline): (MYWrapAroundUnderline, MYWrapAroundUnderline) = underlineOne.center.x <= underlineTwo.center.x ? (underlineOne,underlineTwo) : (underlineTwo, underlineOne)
            switch secondaryCardContainerSide {
            case _ where secondaryCardContainerSide == .left :
                print("secondaryCardContainerSide == .left")
                // NOTE: no wrapping around required (so everything can use default animations)
                // animate underlineOne (left-most) (right to left)
                leftUnderline.animate(toPosition: -publicPrivateButtonCentersDistance*0.5)
                // animate underlineTwo (right-most) (right to left)
                rightUnderline.animate(toPosition: publicPrivateButtonCentersDistance*1.5)
                // animate publicCardContainer (left to right)
                publicCardContainer.animate(toPosition: ScreenDimension.width*2.5)
                // animate privateCardContainer (left to right)
                privateCardContainer.animate(toPosition: ScreenDimension.width*1.5)
            case _ where secondaryCardContainerSide == .right :
                print("secondaryCardContainerSide == .right")
                // animate underlineOne (left-most) (left to right)
                leftUnderline.animate(toPosition: publicPrivateButtonCentersDistance*1.5)
                // animate underlineTwo (right-most) (wrap-around to left-most (no animation))
                rightUnderline.animate(toPosition: -publicPrivateButtonCentersDistance*0.5, animated: false)
                // animate publicCardContainer (right to left)
                publicCardContainer.animate(toPosition: ScreenDimension.width*0.5)
                // animate privateCardContainer (right to left)
                privateCardContainer.animate(toPosition: ScreenDimension.width*1.5)
            default:
                break
            }
        case .privateCommunity:
            break
            // if private do nothing
        default:
            break
        }
    }
}
