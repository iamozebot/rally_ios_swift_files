//
//  InfiniteScrollView.swift
//  Rally
//
//  Created by Ian Moses on 1/15/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class InfiniteCardContainerScrollView: UIScrollView {
    //var visibleCards: [MYCard]
    //var cardsContainerView: UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame.size.width = ScreenDimension.width*3
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentSize = CGSize(width:5000,height:5000)
    }
    
    override func layoutSubviews() {
        print("func viewDidLayoutSubviews is running...")
        super.layoutSubviews()
        self.recenterIfNecessary()
        
        // calculate visible bounds
        let visibleBounds = self.bounds
        let minimumVisibleX = visibleBounds.minX
        let maximumVisibleX = visibleBounds.maxX
        
        
    }
    
    func recenterIfNecessary() {
        print("func recenterIfNecessary is running...")
        let currentOffset = self.contentOffset
        let contentWidth = self.contentSize.width
        let centerOffsetX = (contentWidth-self.bounds.size.width)/2.0
        let distanceFromCenter = fabs(currentOffset.x-centerOffsetX)
        
        if (distanceFromCenter > (contentWidth/4.0)) {
            
            self.contentOffset = CGPoint(x:centerOffsetX,y:currentOffset.y)
            
            // move any subviews over so they don't appear to change position
            
        }
    }
    
    /*
     
     let horizontalPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(HomeViewController.handleHorizontalPanGesture))
     
     */
    
    func swapSecondaryCardContainerSideIfNecessary(direction: CardContainerScrollDirection, primaryContainer: CardContainer, secondaryContainerSide: CardContainerSide) {
        switch (direction, secondaryContainerSide) {
        case _ where direction == .left && secondaryContainerSide == .left:
            print("stuffffff")
            // since going left, and secondary container is left, switch it to right side before needing to come into view as scroll continues and update secondaryContainerSide
        case _ where direction == .left && secondaryContainerSide == .right:
            print("stuffffff")
            // since going left and secondary container is on the right, update primaryContainer/secondaryContainer once secondaryContainer passes more than halfway in screen x-bounds
        case _ where direction == .right && secondaryContainerSide == .left:
            print("stuffffff")
            // if going right, and secondary container is right, switch it to left side before needing to come into view as scroll continues and update secondaryContainerSide
        case _ where direction == .right && secondaryContainerSide == .right:
            print("stuffffff")
            // if going right, and secondary container is on the left, update primaryContainer/secondaryContainer if secondaryContainer is over halfway past screen x-bounds
        default:
            break
        }
    }
}
