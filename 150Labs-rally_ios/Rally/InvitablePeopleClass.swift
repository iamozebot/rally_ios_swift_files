//
//  InvitablePeopleClass.swift
//  Rally
//
//  Created by Ian Moses on 9/12/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation
import Contacts

class InvitablePerson: Person, Invitable {
    var inviteMethods = [InviteMethod]()
    var selected = false // default value
    /*thumbnailProfileImage: UIImage {
        get {
            // check for profileImage, if none, use stockImage
        }
    }*/
    
    // Person protocol properties
    var fullName: String?
    var firstName: String? {
        didSet {
            var lastName = ""
            if let value = MYUser.lastName {
                lastName += value
            }
            if let value = MYUser.firstName {
                self.fullName = value+" "+lastName
            }
            else {
                self.fullName = self.lastName
            }
        }
    }
    var lastName: String? {
        didSet {
            var lastName = ""
            if let value = MYUser.lastName {
                lastName += value
            }
            if let value = MYUser.firstName {
                self.fullName = value+" "+lastName
            }
            else {
                self.fullName = self.lastName
            }
        }
    }
}

protocol Person {
    var fullName: String? { get set }
    var firstName: String? { get set }
    var lastName: String? { get set }
}

protocol Invitable {
    var selected: Bool { get set }
    var inviteMethods: [InviteMethod] { get set } // method, option, destination, platform,
}

// this doesn't work as written. conforming types only use default values if properties aren't defined in type. otherwise, if defined there must be an initialization.
/*extension Invitable { // provide default values
    var selected: Bool {
        return false
    }
    var inviteMethods: [InviteMethod] {
        return []
    }
}*/

enum InviteMethod { // contactSource, destinationType,
    case email
    case text
    case rally
    case facebook
}

/*
protocol FacebookFriend {
    var facebookEmailAddress: String? { get set }
    var facebookID: String? { get set }
    var cover: String? { get set }
    var ageRange: String? { get set }
    var link: String? { get set }
    var gender: String? { get set }
    var locale: String? { get set }
    var facebookProfilePicture: String? { get set }
    var timezone: String? { get set }
    var updatedTime: String? { get set }
    var verified: String? { get set }
}

// in consideration, this protocol doesn't seem right because it is relational to the user versus being an innate property independent of the user
protocol iPhoneContact {
    var identifier: String? { get set }
    var addressBookEmailAddresses: [CNLabeledValue<NSString>]? { get set }
    var addressBookPhoneNumbers: [CNLabeledValue<CNPhoneNumber>]? { get set }
    var addressBookImageData: Data? // The profile picture of a contact.
    var addressBookThumbnailImageData: Data? // The thumbnail version of the contact’s profile picture.
    var addressBookImageDataAvailable: Bool { get set } // Indicates whether a contact has a profile picture.
}
*/
// all three
// any two
// any one
// need one type that has invitable properties (phonenumber, etc.)
