//
//  InviteViewController.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Recreated by Ian Moses on 6/4/15 to rename as InviteViewController from selectFriendsViewController
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import UIKit
import SwiftyJSON
import Contacts
import AddressBook

class InviteViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    var collectionView: UICollectionView!
    var flowLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    lazy var resultSearchController: UISearchController = {
        [unowned self] in
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.barTintColor =  MYColor.midGrey
        controller.searchBar.isTranslucent = false // for searchBar animation to avoid nav bar color showing through
        controller.searchBar.layer.borderColor = MYColor.midGrey.cgColor
        controller.searchBar.sizeToFit()
        controller.searchBar.layer.borderWidth = 1.0
        controller.hidesNavigationBarDuringPresentation = true
        controller.searchBar.delegate = self
        controller.delegate = self
        return controller
    }()
    var invitablePeople = [Friend]() {
        
    }
    var filteredFriends: [Friend] = []
    var selectedFriends: [Friend] = [] {
        didSet {
            defineDoneButtonInteractionState()
        }
    }
    var selectedFilteredFriends: [Friend] = []
    var selectedTableDataIndexPaths: [AnyObject] = [] //indexPaths for selected items in unfiltered view
    var selectedFilteredTableDataIndexPaths: [IndexPath] = []
    var j = 0 //used to loop within func createSelectedFilteredFriendsArray()
    var numberOfContactsToLoad = 24
    var searchBarText = ""
    var rallyObject: Rally?
    var doneButton: UIButton!
    var searchBarCancelButtonClicked = false
    var statusBarBackgroundColorView: passThroughView!
    var statusBarBackgroundColorCollectionView: UIView!
    var searchBarClicked = false
    var statusBarBackgroundColorCollectionViewBackToClearColor = false
    var screenJustLoaded = 0
    var keyboardHeight: CGFloat!
    var statusBarDisplayState: Bool = false
    var statusBarAnimationState: UIStatusBarAnimation = .slide
    
    override var prefersStatusBarHidden : Bool {
        get {
            return self.statusBarDisplayState
        }
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        get {
            return self.statusBarAnimationState
        }
    }
    var community: Community // for now, always community variable until inviting friends to community from contacts is re-implemented

    var mode: InviteMode
    
    var navBarTitle: String {
        get {
            switch self.mode {
            case .community:
                return "Invite to Community"
            case .rally:
                return "Invite to Rally"
            case .thread:
                return "Invite to Thread"
            }
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        print("override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {")
        //self.example = "Just an example"
        // not doing anything here, can probably delete (test when app is back up and running)
        self.mode = .thread // this is stupid, not required
        self.community = Community() // this is stupid, not required
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        //        fatalError("init(coder:) has not been implemented")
        //self.example = "Just a coder example"
        print("required init?(coder aDecoder: NSCoder) {")
        self.mode = .thread // this is stupid, not required
        self.community = Community() // this is stupid, not required
        super.init(coder: aDecoder)
    }
    
    init(mode: InviteMode, community: Community) {
        // set type
        self.mode = mode
        self.community = community
        // initialize view controller
        super.init(nibName: nil, bundle: nil)
    }
    
    /*NOTES:
        1.
         - For Rally case: let's avoid +1 feature for release. This means the only way people are selected for Rally is through community messaging view controller workflow. So, NO PRESELECT (to avoid confusion) instead of preselecting activeThread (as was originally desired).
         - For community case: anyone in phone contacts (will receive text). no one is preselected. there is no community or rally to pull contact list from.
         - For thread case: anyone in community may be invited (include people already in thread and make them non-selectable). PRESELECT AND NON-SELECTABLE.
     */
    
    override func viewDidLoad() {
        print("class InviteViewController,func viewDidLoad is running...")
        super.viewDidLoad()
        WebCallStatus.inviteViewControllerLoaded = 1
        view.backgroundColor = MYColor.midGrey
        // use later: http://natashatherobot.com/navigation-bar-interactions-ios8/
        // http://stackoverflow.com/questions/26222671/uisearchcontroller-searchbar-in-tableheaderview-animating-out-of-the-screen
        defineCollectionView()
        defineFlowLayout()
        view.addSubview(collectionView)
        navigationBarShadowRemoval()
        self.navigationController?.toolbar.isTranslucent = false
        self.definesPresentationContext = true
        defineNavBar()
        defineSearchBarAnimationHotfixViews()
        view.bringSubview(toFront: statusBarBackgroundColorView)
        //defineInvitablePeople()
        //define()
    }
    
    func defineCollectionView() {
        print("func defineCollectionView is running...")
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width, height: ScreenDimension.height-UINavigationBarTaller.navigationBarHeight-self.navigationController!.toolbar.frame.height-UIApplication.shared.statusBarFrame.height), collectionViewLayout: flowLayout)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "collectionCell")
        // collectionView.contentInset =  UIEdgeInsets(top: 0, left: 3, bottom: 6, right: 3)
        collectionView.delegate = self
        collectionView.dataSource = self
        //collectionView.backgroundColor = MYColor.midDarkGrey
        collectionView.backgroundColor = MYColor.midGrey
        collectionView.allowsMultipleSelection = true
    }
    
    func defineFlowLayout() {
        print("func defineFlowLayout is running...")
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0) // increasing allows edges to be grey for more of a button look
        flowLayout.itemSize = CGSize(width: ScreenDimension.width/2-1.0, height: ScreenDimension.width/6)
        flowLayout.minimumInteritemSpacing = 2.0
        flowLayout.minimumLineSpacing = 2.0
        // https://www.hackingwithswift.com/ios9
        flowLayout.sectionHeadersPinToVisibleBounds = true
    }
    
    func defineNavBar() {
        print("func defineNavBar is running...")
        let leftButtonSelector: Selector = #selector(InviteViewController.backToHomeViewController(_:))
        let rightMostButtonSelector: Selector = #selector(InviteViewController.saveNewRally(_:))
        navBarClass.navBar(self, navigationBarItemReference: navigationItem, leftButtonImageReference: "back", rightButtonImageReference: nil, rightMostButtonImageReference: nil, leftButtonActionReference: leftButtonSelector, rightButtonActionReference: nil, rightMostButtonActionReference: rightMostButtonSelector, titleReference: "Add Friends")
    }
    
    func defineSearchBarAnimationHotfixViews() {
        print("func defineSearchBarAnimationHotfixViews is running...")
        statusBarBackgroundColorView = passThroughView(frame: CGRect(x: 0,y: 0,width: ScreenDimension.width, height: 20))
        statusBarBackgroundColorCollectionView = UIView(frame: CGRect(x: 0,y: -60,width: ScreenDimension.width, height: 60))
        view.addSubview(statusBarBackgroundColorView)
        collectionView.addSubview(statusBarBackgroundColorCollectionView)
        statusBarBackgroundColorCollectionView.backgroundColor = UIColor.clear
        statusBarBackgroundColorView.backgroundColor = UIColor.clear
    }
    /**
     Scroll to make the the given section header visible.
     The function scrollToItemAtIndexPath will scroll to the item and hide the section header.
     */
    // http://www.rockhoppertech.com/blog/scroll-to-uicollectionview-header/
    func scrollToSection(_ section:Int)  {
        print("func scrollToSection is running...")
        let indexPath = IndexPath(item: 1, section: section)
        if let cv = self.collectionView {
            if let _ = cv.layoutAttributesForSupplementaryElement(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
                let topOfHeader = CGPoint(x: 0, y: 0)
                cv.setContentOffset(topOfHeader, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        print("func collectionView:referenceSizeForHeaderInSection is running...")
        return CGSize(width: resultSearchController.searchBar.frame.width,height: resultSearchController.searchBar.frame.height) // Header size
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("func collectionView:viewForSupplementaryElementOfKind is running...")
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
        if headerView.subviews.count == 0 {
            headerView.addSubview(self.resultSearchController.searchBar)
        }
        return headerView
    }
    
    // http://stackoverflow.com/questions/30858969/can-the-height-of-the-uisearchbar-textfield-be-modified
    override func viewDidLayoutSubviews() {
        print("func viewDidLayoutSubviews is running...")
        super.viewDidLayoutSubviews()
        resultSearchController.searchBar.layoutIfNeeded()
        resultSearchController.searchBar.layoutSubviews()
        //UIApplication.shared.setStatusBarHidden(false, with: .none)
        // display status bar, w/o animation
        statusBarDisplayState = false
        statusBarAnimationState = .none
        setNeedsStatusBarAppearanceUpdate()
        AppDelegate.getAppDelegate().window?.backgroundColor = nil
        if screenJustLoaded == 0 {
            if let rallyObject = rallyObject {
                for i in 0..<rallyObject.rallyUserList.count-1 {
                    self.collectionView.selectItem(at: IndexPath(item: i, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
                }
            }
        }
        if searchBarClicked {
            searchBarClicked = false
            statusBarBackgroundColorView.backgroundColor = MYColor.rallyBlue
            statusBarBackgroundColorCollectionView.backgroundColor = UIColor.clear
        }
        else if statusBarBackgroundColorCollectionViewBackToClearColor {
            statusBarBackgroundColorView.backgroundColor = UIColor.clear
            statusBarBackgroundColorCollectionView.backgroundColor = UIColor.clear
            statusBarBackgroundColorCollectionViewBackToClearColor = false
            UIApplication.shared.isStatusBarHidden = false
            self.navigationController?.navigationBar.layer.zPosition = 0
        }
        /*
        let barButtonItem = self.navigationItem.rightBarButtonItem!
        let barButtonItemImageViewFrame = (barButtonItem.value(forKey: "view")! as AnyObject).frame
        TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.howToUseSaveButtonToSaveGroups, viewControllerView: navigationController!.view, navigationController: self.navigationController!, referenceCard: nil, VCViewAssociatedWithCard: nil, barButtonItem: barButtonItem, barButtonItemImageViewFrame: barButtonItemImageViewFrame)
         */
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("func searchBarShouldBeginEditing is running...")
        searchBarClicked = true
        statusBarBackgroundColorCollectionView.backgroundColor = MYColor.rallyBlue
        // http://stackoverflow.com/questions/21850436/add-a-uiview-above-all-even-the-navigation-bar
        self.navigationController?.navigationBar.layer.zPosition = -1
        AppDelegate.getAppDelegate().window?.backgroundColor = MYColor.rallyBlue
        return true
    }
    
    // http://stackoverflow.com/questions/26390072/remove-border-in-navigationbar-in-swift
    func navigationBarShadowRemoval() {
        print("func navigationBarShadowRemoval is running...")
        for parent in (self.navigationController?.navigationBar.subviews)! {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        print("func searchBarShouldEndEditing is running...")
        if searchBarCancelButtonClicked {
            searchBarCancelButtonClicked = false
            
            //delay(0.3) {self.scrollToSection(0)}
            return true
        }
        return false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("func searchBarCancelButtonClicked is running...")
        searchBarCancelButtonClicked = true
        statusBarBackgroundColorCollectionViewBackToClearColor = true
        //UIApplication.shared.setStatusBarHidden(true, with: .fade)
        // display status bar, w/o animation
        statusBarDisplayState = true
        statusBarAnimationState = .fade
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class InviteViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: animated)
        defineDoneButton()
        //defineSaveButtonInteractionState()
        defineDoneButtonInteractionState() // only need to call once, defines interaction state of defineDoneButton. also, can't call until both buttons have been defined
        // determine and if necessary define and display .whatAreCommunities tutorial blurb
        NotificationCenter.default.addObserver(self, selector: #selector(InviteViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InviteViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InviteViewController.appWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        // not deleting below line bc it seems there was a method I was going to write for updating smt, can't remember right now though
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadDataRemaining:", name: "AddressBookContactsUpdated", object: nil)
    }
    
    func appWillEnterForeground(_ notification:Notification) {
        print("func InviteViewController:appWillEnterForeground is running...")
        // this assures user doesn't exit app mid-instance and turn off a permission, then re-enter and attempt to use permission when disabled and crash app
        // checks if any permissions are disabled, if so displays permissions access view handler
        //_ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("class inviteViewController, func viewDidAppear is running...")
        super.viewDidAppear(animated)
        if invitablePeople.count>=numberOfContactsToLoad+100 {
            print("If clause using friends.count is running...")
            numberOfContactsToLoad += 100
            screenJustLoaded = 1
            self.collectionView.reloadData()
        }
        let (viewsToBringToFront,_):([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType]) = TutorialHelper.tutorialViewsInViewControllerView(view)
        for subview in viewsToBringToFront {
            print("viewsToBringToFront.description:\(subview.description)")
            view.bringSubview(toFront: subview)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("class InviteViewController, func viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        // remove any tutorial blurbs (subviews of navController view)
        TutorialHelper.removeTutorialBlurbsFromViewControllerView(navigationController!.view)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        // not deleting below line bc it seems there was a method I was going to write for updating smt, can't remember right now though
        // NSNotificationCenter.defaultCenter().removeObserver(self, name: "AddressBookContactsUpdated", object: nil)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print("func updateSearchResultsForSearchController running...")
        if searchController.searchBar.text!.isEmpty == true {
            filteredFriends = invitablePeople //Initialize filteredFriends. These values are equal prior to filering. If statement only executes when filtering is not present.
            self.createSelectedFilteredFriendsArray()
            self.collectionView.reloadData()
            print("selectedTableDataIndexPaths values: \(selectedTableDataIndexPaths)")
            for index in selectedTableDataIndexPaths {
                //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
                self.collectionView.selectItem(at: index as? IndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
            }
        }
        else {
            searchBarText = searchController.searchBar.text!
            filteredFriends = invitablePeople.filter() { $0.fullName!.range(of: "\(self.searchBarText)") != nil}
            //http://stackoverflow.com/questions/24034043/how-do-i-check-if-a-string-contains-another-string-in-swift
            //http://stackoverflow.com/questions/24382580/filteredarrayusingpredicate-not-exist-in-swift-array
            self.createSelectedFilteredFriendsArray() //Placed after filteredTableData has been updated with respect to searchbar entry bc var is used in func
            self.collectionView.reloadData()
            for index in selectedFilteredTableDataIndexPaths {
                //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
                print("We are in the for loop selectedFilteredTableDataIndexPaths...")
                self.collectionView.selectItem(at: index, animated: false, scrollPosition: UICollectionViewScrollPosition())
            }
        }
    }
    
    func createSelectedFilteredFriendsArray() {
        print("func createSelectedFilteredFriendsArray running...")
        // LIST OF PEOPLE DATA SOURCES:
            // 1. Phone address book
            // 2. community
            // 3.
        self.selectedFilteredFriends.removeAll() //(keepingCapacity: false)
        var forLoopCounter = 0
        let selectedFriendsNames = selectedFriends.map { $0.fullName! }
        let filteredFriendsNames = filteredFriends.map { $0.fullName! }
        for friendName in selectedFriendsNames {
            if filteredFriendsNames.contains(friendName) {
                self.selectedFilteredFriends.append(selectedFriends[forLoopCounter])
            }
            forLoopCounter += 1
        }
        while j > selectedFilteredFriends.count {
            // var numberOfElementsInselectedFilteredTableDataIndexPaths = selectedFilteredTableDataIndexPaths.count
            selectedFilteredTableDataIndexPaths.removeLast()
            j -= 1
        }
        while j<selectedFilteredFriends.count {
            let newIndexPathForSelectedItem = IndexPath(row: j, section: 0)
            selectedFilteredTableDataIndexPaths.append(newIndexPathForSelectedItem)
            j += 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print("func collectionView:didSelectItemAtIndexPath is running...")
        if (self.resultSearchController.isActive) {
            self.selectedFriends.append(filteredFriends[(indexPath as NSIndexPath).row])
            self.selectedFilteredFriends.append(filteredFriends[(indexPath as NSIndexPath).row])
            if let foundIndex = invitablePeople.index(where: {$0.ID! == filteredFriends[(indexPath as NSIndexPath).row].ID!}) {
                invitablePeople.remove(at: foundIndex) //need tableData up to date when exiting filtered table
                invitablePeople.insert(selectedFriends.last!, at: selectedFriends.count-1)
                self.filteredFriends.remove(at: (indexPath as NSIndexPath).row)
                self.filteredFriends.insert(invitablePeople[selectedFriends.count-1], at: selectedFilteredFriends.count-1)
                let newIndexPathForSelectedItemInCurrentView = IndexPath(row: selectedFilteredFriends.count-1, section: 0)
                self.collectionView.moveItem(at: indexPath, to: newIndexPathForSelectedItemInCurrentView)
                let newIndexPathForSelectedItemInUnfilteredView = IndexPath(row: self.selectedFriends.count-1, section: 0)
                self.selectedTableDataIndexPaths.append(newIndexPathForSelectedItemInUnfilteredView as AnyObject)
            }
        }
        else {
            self.selectedFriends.append(invitablePeople[(indexPath as NSIndexPath).row])
            invitablePeople.remove(at: (indexPath as NSIndexPath).row)
            invitablePeople.insert(self.selectedFriends.last!, at: selectedFriends.count - 1)
            let newIndexPathForSelectedItem = IndexPath(row: selectedFriends.count - 1, section: 0) /*http://stackoverflow.com/questions/29428090/how-to-convert-int-to-nsindexpath*/
            self.collectionView.moveItem(at: indexPath, to: newIndexPathForSelectedItem)
            self.selectedTableDataIndexPaths.append(newIndexPathForSelectedItem as AnyObject)
            selectedFilteredFriends = selectedFriends
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        /*Retrieves index of friend[indexPath.row] within selectedFriendNamesForRally array
        and then uses index to know which index to remove in order to remove the deselected friend from the selectedFriendNamesForRally array. Code logic found at: https://teamtreehouse.com/forum/how-to-remove-a-friend-from-the-array-of-friends-using-swift-removeobject-method-seems-not-available-at-swift*/
        print("func collectionView:didDeselectItemAtIndexPath is running...")
        if (self.resultSearchController.isActive) {
            //remove the item at the found index
            //Map phone numbers instead of numbers later on. Variable update may be redundant
            if let foundIndexForNameIntableData = invitablePeople.index(where: {$0.ID! == filteredFriends[(indexPath as NSIndexPath).row].ID!}) /*duplicate names shouldn't be a problem because when tableData is searched through, the deselected name will be in the front of the array along with other selected friend names. Only time this is a problem is if identical names are selected and the deselected name is second*/ {
                invitablePeople.remove(at: foundIndexForNameIntableData)
                invitablePeople.insert(filteredFriends[(indexPath as NSIndexPath).row], at: selectedFriends.count - 1)
                self.filteredFriends.remove(at: (indexPath as NSIndexPath).row) //filteredFriends is updated after friends intentionally
                self.filteredFriends.insert(invitablePeople[selectedFriends.count - 1], at: selectedFilteredFriends.count - 1)
            }
            self.selectedFriends.remove(at: (indexPath as NSIndexPath).row)
            self.selectedFilteredFriends.remove(at: (indexPath as NSIndexPath).row)
            let newIndexPathForDeselectedItem = IndexPath(row: selectedFilteredFriends.count, section: 0)
            self.collectionView.moveItem(at: indexPath, to: newIndexPathForDeselectedItem)
            self.selectedTableDataIndexPaths.removeLast()
        }
        else {
            //remove the item at the found index
            invitablePeople.remove(at: (indexPath as NSIndexPath).row)
            print("selectedFriends at start of didDeselectItemAtIndexPath:\(selectedFriends.count)")
            invitablePeople.insert(self.selectedFriends[(indexPath as NSIndexPath).row], at: selectedFriends.count - 1)
            self.selectedFriends.remove(at: (indexPath as NSIndexPath).row) //selectedFriends is updated after friends intentionally
            let newIndexPathForDeselectedItem = IndexPath(row: selectedFriends.count, section: 0) /*http://stackoverflow.com/questions/29428090/how-to-convert-int-to-nsindexpath*/
            self.collectionView.moveItem(at: indexPath, to: newIndexPathForDeselectedItem)
            self.selectedTableDataIndexPaths.removeLast()
            print("func didDeselectItemAtIndexPath is running, removing indexPath location of newly deselected item: \(selectedTableDataIndexPaths)")
            selectedFilteredFriends = selectedFriends
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("func collectionView numberOfItemsInSection is running...")
        if (self.resultSearchController.isActive) {
            if filteredFriends.count < 24 {
                return filteredFriends.count
            }
            else {
                return numberOfContactsToLoad // if user scrolled excessively in non-filter screen, may affect load times in filtered view. Improve if necessary
            }
            
        }
        else {
            if invitablePeople.count < 24 { // ELSE IF assures if user has less than 24 contacts, program does not load a bunch of empty cells
                return invitablePeople.count
            }
            else {
                return numberOfContactsToLoad
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("func collectionView cellForItemAtIndexPath is running...")
        let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        //see page 436 for reusable cell test, not that hard looking
        cell.backgroundColor = MYColor.cellMidGrey

        /*let rallyButtonBottomRectShape = CAShapeLayer()
        rallyButtonBottomRectShape.bounds = cell.frame
        rallyButtonBottomRectShape.position = cell.center
        if indexPath.row%2 == 0 {
            rallyButtonBottomRectShape.path = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.TopRight, .BottomRight], cornerRadii: CGSize(width: 3.5, height: 3.5)).CGPath
        }
        else {
            rallyButtonBottomRectShape.path = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.TopLeft, .BottomLeft], cornerRadii: CGSize(width: 3.5, height: 3.5)).CGPath
        }
        //Here I'm masking the textView's layer with rallyButtonBottomRectShape layer
        cell.layer.mask = rallyButtonBottomRectShape*/
        cell.clipsToBounds = true
        /*when a new cell is requested for the view (for initial page loading or scrolling to new cells), a cell that may have already been populated with
        subviews (in the case of scrolling) may be pulled from the cell queue for use. This assures all subviews are removed prior to populating with
        correct data (friend name and picture)*/
        for subview in cell.contentView.subviews {
            subview.removeFromSuperview()
        }
        let textLabel = UILabel(frame: CGRect(x: cell.frame.size.width/3, y: cell.frame.size.height/4, width: cell.frame.size.width*5/8, height: cell.frame.size.height/2))
        textLabel.font = UIFont.systemFont(ofSize: 15)
        textLabel.textAlignment = .left
        var imageView: UIImageView?
        if (self.resultSearchController.isActive) {
            textLabel.text = self.filteredFriends[(indexPath as NSIndexPath).row].fullName!
        }
        else {
            textLabel.text = invitablePeople[(indexPath as NSIndexPath).row].fullName
            imageView = UIImageView(image: invitablePeople[(indexPath as NSIndexPath).row].getProfileImage())
        }
        let friendProfilePicSizeAsInt = round(cell.frame.size.height*73/100)
        let friendProfilePicCellWallPadding = (cell.frame.size.height - friendProfilePicSizeAsInt)/2
        cell.contentView.addSubview(textLabel)
        imageView?.frame = CGRect(x: friendProfilePicCellWallPadding, y: friendProfilePicCellWallPadding, width: friendProfilePicSizeAsInt, height: friendProfilePicSizeAsInt)
        //imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        if let image = imageView {
            cell.contentView.addSubview(image) //should send to contentView, not to the cell itself. See page 428 of programming ios8 for more details
        }
        let myBackView = UIView(frame:cell.frame)
        //myBackView.backgroundColor = MYColor.midDarkGrey
        myBackView.backgroundColor = UIColor.white
        cell.selectedBackgroundView = myBackView
        if screenJustLoaded < 2 {
            if let rallyObject = rallyObject {
                if (indexPath as NSIndexPath).row<rallyObject.rallyUserList.count-1 {
                    self.collectionView.selectItem(at: IndexPath(item: (indexPath as NSIndexPath).row, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
                    if screenJustLoaded == 1 {
                        cell.isSelected = true
                        self.selectedFriends.append(invitablePeople[(indexPath as NSIndexPath).row])
                        self.selectedTableDataIndexPaths.append(indexPath as AnyObject)
                        selectedFilteredFriends = selectedFriends
                        if (indexPath as NSIndexPath).row == rallyObject.rallyUserList.count-2 {
                            screenJustLoaded = 2
                        }
                    }
                }
            }
        }
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        print("func didReceiveMemoryWarning is running...")
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func scrollViewDidLoad observes when you reach the final of the CollectionView then you can update the numberOfRowInSections and call the reloadData
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("func scrollViewDidScroll is running...")
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height && invitablePeople.count>=numberOfContactsToLoad+100 {
            print("If clause using friends.count is running...")
            numberOfContactsToLoad += 100
            self.collectionView.reloadData()
            // flowLayout.collectionViewContentSize() += doneButton.frame.size.height + UINavigationBarTaller.navigationBarHeight
            print("selectedTableDataIndexPaths:\(selectedTableDataIndexPaths)")
            for index in selectedTableDataIndexPaths {
                //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
                self.collectionView.selectItem(at: index as? IndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
            }
        }
        else {
            print("scrollView IF clause wasn't true")
            if offsetY > contentHeight - scrollView.frame.size.height && invitablePeople.count - numberOfContactsToLoad > 0 && invitablePeople.count<invitablePeople.count+100 {
                print("ELSE AND nested IF clause is running!!!")
                numberOfContactsToLoad += invitablePeople.count - numberOfContactsToLoad
                self.collectionView.reloadData()
                // collectionViewContentSize += doneButton.frame.size.height + UINavigationBarTaller.navigationBarHeight
                for index in selectedTableDataIndexPaths {
                    //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
                    self.collectionView.selectItem(at: index as? IndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
                }
            }
        }
    }
    
    /*
    override func collectionViewContentSize() -> CGSize {
    
    }
    */
    
    /*
    // Not sure if this function works yet, haven't tested while having enough contacts to scroll down to additional content
    func scrollViewWillBeginDragging(scrollView: UIScrollView) { // this is to allow screen to scroll and unfocus from textview in searchbar
    //dismiss the keyboard if the search results are scrolled
    print("func scrollViewWillBeginDragging is running...")
    resultSearchController.searchBar.resignFirstResponder()
    }
    */
    
    /*func isSearchControllerActive() {
    if (self.resultSearchController.active) {
    filteredFriends = friends.filter() { $0.fullName.rangeOfString("\(createGlobalVariableOfSearchBarText)") != nil}
    //http://stackoverflow.com/questions/24034043/how-do-i-check-if-a-string-contains-another-string-in-swift
    //http://stackoverflow.com/questions/24382580/filteredarrayusingpredicate-not-exist-in-swift-array
    self.createSelectedFilteredFriendsArray() //Placed after filteredTableData has been updated with respect to searchbar entry bc var is used in func
    }
    }*/
    
    func saveNewRally(_ sender: UIButton!) {
        print("func saveNewRally is running...")
        /*
        rallyObject = rally(selectedFriends)
        //rallyObject!.CreatedDateTimeStamp = NSDate()
        // IsRallyActive default is 0 (inactive), so no need to specify
        
        let intent = WebServiceCallIntent.createRally(rallyObject!)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.navigationController!.popToRootViewController(animated: true)
                })
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        */
    }
    /*
    func defineSaveButtonInteractionState() {
        print("func defineSaveButtonInteractionState is running...")
        if selectedFriends.count < 2 { // on first load, selectedFriends aren't added until
            if screenJustLoaded == 0 {
                if let _ = rallyObject {
                    navigationItem.rightBarButtonItem?.isEnabled = true
                    navigationItem.rightBarButtonItem?.tintColor = nil
                }
            }
            else { // normal case
                navigationItem.rightBarButtonItem?.isEnabled = false
                navigationItem.rightBarButtonItem?.tintColor = MYColor.greyBlue
            }
        }
        else {
            navigationItem.rightBarButtonItem?.isEnabled = true
            navigationItem.rightBarButtonItem?.tintColor = nil
        }
    }
    */
    
    func backToHomeViewController(_ sender: UIButton!) {
        print("func backToHomeViewController is running...")
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    func defineDoneButtonInteractionState() {
        print("func defineDoneButtonInteractionState is running...")
        if selectedFriends.count < 2 {
            if screenJustLoaded == 0 {
                if let _ = rallyObject {
                    self.doneButton.isUserInteractionEnabled = true
                    self.doneButton.backgroundColor = MYColor.rallyBlue
                }
                //screenJustLoaded == 0
            }
            else { // normal case
                self.doneButton.isUserInteractionEnabled = false
                self.doneButton.backgroundColor = MYColor.greyBlue
            }
        }
        else {
            self.doneButton.isUserInteractionEnabled = true
            self.doneButton.backgroundColor = MYColor.rallyBlue
        }
    }
    
    func defineDoneButton() {
        print("func defineDoneButton is running...")
        doneButton = UIButton(type: .custom) // .System depending on button click animation preference // was .System before switching to two buttons. try if there is a layout problem
        doneButton.titleLabel!.font = UIFont.systemFont(ofSize: 17)
        doneButton.titleLabel!.lineBreakMode = .byTruncatingTail
        doneButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: (self.navigationController?.toolbar.frame.height)!)
        doneButton.backgroundColor = MYColor.greyBlue
        doneButton.layer.masksToBounds = true
        doneButton.setTitle("Invite Friends", for: UIControlState())
        doneButton.setTitleColor(.white, for: UIControlState())
        doneButton.addTarget(self, action: #selector(InviteViewController.onDoneTouch(_:)), for: UIControlEvents.touchUpInside)
        self.navigationController?.toolbar.addSubview(doneButton)
        let border = CALayer()
        let width = CGFloat(0.3125)
        border.borderColor = MYColor.midGrey.cgColor
        border.frame = CGRect(x: doneButton.frame.size.width - width, y: 0, width:  doneButton.frame.size.width, height: doneButton.frame.size.height)
        border.borderWidth = width
        doneButton.layer.addSublayer(border)
        doneButton.layer.masksToBounds = true
        /* not sure if this is required, check workflow in xMind later
        for childView in (self.navigationController?.toolbar.subviews)! {
        childView.removeFromSuperview()
        }
        */
    }
    
    func onDoneTouch(_ sender: UIButton!) { // sends on touch
        print("func onSendRallyMoreDetailsTouch is running...")
        // update rally object's location, date, name, and start time info before sending
        if selectedFriends.count > 1 {
            rallyObject = Rally(members: selectedFriends)
            // default rallyObject type is activeUsersRally, can't be saved from details screen so this type works for not sent/sent case options
            // IsRallyActive default is 0 (inactive), so no need to specify
            rallyObject!.rallyStartDateTime = dateHelperClass().defaultRallyStartDateTime() // add default rallyStartDateTime
            let intent = WebServiceCallIntent.createRally(rallyObject!)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        //http://stackoverflow.com/questions/29734954/how-do-you-share-data-between-view-controllers-and-other-objects-in-swift
                        let moveToDetailsViewController: DetailsViewController = DetailsViewController(nibName: nil, bundle: nil)
                        moveToDetailsViewController.rallyObject = self.rallyObject
                        self.navigationController!.pushViewController(moveToDetailsViewController, animated: true)
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
}
