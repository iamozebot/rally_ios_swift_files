//
//  KeyboardProtocol.swift
//  Rally
//
//  Created by Ian Moses on 8/30/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

protocol KeyboardNotifications: class {
    associatedtype parametersHandler = (keyboardHeight: CGFloat, animationDuration: Double, animationStyleValue: UInt)
    func keyboardWillShow(_ notification: Notification)
    func keyboardWillHide(_ notification: Notification)
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt))
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt))
}

extension KeyboardNotifications {
    
    func showKeyboard(_ notification: Notification) {
        print("func keyboardWillShow is running...")
        // http://stackoverflow.com/questions/27973688/uikeyboardanimationdurationuserinfokey-returns-extremely-small-value
        print("notification:\(notification)")
        let computedParameter = computeParameters(notification)
        print("computedParameter:\(String(describing: computedParameter))")
        guard let unwrappedParameter = computedParameter else { return }
        onKeyboardWillShow(unwrappedParameter)
    }
    
    func hideKeyboard(_ notification: Notification) {
        print("func keyboardWillHide is running...")
        let computedParameter = computeParameters(notification)
        guard let unwrappedParameter = computedParameter else { return }
        onKeyboardWillHide(unwrappedParameter)
    }
    
    func computeParameters(_ notification: Notification)-> (CGFloat, Double, UInt)? {
        print("func computeParameters is running...")
        let userInfoOptional: [AnyHashable: Any]? = (notification as NSNotification).userInfo
        guard let userInfo = userInfoOptional else { return nil}
        let  keyboardFrame: NSValue = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight: CGFloat = keyboardFrame.cgRectValue.size.height
        let animationDurationValue: NSNumber = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let animationDuration: Double = animationDurationValue.doubleValue
        let animationStyleValue: UInt = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        return (keyboardHeight, animationDuration, animationStyleValue)
    }
}

extension VerifyPhoneNumberViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        self.showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        keyboardHeight = parameters.0
        print("keyboardHeight:\(keyboardHeight)")
        if submitButton.superview == nil {
            defineSubmitButton()
            view.addSubview(submitButton)
        }
        // update y-origin
        submitButton.frame.origin.y = ScreenDimension.height-submitButton.frame.height-UINavigationBarTaller.navigationBarHeight-keyboardHeight-UIApplication.shared.statusBarFrame.height
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

/*extension HomeViewController: KeyboardNotifications {
    
    func onKeyboardWillShow(parameters: HomeViewController.parametersHandler) {
        
    }
    
    func onKeyboardWillHide(parameters: HomeViewController.parametersHandler) {
        
    }
}*/

extension ReportAProblemClass: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        print("Notification firing correctly")
        keyboardHeight = parameters.0
        if let _ = navigationControllerClassVariable.view.viewWithTag(165) { // 165 is dulling view tag
        }
        else {
            let dullingViewForViewControllerView = DullingViewClass.defineDullingView(0.87)
            self.navigationControllerClassVariable.view.addSubview(dullingViewForViewControllerView)
        }
        let padding = ScreenDimension.height*1.5/100 // doesn't need to be this but it's a good padding amount so I'm just using it
        textViewAndHeaderBarSuperView.frame.size.height = ScreenDimension.height-keyboardHeight-padding*3 // one padding amount is for the status bar height
        textViewAndHeaderBarSuperView.frame.origin.y = padding*2
        let cancelTitleAndSendSuperView = textViewAndHeaderBarSuperView.viewWithTag(928)!
        let textView = textViewAndHeaderBarSuperView.viewWithTag(929)! as! UITextView // 929 is textview tag
        textView.frame.size.height = textViewAndHeaderBarSuperView.frame.height-cancelTitleAndSendSuperView.frame.height // one padding amount is for the status bar height
        navigationControllerClassVariable.view.bringSubview(toFront: textViewAndHeaderBarSuperView)
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

extension CommunityMessagingViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        print("CommunityMessagingViewController:keyboardWillShow is running...")
        showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        // move scrollView up to compensate for keyboard coming onto screen
        let detailsContainerHeight = ScreenDimension.height*9.16/100
        let scrollViewContentHeight = scrollView.contentSize.height
        let visibleScrollViewHeight = scrollView.frame.height-parameters.0
        
        UIView.animate(withDuration: parameters.1,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: parameters.2),
                                   animations: {
                                    if let textInputBarAndSendButtonSuperview = self.view.viewWithTag(343) {
                                        textInputBarAndSendButtonSuperview.frame.origin.y = ScreenDimension.heightMinusStatusAndNavigationBar-textInputBarAndSendButtonSuperview.frame.height-parameters.0
                                    }
                                    print("opergnpo:\(self.scrollView.contentOffset.y)")
                                    self.scrollView.contentOffset.y = scrollViewContentHeight-visibleScrollViewHeight
                                    self.scrollView.contentInset.bottom = parameters.0
                                    print("opergnpo:\(self.scrollView.contentOffset.y)")
            },
                                   completion: { finished in
                                    print("keyboardWillShow:animateWithDuration:completion")
        })
        
        /*
        if scrollView.contentSize.height > scrollView.frame.height {
            let contentYOffset = scrollView.contentSize.height-scrollView.frame.height
            scrollView.contentOffset = CGPoint(x: 0.0, y: contentYOffset)
        }
        */
        self.keyboardIsActive = true
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        UIView.animate(withDuration: parameters.1,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: parameters.2),
                                   animations: {
                                    if let textInputBarAndSendButtonSuperview = self.view.viewWithTag(343) {
                                        textInputBarAndSendButtonSuperview.frame.origin.y = ScreenDimension.heightMinusStatusAndNavigationBar-textInputBarAndSendButtonSuperview.frame.height
                                    }
            },
                                   completion: { finished in
                                    print("keyboardWillHide:animateWithDuration:completion")
        })
        // move scrollView back down to compensate for keyboard leaving screen
        let detailsContainerHeight = ScreenDimension.height*9.16/100
        //self.scrollView.frame.origin.y = detailsContainerHeight
        self.scrollView.contentInset.bottom = 0.0
        self.keyboardIsActive = false
    }
}

extension InviteViewController: KeyboardNotifications {

    func keyboardWillShow(_ notification: Notification) {
        showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        TutorialHelper.removeTutorialBlurbsFromViewControllerView(view)
        navigationController!.view.setNeedsDisplay() // hope this works...
        keyboardHeight = parameters.0
        if keyboardHeight != 0.0 {
            delay(parameters.1) {
                self.collectionView.frame.size.height = ScreenDimension.height-self.keyboardHeight
            }
        }
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        delay(0.05) { // the delay avoids the resized collectionView messing up the searchBar animation, not ideal but until I build a custom UISearchBar and custom flow layout it will do
            self.collectionView.frame.size.height = ScreenDimension.height-UINavigationBarTaller.navigationBarHeight-self.navigationController!.toolbar.frame.height-UIApplication.shared.statusBarFrame.height
        }
        delay(0.5) {
            // determine and if necessary define and display .whatAreCommunities tutorial blurb
            let barButtonItem = self.navigationItem.rightBarButtonItem!
            let barButtonItemImageViewFrame = (barButtonItem.value(forKey: "view")! as AnyObject).frame
            TutorialHelper.tutorialHelperClassSingleton.tutorialHandler(.howToUseSaveButtonToSaveGroups, viewControllerView: self.view, navigationController: self.navigationController!, referenceCard: nil, VCViewAssociatedWithCard: nil, barButtonItem: barButtonItem, barButtonItemImageViewFrame: barButtonItemImageViewFrame)
        }
    }
}

extension DetailsViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        keyboardHeight = parameters.0
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

extension MessagingViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
        UIView.animate(withDuration: parameters.1,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: parameters.2),
                                   animations: {
                                    if let textInputBarAndSendButtonSuperview = self.view.viewWithTag(343) {
                                        textInputBarAndSendButtonSuperview.frame.origin.y = ScreenDimension.heightMinusStatusAndNavigationBar-textInputBarAndSendButtonSuperview.frame.height-parameters.0
                                    }
            },
                                   completion: { finished in
                                    print("keyboardWillShow:animateWithDuration:completion")
        })
        // move scrollView up to compensate for keyboard coming onto screen
        let detailsContainerHeight = ScreenDimension.height*9.16/100
        scrollView.frame.origin.y = detailsContainerHeight-parameters.0
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        UIView.animate(withDuration: parameters.1,
                                   delay: 0.0,
                                   options: UIViewAnimationOptions(rawValue: parameters.2),
                                   animations: {
                                    if let textInputBarAndSendButtonSuperview = self.view.viewWithTag(343) {
                                        textInputBarAndSendButtonSuperview.frame.origin.y = ScreenDimension.heightMinusStatusAndNavigationBar-textInputBarAndSendButtonSuperview.frame.height
                                    }
            },
                                   completion: { finished in
                                    print("keyboardWillHide:animateWithDuration:completion")
        })
        // move scrollView back down to compensate for keyboard leaving screen
        let detailsContainerHeight = ScreenDimension.height*9.16/100
        self.scrollView.frame.origin.y = detailsContainerHeight
    }
}

extension EditProfileViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        self.showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

extension CreateCommunityViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        self.showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

extension CreateThreadViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        self.showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}

extension LeftPanelViewController: KeyboardNotifications {
    func keyboardWillShow(_ notification: Notification) {
        print("LeftPanelViewController:keyboardWillShow is running...")
        self.showKeyboard(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.hideKeyboard(notification)
    }
    
    func onKeyboardWillShow(_ parameters: (CGFloat, Double, UInt)) {
    }
    
    func onKeyboardWillHide(_ parameters: (CGFloat, Double, UInt)) {
        print("Do nothing...")
    }
}
