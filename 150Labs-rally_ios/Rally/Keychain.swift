//
//  Keychain.swift
//  Rally
//
//  Created by Ian Moses on 8/19/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
// https://github.com/kishikawakatsumi/KeychainAccess

import Foundation
import KeychainAccess

class KeychainHelper: NSObject {
    var keychain = Keychain()
    var publicKey: String? {
        get {
            guard let publicKey = try? keychain.getString("publicKey") else {
                print("getPublicKey failed...")
                return nil
            }
            if let value = publicKey {
                print("getPublicKey return value:\(value)...")
                return value
            }
            else {
                print("getPublicKey failed...")
                return nil
            }
        }
    }
    var privateKey: String? {
        get {
            guard let privateKey = try? self.keychain.getString("privateKey") else {
                print("getPrivateKey failed...")
                return nil
            }
            if let value = privateKey {
                print("getPrivateKey return value:\(value)...")
                return value
            }
            else {
                print("getPrivateKey failed...")
                return nil
            }
        }
    }
    
    func savePrivateKey(_ value: String) {
        print("func savePrivateKey is running...")
        // save private key to keychain
        keychain["privateKey"] = "\(value)"
    }
    
    func savePublicKey(_ value: String) {
        print("func savePublicKey is running...")
        // save private key to keychain
        keychain["publicKey"] = "\(value)"
    }
}
