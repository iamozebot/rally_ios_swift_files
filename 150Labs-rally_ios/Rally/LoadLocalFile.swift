//
//  LoadLocalFile.swift
//  Rally
//
//  Created by Ian Moses on 10/10/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import SwiftyJSON
//StockImagesHelper

class LoadLocalFile {
    class func loadUpToDateStockImagesIntoStockImageGlobalDictionary() {
        print("class func loadUpToDateStockImagesIntoStockImageGlobalDictionary is running...")
        DispatchQueue.global(qos: .userInitiated).async { // get any new stock images, background thread
            let webServiceCallManagerInstance = WebServiceCallManager(intent: .getNewStockImages)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    print("getNewStockImages call succeeded, start loading stock images into global dictionary")
                    LoadLocalFile.stockImages()
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                    })
                }
            })
        }
    }
    
    class func stockImages() {
        print("class func stockImages is running...")
        let defaults = UserDefaults.standard
        if let stockImageObjects = defaults.value(forKey: "stockImagesResultKey") as? Data { // get instance of stockImageObjects
            let stockImagesJSONDictionary = JSON(data: stockImageObjects)
            if stockImageGlobalDictionary.count < stockImagesJSONDictionary.count { // this means new images were saved in user defaults file in web call, need to be added to global dictionary
                for i in 0..<stockImagesJSONDictionary.count { // loop through the array of stockImageObjects
                    print("stockImages:i:\(i)")
                    let fileName = "stockImageFileStockImageID\(stockImagesJSONDictionary[i]["StockImageID"])"
                    let stockImageFile = fileSaveHelper(fileName:"\(fileName)", fileExtension: .PNG, subDirectory: "SavingStockImageFiles", directory: .libraryDirectory)
                    do {
                        let fetchedStockImage = try stockImageFile.getImage()
                        let stockImageID: Int = stockImagesJSONDictionary[i]["StockImageID"].intValue
                        if stockImageGlobalDictionary[stockImageID] == nil {
                            stockImageGlobalDictionary[stockImageID] = fetchedStockImage // adds stock image to stockImageGlobalDictionary
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        }
    }
    
    class func saveLargeProfileImageToLocalFile(largeProfileImage: UIImage) {
        print("class func saveLargeProfileImageToLocalFile is running...")
        let stockImageFile = fileSaveHelper(fileName: "largeProfilePicture", fileExtension: .PNG, subDirectory: "userProfilePictures", directory: .libraryDirectory)
        do {
            try stockImageFile.saveFile(image: largeProfileImage)
        }
        catch {
            print("There was an error saving the file: \(error)")
        }
    }
    
    class func saveThumbnailProfileImageToLocalFile(thumbnailProfileImage: UIImage) {
        print("class func saveThumbnailProfileImageToLocalFile is running...")
        let stockImageFile = fileSaveHelper(fileName: "thumbnailProfilePicture", fileExtension: .PNG, subDirectory: "userProfilePictures", directory: .libraryDirectory)
        do {
            try stockImageFile.saveFile(image: thumbnailProfileImage)
        }
        catch {
            print("There was an error saving the file: \(error)")
        }
    }
    
    ////////////////////////////////////////////
    class func loadLargeProfileImage() {
        print("class func loadLargeProfilePicture is running...")
        DispatchQueue.global(qos: .userInitiated).async { // get any new stock images, background thread
            let stockImageFile = fileSaveHelper(fileName: "largeProfileImage", fileExtension: .PNG, subDirectory: "userProfileImages", directory: .libraryDirectory)
            do {
                let largeProfileImage = try stockImageFile.getImage()
                MYUser.largeProfileImage = largeProfileImage
                
            }
            catch {
                print(error)
            }
        }
    }
    
    class func loadThumbnailProfileImage() {
        print("class func loadThumbnailProfileImage is running...")
        DispatchQueue.global(qos: .userInitiated).async {
            let stockImageFile = fileSaveHelper(fileName: "thumbnailProfileImage", fileExtension: .PNG, subDirectory: "userProfileImages", directory: .libraryDirectory)
            do {
                let thumbnailProfileImage = try stockImageFile.getImage()
                MYUser.thumbnailProfileImage = thumbnailProfileImage
            }
            catch {
                print(error)
            }
        }
    }
    
    // save community thumbnails used for community profile picture
    class func saveCommunityMember(thumbnailProfileImage: UIImage, thumbnailProfileImageID: Int, for userID: Int) {
        print("class func saveCommunityMember: thumbnailProfilePicture is running...")
        DispatchQueue.global(qos: .userInitiated).async { // get any new stock images, background thread
            let fileName = String(userID)
            let thumbnailFile = fileSaveHelper(fileName: fileName, fileExtension: .PNG, subDirectory: "communityMemberThumbnails", directory: .libraryDirectory)
            do {
                try thumbnailFile.saveFile(image: thumbnailProfileImage)
                
            // update UserThumbnailUserIDDictionary
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionary(forKey: "UserThumbnailUserIDDictionary") as? [String:Int] ?? [String:Int]() // ?? [Int:Int]() makes variable safe for use prior to array containing any key-value pairs
                
                // convert to correct type
                var properTypeDictionary = [Int: Int]()
                dictionary.forEach { properTypeDictionary[Int($0.0)!] = $0.1 }
                
                // update key-value pair
                properTypeDictionary[userID] = thumbnailProfileImageID
                
                // convert back to NSUserDefaultsClass accepted type
                var reconvertedDictionaryToStore = [String: Int]()
                properTypeDictionary.forEach { reconvertedDictionaryToStore[String($0.0)] = $0.1 }
                
                // save updated set in local file
                NSUserDefaultsClass.updateUserThumbnailUserID(dictionary: reconvertedDictionaryToStore)
                
                // update communityMemberThumbnailUserIDsGlobalDictionary
                communityMemberThumbnailUserIDsGlobalDictionary = properTypeDictionary
                
                // update communityMemberThumbnailsGlobalDictionary
                communityMemberThumbnailsGlobalDictionary[userID] = thumbnailProfileImage // adds thumbnail to communityMemberThumbnailsGlobalDictionary
            }
            catch {
                print("There was an error saving the file: \(error)")
            }
        }
    }
    
    // load community member userID profile picture thumbnail
    class func loadProfilePictureThumbnailToGlobalDictionary(for userID: Int, completionHandler: @escaping () -> Void) { // key-value pair, userID: image
        //DispatchQueue.global(qos: .userInitiated).async { // get any new stock images, background thread
            //let defaults = UserDefaults.standard
            //let UserThumbnailUserIDArray = defaults.array(forKey: "UserThumbnailUserIDArray")  as? [Int] ?? [Int]() // ?? [Int]() makes variable safe for use prior to array containing any values (userIDs)
            //for userID in UserThumbnailUserIDArray { // loop through the array of locally saved userID values that have profile picture thumbnails saved in sub-directory communityMemberThumbnails
                let fileName = String(userID)
                let thumbnailFile = fileSaveHelper(fileName: fileName, fileExtension: .PNG, subDirectory: "communityMemberThumbnails", directory: .libraryDirectory)
                do {
                    let thumbnailProfileImage = try thumbnailFile.getImage()
                    communityMemberThumbnailsGlobalDictionary[userID] = thumbnailProfileImage // adds thumbnail to communityMemberThumbnailsGlobalDictionary
                } catch {
                    print(error)
                }
                completionHandler()
            //}
        //}
    }
}
