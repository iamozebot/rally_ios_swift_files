//
//  MYButton.swift
//  Rally
//
//  Created by Ian Moses on 1/10/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYButton: UIButton {
    var label: UILabel = UILabel()
    
    init(title: String? = nil) {
        print("class MYButton: func init is running...")
        // initialize class for super (UIButton)
        super.init(frame: CGRect.zero)
        if let value = title {
            self.label.text = value
        }
        self.addSubview(self.label)
        
        // set default label font color
        //self.label.textColor = MYColor.extremelyDarkGrey
        self.label.textColor = MYColor.homeSectionSublabelGrey
        
        // set default label font size
        self.label.font = UIFont.systemFont(ofSize: 18.08, weight: UIFontWeightMedium)
        //self.label.font = UIFont.systemFont(ofSize: 19.25)
        self.label.sizeToFit()
    }
    
    convenience init(labelTitle: String) {
        self.init(title: labelTitle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
