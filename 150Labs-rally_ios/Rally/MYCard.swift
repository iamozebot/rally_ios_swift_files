//
//  MYCard.swift
//  Rally
//
//  Created by Ian Moses on 1/3/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

class MYCard: UIButton {
    var cardType: CardType
    // http://stackoverflow.com/questions/32551922/initialize-uibutton-with-parameters
    var cardID: Int
    let detailsTextView = MYNonInteractiveTextView()
    let titleTextView = MYNonInteractiveTextView() //MYVerticalAlignTextView()
    var pictureContainer = passThroughView()
    let whiteOverlay = passThroughView()
    let numberOfMembersTextView = UITextView()
    static let cardHeight: CGFloat = ScreenDimension.height*27.5/100
    //static let cardWidth: CGFloat = screenDimension.screenWidth*31/100
    //static let upcomingRallyCardHeight: CGFloat = screenDimension.screenHeight*20/100
    static let upcomingRallyCardWidth: CGFloat = ScreenDimension.width*40/100
    static var horizontalCardsSpacingUnit: CGFloat {
        get {
            return (ScreenDimension.width-ScreenDimension.width*93/100)/6.25
        }
    }
    
    var cardWidth: CGFloat {
        get {
            if upcomingRally != nil {
                return ScreenDimension.width*40/100
            }
            else {
                return ScreenDimension.width*31/100
            }
        }
    }
    
    var cardHeight: CGFloat {
        get {
            if upcomingRally != nil {
                return ScreenDimension.height*20/100
            }
            else {
                return ScreenDimension.height*27.5/100
            }
        }
    }
    
    var upcomingRally: Rally? {
        get {
            if case let CardType.upcomingRally(upcomingRally) = self.cardType {
                return upcomingRally
            }
            else {
                return nil
            }
        }
    }
    var publicCommunity: Community? {
        get {
            if case let CardType.publicCommunity(publicCommunity) = self.cardType {
                return publicCommunity
            }
            else {
                return nil
            }
        }
    }
    var privateCommunity: Community? {
        get {
            if case let CardType.privateCommunity(privateCommunity) = self.cardType {
                return privateCommunity
            }
            else {
                return nil
            }
        }
    }
    var communityNearby: Community? {
        get {
            if case let CardType.communityNearby(communityNearby) = self.cardType {
                return communityNearby
            }
            else {
                return nil
            }
        }
    }
    var numberOfImagesLoaded: Int {
        get {
            let numberOfImages: Int = self.images.count
            return numberOfImages
        }
    }
    var numberOfThumbnailsCardDisplays: Int {
        get {
            // set card type specific details
            switch self.cardType {
            case .upcomingRally:
                return 2
            case .publicCommunity, .privateCommunity, .communityNearby:
                return 4
            default:
                return 4 // no other card cases to return (old cardTypes not being deleted yet, so compiler requires default
            }
        }
    }
    var members: [Friend] {
        get {
            switch self.cardType {
            case .upcomingRally(let upcomingRally):
                return upcomingRally!.rallyUserList
            case .publicCommunity(let publicCommunity):
                return publicCommunity!.members
            case .privateCommunity(let privateCommunity):
                return privateCommunity!.members
            case .communityNearby(let communityNearby):
                return communityNearby!.members
            default:
                return [Friend]()
            }
        }
    }
    var images: [Dictionary<ImageType, Int>] = [] // ImageType: stockImage and customPicture - Int: stockImageID and memberID. Use IDs to search global dictionaries
    var updatedImagesCheckCounter: Int = 0
    fileprivate lazy var pictureDisplayHelper: PicturesDisplayHelper = {
        let itemSideLength: CGFloat = self.cardWidth/2
        let picturesDisplayData: PicturesDisplayDataSource = PicturesDisplayDataSource(nil, self.members, self.pictureContainer, self.cardType, itemSideLength)
        return PicturesDisplayHelper(picturesDisplayData: picturesDisplayData)
    }()
    
    init(cardTypeValue: CardType,cardPositionIndex: Int) {
        print("class MYCard: func init is running...")
        // set class-level un-initialized card properties
        self.cardType = cardTypeValue
        self.cardID = cardPositionIndex // code was using uibutton tag, should switch to cardID variable for improved readability
        
        // initialize class for super (UIButton)
        super.init(frame: CGRect.zero)
        
        // set generic card properties
        self.tag = cardPositionIndex
        self.backgroundColor = .white // white as default to avoid blue line between photos and whiteOverlay view. ELSE statement to differentiate for rally card designates a blue background
        
        // generate card shadow
        self.layer.cornerRadius = 3
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 1.5
        
        // define origin
        var xOffSet = MYCard.horizontalCardsSpacingUnit + CGFloat((cardPositionIndex + 3) % 3)*(2*MYCard.horizontalCardsSpacingUnit+cardWidth)
        var yOffSet = (cardHeight + 2*MYCard.horizontalCardsSpacingUnit)*CGFloat(floor(Double(cardPositionIndex)/3.0))+MYCard.horizontalCardsSpacingUnit
        if self.upcomingRally != nil { // if non-nil, card type is upcoming rally which has different spacing
            xOffSet = MYCard.horizontalCardsSpacingUnit + CGFloat(cardPositionIndex)*(2*MYCard.horizontalCardsSpacingUnit+cardWidth)
            yOffSet = 0
        }
        self.frame.origin = CGPoint(x:xOffSet, y:yOffSet)
        
        // define size values
        var pictureContainerHeight = cardWidth
        var whiteOverlayHeight = cardHeight-cardWidth
        if self.upcomingRally != nil {
            pictureContainerHeight = cardWidth/2
            whiteOverlayHeight = cardHeight-cardWidth/2
        }
        self.frame.size = CGSize(width:cardWidth, height:cardHeight)
        
        // set picture container
        definePictureContainer(width: cardWidth, height: pictureContainerHeight)
        self.addSubview(self.pictureContainer)
        
        // set whiteoverlay
        defineWhiteOverlay(pictureContainerHeight, width: cardWidth, height: whiteOverlayHeight)
        self.addSubview(self.whiteOverlay)
        
        // set card type specific details
        switch self.cardType {
        case .upcomingRally:
            // set details label for card
            defineUpcomingRallyDetailsTextView()
            self.addSubview(self.detailsTextView)
        case .publicCommunity, .privateCommunity:
            defineCommunityCardNumberOfMembersTextView()
            self.addSubview(self.numberOfMembersTextView)
        case .communityNearby:
            defineCommunityCardNumberOfMembersTextView()
            self.addSubview(self.numberOfMembersTextView)
        default:
            break
        }
        
        // set title textview
        defineTitleTextView()
        self.whiteOverlay.addSubview(self.titleTextView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func defineWhiteOverlay(_ yOffset: CGFloat, width: CGFloat, height: CGFloat) {
        print("func defineWhiteOverlay is running...")
        whiteOverlay.frame = CGRect(x: 0, y: yOffset, width: width, height: height)
        whiteOverlay.backgroundColor = .white
        // http://stackoverflow.com/questions/31232689/how-to-set-cornerradius-for-only-bottom-left-bottom-right-and-top-left-corner-te
        // http://stackoverflow.com/questions/24900381/uibezierpath-init-doesnt-expect-byroundingcorners-parameter
        let rallyButtonBottomRectShape = CAShapeLayer()
        rallyButtonBottomRectShape.bounds = whiteOverlay.frame
        rallyButtonBottomRectShape.position = whiteOverlay.center
        rallyButtonBottomRectShape.path = UIBezierPath(roundedRect: whiteOverlay.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 3, height: 3)).cgPath
        //Here I'm masking the textView's layer with rallyButtonBottomRectShape layer
        whiteOverlay.layer.mask = rallyButtonBottomRectShape
    }
    
    func definePictureContainer(width: CGFloat, height: CGFloat) {
        print("func definePictureContainer is running...")
        pictureContainer.frame.size = CGSize(width: width, height: height)
        let pictureContainerTopRectShape = CAShapeLayer()
        pictureContainerTopRectShape.bounds = self.frame
        pictureContainerTopRectShape.position = self.center
        pictureContainerTopRectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 3, height: 3)).cgPath
        //Here I'm masking the textView's layer with pictureContainerTopRectShape layer
        pictureContainer.layer.mask = pictureContainerTopRectShape
        //pictureContainer.backgroundColor = MYColor.cardBackground.correspondingColor(for:self.cardID)
        
        pictureContainer.backgroundColor = MYColor.cardBackground.random()
    }
    
    func defineCommunityCardNumberOfMembersTextView() {
        print("func defineCommunityCardNumberOfMembersTextView is running...")
        // set user interaction properties
        numberOfMembersTextView.isUserInteractionEnabled = false
        numberOfMembersTextView.isScrollEnabled = false
        numberOfMembersTextView.isEditable = false
        
        // set text
        numberOfMembersTextView.textColor = MYColor.darkGrey
        numberOfMembersTextView.font = UIFont.systemFont(ofSize: 10.5)
        var community: Community
        switch self.cardType { // method currently relevant only to community cards to define number of members, assigning var default then only necessitates one case. case, not if statement architecture to allow for simple scaling of different card types
        case .publicCommunity(let publicCommunity):
            community = publicCommunity!
        case .privateCommunity(let privateCommunity):
            community = privateCommunity!
        case .communityNearby(let communityNearby):
            community = communityNearby!
        default:
            community = Community() // this should never happen, is for compiler so below 'community' uses are safely initialized
        }
        if community.isMemberMaximum {
            numberOfMembersTextView.text = "\(community.members.count)/\(community.maximumNumberOfMembers) members"
        }
        else {
            if community.members.count > 1 {
                numberOfMembersTextView.text = "\(community.members.count) members"
            }
            else {
                numberOfMembersTextView.text = "1 member"
            }
        }
        
        // set frame
        //numberOfMembersTextView.textContainerInset = UIEdgeInsetsMake((cardHeight-cardWidth)*1.75/100, cardWidth*3/100, (cardHeight-cardWidth)*2/100, 0)
        numberOfMembersTextView.sizeToFit()
        numberOfMembersTextView.frame.size.width = cardWidth
        numberOfMembersTextView.frame.origin.y = cardHeight-numberOfMembersTextView.frame.height-cardHeight*1/100
        numberOfMembersTextView.textContainerInset.left = numberOfMembersTextView.frame.height*3/100
        //numberOfMembersTextView.frame.size.width = cardWidth*102/100
        
        // set masking for rounded lower edges
        let cardBottomRectShape = CAShapeLayer()
        cardBottomRectShape.bounds = numberOfMembersTextView.frame
        cardBottomRectShape.position = numberOfMembersTextView.center
        cardBottomRectShape.path = UIBezierPath(roundedRect: numberOfMembersTextView.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 3, height: 3)).cgPath
        //Here I'm masking the textView's layer with cardBottomRectShape layer
        numberOfMembersTextView.layer.mask = cardBottomRectShape
    }
    
    func defineTitleTextView() {
        print("func defineTitleTextView is running...")
        //rallyTitle.textContainerInset = UIEdgeInsetsMake((cardHeight-cardWidth)*6/100, cardWidth*4/100, 0, cardWidth*4/100) // may expand rallyTitle to full width of button and create right inset of x*25/100
        titleTextView.frame.size = CGSize(width: cardWidth, height: self.whiteOverlay.frame.height*65/100)
        switch self.cardType {
        case .upcomingRally(let upcomingRally):
            titleTextView.text = upcomingRally!.rallyName
            //titleTextView.frame.size.height = cardHeight-cardWidth/2-cardHeight*23/100
        case .publicCommunity(let publicCommunity):
            titleTextView.text = publicCommunity!.name
        case .privateCommunity(let privateCommunity):
            titleTextView.text = privateCommunity!.name
        case .communityNearby(let communityNearby):
            titleTextView.text = communityNearby!.name // public groups view doesn't display card so index is increased by one to compensate
        default:
            break
        }
        //titleTextView.contentSize = titleTextView.bounds.size
        var fontSize: CGFloat = 17.5
        titleTextView.font = UIFont.systemFont(ofSize: fontSize)
        //titleTextView.backgroundColor = .red
        let minSize: CGFloat = 13 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
        while fontSize > minSize &&  titleTextView.sizeThatFits(CGSize(width: titleTextView.frame.width, height: CGFloat(Float.greatestFiniteMagnitude))).height >= titleTextView.frame.height {
            fontSize -= 0.5
            titleTextView.font = UIFont.systemFont(ofSize: fontSize)
        }
        //titleTextView.frame.origin = CGPoint(x: 0, y: 0)
        //titleTextView.textContainerInset = UIEdgeInsetsMake((cardHeight-cardWidth)*2/100, 0.0, (cardHeight-cardWidth)*2/100, 0)
        //titleTextView.verticalAlignment = .top
        titleTextView.textContainerInset.top = titleTextView.frame.height*13/100
        titleTextView.textContainerInset.left = titleTextView.frame.height*3/100
    }
    
    func defineUpcomingRallyDetailsTextView() {
        print("func defineUpcomingRallyDetailsTextView is running...")
        
        // set user interaction properties
        detailsTextView.isUserInteractionEnabled = false
        detailsTextView.isScrollEnabled = false
        detailsTextView.isEditable = false
        
        // set text
        let detailsText = dateHelperClass().formatNSDateForDetailsScreenDateButtonTitle(self.upcomingRally!)
        detailsTextView.text = detailsText
        detailsTextView.textColor = MYColor.darkGrey
        detailsTextView.font = UIFont.systemFont(ofSize: 10.5)
        
        // set frame
        detailsTextView.frame.size = CGSize(width: cardWidth, height: cardHeight*23/100) // CGFloat(Float.greatestFiniteMagnitude)
        //detailsTextView.backgroundColor = .red
        var fontSize: CGFloat = 18
        detailsTextView.font = UIFont.systemFont(ofSize: fontSize)
        let minSize: CGFloat = 12 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
        while fontSize > minSize &&  detailsTextView.sizeThatFits(CGSize(width: detailsTextView.frame.size.width, height: CGFloat(Float.greatestFiniteMagnitude))).height >= detailsTextView.frame.size.height {
            fontSize -= 1.0
            detailsTextView.font = UIFont.systemFont(ofSize: fontSize)
        }
        detailsTextView.frame.origin = CGPoint(x: 0, y: cardHeight-detailsTextView.frame.height)
        detailsTextView.textContainerInset = UIEdgeInsetsMake((cardHeight-cardWidth)*2/100, cardWidth*2.0/100, (cardHeight-cardWidth)*2/100, 0)
        
        // set masking for rounded lower edges
        let cardBottomRectShape = CAShapeLayer()
        cardBottomRectShape.bounds = detailsTextView.frame
        cardBottomRectShape.position = detailsTextView.center
        cardBottomRectShape.path = UIBezierPath(roundedRect: detailsTextView.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 3, height: 3)).cgPath
        //Here I'm masking the textView's layer with cardBottomRectShape layer
        detailsTextView.layer.mask = cardBottomRectShape
    }
    
// live-load thumbnails for cards
    func liveLoadThumbnailsHandler(precedingMemberIndex: Int?) { // if no precedingMemberIndex, this is the first call (so use index 0)
        print("func liveLoadThumbnailsHandler is running...")
        var memberIndex: Int // index of member in members(Community)/rallyUserList(rally) array
        if let value = precedingMemberIndex {
            // use next memberID in community/rally members array for fetchThumbnail method
            memberIndex = value+1
        }
        else {
            // use first memberID in community/rally members array to start fetchThumbnail method
            memberIndex = 0
        }
        let memberIDValue = memberID(forMemberAt: memberIndex)
        // check if thumbnail is already loaded for first member
        if communityMemberThumbnailsGlobalDictionary[memberIDValue] != nil { // if thumbnail exists, add to self.images
            liveLoadThumbnailHandler(membersArrayMemberIndex: memberIndex, imageFetched: true)
        }
        else if communityMemberThumbnailUserIDsGlobalDictionary[memberIDValue] != nil {
            // load thumbnail into communityMemberThumbnailsGlobalDictionary
            LoadLocalFile.loadProfilePictureThumbnailToGlobalDictionary(for: memberIDValue, completionHandler: {() -> Void in
                    self.liveLoadThumbnailHandler(membersArrayMemberIndex: memberIndex, imageFetched: true)
            })
        }
        else { // if member userID doesn't exist in global profile picture dictionary, request profile picture
            if let hasThumbnailProfileImage = self.members[memberIndex].hasThumbnailProfileImage {
                if hasThumbnailProfileImage == 0 {
                    // Do not fetch thumbnail; send straight to liveLoadThumbnailHandler
                    self.liveLoadThumbnailHandler(membersArrayMemberIndex: memberIndex, imageFetched: false)
                }
                else {
                    fetchThumbnail(for: memberIDValue, membersArrayMemberIndex: memberIndex)
                }
            }
            else {
                fetchThumbnail(for: memberIDValue, membersArrayMemberIndex: memberIndex)
            }
        }
    }
    
    func liveLoadThumbnailHandler(membersArrayMemberIndex: Int, imageFetched: Bool) { // responsible for loading fetched thumbnail picture to appropriate card
        print("func liveLoadThumbnailHandler is running...")
        // check if image was returned
        if imageFetched { // if fetched, live-load image into picture container
            // add thumbnail to images array
            let memberID = self.memberID(forMemberAt:membersArrayMemberIndex)
            self.images.append([ImageType.customPicture:memberID])
            // increment number of images loaded
        }
        else { // if not, check if member stock image must be added to self.images array because remaining card images required equals remaining members left unloaded. otherwise, move straight to fetching image for next member
            
            // determine numberOfPicturesLeftToAdd
            let numberOfPicturesLeftToAdd = self.numberOfThumbnailsCardDisplays-self.numberOfImagesLoaded
            
            // determine remainingMembers
            let numberOfRemainingMembers: Int = self.members.count-(membersArrayMemberIndex+1)
            
            if numberOfRemainingMembers <= numberOfPicturesLeftToAdd { // if equal, must add images for all remaining members (even though non-stock image is preferred)
                // save relevant image ID reference
                // save picture reference
                let stockImageID = self.members[membersArrayMemberIndex].stockImageID as Int
                self.images.append([ImageType.stockImage:stockImageID])
                // increment number of images loaded
            }
        }
        // check if remaining images need to be loaded
        // determine numberOfPicturesLeftToAdd
        let numberOfPicturesLeftToAdd = self.numberOfThumbnailsCardDisplays-self.numberOfImagesLoaded
        // check if more images are required to be displayed
        if numberOfPicturesLeftToAdd > 0 {
            let numberOfRemainingMembers = self.members.count-(membersArrayMemberIndex+1)
            if numberOfRemainingMembers > 0 { // if need more thumbnails and more members are in array, try to fetch next thumbnail
                liveLoadThumbnailsHandler(precedingMemberIndex: membersArrayMemberIndex)
            }
            else {
                //checkForUpdatedThumbnailProfileImagesHandler()
                // if no more members, duplicate images already loaded as required
                // in the case where images are less than images needed
                var finalPictureContainerImageSet: [UIImage] = []
                let numberOfImages = self.images.count
                let imageOneDictionary: Dictionary<ImageType,Int> = self.images[0]
                let imageOne = self.image(for:imageOneDictionary)
                switch numberOfImages {
                case _ where numberOfImages == 1:
                    // define profile picture array to use for card picture container
                    finalPictureContainerImageSet = [imageOne,imageOne,imageOne,imageOne]
                case _ where numberOfImages == 2:
                    let imageTwoDictionary: Dictionary<ImageType,Int> = self.images[1]
                    let imageTwo = self.image(for:imageTwoDictionary)
                    // define profile picture array to use for card picture container
                    finalPictureContainerImageSet = [imageOne,imageTwo,imageTwo,imageOne]
                case _ where numberOfImages == 3:
                    let imageTwoDictionary: Dictionary<ImageType,Int> = self.images[1]
                    let imageThreeDictionary: Dictionary<ImageType,Int> = self.images[2]
                    let imageTwo = self.image(for:imageTwoDictionary)
                    let imageThree = self.image(for:imageThreeDictionary)
                    // define profile picture array to use for card picture container
                    finalPictureContainerImageSet = [imageOne,imageTwo,imageThree,imageOne]
                default:
                    break
                }
                // call live-loader to load card images; picture display helper is a lazy var, methods assist live-load images
                pictureDisplayHelper.defineImageSetForPictureContainer(finalPictureContainerImageSet)
            }
        }
        else { // else all images are ready for live-load into card picture container
            //checkForUpdatedThumbnailProfileImagesHandler()
            var finalPictureContainerImageSet: [UIImage] = []
            for imageSet in self.images { // get images for
                //self.images
                let image = self.image(for:imageSet)
                finalPictureContainerImageSet.append(image)
            }
            // call live-loader to load card images; picture display helper is a lazy var, methods assist live-load images
            pictureDisplayHelper.defineImageSetForPictureContainer(finalPictureContainerImageSet)
        }
    }
    
    func prepareImagesForPictureContainer() {
        print("func prepareImagesForPictureContainer is running...")
        var finalPictureContainerImageSet: [UIImage] = []
        if self.images.count < 4 {
            
        }
        else {
            
        }
    }
    
    func fetchThumbnail(for memberID: Int, membersArrayMemberIndex: Int) { // fetch as many thumbnails as needed to display, live-load returned thumbnails
        print("func fetchThumbnail is running...")
        let intent = WebServiceCallIntent.getThumbnailProfileImage(userID: memberID, membersArrayMemberIndex: membersArrayMemberIndex)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                print("func fetchThumbnail:getThumbnailProfileImage intent web call succeeded")
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getThumbnailProfileImage(membersArrayMemberIndex, imageFetched, thumbnailProfileImageID) = returnIntent {
                        if let memberIndex = membersArrayMemberIndex {
                            // save custom profile picture in local file
                            let userID = self.members[memberIndex].ID!
                            let imageParameters: Dictionary<ImageType,Int> = [ImageType.customPicture: userID]
                            let image = self.image(for:imageParameters)
                            LoadLocalFile.saveCommunityMember(thumbnailProfileImage: image, thumbnailProfileImageID: thumbnailProfileImageID, for: userID)
                            
                            // go to handler to continue loading images
                            self.liveLoadThumbnailHandler(membersArrayMemberIndex: memberIndex, imageFetched: imageFetched)
                        }
                    }
                }
                // else cannot do anything to continue fetching remaining thumbnails without having supporting member info
            }
            // did not successfully fetch thumbnail, consequently precedingIndex/memberID/rally or community instances being searched for are not available to continue fetching thumbnails
        })
    }
    
    func checkForUpdatedThumbnailProfileImagesHandler() {
        print("func checkForUpdatedThumbnailProfileImagesHandler is running...")
        //while index<self.images.count { // while used so if let condition not met will continue looping
        let image = self.images[updatedImagesCheckCounter]
        let userID = image[.customPicture]!
        if let thumbnailProfileImageID = communityMemberThumbnailUserIDsGlobalDictionary[userID] {
            checkForUpdatedThumbnailProfileImages(for: thumbnailProfileImageID, userID: userID)
        }
        else {
            updatedImagesCheckCounter += 1
            if updatedImagesCheckCounter < self.images.count {
                checkForUpdatedThumbnailProfileImagesHandler()
            }
            else {
                prepareImagesForPictureContainer()
            }
        }
    }
    
    func checkForUpdatedThumbnailProfileImages(for thumbnailProfileImageID: Int, userID: Int) {
        print("func checkForUpdatedThumbnailProfileImages is running...")
        let intent = WebServiceCallIntent.checkForUpdatedThumbnailProfileImage(userID: userID, thumbnailProfileImageID: thumbnailProfileImageID)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                print("func fetchThumbnail:getThumbnailProfileImage intent web call succeeded")
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID, thumbnailUpdated) = returnIntent {
                        if thumbnailUpdated {
                            // save custom profile picture in local file
                            let imageParameters: Dictionary<ImageType,Int> = [ImageType.customPicture: userID]
                            let image = self.image(for:imageParameters)
                            LoadLocalFile.saveCommunityMember(thumbnailProfileImage: image, thumbnailProfileImageID: thumbnailProfileImageID, for: userID)
                            
                            // continue iterating through self.images to assure all images are updated. if all have been checked, format images in UI
                            self.updatedImagesCheckCounter += 1
                            if self.updatedImagesCheckCounter < self.images.count {
                                self.checkForUpdatedThumbnailProfileImagesHandler()
                            }
                            else {
                                self.prepareImagesForPictureContainer()
                            }
                        }
                        else {
                            // do nothing, image is up-to-date
                        }
                    }
                }
                // else cannot do anything to continue fetching remaining thumbnails without having supporting member info
            }
            // did not successfully fetch thumbnail, consequently precedingIndex/memberID/rally or community instances being searched for are not available to continue fetching thumbnails
        })
    }
    
    func memberID(forMemberAt respectiveArrayIndex: Int)->Int {
        print("func memberID is running...")
        if let value = self.members[respectiveArrayIndex].ID {
            return value
        }
        return 0 // should never happen
    }
    
    func members(at respectiveArrayIndexArray: [Int])-> [Friend] {
        print("func members is running...")
        var members: [Friend]!
        for respectiveArrayIndex in respectiveArrayIndexArray {
            switch self.cardType {
            case .upcomingRally(let upcomingRally):
                // make request for member profile pictures from database
                members.append(upcomingRally!.rallyUserList[respectiveArrayIndex])
            case .publicCommunity(let publicCommunity):
                members.append(publicCommunity!.members[respectiveArrayIndex])
            case .privateCommunity(let privateCommunity):
                members.append(privateCommunity!.members[respectiveArrayIndex])
            case .communityNearby(let communityNearby):
                members.append(communityNearby!.members[respectiveArrayIndex])
            default:
                break
            }
        }
        return members
    }
    
    func relevantImageIDAndType(for member: Friend)-> (Int,ImageType) {
        print("func relevantImageIDAndType is running...")
        if member.thumbnailProfileImage != nil {
            return (member.ID!,ImageType.customPicture)
        }
        else {
            return (member.stockImageID,ImageType.stockImage)
        }
    }
    
    func image(for relevantIDAndType: Dictionary<ImageType,Int>) -> UIImage {
        print("func image is running...")
        if let relevantID = relevantIDAndType[.stockImage] {
            return stockImageGlobalDictionary[relevantID]!
        }
        else {
            let value = relevantIDAndType[.customPicture]!
            let image = communityMemberThumbnailsGlobalDictionary[value]!
            return image
        }
    }
    // live-load thumbnails for cards
}

