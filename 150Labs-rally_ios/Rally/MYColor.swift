//
//  MYColor.swift
//  Rally
//
//  Created by Ian Moses on 1/6/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYColor: UIColor {
    
    static var rallyBlue = UIColor( red: 9/255, green: 149/255, blue: 219/255, alpha: 1.0 )
    static var darkBlue = UIColor( red: 9/255, green: 109/255, blue: 159/255, alpha: 1.0 )
    // static var greyBlue = UIColor( red: 121/255, green: 188/255, blue: 230/255, alpha: 1.0 )
    static var greyBlue = UIColor( red: 170/255, green: 208/255, blue: 239/255, alpha: 1.0 )
    static var detailButtonTitleGrey = UIColor( red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0 )
    static var JoinedAndTimeLeftLabelGrey = UIColor( red: 183/255, green: 183/255, blue: 183/255, alpha: 1.0 )
    static var JoinedAndTimeLeftViewGrey = UIColor( red: 164/255, green: 164/255, blue: 164/255, alpha: 1.0 )
    static var lightGrey = UIColor( red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0 )
    static var cellMidGrey = UIColor( red: 214/255, green: 217/255, blue: 218/255, alpha: 1.0 )
    static var midGrey = UIColor( red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0 )
    static var inactiveMenuItem = UIColor( red: 207/255, green: 210/255, blue: 215/255, alpha: 0.94 )
    static let inactiveGrey = UIColor( red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0 )
    static var midDarkGrey = UIColor( red: 183/255, green: 183/255, blue: 183/255, alpha: 1.0 )
    static var darkGrey = UIColor( red: 169/255, green: 169/255, blue: 169/255, alpha: 1.0 ) // might not need. Only known use is 'more' icon on homeViewController rally card. Most probable use would be text
    static var veryDarkGrey = UIColor( red: 147.5/255, green: 147.5/255, blue: 147.5/255, alpha: 1.0 )
    static var secondVeryDarkGrey = UIColor( red: 143.5/255, green: 143.5/255, blue: 143.5/255, alpha: 1.0 )
    static var extremelyDarkGrey = UIColor( red: 124/255, green: 124/255, blue: 124/255, alpha: 1.0 )
    static var menuGreyBlack = UIColor( red: 82/255, green: 82/255, blue: 82/255, alpha: 1.0 )
    static var lightBlack = UIColor( red: 41/255, green: 47/255, blue: 51/255, alpha: 1.0 )
    static var selectedThread = UIColor( red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0 )
    static var homeSectionSublabelGrey = UIColor( red: 0, green: 0, blue: 0, alpha: 0.701961 )
    static var communityMessagingMessageBlurbName = UIColor( red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0 )
    static var cardBackground: CardBackground {
        get {
            return CardBackground()
        }
    }
    
    override func setFill() {
        super.setFill()
    }
}

/*
 func random() -> UIColor {
 print("func random is running...")
 // alt base two/three: use skewed distribution (different transform than box-muller (centered around baseOne, entering cutoff for each side: [255-baseOne, baseOne-1]))
 
 func proceedingBase(precedingBase: Int, multiplier: CGFloat)-> Int {
 print("func proceedingBase is running...")
 let minRange = min(255-precedingBase, precedingBase-1)
 let fullRange = CGFloat(minRange*2)
 let baseDiff: Int = Int(round(multiplier*fullRange))
 let addToBaseTwo = minRange-baseDiff
 let proceedingBase = precedingBase - addToBaseTwo
 return proceedingBase
 }
 
 // generates two random, uniformly distributed numbers: (0,1)
 // transforms randomOne and randomTwo into normally distributed numbers
 let u360 = UInt32(360)
 let randomOne = CGFloat(arc4random_uniform(u360)+1)/360
 let r = (-2*log(randomOne)).squareRoot()
 let randomTwo = 0.01745*(CGFloat(arc4random_uniform(u360)+1))
 let uOne = r*cos(randomTwo)
 let uTwo = r*sin(randomTwo)
 
 // shift uOne and uTwo -> [r1,r2] to [r1-r1,r1+r2]
 let translatedUpperBound: CGFloat = 2*(-2*log(1/360)).squareRoot()
 // get base multipliers
 let baseOneMultiplier = 0.5+uOne/translatedUpperBound
 let baseTwoMultiplier = 0.5+uTwo/translatedUpperBound
 
 // get baseOne
 let baseOne = Int(arc4random_uniform(UInt32(248))+8) // first value never under 8 (less black backgrounds)
 
 // get baseTwo
 let baseTwo = proceedingBase(precedingBase: baseOne, multiplier: baseOneMultiplier)
 
 // get baseThree
 let baseThree = proceedingBase(precedingBase: baseTwo, multiplier: baseTwoMultiplier)
 
 return UIColor(red: baseOne, green: baseTwo, blue: baseThree)
 }
 */

class CardBackground {
    static var softOrange = UIColor( red: 147.5/255, green: 147.5/255, blue: 147.5/255, alpha: 1.0 )
    static var lightBlue = UIColor( red: 110/255, green: 143.5/255, blue: 143.5/255, alpha: 1.0 )
    static var fuzzyGreen = UIColor( red: 15/255, green: 143.5/255, blue: 143.5/255, alpha: 1.0 )
    static var cream = UIColor( red: 143.5/255, green: 59/255, blue: 143.5/255, alpha: 1.0 )
    static var invitingBrown = UIColor( red: 143.5/255, green: 143.5/255, blue: 143.5/255, alpha: 1.0 )
    static var warmRed = UIColor( red: 143.5/255, green: 54/255, blue: 34/255, alpha: 1.0 )
    static var peach = UIColor( red: 4/255, green: 234/255, blue: 23/255, alpha: 1.0 )
    static var easyPurple = UIColor( red: 34/255, green: 143.5/255, blue: 54/255, alpha: 1.0 )
    static var fadedTeal = UIColor( red: 143.5/255, green: 143.5/255, blue: 46/255, alpha: 1.0 )
    static var bearBrown = UIColor( red: 123/255, green: 66/255, blue: 26/255, alpha: 1.0 )
    
    func random()-> UIColor {
        print("func random is running...")
        let ceiling: UInt32 = UInt32(1000)
        var (r, g, b): (Int,Int,Int) = (1,1,1)
        let randomInteger = arc4random_uniform(ceiling)
        switch randomInteger {
        case 0:
            (r, g, b) = (177, 165, 146)
        case 1:
            (r, g, b) = (91, 117, 103)
        case 2:
            (r, g, b) = (193, 183, 225)
        case 3:
            (r, g, b) = (170, 176, 203)
        case 4:
            (r, g, b) = (151, 165, 178)
        case 5:
            (r, g, b) = (243, 238, 242)
        case 6:
            (r, g, b) = (82, 59, 65)
        case 7:
            (r, g, b) = (110, 118, 137)
        case 8:
            (r, g, b) = (45, 64, 50)
        case 9:
            (r, g, b) = (200, 185, 173)
        case 10:
            (r, g, b) = (236, 233, 217)
        case 11:
            (r, g, b) = (186, 186, 177)
        case 12:
            (r, g, b) = (42, 55, 38)
        case 13:
            (r, g, b) = (24, 27, 22)
        case 14:
            (r, g, b) = (201, 207, 200)
        case 15:
            (r, g, b) = (233, 224, 233)
        case 16:
            (r, g, b) = (48, 66, 6)
        case 17:
            (r, g, b) = (71, 107, 86)
        case 18:
            (r, g, b) = (174, 174, 183)
        case 19:
            (r, g, b) = (35, 29, 34)
        case 20:
            (r, g, b) = (165, 181, 176)
        case 21:
            (r, g, b) = (23, 30, 27)
        case 22:
            (r, g, b) = (107, 40, 43)
        case 23:
            (r, g, b) = (67, 50, 27)
        case 24:
            (r, g, b) = (152, 180, 192)
        case 25:
            (r, g, b) = (145, 133, 182)
        case 26:
            (r, g, b) = (107, 53, 32)
        case 27:
            (r, g, b) = (54, 61, 47)
        case 28:
            (r, g, b) = (167, 155, 73)
        case 29:
            (r, g, b) = (69, 49, 50)
        case 30:
            (r, g, b) = (233, 229, 221)
        case 31:
            (r, g, b) = (179, 226, 224)
        case 32:
            (r, g, b) = (66, 76, 49)
        case 33:
            (r, g, b) = (109, 177, 130)
        case 34:
            (r, g, b) = (59, 51, 29)
        case 35:
            (r, g, b) = (141, 138, 156)
        case 36:
            (r, g, b) = (235, 240, 240)
        case 37:
            (r, g, b) = (50, 44, 30)
        case 38:
            (r, g, b) = (183, 182, 201)
        case 39:
            (r, g, b) = (176, 207, 208)
        case 40:
            (r, g, b) = (12, 15, 22)
        case 41:
            (r, g, b) = (84, 85, 105)
        case 42:
            (r, g, b) = (111, 130, 139)
        case 43:
            (r, g, b) = (53, 84, 108)
        case 44:
            (r, g, b) = (223, 225, 222)
        case 45:
            (r, g, b) = (178, 184, 174)
        case 46:
            (r, g, b) = (227, 227, 226)
        case 47:
            (r, g, b) = (48, 24, 17)
        case 48:
            (r, g, b) = (206, 188, 171)
        case 49:
            (r, g, b) = (239, 233, 235)
        case 50:
            (r, g, b) = (130, 115, 171)
        case 51:
            (r, g, b) = (96, 100, 105)
        case 52:
            (r, g, b) = (121, 152, 151)
        case 53:
            (r, g, b) = (226, 248, 245)
        case 54:
            (r, g, b) = (184, 149, 152)
        case 55:
            (r, g, b) = (253, 254, 254)
        case 56:
            (r, g, b) = (97, 108, 115)
        case 57:
            (r, g, b) = (227, 236, 229)
        case 58:
            (r, g, b) = (207, 215, 223)
        case 59:
            (r, g, b) = (152, 162, 199)
        case 60:
            (r, g, b) = (171, 134, 120)
        case 61:
            (r, g, b) = (57, 55, 44)
        case 62:
            (r, g, b) = (21, 15, 21)
        case 63:
            (r, g, b) = (126, 110, 120)
        case 64:
            (r, g, b) = (172, 167, 178)
        case 65:
            (r, g, b) = (32, 18, 21)
        case 66:
            (r, g, b) = (161, 150, 132)
        case 67:
            (r, g, b) = (98, 111, 134)
        case 68:
            (r, g, b) = (65, 64, 64)
        case 69:
            (r, g, b) = (174, 164, 208)
        case 70:
            (r, g, b) = (13, 13, 16)
        case 71:
            (r, g, b) = (69, 110, 103)
        case 72:
            (r, g, b) = (46, 58, 70)
        case 73:
            (r, g, b) = (81, 103, 109)
        case 74:
            (r, g, b) = (148, 130, 130)
        case 75:
            (r, g, b) = (12, 12, 14)
        case 76:
            (r, g, b) = (109, 127, 159)
        case 77:
            (r, g, b) = (184, 199, 199)
        case 78:
            (r, g, b) = (228, 229, 220)
        case 79:
            (r, g, b) = (220, 227, 235)
        case 80:
            (r, g, b) = (67, 86, 72)
        case 81:
            (r, g, b) = (9, 9, 9)
        case 82:
            (r, g, b) = (201, 209, 217)
        case 83:
            (r, g, b) = (208, 224, 229)
        case 84:
            (r, g, b) = (211, 216, 234)
        case 85:
            (r, g, b) = (14, 9, 7)
        case 86:
            (r, g, b) = (201, 220, 222)
        case 87:
            (r, g, b) = (216, 206, 188)
        case 88:
            (r, g, b) = (112, 87, 118)
        case 89:
            (r, g, b) = (62, 57, 46)
        case 90:
            (r, g, b) = (237, 238, 236)
        case 91:
            (r, g, b) = (80, 65, 50)
        case 92:
            (r, g, b) = (49, 60, 67)
        case 93:
            (r, g, b) = (170, 176, 169)
        case 94:
            (r, g, b) = (136, 125, 94)
        case 95:
            (r, g, b) = (194, 159, 183)
        case 96:
            (r, g, b) = (238, 246, 249)
        case 97:
            (r, g, b) = (45, 54, 51)
        case 98:
            (r, g, b) = (254, 253, 252)
        case 99:
            (r, g, b) = (169, 183, 201)
        case 100:
            (r, g, b) = (211, 227, 232)
        case 101:
            (r, g, b) = (26, 20, 22)
        case 102:
            (r, g, b) = (147, 157, 184)
        case 103:
            (r, g, b) = (211, 220, 223)
        case 104:
            (r, g, b) = (126, 105, 112)
        case 105:
            (r, g, b) = (45, 21, 20)
        case 106:
            (r, g, b) = (96, 130, 83)
        case 107:
            (r, g, b) = (127, 123, 180)
        case 108:
            (r, g, b) = (221, 212, 210)
        case 109:
            (r, g, b) = (87, 97, 131)
        case 110:
            (r, g, b) = (50, 88, 89)
        case 111:
            (r, g, b) = (28, 19, 20)
        case 112:
            (r, g, b) = (49, 38, 37)
        case 113:
            (r, g, b) = (228, 216, 220)
        case 114:
            (r, g, b) = (137, 116, 38)
        case 115:
            (r, g, b) = (37, 38, 29)
        case 116:
            (r, g, b) = (196, 189, 174)
        case 117:
            (r, g, b) = (35, 36, 36)
        case 118:
            (r, g, b) = (131, 133, 185)
        case 119:
            (r, g, b) = (32, 28, 16)
        case 120:
            (r, g, b) = (97, 158, 195)
        case 121:
            (r, g, b) = (202, 177, 189)
        case 122:
            (r, g, b) = (110, 126, 84)
        case 123:
            (r, g, b) = (48, 36, 45)
        case 124:
            (r, g, b) = (228, 221, 210)
        case 125:
            (r, g, b) = (79, 61, 79)
        case 126:
            (r, g, b) = (244, 246, 246)
        case 127:
            (r, g, b) = (228, 236, 223)
        case 128:
            (r, g, b) = (243, 250, 249)
        case 129:
            (r, g, b) = (99, 141, 168)
        case 130:
            (r, g, b) = (238, 225, 226)
        case 131:
            (r, g, b) = (78, 119, 143)
        case 132:
            (r, g, b) = (48, 60, 47)
        case 133:
            (r, g, b) = (186, 196, 191)
        case 134:
            (r, g, b) = (101, 107, 73)
        case 135:
            (r, g, b) = (238, 237, 238)
        case 136:
            (r, g, b) = (49, 51, 46)
        case 137:
            (r, g, b) = (186, 162, 207)
        case 138:
            (r, g, b) = (8, 11, 8)
        case 139:
            (r, g, b) = (48, 66, 46)
        case 140:
            (r, g, b) = (164, 155, 151)
        case 141:
            (r, g, b) = (218, 228, 229)
        case 142:
            (r, g, b) = (65, 71, 82)
        case 143:
            (r, g, b) = (186, 214, 201)
        case 144:
            (r, g, b) = (23, 26, 33)
        case 145:
            (r, g, b) = (228, 222, 217)
        case 146:
            (r, g, b) = (193, 195, 214)
        case 147:
            (r, g, b) = (201, 222, 220)
        case 148:
            (r, g, b) = (145, 128, 56)
        case 149:
            (r, g, b) = (15, 16, 17)
        case 150:
            (r, g, b) = (33, 36, 19)
        case 151:
            (r, g, b) = (234, 234, 228)
        case 152:
            (r, g, b) = (88, 95, 137)
        case 153:
            (r, g, b) = (58, 58, 58)
        case 154:
            (r, g, b) = (220, 220, 208)
        case 155:
            (r, g, b) = (24, 24, 26)
        case 156:
            (r, g, b) = (206, 195, 223)
        case 157:
            (r, g, b) = (116, 112, 99)
        case 158:
            (r, g, b) = (23, 11, 10)
        case 159:
            (r, g, b) = (59, 47, 53)
        case 160:
            (r, g, b) = (24, 21, 24)
        case 161:
            (r, g, b) = (83, 67, 91)
        case 162:
            (r, g, b) = (251, 250, 250)
        case 163:
            (r, g, b) = (254, 255, 255)
        case 164:
            (r, g, b) = (221, 230, 239)
        case 165:
            (r, g, b) = (171, 183, 182)
        case 166:
            (r, g, b) = (22, 19, 25)
        case 167:
            (r, g, b) = (136, 161, 140)
        case 168:
            (r, g, b) = (235, 236, 233)
        case 169:
            (r, g, b) = (137, 147, 159)
        case 170:
            (r, g, b) = (183, 188, 172)
        case 171:
            (r, g, b) = (124, 154, 178)
        case 172:
            (r, g, b) = (101, 103, 119)
        case 173:
            (r, g, b) = (78, 98, 120)
        case 174:
            (r, g, b) = (214, 196, 166)
        case 175:
            (r, g, b) = (20, 30, 23)
        case 176:
            (r, g, b) = (19, 19, 17)
        case 177:
            (r, g, b) = (44, 40, 21)
        case 178:
            (r, g, b) = (61, 53, 62)
        case 179:
            (r, g, b) = (55, 59, 55)
        case 180:
            (r, g, b) = (142, 149, 154)
        case 181:
            (r, g, b) = (82, 78, 81)
        case 182:
            (r, g, b) = (140, 111, 166)
        case 183:
            (r, g, b) = (250, 251, 251)
        case 184:
            (r, g, b) = (240, 245, 243)
        case 185:
            (r, g, b) = (215, 206, 212)
        case 186:
            (r, g, b) = (235, 69, 65)
        case 187:
            (r, g, b) = (119, 120, 126)
        case 188:
            (r, g, b) = (118, 138, 118)
        case 189:
            (r, g, b) = (58, 57, 86)
        case 190:
            (r, g, b) = (205, 234, 239)
        case 191:
            (r, g, b) = (33, 38, 44)
        case 192:
            (r, g, b) = (191, 209, 194)
        case 193:
            (r, g, b) = (191, 158, 187)
        case 194:
            (r, g, b) = (135, 203, 182)
        case 195:
            (r, g, b) = (122, 93, 182)
        case 196:
            (r, g, b) = (212, 210, 191)
        case 197:
            (r, g, b) = (250, 248, 246)
        case 198:
            (r, g, b) = (152, 119, 127)
        case 199:
            (r, g, b) = (159, 158, 224)
        case 200:
            (r, g, b) = (36, 56, 84)
        case 201:
            (r, g, b) = (32, 26, 26)
        case 202:
            (r, g, b) = (178, 193, 178)
        case 203:
            (r, g, b) = (67, 63, 106)
        case 204:
            (r, g, b) = (250, 252, 252)
        case 205:
            (r, g, b) = (223, 212, 190)
        case 206:
            (r, g, b) = (13, 20, 18)
        case 207:
            (r, g, b) = (106, 99, 112)
        case 208:
            (r, g, b) = (66, 48, 43)
        case 209:
            (r, g, b) = (100, 104, 96)
        case 210:
            (r, g, b) = (240, 244, 248)
        case 211:
            (r, g, b) = (168, 122, 137)
        case 212:
            (r, g, b) = (156, 157, 136)
        case 213:
            (r, g, b) = (241, 240, 235)
        case 214:
            (r, g, b) = (148, 153, 149)
        case 215:
            (r, g, b) = (139, 126, 174)
        case 216:
            (r, g, b) = (246, 249, 250)
        case 217:
            (r, g, b) = (57, 58, 38)
        case 218:
            (r, g, b) = (125, 185, 202)
        case 219:
            (r, g, b) = (18, 21, 16)
        case 220:
            (r, g, b) = (102, 102, 74)
        case 221:
            (r, g, b) = (162, 172, 113)
        case 222:
            (r, g, b) = (248, 248, 250)
        case 223:
            (r, g, b) = (135, 153, 169)
        case 224:
            (r, g, b) = (189, 175, 174)
        case 225:
            (r, g, b) = (213, 199, 170)
        case 226:
            (r, g, b) = (104, 101, 153)
        case 227:
            (r, g, b) = (24, 46, 58)
        case 228:
            (r, g, b) = (246, 246, 247)
        case 229:
            (r, g, b) = (249, 248, 250)
        case 230:
            (r, g, b) = (54, 72, 69)
        case 231:
            (r, g, b) = (26, 30, 33)
        case 232:
            (r, g, b) = (120, 125, 120)
        case 233:
            (r, g, b) = (28, 28, 15)
        case 234:
            (r, g, b) = (237, 237, 237)
        case 235:
            (r, g, b) = (92, 97, 90)
        case 236:
            (r, g, b) = (178, 205, 202)
        case 237:
            (r, g, b) = (200, 182, 162)
        case 238:
            (r, g, b) = (134, 132, 102)
        case 239:
            (r, g, b) = (45, 20, 19)
        case 240:
            (r, g, b) = (114, 72, 74)
        case 241:
            (r, g, b) = (103, 87, 71)
        case 242:
            (r, g, b) = (219, 210, 243)
        case 243:
            (r, g, b) = (67, 90, 86)
        case 244:
            (r, g, b) = (149, 209, 204)
        case 245:
            (r, g, b) = (77, 42, 35)
        case 246:
            (r, g, b) = (147, 122, 163)
        case 247:
            (r, g, b) = (37, 43, 47)
        case 248:
            (r, g, b) = (46, 55, 65)
        case 249:
            (r, g, b) = (36, 37, 45)
        case 250:
            (r, g, b) = (200, 196, 160)
        case 251:
            (r, g, b) = (54, 73, 71)
        case 252:
            (r, g, b) = (16, 18, 17)
        case 253:
            (r, g, b) = (203, 201, 196)
        case 254:
            (r, g, b) = (43, 35, 43)
        case 255:
            (r, g, b) = (82, 61, 37)
        case 256:
            (r, g, b) = (99, 91, 120)
        case 257:
            (r, g, b) = (142, 200, 188)
        case 258:
            (r, g, b) = (179, 198, 195)
        case 259:
            (r, g, b) = (220, 196, 205)
        case 260:
            (r, g, b) = (86, 44, 51)
        case 261:
            (r, g, b) = (100, 97, 111)
        case 262:
            (r, g, b) = (250, 250, 249)
        case 263:
            (r, g, b) = (150, 196, 196)
        case 264:
            (r, g, b) = (10, 11, 8)
        case 265:
            (r, g, b) = (66, 43, 37)
        case 266:
            (r, g, b) = (174, 179, 181)
        case 267:
            (r, g, b) = (190, 199, 214)
        case 268:
            (r, g, b) = (171, 168, 189)
        case 269:
            (r, g, b) = (249, 251, 253)
        case 270:
            (r, g, b) = (235, 242, 230)
        case 271:
            (r, g, b) = (184, 183, 184)
        case 272:
            (r, g, b) = (83, 63, 73)
        case 273:
            (r, g, b) = (28, 18, 11)
        case 274:
            (r, g, b) = (50, 43, 32)
        case 275:
            (r, g, b) = (218, 188, 215)
        case 276:
            (r, g, b) = (119, 116, 103)
        case 277:
            (r, g, b) = (247, 247, 248)
        case 278:
            (r, g, b) = (218, 233, 229)
        case 279:
            (r, g, b) = (208, 214, 221)
        case 280:
            (r, g, b) = (235, 238, 239)
        case 281:
            (r, g, b) = (65, 76, 74)
        case 282:
            (r, g, b) = (190, 202, 190)
        case 283:
            (r, g, b) = (111, 119, 169)
        case 284:
            (r, g, b) = (125, 134, 162)
        case 285:
            (r, g, b) = (166, 195, 180)
        case 286:
            (r, g, b) = (34, 19, 21)
        case 287:
            (r, g, b) = (77, 116, 120)
        case 288:
            (r, g, b) = (157, 132, 140)
        case 289:
            (r, g, b) = (121, 71, 39)
        case 290:
            (r, g, b) = (157, 168, 189)
        case 291:
            (r, g, b) = (251, 250, 250)
        case 292:
            (r, g, b) = (240, 244, 244)
        case 293:
            (r, g, b) = (99, 43, 46)
        case 294:
            (r, g, b) = (19, 12, 11)
        case 295:
            (r, g, b) = (202, 209, 207)
        case 296:
            (r, g, b) = (31, 33, 22)
        case 297:
            (r, g, b) = (95, 125, 146)
        case 298:
            (r, g, b) = (222, 233, 225)
        case 299:
            (r, g, b) = (131, 102, 104)
        case 300:
            (r, g, b) = (121, 133, 111)
        case 301:
            (r, g, b) = (187, 199, 178)
        case 302:
            (r, g, b) = (43, 45, 35)
        case 303:
            (r, g, b) = (47, 33, 25)
        case 304:
            (r, g, b) = (236, 231, 231)
        case 305:
            (r, g, b) = (117, 141, 130)
        case 306:
            (r, g, b) = (211, 219, 208)
        case 307:
            (r, g, b) = (54, 62, 45)
        case 308:
            (r, g, b) = (67, 64, 67)
        case 309:
            (r, g, b) = (90, 115, 122)
        case 310:
            (r, g, b) = (90, 123, 115)
        case 311:
            (r, g, b) = (146, 144, 131)
        case 312:
            (r, g, b) = (121, 143, 142)
        case 313:
            (r, g, b) = (41, 49, 82)
        case 314:
            (r, g, b) = (115, 117, 84)
        case 315:
            (r, g, b) = (191, 214, 196)
        case 316:
            (r, g, b) = (228, 250, 250)
        case 317:
            (r, g, b) = (155, 144, 124)
        case 318:
            (r, g, b) = (232, 247, 247)
        case 319:
            (r, g, b) = (239, 234, 236)
        case 320:
            (r, g, b) = (150, 141, 198)
        case 321:
            (r, g, b) = (80, 101, 108)
        case 322:
            (r, g, b) = (125, 82, 83)
        case 323:
            (r, g, b) = (103, 144, 145)
        case 324:
            (r, g, b) = (67, 61, 59)
        case 325:
            (r, g, b) = (251, 253, 252)
        case 326:
            (r, g, b) = (136, 168, 141)
        case 327:
            (r, g, b) = (90, 51, 54)
        case 328:
            (r, g, b) = (79, 99, 163)
        case 329:
            (r, g, b) = (188, 176, 172)
        case 330:
            (r, g, b) = (239, 234, 239)
        case 331:
            (r, g, b) = (49, 58, 6)
        case 332:
            (r, g, b) = (245, 248, 247)
        case 333:
            (r, g, b) = (206, 206, 226)
        case 334:
            (r, g, b) = (161, 125, 55)
        case 335:
            (r, g, b) = (143, 87, 96)
        case 336:
            (r, g, b) = (16, 14, 11)
        case 337:
            (r, g, b) = (9, 3, 3)
        case 338:
            (r, g, b) = (143, 118, 132)
        case 339:
            (r, g, b) = (16, 13, 16)
        case 340:
            (r, g, b) = (194, 179, 178)
        case 341:
            (r, g, b) = (45, 44, 33)
        case 342:
            (r, g, b) = (223, 228, 217)
        case 343:
            (r, g, b) = (239, 241, 238)
        case 344:
            (r, g, b) = (46, 52, 51)
        case 345:
            (r, g, b) = (126, 136, 110)
        case 346:
            (r, g, b) = (109, 100, 105)
        case 347:
            (r, g, b) = (198, 205, 208)
        case 348:
            (r, g, b) = (42, 42, 33)
        case 349:
            (r, g, b) = (249, 248, 247)
        case 350:
            (r, g, b) = (49, 32, 38)
        case 351:
            (r, g, b) = (59, 47, 39)
        case 352:
            (r, g, b) = (196, 201, 167)
        case 353:
            (r, g, b) = (246, 245, 245)
        case 354:
            (r, g, b) = (107, 52, 62)
        case 355:
            (r, g, b) = (54, 69, 98)
        case 356:
            (r, g, b) = (41, 49, 62)
        case 357:
            (r, g, b) = (48, 62, 88)
        case 358:
            (r, g, b) = (227, 225, 232)
        case 359:
            (r, g, b) = (86, 66, 54)
        case 360:
            (r, g, b) = (205, 202, 220)
        case 361:
            (r, g, b) = (164, 147, 90)
        case 362:
            (r, g, b) = (164, 206, 219)
        case 363:
            (r, g, b) = (88, 141, 178)
        case 364:
            (r, g, b) = (198, 187, 160)
        case 365:
            (r, g, b) = (200, 221, 219)
        case 366:
            (r, g, b) = (93, 53, 37)
        case 367:
            (r, g, b) = (68, 91, 108)
        case 368:
            (r, g, b) = (140, 197, 185)
        case 369:
            (r, g, b) = (223, 216, 229)
        case 370:
            (r, g, b) = (27, 15, 13)
        case 371:
            (r, g, b) = (61, 69, 71)
        case 372:
            (r, g, b) = (143, 178, 180)
        case 373:
            (r, g, b) = (64, 56, 56)
        case 374:
            (r, g, b) = (133, 135, 137)
        case 375:
            (r, g, b) = (162, 151, 176)
        case 376:
            (r, g, b) = (164, 187, 221)
        case 377:
            (r, g, b) = (57, 46, 42)
        case 378:
            (r, g, b) = (76, 48, 46)
        case 379:
            (r, g, b) = (79, 86, 31)
        case 380:
            (r, g, b) = (20, 21, 18)
        case 381:
            (r, g, b) = (13, 11, 11)
        case 382:
            (r, g, b) = (118, 140, 132)
        case 383:
            (r, g, b) = (245, 252, 252)
        case 384:
            (r, g, b) = (59, 50, 59)
        case 385:
            (r, g, b) = (146, 137, 116)
        case 386:
            (r, g, b) = (51, 53, 34)
        case 387:
            (r, g, b) = (231, 238, 241)
        case 388:
            (r, g, b) = (245, 246, 245)
        case 389:
            (r, g, b) = (151, 156, 181)
        case 390:
            (r, g, b) = (43, 62, 51)
        case 391:
            (r, g, b) = (83, 60, 47)
        case 392:
            (r, g, b) = (229, 241, 243)
        case 393:
            (r, g, b) = (34, 44, 58)
        case 394:
            (r, g, b) = (135, 128, 148)
        case 395:
            (r, g, b) = (89, 90, 16)
        case 396:
            (r, g, b) = (175, 213, 226)
        case 397:
            (r, g, b) = (124, 71, 47)
        case 398:
            (r, g, b) = (65, 55, 69)
        case 399:
            (r, g, b) = (140, 97, 67)
        case 400:
            (r, g, b) = (190, 170, 148)
        case 401:
            (r, g, b) = (86, 95, 116)
        case 402:
            (r, g, b) = (175, 193, 204)
        case 403:
            (r, g, b) = (209, 197, 178)
        case 404:
            (r, g, b) = (126, 124, 60)
        case 405:
            (r, g, b) = (18, 16, 4)
        case 406:
            (r, g, b) = (8, 6, 5)
        case 407:
            (r, g, b) = (243, 243, 239)
        case 408:
            (r, g, b) = (206, 214, 207)
        case 409:
            (r, g, b) = (167, 196, 180)
        case 410:
            (r, g, b) = (63, 73, 38)
        case 411:
            (r, g, b) = (211, 201, 171)
        case 412:
            (r, g, b) = (204, 206, 186)
        case 413:
            (r, g, b) = (151, 154, 136)
        case 414:
            (r, g, b) = (99, 131, 161)
        case 415:
            (r, g, b) = (115, 139, 87)
        case 416:
            (r, g, b) = (41, 32, 19)
        case 417:
            (r, g, b) = (226, 224, 220)
        case 418:
            (r, g, b) = (81, 120, 130)
        case 419:
            (r, g, b) = (250, 250, 250)
        case 420:
            (r, g, b) = (221, 219, 225)
        case 421:
            (r, g, b) = (43, 51, 63)
        case 422:
            (r, g, b) = (146, 132, 118)
        case 423:
            (r, g, b) = (88, 143, 162)
        case 424:
            (r, g, b) = (44, 34, 21)
        case 425:
            (r, g, b) = (201, 222, 239)
        case 426:
            (r, g, b) = (184, 196, 189)
        case 427:
            (r, g, b) = (216, 196, 242)
        case 428:
            (r, g, b) = (134, 157, 155)
        case 429:
            (r, g, b) = (99, 114, 133)
        case 430:
            (r, g, b) = (13, 12, 9)
        case 431:
            (r, g, b) = (93, 115, 140)
        case 432:
            (r, g, b) = (15, 15, 13)
        case 433:
            (r, g, b) = (246, 244, 244)
        case 434:
            (r, g, b) = (123, 56, 62)
        case 435:
            (r, g, b) = (192, 212, 195)
        case 436:
            (r, g, b) = (148, 183, 213)
        case 437:
            (r, g, b) = (160, 188, 180)
        case 438:
            (r, g, b) = (152, 171, 141)
        case 439:
            (r, g, b) = (125, 148, 138)
        case 440:
            (r, g, b) = (206, 190, 175)
        case 441:
            (r, g, b) = (25, 24, 19)
        case 442:
            (r, g, b) = (58, 59, 60)
        case 443:
            (r, g, b) = (242, 245, 245)
        case 444:
            (r, g, b) = (40, 37, 26)
        case 445:
            (r, g, b) = (206, 207, 195)
        case 446:
            (r, g, b) = (96, 114, 156)
        case 447:
            (r, g, b) = (231, 235, 237)
        case 448:
            (r, g, b) = (150, 136, 142)
        case 449:
            (r, g, b) = (112, 106, 101)
        case 450:
            (r, g, b) = (197, 212, 210)
        case 451:
            (r, g, b) = (51, 48, 49)
        case 452:
            (r, g, b) = (138, 157, 171)
        case 453:
            (r, g, b) = (213, 216, 209)
        case 454:
            (r, g, b) = (62, 48, 31)
        case 455:
            (r, g, b) = (87, 115, 95)
        case 456:
            (r, g, b) = (118, 135, 76)
        case 457:
            (r, g, b) = (117, 122, 126)
        case 458:
            (r, g, b) = (53, 51, 34)
        case 459:
            (r, g, b) = (247, 245, 244)
        case 460:
            (r, g, b) = (56, 48, 43)
        case 461:
            (r, g, b) = (97, 150, 195)
        case 462:
            (r, g, b) = (197, 185, 176)
        case 463:
            (r, g, b) = (69, 88, 50)
        case 464:
            (r, g, b) = (154, 80, 62)
        case 465:
            (r, g, b) = (80, 44, 34)
        case 466:
            (r, g, b) = (51, 74, 55)
        case 467:
            (r, g, b) = (182, 179, 204)
        case 468:
            (r, g, b) = (105, 116, 78)
        case 469:
            (r, g, b) = (240, 243, 244)
        case 470:
            (r, g, b) = (45, 79, 72)
        case 471:
            (r, g, b) = (36, 36, 44)
        case 472:
            (r, g, b) = (114, 104, 50)
        case 473:
            (r, g, b) = (203, 212, 207)
        case 474:
            (r, g, b) = (122, 110, 93)
        case 475:
            (r, g, b) = (25, 17, 24)
        case 476:
            (r, g, b) = (140, 193, 199)
        case 477:
            (r, g, b) = (254, 254, 254)
        case 478:
            (r, g, b) = (200, 188, 187)
        case 479:
            (r, g, b) = (165, 207, 216)
        case 480:
            (r, g, b) = (22, 17, 19)
        case 481:
            (r, g, b) = (16, 16, 23)
        case 482:
            (r, g, b) = (98, 112, 167)
        case 483:
            (r, g, b) = (150, 118, 122)
        case 484:
            (r, g, b) = (41, 17, 18)
        case 485:
            (r, g, b) = (35, 34, 47)
        case 486:
            (r, g, b) = (144, 153, 148)
        case 487:
            (r, g, b) = (130, 152, 137)
        case 488:
            (r, g, b) = (233, 236, 233)
        case 489:
            (r, g, b) = (182, 199, 220)
        case 490:
            (r, g, b) = (91, 146, 151)
        case 491:
            (r, g, b) = (73, 71, 75)
        case 492:
            (r, g, b) = (111, 110, 107)
        case 493:
            (r, g, b) = (118, 69, 38)
        case 494:
            (r, g, b) = (62, 70, 53)
        case 495:
            (r, g, b) = (73, 127, 96)
        case 496:
            (r, g, b) = (76, 68, 108)
        case 497:
            (r, g, b) = (225, 224, 217)
        case 498:
            (r, g, b) = (228, 231, 231)
        case 499:
            (r, g, b) = (225, 222, 212)
        case 500:
            (r, g, b) = (233, 223, 204)
        case 501:
            (r, g, b) = (175, 187, 167)
        case 502:
            (r, g, b) = (72, 78, 43)
        case 503:
            (r, g, b) = (116, 114, 132)
        case 504:
            (r, g, b) = (128, 118, 94)
        case 505:
            (r, g, b) = (178, 170, 121)
        case 506:
            (r, g, b) = (251, 253, 253)
        case 507:
            (r, g, b) = (243, 246, 246)
        case 508:
            (r, g, b) = (173, 165, 182)
        case 509:
            (r, g, b) = (170, 191, 170)
        case 510:
            (r, g, b) = (228, 222, 218)
        case 511:
            (r, g, b) = (216, 228, 221)
        case 512:
            (r, g, b) = (57, 45, 48)
        case 513:
            (r, g, b) = (112, 88, 88)
        case 514:
            (r, g, b) = (164, 107, 79)
        case 515:
            (r, g, b) = (107, 95, 58)
        case 516:
            (r, g, b) = (192, 184, 204)
        case 517:
            (r, g, b) = (36, 58, 66)
        case 518:
            (r, g, b) = (118, 136, 150)
        case 519:
            (r, g, b) = (103, 126, 144)
        case 520:
            (r, g, b) = (84, 76, 111)
        case 521:
            (r, g, b) = (242, 243, 245)
        case 522:
            (r, g, b) = (167, 220, 213)
        case 523:
            (r, g, b) = (17, 15, 12)
        case 524:
            (r, g, b) = (213, 206, 201)
        case 525:
            (r, g, b) = (251, 250, 251)
        case 526:
            (r, g, b) = (52, 45, 36)
        case 527:
            (r, g, b) = (233, 236, 204)
        case 528:
            (r, g, b) = (121, 83, 99)
        case 529:
            (r, g, b) = (53, 66, 76)
        case 530:
            (r, g, b) = (167, 153, 168)
        case 531:
            (r, g, b) = (232, 219, 218)
        case 532:
            (r, g, b) = (62, 33, 36)
        case 533:
            (r, g, b) = (86, 107, 70)
        case 534:
            (r, g, b) = (209, 200, 191)
        case 535:
            (r, g, b) = (215, 230, 246)
        case 536:
            (r, g, b) = (248, 249, 248)
        case 537:
            (r, g, b) = (42, 37, 43)
        case 538:
            (r, g, b) = (85, 102, 111)
        case 539:
            (r, g, b) = (149, 128, 157)
        case 540:
            (r, g, b) = (63, 101, 98)
        case 541:
            (r, g, b) = (93, 82, 93)
        case 542:
            (r, g, b) = (242, 238, 244)
        case 543:
            (r, g, b) = (148, 160, 135)
        case 544:
            (r, g, b) = (80, 88, 107)
        case 545:
            (r, g, b) = (59, 47, 54)
        case 546:
            (r, g, b) = (143, 106, 46)
        case 547:
            (r, g, b) = (244, 250, 251)
        case 548:
            (r, g, b) = (180, 157, 128)
        case 549:
            (r, g, b) = (140, 79, 91)
        case 550:
            (r, g, b) = (88, 104, 13)
        case 551:
            (r, g, b) = (168, 179, 230)
        case 552:
            (r, g, b) = (105, 119, 124)
        case 553:
            (r, g, b) = (197, 178, 166)
        case 554:
            (r, g, b) = (101, 54, 38)
        case 555:
            (r, g, b) = (194, 187, 191)
        case 556:
            (r, g, b) = (228, 219, 221)
        case 557:
            (r, g, b) = (92, 67, 77)
        case 558:
            (r, g, b) = (66, 44, 50)
        case 559:
            (r, g, b) = (55, 91, 102)
        case 560:
            (r, g, b) = (248, 242, 247)
        case 561:
            (r, g, b) = (177, 165, 205)
        case 562:
            (r, g, b) = (137, 127, 130)
        case 563:
            (r, g, b) = (71, 70, 52)
        case 564:
            (r, g, b) = (207, 172, 164)
        case 565:
            (r, g, b) = (51, 34, 26)
        case 566:
            (r, g, b) = (153, 188, 151)
        case 567:
            (r, g, b) = (99, 131, 109)
        case 568:
            (r, g, b) = (181, 163, 189)
        case 569:
            (r, g, b) = (92, 111, 102)
        case 570:
            (r, g, b) = (113, 135, 135)
        case 571:
            (r, g, b) = (177, 204, 190)
        case 572:
            (r, g, b) = (34, 35, 36)
        case 573:
            (r, g, b) = (71, 99, 103)
        case 574:
            (r, g, b) = (246, 248, 249)
        case 575:
            (r, g, b) = (63, 51, 41)
        case 576:
            (r, g, b) = (17, 13, 15)
        case 577:
            (r, g, b) = (45, 59, 71)
        case 578:
            (r, g, b) = (32, 33, 28)
        case 579:
            (r, g, b) = (192, 231, 211)
        case 580:
            (r, g, b) = (143, 97, 70)
        case 581:
            (r, g, b) = (82, 80, 98)
        case 582:
            (r, g, b) = (147, 210, 213)
        case 583:
            (r, g, b) = (123, 135, 143)
        case 584:
            (r, g, b) = (187, 171, 191)
        case 585:
            (r, g, b) = (32, 28, 41)
        case 586:
            (r, g, b) = (208, 213, 198)
        case 587:
            (r, g, b) = (73, 48, 56)
        case 588:
            (r, g, b) = (113, 132, 109)
        case 589:
            (r, g, b) = (122, 100, 77)
        case 590:
            (r, g, b) = (153, 136, 99)
        case 591:
            (r, g, b) = (105, 89, 117)
        case 592:
            (r, g, b) = (114, 120, 94)
        case 593:
            (r, g, b) = (186, 158, 160)
        case 594:
            (r, g, b) = (204, 214, 204)
        case 595:
            (r, g, b) = (246, 249, 250)
        case 596:
            (r, g, b) = (81, 54, 65)
        case 597:
            (r, g, b) = (243, 243, 242)
        case 598:
            (r, g, b) = (68, 60, 75)
        case 599:
            (r, g, b) = (82, 76, 88)
        case 600:
            (r, g, b) = (232, 228, 229)
        case 601:
            (r, g, b) = (238, 237, 232)
        case 602:
            (r, g, b) = (68, 32, 30)
        case 603:
            (r, g, b) = (103, 133, 90)
        case 604:
            (r, g, b) = (52, 66, 99)
        case 605:
            (r, g, b) = (202, 213, 209)
        case 606:
            (r, g, b) = (110, 110, 194)
        case 607:
            (r, g, b) = (67, 53, 40)
        case 608:
            (r, g, b) = (65, 68, 45)
        case 609:
            (r, g, b) = (46, 70, 76)
        case 610:
            (r, g, b) = (94, 134, 154)
        case 611:
            (r, g, b) = (34, 24, 16)
        case 612:
            (r, g, b) = (200, 213, 217)
        case 613:
            (r, g, b) = (220, 215, 206)
        case 614:
            (r, g, b) = (218, 218, 209)
        case 615:
            (r, g, b) = (196, 194, 185)
        case 616:
            (r, g, b) = (89, 65, 70)
        case 617:
            (r, g, b) = (181, 177, 52)
        case 618:
            (r, g, b) = (57, 48, 53)
        case 619:
            (r, g, b) = (237, 236, 243)
        case 620:
            (r, g, b) = (206, 218, 221)
        case 621:
            (r, g, b) = (68, 76, 124)
        case 622:
            (r, g, b) = (195, 181, 176)
        case 623:
            (r, g, b) = (75, 99, 121)
        case 624:
            (r, g, b) = (143, 129, 59)
        case 625:
            (r, g, b) = (239, 242, 247)
        case 626:
            (r, g, b) = (204, 190, 178)
        case 627:
            (r, g, b) = (141, 165, 223)
        case 628:
            (r, g, b) = (85, 65, 70)
        case 629:
            (r, g, b) = (254, 254, 254)
        case 630:
            (r, g, b) = (109, 109, 105)
        case 631:
            (r, g, b) = (81, 97, 82)
        case 632:
            (r, g, b) = (146, 120, 167)
        case 633:
            (r, g, b) = (144, 112, 89)
        case 634:
            (r, g, b) = (203, 182, 175)
        case 635:
            (r, g, b) = (156, 133, 77)
        case 636:
            (r, g, b) = (165, 84, 120)
        case 637:
            (r, g, b) = (237, 228, 228)
        case 638:
            (r, g, b) = (242, 239, 237)
        case 639:
            (r, g, b) = (255, 255, 255)
        case 640:
            (r, g, b) = (145, 106, 120)
        case 641:
            (r, g, b) = (121, 132, 83)
        case 642:
            (r, g, b) = (139, 219, 203)
        case 643:
            (r, g, b) = (150, 175, 173)
        case 644:
            (r, g, b) = (208, 212, 204)
        case 645:
            (r, g, b) = (237, 251, 250)
        case 646:
            (r, g, b) = (251, 253, 254)
        case 647:
            (r, g, b) = (16, 13, 18)
        case 648:
            (r, g, b) = (177, 152, 153)
        case 649:
            (r, g, b) = (130, 93, 77)
        case 650:
            (r, g, b) = (96, 121, 133)
        case 651:
            (r, g, b) = (184, 202, 228)
        case 652:
            (r, g, b) = (115, 101, 145)
        case 653:
            (r, g, b) = (203, 210, 221)
        case 654:
            (r, g, b) = (127, 106, 95)
        case 655:
            (r, g, b) = (215, 229, 233)
        case 656:
            (r, g, b) = (32, 43, 76)
        case 657:
            (r, g, b) = (70, 90, 82)
        case 658:
            (r, g, b) = (127, 129, 162)
        case 659:
            (r, g, b) = (162, 166, 148)
        case 660:
            (r, g, b) = (170, 127, 107)
        case 661:
            (r, g, b) = (117, 44, 56)
        case 662:
            (r, g, b) = (118, 122, 164)
        case 663:
            (r, g, b) = (67, 95, 108)
        case 664:
            (r, g, b) = (71, 103, 150)
        case 665:
            (r, g, b) = (252, 251, 251)
        case 666:
            (r, g, b) = (240, 237, 233)
        case 667:
            (r, g, b) = (65, 88, 90)
        case 668:
            (r, g, b) = (185, 186, 162)
        case 669:
            (r, g, b) = (66, 76, 90)
        case 670:
            (r, g, b) = (146, 154, 163)
        case 671:
            (r, g, b) = (45, 53, 33)
        case 672:
            (r, g, b) = (124, 52, 52)
        case 673: 
            (r, g, b) = (66, 68, 71)
        case 674: 
            (r, g, b) = (239, 235, 235)
        case 675: 
            (r, g, b) = (101, 134, 134)
        case 676: 
            (r, g, b) = (144, 177, 169)
        case 677: 
            (r, g, b) = (122, 102, 97)
        case 678: 
            (r, g, b) = (101, 146, 115)
        case 679: 
            (r, g, b) = (239, 239, 245)
        case 680: 
            (r, g, b) = (112, 104, 124)
        case 681: 
            (r, g, b) = (185, 175, 177)
        case 682: 
            (r, g, b) = (185, 170, 162)
        case 683: 
            (r, g, b) = (82, 111, 142)
        case 684: 
            (r, g, b) = (214, 218, 202)
        case 685: 
            (r, g, b) = (112, 119, 125)
        case 686: 
            (r, g, b) = (76, 62, 73)
        case 687: 
            (r, g, b) = (63, 70, 87)
        case 688: 
            (r, g, b) = (92, 62, 85)
        case 689: 
            (r, g, b) = (79, 96, 107)
        case 690: 
            (r, g, b) = (117, 131, 112)
        case 691: 
            (r, g, b) = (97, 95, 118)
        case 692: 
            (r, g, b) = (125, 101, 109)
        case 693: 
            (r, g, b) = (89, 100, 127)
        case 694: 
            (r, g, b) = (113, 123, 107)
        case 695: 
            (r, g, b) = (119, 66, 67)
        case 696: 
            (r, g, b) = (112, 155, 158)
        case 697: 
            (r, g, b) = (170, 117, 75)
        case 698: 
            (r, g, b) = (98, 111, 97)
        case 699: 
            (r, g, b) = (220, 232, 237)
        case 700: 
            (r, g, b) = (66, 61, 85)
        case 701: 
            (r, g, b) = (152, 198, 197)
        case 702: 
            (r, g, b) = (84, 80, 50)
        case 703: 
            (r, g, b) = (39, 63, 60)
        case 704: 
            (r, g, b) = (191, 190, 176)
        case 705: 
            (r, g, b) = (45, 38, 54)
        case 706: 
            (r, g, b) = (251, 251, 250)
        case 707: 
            (r, g, b) = (174, 123, 94)
        case 708: 
            (r, g, b) = (101, 144, 174)
        case 709: 
            (r, g, b) = (97, 109, 125)
        case 710: 
            (r, g, b) = (126, 95, 77)
        case 711: 
            (r, g, b) = (112, 93, 56)
        case 712: 
            (r, g, b) = (245, 240, 232)
        case 713: 
            (r, g, b) = (200, 181, 209)
        case 714: 
            (r, g, b) = (175, 160, 166)
        case 715: 
            (r, g, b) = (65, 76, 82)
        case 716: 
            (r, g, b) = (141, 188, 172)
        case 717: 
            (r, g, b) = (55, 59, 51)
        case 718: 
            (r, g, b) = (250, 250, 252)
        case 719: 
            (r, g, b) = (250, 252, 253)
        case 720: 
            (r, g, b) = (245, 245, 248)
        case 721: 
            (r, g, b) = (184, 175, 173)
        case 722: 
            (r, g, b) = (220, 221, 216)
        case 723: 
            (r, g, b) = (121, 87, 97)
        case 724: 
            (r, g, b) = (220, 210, 194)
        case 725: 
            (r, g, b) = (68, 96, 87)
        case 726: 
            (r, g, b) = (226, 221, 206)
        case 727: 
            (r, g, b) = (36, 35, 30)
        case 728: 
            (r, g, b) = (36, 9, 9)
        case 729: 
            (r, g, b) = (96, 84, 54)
        case 730: 
            (r, g, b) = (64, 66, 68)
        case 731: 
            (r, g, b) = (21, 11, 10)
        case 732: 
            (r, g, b) = (169, 204, 201)
        case 733: 
            (r, g, b) = (64, 86, 72)
        case 734: 
            (r, g, b) = (103, 128, 139)
        case 735: 
            (r, g, b) = (89, 84, 106)
        case 736: 
            (r, g, b) = (47, 57, 33)
        case 737: 
            (r, g, b) = (91, 115, 82)
        case 738: 
            (r, g, b) = (250, 252, 253)
        case 739: 
            (r, g, b) = (174, 133, 67)
        case 740: 
            (r, g, b) = (239, 237, 233)
        case 741: 
            (r, g, b) = (190, 212, 218)
        case 742: 
            (r, g, b) = (242, 239, 236)
        case 743: 
            (r, g, b) = (205, 188, 187)
        case 744: 
            (r, g, b) = (237, 238, 238)
        case 745: 
            (r, g, b) = (220, 212, 202)
        case 746: 
            (r, g, b) = (26, 37, 32)
        case 747: 
            (r, g, b) = (135, 147, 83)
        case 748: 
            (r, g, b) = (19, 20, 21)
        case 749: 
            (r, g, b) = (134, 138, 208)
        case 750: 
            (r, g, b) = (124, 138, 166)
        case 751: 
            (r, g, b) = (217, 208, 232)
        case 752: 
            (r, g, b) = (74, 68, 61)
        case 753: 
            (r, g, b) = (34, 25, 24)
        case 754: 
            (r, g, b) = (196, 219, 200)
        case 755: 
            (r, g, b) = (141, 140, 136)
        case 756: 
            (r, g, b) = (217, 222, 235)
        case 757: 
            (r, g, b) = (204, 210, 195)
        case 758: 
            (r, g, b) = (168, 155, 157)
        case 759: 
            (r, g, b) = (19, 28, 13)
        case 760: 
            (r, g, b) = (234, 235, 233)
        case 761: 
            (r, g, b) = (208, 208, 207)
        case 762: 
            (r, g, b) = (196, 221, 227)
        case 763: 
            (r, g, b) = (51, 65, 58)
        case 764: 
            (r, g, b) = (214, 201, 218)
        case 765: 
            (r, g, b) = (118, 128, 93)
        case 766: 
            (r, g, b) = (219, 232, 219)
        case 767: 
            (r, g, b) = (37, 30, 15)
        case 768: 
            (r, g, b) = (58, 58, 69)
        case 769: 
            (r, g, b) = (185, 202, 208)
        case 770: 
            (r, g, b) = (161, 184, 184)
        case 771: 
            (r, g, b) = (160, 160, 138)
        case 772: 
            (r, g, b) = (29, 44, 30)
        case 773: 
            (r, g, b) = (101, 74, 45)
        case 774: 
            (r, g, b) = (224, 228, 218)
        case 775: 
            (r, g, b) = (230, 221, 224)
        case 776: 
            (r, g, b) = (74, 74, 102)
        case 777: 
            (r, g, b) = (101, 104, 110)
        case 778: 
            (r, g, b) = (89, 73, 108)
        case 779: 
            (r, g, b) = (255, 255, 255)
        case 780: 
            (r, g, b) = (169, 150, 79)
        case 781: 
            (r, g, b) = (247, 249, 248)
        case 782: 
            (r, g, b) = (107, 108, 164)
        case 783: 
            (r, g, b) = (227, 238, 239)
        case 784: 
            (r, g, b) = (234, 234, 234)
        case 785: 
            (r, g, b) = (188, 195, 190)
        case 786: 
            (r, g, b) = (33, 16, 12)
        case 787: 
            (r, g, b) = (249, 249, 248)
        case 788: 
            (r, g, b) = (188, 183, 201)
        case 789: 
            (r, g, b) = (210, 199, 205)
        case 790: 
            (r, g, b) = (49, 42, 32)
        case 791: 
            (r, g, b) = (197, 216, 218)
        case 792: 
            (r, g, b) = (200, 235, 235)
        case 793: 
            (r, g, b) = (46, 44, 38)
        case 794: 
            (r, g, b) = (47, 40, 49)
        case 795: 
            (r, g, b) = (210, 224, 210)
        case 796: 
            (r, g, b) = (141, 69, 56)
        case 797: 
            (r, g, b) = (142, 158, 153)
        case 798: 
            (r, g, b) = (201, 147, 147)
        case 799: 
            (r, g, b) = (213, 215, 224)
        case 800: 
            (r, g, b) = (246, 250, 252)
        case 801: 
            (r, g, b) = (100, 116, 77)
        case 802: 
            (r, g, b) = (149, 175, 184)
        case 803: 
            (r, g, b) = (84, 87, 30)
        case 804: 
            (r, g, b) = (139, 163, 161)
        case 805: 
            (r, g, b) = (194, 185, 174)
        case 806: 
            (r, g, b) = (149, 175, 176)
        case 807: 
            (r, g, b) = (199, 218, 214)
        case 808: 
            (r, g, b) = (65, 70, 64)
        case 809: 
            (r, g, b) = (19, 22, 5)
        case 810: 
            (r, g, b) = (89, 85, 84)
        case 811: 
            (r, g, b) = (144, 114, 81)
        case 812: 
            (r, g, b) = (74, 75, 66)
        case 813: 
            (r, g, b) = (175, 156, 123)
        case 814: 
            (r, g, b) = (38, 19, 16)
        case 815: 
            (r, g, b) = (165, 148, 149)
        case 816: 
            (r, g, b) = (69, 57, 76)
        case 817: 
            (r, g, b) = (214, 202, 206)
        case 818: 
            (r, g, b) = (14, 18, 19)
        case 819: 
            (r, g, b) = (253, 253, 254)
        case 820: 
            (r, g, b) = (58, 46, 38)
        case 821: 
            (r, g, b) = (249, 250, 251)
        case 822: 
            (r, g, b) = (23, 45, 51)
        case 823: 
            (r, g, b) = (195, 193, 235)
        case 824: 
            (r, g, b) = (236, 238, 228)
        case 825: 
            (r, g, b) = (93, 98, 77)
        case 826: 
            (r, g, b) = (133, 102, 113)
        case 827: 
            (r, g, b) = (183, 173, 219)
        case 828: 
            (r, g, b) = (122, 137, 119)
        case 829: 
            (r, g, b) = (138, 174, 178)
        case 830: 
            (r, g, b) = (112, 142, 138)
        case 831: 
            (r, g, b) = (175, 167, 119)
        case 832: 
            (r, g, b) = (190, 209, 222)
        case 833: 
            (r, g, b) = (163, 179, 204)
        case 834: 
            (r, g, b) = (207, 213, 204)
        case 835: 
            (r, g, b) = (197, 192, 201)
        case 836: 
            (r, g, b) = (97, 59, 81)
        case 837: 
            (r, g, b) = (119, 148, 76)
        case 838: 
            (r, g, b) = (245, 251, 251)
        case 839: 
            (r, g, b) = (72, 72, 44)
        case 840: 
            (r, g, b) = (126, 86, 111)
        case 841: 
            (r, g, b) = (58, 56, 69)
        case 842: 
            (r, g, b) = (27, 26, 21)
        case 843: 
            (r, g, b) = (53, 16, 14)
        case 844: 
            (r, g, b) = (250, 250, 248)
        case 845: 
            (r, g, b) = (216, 237, 244)
        case 846: 
            (r, g, b) = (137, 135, 137)
        case 847: 
            (r, g, b) = (175, 202, 213)
        case 848: 
            (r, g, b) = (114, 114, 105)
        case 849: 
            (r, g, b) = (129, 125, 207)
        case 850: 
            (r, g, b) = (204, 187, 179)
        case 851: 
            (r, g, b) = (221, 222, 224)
        case 852: 
            (r, g, b) = (165, 177, 217)
        case 853: 
            (r, g, b) = (36, 35, 23)
        case 854: 
            (r, g, b) = (72, 62, 110)
        case 855: 
            (r, g, b) = (210, 205, 208)
        case 856: 
            (r, g, b) = (216, 215, 222)
        case 857: 
            (r, g, b) = (202, 222, 230)
        case 858: 
            (r, g, b) = (221, 223, 220)
        case 859: 
            (r, g, b) = (135, 142, 173)
        case 860: 
            (r, g, b) = (228, 228, 232)
        case 861: 
            (r, g, b) = (153, 135, 130)
        case 862: 
            (r, g, b) = (140, 182, 154)
        case 863: 
            (r, g, b) = (162, 145, 115)
        case 864: 
            (r, g, b) = (48, 60, 43)
        case 865: 
            (r, g, b) = (78, 71, 68)
        case 866: 
            (r, g, b) = (236, 244, 246)
        case 867: 
            (r, g, b) = (132, 153, 173)
        case 868: 
            (r, g, b) = (164, 158, 147)
        case 869: 
            (r, g, b) = (148, 165, 159)
        case 870: 
            (r, g, b) = (147, 118, 88)
        case 871: 
            (r, g, b) = (233, 232, 230)
        case 872: 
            (r, g, b) = (122, 138, 92)
        case 873: 
            (r, g, b) = (242, 237, 243)
        case 874: 
            (r, g, b) = (96, 99, 110)
        case 875: 
            (r, g, b) = (136, 155, 165)
        case 876: 
            (r, g, b) = (180, 149, 186)
        case 877: 
            (r, g, b) = (21, 14, 15)
        case 878: 
            (r, g, b) = (120, 97, 114)
        case 879: 
            (r, g, b) = (115, 106, 163)
        case 880: 
            (r, g, b) = (165, 182, 203)
        case 881: 
            (r, g, b) = (144, 144, 90)
        case 882: 
            (r, g, b) = (193, 169, 201)
        case 883: 
            (r, g, b) = (49, 59, 58)
        case 884: 
            (r, g, b) = (114, 134, 139)
        case 885: 
            (r, g, b) = (207, 155, 125)
        case 886: 
            (r, g, b) = (116, 123, 112)
        case 887: 
            (r, g, b) = (229, 221, 219)
        case 888: 
            (r, g, b) = (109, 131, 183)
        case 889: 
            (r, g, b) = (236, 236, 234)
        case 890: 
            (r, g, b) = (158, 167, 177)
        case 891: 
            (r, g, b) = (181, 222, 236)
        case 892: 
            (r, g, b) = (181, 195, 216)
        case 893: 
            (r, g, b) = (66, 9, 11)
        case 894: 
            (r, g, b) = (219, 210, 210)
        case 895: 
            (r, g, b) = (188, 165, 208)
        case 896: 
            (r, g, b) = (135, 171, 187)
        case 897: 
            (r, g, b) = (152, 152, 180)
        case 898: 
            (r, g, b) = (83, 124, 129)
        case 899: 
            (r, g, b) = (122, 176, 163)
        case 900: 
            (r, g, b) = (21, 28, 37)
        case 901: 
            (r, g, b) = (168, 183, 188)
        case 902: 
            (r, g, b) = (254, 254, 254)
        case 903: 
            (r, g, b) = (131, 154, 149)
        case 904: 
            (r, g, b) = (144, 143, 176)
        case 905: 
            (r, g, b) = (176, 185, 203)
        case 906: 
            (r, g, b) = (211, 235, 231)
        case 907: 
            (r, g, b) = (218, 220, 204)
        case 908: 
            (r, g, b) = (215, 228, 227)
        case 909: 
            (r, g, b) = (178, 185, 142)
        case 910: 
            (r, g, b) = (39, 41, 35)
        case 911: 
            (r, g, b) = (60, 69, 93)
        case 912: 
            (r, g, b) = (114, 79, 70)
        case 913: 
            (r, g, b) = (133, 134, 121)
        case 914: 
            (r, g, b) = (249, 248, 247)
        case 915: 
            (r, g, b) = (187, 182, 211)
        case 916: 
            (r, g, b) = (42, 70, 68)
        case 917: 
            (r, g, b) = (134, 155, 178)
        case 918: 
            (r, g, b) = (32, 40, 58)
        case 919: 
            (r, g, b) = (54, 43, 27)
        case 920: 
            (r, g, b) = (135, 169, 112)
        case 921: 
            (r, g, b) = (37, 52, 55)
        case 922: 
            (r, g, b) = (98, 36, 37)
        case 923: 
            (r, g, b) = (153, 140, 182)
        case 924: 
            (r, g, b) = (162, 140, 155)
        case 925: 
            (r, g, b) = (229, 212, 214)
        case 926: 
            (r, g, b) = (195, 180, 198)
        case 927: 
            (r, g, b) = (100, 51, 26)
        case 928: 
            (r, g, b) = (190, 207, 175)
        case 929: 
            (r, g, b) = (141, 110, 92)
        case 930: 
            (r, g, b) = (252, 253, 253)
        case 931: 
            (r, g, b) = (190, 197, 186)
        case 932: 
            (r, g, b) = (201, 212, 204)
        case 933: 
            (r, g, b) = (173, 161, 189)
        case 934: 
            (r, g, b) = (146, 118, 157)
        case 935: 
            (r, g, b) = (200, 211, 220)
        case 936: 
            (r, g, b) = (107, 129, 182)
        case 937: 
            (r, g, b) = (198, 189, 187)
        case 938: 
            (r, g, b) = (139, 178, 192)
        case 939: 
            (r, g, b) = (140, 107, 125)
        case 940: 
            (r, g, b) = (197, 176, 199)
        case 941: 
            (r, g, b) = (239, 247, 244)
        case 942: 
            (r, g, b) = (197, 193, 189)
        case 943: 
            (r, g, b) = (84, 78, 55)
        case 944: 
            (r, g, b) = (226, 218, 200)
        case 945: 
            (r, g, b) = (225, 221, 225)
        case 946: 
            (r, g, b) = (226, 221, 212)
        case 947: 
            (r, g, b) = (107, 81, 70)
        case 948: 
            (r, g, b) = (123, 218, 223)
        case 949: 
            (r, g, b) = (142, 163, 168)
        case 950: 
            (r, g, b) = (225, 233, 229)
        case 951: 
            (r, g, b) = (119, 119, 192)
        case 952: 
            (r, g, b) = (177, 146, 98)
        case 953: 
            (r, g, b) = (24, 23, 30)
        case 954: 
            (r, g, b) = (35, 43, 50)
        case 955: 
            (r, g, b) = (173, 189, 159)
        case 956: 
            (r, g, b) = (137, 124, 133)
        case 957: 
            (r, g, b) = (169, 178, 140)
        case 958: 
            (r, g, b) = (64, 77, 100)
        case 959: 
            (r, g, b) = (74, 62, 65)
        case 960: 
            (r, g, b) = (35, 30, 23)
        case 961: 
            (r, g, b) = (157, 180, 197)
        case 962: 
            (r, g, b) = (184, 230, 240)
        case 963: 
            (r, g, b) = (30, 37, 39)
        case 964: 
            (r, g, b) = (101, 95, 52)
        case 965: 
            (r, g, b) = (145, 168, 146)
        case 966: 
            (r, g, b) = (101, 147, 157)
        case 967: 
            (r, g, b) = (21, 17, 17)
        case 968: 
            (r, g, b) = (124, 138, 109)
        case 969: 
            (r, g, b) = (21, 20, 27)
        case 970: 
            (r, g, b) = (130, 131, 117)
        case 971: 
            (r, g, b) = (130, 101, 104)
        case 972: 
            (r, g, b) = (52, 60, 49)
        case 973: 
            (r, g, b) = (158, 140, 162)
        case 974: 
            (r, g, b) = (64, 59, 70)
        case 975: 
            (r, g, b) = (240, 243, 237)
        case 976: 
            (r, g, b) = (92, 92, 87)
        case 977: 
            (r, g, b) = (28, 32, 42)
        case 978: 
            (r, g, b) = (207, 206, 205)
        case 979: 
            (r, g, b) = (73, 79, 100)
        case 980: 
            (r, g, b) = (165, 179, 186)
        case 981: 
            (r, g, b) = (125, 191, 174)
        case 982: 
            (r, g, b) = (246, 246, 242)
        case 983: 
            (r, g, b) = (106, 114, 134)
        case 984: 
            (r, g, b) = (151, 144, 174)
        case 985: 
            (r, g, b) = (180, 190, 201)
        case 986: 
            (r, g, b) = (84, 100, 140)
        case 987: 
            (r, g, b) = (142, 180, 159)
        case 988: 
            (r, g, b) = (247, 245, 242)
        case 989: 
            (r, g, b) = (239, 237, 236)
        case 990: 
            (r, g, b) = (33, 47, 34)
        case 991: 
            (r, g, b) = (175, 168, 153)
        case 992: 
            (r, g, b) = (175, 178, 155)
        case 993: 
            (r, g, b) = (100, 121, 134)
        case 994: 
            (r, g, b) = (227, 236, 236)
        case 995: 
            (r, g, b) = (56, 65, 51)
        case 996: 
            (r, g, b) = (67, 93, 67)
        case 997: 
            (r, g, b) = (193, 167, 154)
        case 998: 
            (r, g, b) = (119, 97, 93)
        case 999: 
            (r, g, b) = (83, 108, 40)
        default:
            print("do nothing...")
        }
        return UIColor(red: r, green: g, blue: b)
    }
    
    func correspondingColor(for cardID: Int)->UIColor {
        print("func correspondingColor is running...")
        let cardIDDigitsAsArray = String(cardID).characters.map { Int(String($0))! } // [1,2,3]
        let firstDigit = cardIDDigitsAsArray[0]
        switch firstDigit {
        case 0:
            return CardBackground.softOrange
        case 1:
            return CardBackground.lightBlue
        case 2:
            return CardBackground.fuzzyGreen
        case 3:
            return CardBackground.cream
        case 4:
            return CardBackground.invitingBrown
        case 5:
            return CardBackground.warmRed
        case 6:
            return CardBackground.peach
        case 7:
            return CardBackground.easyPurple
        case 8:
            return CardBackground.fadedTeal
        case 9:
            return CardBackground.bearBrown
        default:
            return CardBackground.bearBrown
        }
    }
}

extension UIColor {
    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let color = coreImageColor
        return (color.red, color.green, color.blue, color.alpha)
    }
}
