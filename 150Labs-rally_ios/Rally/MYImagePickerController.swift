//
//  MYImagePickerController.swift
//  Rally
//
//  Created by Ian Moses on 5/30/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYImagePickerController: UIImagePickerController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}
