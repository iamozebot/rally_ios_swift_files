//
//  MYMessageBlurb.swift
//  Rally
//
//  Created by Ian Moses on 3/15/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

struct MYMessageBlurb {
    var messageData: CommThreadMessage!
    var font: UIFont! // font for name and text labels
    var precedingMessageBlurbYOffset: CGFloat = 0.0 // providing default value to avoid optionals; never used
    lazy var profilePictureView: PersonImageView = {
        let profilePictureView = PersonImageView(person: self.messageData.member)
        let sideLength: CGFloat = ScreenDimension.width*11.35/100
        let xOffset: CGFloat = ScreenDimension.width*2.4/100
        let yOffset: CGFloat = 0.0
        profilePictureView.frame = CGRect(x: xOffset, y: yOffset, width: sideLength, height: sideLength)
        profilePictureView.image = self.messageData.member.getProfileImage()
        profilePictureView.layer.cornerRadius = profilePictureView.frame.height/2
        profilePictureView.clipsToBounds = true
        profilePictureView.isUserInteractionEnabled = true
        print("profilePictureView return")
        return profilePictureView
    }()
    lazy var nameLabel: UILabel = {
        let nameLabel = UILabel(frame: CGRect(x: 0, y: ScreenDimension.height*0.19/100, width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        nameLabel.numberOfLines = 1
        nameLabel.lineBreakMode = .byTruncatingTail
        if let firstName = self.messageData.member.firstName {
            nameLabel.text = firstName
        }
        var fontDownSize: CGFloat = 0
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            fontDownSize = 1
        default:
            print("Do nothing...")
        }
        nameLabel.font = UIFont.systemFont(ofSize: nameLabel.font.pointSize-1.25-fontDownSize, weight: UIFontWeightMedium )
        nameLabel.textColor = MYColor.communityMessagingMessageBlurbName
        nameLabel.sizeToFit()
        print("nameLabel return")
        return nameLabel
    }()
    lazy var textLabel: UILabel = {
        let width = self.view.frame.width*70/100
        let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        textLabel.frame.origin.x = ScreenDimension.width*3.6/100
        textLabel.frame.origin.y = self.nameLabel.frame.height+ScreenDimension.height*0.1/100
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        textLabel.textColor = MYColor.lightBlack
        
        var fontDownSize: CGFloat = 0
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            fontDownSize = 1
        default:
            print("Do nothing...")
        }
        textLabel.font = UIFont.systemFont(ofSize: self.font.pointSize+0.75-fontDownSize)
        textLabel.text = self.messageData.text
        textLabel.sizeToFit()
        textLabel.frame.size.width = width
        print("textLabel return")
        return textLabel
    }()
    lazy var likeButton: UIButton = {
        let width = self.view.frame.width*6/100
        let likeButton = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        likeButton.frame.origin.x = self.view.frame.width*95/100
        likeButton.setImage(UIImage(named:"heart")!.withRenderingMode(.alwaysTemplate), for: .normal)
        // generate label to display number of likes
        let likeButtonLabel = UILabel()
        likeButton.center.x = width/2
        // If message has any likes, display number and color heart red. Otherwise, do not display 0 likes
        if self.messageData.numberOfLikes != 0 {
            likeButtonLabel.text = String(self.messageData.numberOfLikes)
            likeButton.imageView!.tintColor = .red
        }
        else {
            likeButtonLabel.text = nil
            likeButton.imageView!.tintColor = nil
        }
        likeButtonLabel.textAlignment = .center
        likeButtonLabel.font = self.font
        likeButtonLabel.textColor = MYColor.lightBlack
        likeButtonLabel.sizeToFit()
        likeButton.addSubview(likeButtonLabel)
        
        // FIX THIS ONCE DATA EXISTS
        likeButton.frame.origin.y = self.view.frame.height/2
        print("likeButton return")
        return likeButton
    }()
    var view: UIView!
    
    init(_ message: CommThreadMessage, font: UIFont)/*, precedingMessageBlurbYOffset: CGFloat?, superviewContentHeight: CGFloat)*/ {
        self.messageData = message
        self.font = font
        /*if let yOffset = precedingMessageBlurbYOffset {
            print("yOffset:\(yOffset)")
            self.precedingMessageBlurbYOffset = yOffset
        }*/
        /*else {
            self.precedingMessageBlurbYOffset = superviewContentHeight
        }*/
        let xOffset: CGFloat = ScreenDimension.width*3/100
        let yOffset: CGFloat = 0 // just to initialize frame, real value set after height is calculated
        let width = ScreenDimension.width*93/100
        self.view = UIView(frame: CGRect(x: xOffset, y: yOffset, width: width, height: 0.0))
        
        self.assembleMessageBlurbUISubviews()
        // finalize addt'l view frame values
        self.updateSubviewFrames()
    }
    
    mutating func assembleMessageBlurbUISubviews() {
        print("func assembleMessageBlurbUISubviews is running...")
        self.view.addSubview(self.profilePictureView)
        self.view.addSubview(self.nameLabel)
        self.view.addSubview(self.textLabel)
        //self.view.addSubview(self.likeButton)
    }
    
    mutating func updateSubviewFrames() {
        print("func updateSubviewXOffsets is running...")
        let xOrigin = profilePictureView.frame.origin.x+profilePictureView.frame.width+ScreenDimension.width*3.34/100
        // NOTE: All other origin components are non-relational values. as such, are defined on view initialization
        // set name and text label x-origin
        self.nameLabel.frame.origin.x = xOrigin
        self.textLabel.frame.origin.x = xOrigin
        // set text label y-origin
        self.textLabel.frame.origin.y = self.nameLabel.frame.origin.y+self.nameLabel.frame.height+ScreenDimension.height*0.65/100
        // update superview size and y-origin
        var topEdgeValue: CGFloat = 100.0 // some value greater than zero so first subview y-origin overrides
        var bottomEdgeValue: CGFloat = 0.0
        for subview in self.view.subviews {
            print("self.view.subview heights:\(subview.frame)")
            if subview.frame.origin.y < topEdgeValue {
                topEdgeValue = subview.frame.origin.y
            }
            if (subview.frame.origin.y+subview.frame.height) > bottomEdgeValue {
                bottomEdgeValue = subview.frame.origin.y+subview.frame.height
            }
        }
        self.view.frame.size = CGSize(width: ScreenDimension.width*93/100, height: bottomEdgeValue)
        //self.view.sizeToFit() // doesn't work for some reason. maybe bc sizeToFit() is not a mutating func and is trying to change (mutate) a struct (property of self)
        self.view.frame.size.height += ScreenDimension.height*2.15/100 // add padding for inter-message spacing
        self.view.frame.origin.y = precedingMessageBlurbYOffset-ScreenDimension.height*1.5/100-self.view.frame.height
    }
}
