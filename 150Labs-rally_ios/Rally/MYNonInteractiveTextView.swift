//
//  MYNonInteractiveTextView.swift
//  Rally
//
//  Created by Ian Moses on 1/5/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYNonInteractiveTextView: UITextView {
    override var isUserInteractionEnabled: Bool {
        get {
            return false
        }
        set {
            super.isUserInteractionEnabled = newValue
        }
    }
    override var isScrollEnabled: Bool {
        get {
            return false
        }
        set {
            super.isScrollEnabled = newValue
        }
    }
    override var isEditable: Bool {
        get {
            return false
        }
        set {
            super.isEditable = newValue
        }
    }
}
