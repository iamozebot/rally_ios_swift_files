//
//  MYToggleButton.swift
//  Rally
//
//  Created by Ian Moses on 3/9/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYToggleButton<enumType>: UIButton {
    var toggleState: enumType!
    
    init(toggleState: enumType) {
        //self.init()
        // Designed initialiser
        // http://stackoverflow.com/questions/32343198/cannot-subclass-uibutton-must-call-a-designated-initializer-of-the-superclass
        super.init(frame: CGRect.zero)
        self.toggleState = toggleState
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
