//
//  MYUserClass.swift
//  Rally
//
//  Created by Ian Moses on 10/1/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYUser { // AppInstanceUser
    // for app to move forward past splash screen, require webcall response giving user ID. this value is immediately assigned to
    static var firstName: String?
    static var lastName: String?
    static var fullName: String?
    static var ID: Int = 0
    static var stockImageID: Int!
    static var phoneNumberList: [String: String?] = ["Mobile":nil]
    static var operatingSystemVersion: Int?
    static var largeProfileImage: UIImage?
    static var thumbnailProfileImage: UIImage?
    static var GCMToken: String?
    static var latitude: Double = 0.0 // CLLocationCoordinate2D latitude property is of type CLLocationDegress (typealias for Double)
    static var longitude: Double = 0.0 // CLLocationCoordinate2D longitude property is of type CLLocationDegress (typealias for Double)
    // static var UnopenedInvites: Int?
    // NOTE: These are addt'l properties backend is currently returning for createUser, current front-end architecture does not require these. May change in future, as such keeping for reference.
    /*
     static var DeviceType
     static var DeviceModel
     static var DeviceDisplayMetrics
     static var OSVersion
     static var AppVersion
     static var PublicKey
     static var PrivateKey
     static var IsVerified // has this phone number been verified as a user
     static var IsBanned
     static var BannedDateTimeStamp
     static var CreatedDateTimeStamp
     */
    
    class func getProfileImage()-> UIImage { // default profile picture is thumbnail for method
        if let value = MYUser.thumbnailProfileImage {
            //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            return value
        }
        else {
            return stockImageGlobalDictionary[stockImageID]!
        }
    }
    
    class func friendType()->Friend? {
        let userAsFriend = Friend()
        userAsFriend.firstName = self.firstName
        userAsFriend.lastName = self.lastName
        userAsFriend.ID = self.ID
        userAsFriend.stockImageID = self.stockImageID
        userAsFriend.phoneNumberList = self.phoneNumberList
        communityMemberThumbnailsGlobalDictionary[self.ID] = self.thumbnailProfileImage
        return userAsFriend
    }
    
    class func defaultCommunityForFocus()->Community? {
        let defaultCommunity: Community? = nil
        if publicCommunitiesGlobalArray.count != 0 {
            return publicCommunitiesGlobalArray.first
        }
        else if privateCommunitiesGlobalArray.count != 0 {
            return privateCommunitiesGlobalArray.first
        }
        return defaultCommunity
    }
}
