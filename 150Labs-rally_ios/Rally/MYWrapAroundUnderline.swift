//
//  MYWrapAroundUnderline.swift
//  Rally
//
//  Created by Ian Moses on 1/12/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MYWrapAroundUnderline: UIView {
    var quadrant: CGFloat!
    var scrollDirection: UnderlineScrollDirection = .none
    
    init(quadrant: CGFloat) {
        self.quadrant = quadrant
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func animate(toPosition: CGFloat, animated: Bool = true, toPositionOnCompletion: CGFloat? = nil) {
        print("func animateCommunityCardContainer is running...")
        if animated {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions(),animations: {self.center.x = toPosition}, completion: { finished in
                if toPositionOnCompletion != nil {self.center.x = toPositionOnCompletion!}
            })
        }
        else {
            self.center.x = toPosition
        }
    }
}
