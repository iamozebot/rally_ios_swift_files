//
//  MessageClasses.swift
//  Rally
//
//  Created by Ian Moses on 9/8/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class RallyMessage {
    var rallyID: Int!
    var userID: Int!
    var userName: String!
    var rallyMessageText: String!
    var createdDateTimeStamp: String!
    //var profilePicture: UIImage?
    var member: Friend!
    var numberOfLikes: Int!
}

// Message(type: Rally)
// Message.Rally()
// RallyMessage()
