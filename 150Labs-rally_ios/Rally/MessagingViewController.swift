//
//  MessagingViewController.swift
//  Rally
//
//  Created by Ian Moses on 8/29/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import UIKit
import SwiftyJSON

class MessagingViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate {
    let scrollView = UIScrollView()
    var rallyObject: Rally!
    var textInputBar: messagingTextView!
    var keyboardIsShown = false
    
    override func viewDidLoad() {
        print("class MessagingViewController func ViewDidLoad is running...")
        super.viewDidLoad()
        WebCallStatus.messagingViewControllerLoaded = 1
        view.backgroundColor = MYColor.lightGrey
        // fetch rally messages if they haven't been fetched yet this session
        let intent = WebServiceCallIntent.getRallyMessages(rallyObject)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getRallyMessages(updatedRallyObject) = returnIntent {
                        self.rallyObject = updatedRallyObject
                        DispatchQueue.main.async(execute: { () -> Void in
                            // load any messages
                            self.defineScrollView() // initialize scrollView
                            _ = self.defineTextInputBar()
                            self.textInputBar.delegate = self
                            self.defineRallyDetailsContainer()
                            self.defineToolbar() // initialize toolbar
                            self.defineMessageBlurbs()
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        startObservingKeyboardEvents()
        // subscribe to gcm remote notification broadcast to be notified of new messages
        NotificationCenter.default.addObserver(self, selector: #selector(MessagingViewController.addNewMessageToMessagingThread(_:)), name: NSNotification.Name(rawValue: "RallyMessage"), object: nil)
    }
    
    func addNewMessageToMessagingThread(_ notification: Notification){
        print("func addNewMessageToMessagingThread is running...")
        // format message from gcm notification as a string to add the message in a new message blurb
        guard let userInfo = (notification as NSNotification).userInfo else {
            print("func updateRallyDetails: guard let userInfo = notification.userInfo else {")
            return
        }
        guard let payload = userInfo["payload"] as? NSString  else {
            return
        }
        let optionalDataPackage = payload.data(using: String.Encoding.utf8.rawValue)
        guard let unwrappedDataPackage = optionalDataPackage else {
            return
        }
        let swiftyJSONDataPackage = JSON(data:unwrappedDataPackage)
        let rallyID = swiftyJSONDataPackage["RallyMessageID"].int
        guard let unwrappedRallyID = rallyID else {
            return
        }
        let intent = WebServiceCallIntent.getSingleRallyMessage(unwrappedRallyID)
        let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getSingleRallyMessage(rallyMessage) = returnIntent {
                        
                        self.rallyObject.rallyMessages!.append(rallyMessage)
                        // update view with new message
                        DispatchQueue.main.async(execute: { () -> Void in
                            let font = UIFont(name: "Helvetica", size: 17)
                            // generate new message blurb and add to scrollView
                            let (newMessageBlurb, associatedMemberImage) = self.defineMessageBlurb(rallyMessage, font: font!, topMessageBlurbYOffset: self.scrollView.contentSize.height)
                            // increase scrollView contentsize height to accomodate new message blurb plus inter-message blurb spacing
                            // shift scrollView contentOffset up by height of new message blurb plus inter-message blurb spacing
                            let newMessageAndPaddingHeight = ScreenDimension.height*0.8/100+newMessageBlurb.frame.height
                            // adjust y-origin
                            newMessageBlurb.frame.origin.y += newMessageAndPaddingHeight
                            associatedMemberImage.frame.origin.y += newMessageAndPaddingHeight
                            self.scrollView.contentSize.height += newMessageAndPaddingHeight
                            if self.scrollView.contentSize.height > self.scrollView.frame.height {
                                self.scrollView.contentOffset.y += newMessageAndPaddingHeight
                            }
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func appWillEnterForeground(_ notification:Notification) {
        print("func MessagingViewController:appWillEnterForeground is running...")
        // this assures user doesn't exit app mid-instance and turn off a permission, then re-enter and attempt to use permission when disabled and crash app
        // checks if any permissions are disabled, if so displays permissions access view handler
        _ = permissionsHelperClass.permissionsHelperClassSingleton.permissionsAccessHandler(.location,navigationController: navigationController!) // only reason for passing .location vs other cases is to enter first case in switch statement to allow fallthrough to execute properly
        // check to determine if user should get kicked back to homeVC (rally is expired)
        // fetch updated rally info from webservice
        guard let rallyID = rallyObject.rallyID else {return}
        let getUpdatedRallyPackageintent = WebServiceCallIntent.getUpdatedRallyPackage(rallyObject, rallyID)
        let webServiceCallManagerGetUpdatedRallyPackage = WebServiceCallManager(intent: getUpdatedRallyPackageintent)
        webServiceCallManagerGetUpdatedRallyPackage.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally) = returnIntent {
                        DispatchQueue.main.async(execute: { () -> Void in
                            // check if rallyisActive and if rallystartDateTime has passed
                            self.rallyObject = updatedRally
                            if self.rallyObject.isRallyActive == 1 {
                                print("do nothing, rally has not expired")
                            }
                            else {
                                if let rallyStartDateTime = self.rallyObject.rallyStartDateTime {
                                    let order = dateHelperClass().timeFromNowUntilDate(rallyStartDateTime)
                                    // switch cases for whether or not current time is past start time
                                    switch order {
                                    case .orderedDescending:
                                        // rally started and is no longer active (it has expired), so kick user back to homeVC
                                        self.navigationController!.popToRootViewController(animated: true)
                                    case .orderedAscending:
                                        // rally has not started yet
                                        print("do nothing")
                                    case .orderedSame:
                                        // rally is starting at this very second
                                        print("do nothing")
                                    }
                                }
                            }
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
        
        // Update rally details (location, members joined, title, date, time to display in container)
        if let detailsContainer = view.viewWithTag(564) {
            detailsContainer.removeFromSuperview()
        }
        defineRallyDetailsContainer()
        // fetch updated messages
        let getRallyMessagesIntent = WebServiceCallIntent.getRallyMessages(rallyObject)
        let webServiceCallManagerGetRallyMessages = WebServiceCallManager(intent: getRallyMessagesIntent)
        webServiceCallManagerGetRallyMessages.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                if let returnIntent = returnParameters {
                    if case let WebServiceCallReturnParameters.getRallyMessages(updatedRally) = returnIntent {
                        self.rallyObject = updatedRally
                        DispatchQueue.main.async(execute: { () -> Void in
                            // remove old message blurbs
                            for subview in self.scrollView.subviews {
                                subview.removeFromSuperview()
                            }
                            // load any messages
                            self.defineMessageBlurbs()
                        })
                    }
                }
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class MessagingViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(true, animated: false)
        let leftButtonSelector: Selector = #selector(MessagingViewController.backToHomeViewController(_:))
        var title: String = "Messaging"
        if rallyObject.rallyName != "Untitled Rally" && rallyObject.rallyName != "Untitled Community" {
            title = rallyObject.rallyName
        }
        navBarClass.navBar(self, navigationBarItemReference: navigationItem, leftButtonImageReference: "back", rightButtonImageReference: nil, rightMostButtonImageReference: nil, leftButtonActionReference: leftButtonSelector, rightButtonActionReference: nil, rightMostButtonActionReference: nil, titleReference: title)
        NotificationCenter.default.addObserver(self, selector: #selector(MessagingViewController.appWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("func class MessagingViewController, viewWillDisappear is running...")
        super.viewWillDisappear(animated)
        // badges managed in viewWillDisappear to avoid badge relevant to screen user is on being added. in this case, need to be able to offset badge since user sees new information update live on screen
        // subtract badges associated with new Rallys
        // UIApplication.sharedApplication().applicationIconBadgeNumber -=
        // since this is the last view controller in the stack, removing gesture recognizers is not required since they are supposed to be valid the entire time while on view controller and otherwise will be reloaded every time screen is put on stack
        if let detailsContainer = view.viewWithTag(564) {
            if let viewGestureRecognizers = detailsContainer.gestureRecognizers {
                for recognizer in viewGestureRecognizers {
                    detailsContainer.removeGestureRecognizer(recognizer)
                }
            }
        }
        stopObservingKeyboardEvents()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func handleLongPress(_ recognizer: UILongPressGestureRecognizer) {
        print("func handleLongPress is running...")
        navigationController!.popViewController(animated: true)
    }
    
    func handleTap(_ sender:UITapGestureRecognizer) { // button is removed from subview on click, so it cannot be clicked unless
        print("func handleTap is running...")
        navigationController!.popViewController(animated: true)
    }
    
    func backToHomeViewController(_ sender: UIButton!) {
        print("func backToHomeViewController is running...")
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    func startObservingKeyboardEvents() {
        print("func startObservingKeyboardEvents is running...")
        NotificationCenter.default.addObserver(self,
            selector: #selector(MessagingViewController.keyboardWillShow),
            name:NSNotification.Name.UIKeyboardWillShow,
            object:nil)
        NotificationCenter.default.addObserver(self,
            selector: #selector(MessagingViewController.keyboardWillHide),
            name:NSNotification.Name.UIKeyboardWillHide,
            object:nil)
        //textInputBar.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    func stopObservingKeyboardEvents() {
        print("func stopObservingKeyboardEvents is running...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // believe name is "RallyMessage" but not positive
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RallyMessage:"), object: nil)
        /*
         // not sure why but navigationController is nil at this point, I'm guessing it's releasing before this method runs or smt.In any case, textInputBar is not in memory since this view is completely removed from stack so no real need to remove observer, just best practice I'm trying to follow
        textInputBar!.removeObserver(self, forKeyPath: "contentSize")
         */
    }
    
    func defineRallyDetailsContainer() {
        print("func defineRallyDetailsContainer is running...")
        //let yOffset = scrollView.contentOffset
        let detailsContainer = UIView(frame: CGRect(x: 0,y: 0,width: ScreenDimension.width,height: ScreenDimension.height*9.16/100))
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(MessagingViewController.handleLongPress(_:)))
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MessagingViewController.handleTap(_:)))
        detailsContainer.addGestureRecognizer(tap)
        detailsContainer.addGestureRecognizer(lpgr)
        detailsContainer.backgroundColor = UIColor.white
        detailsContainer.tag = 564
        let dCH = detailsContainer.frame.height
        let centerHeightForFirstDetailsRow = dCH*33/100
        let detailsSpacing = ScreenDimension.width*1.75/100
        let rallyMembersTextViewSuperview = UIView(frame: CGRect(x: 0,y: dCH*50/100,width: ScreenDimension.width,height: dCH*50/100))
        rallyMembersTextViewSuperview.clipsToBounds = true
        detailsContainer.addSubview(rallyMembersTextViewSuperview)
        let rallyMembersLabel = UITextView()
        rallyMembersLabel.isUserInteractionEnabled = false
        rallyMembersLabel.textColor = MYColor.rallyBlue
        rallyMembersLabel.font = UIFont.systemFont(ofSize: 14.78)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            rallyMembersLabel.font = UIFont.systemFont(ofSize: 11.5)
        default:
            print("Do nothing...")
        }
        rallyMembersLabel.text = formatNames()
        rallyMembersLabel.contentInset.left = detailsSpacing
        rallyMembersLabel.sizeToFit()
        rallyMembersLabel.frame.origin.x = 0
        rallyMembersLabel.frame.size.width += rallyMembersLabel.contentInset.left
        rallyMembersLabel.center.y = dCH*25/100
        rallyMembersTextViewSuperview.addSubview(rallyMembersLabel)
        print("rallyObject.RallyUserList.count:\(rallyObject.rallyUserList.count)")
        let rallyDateTimeLocationDetailsTextView = UILabel(frame: CGRect(x: detailsSpacing,y: 0,width: ScreenDimension.width,height: CGFloat.greatestFiniteMagnitude))
        rallyDateTimeLocationDetailsTextView.numberOfLines = 1
        rallyDateTimeLocationDetailsTextView.lineBreakMode = .byTruncatingTail
        rallyDateTimeLocationDetailsTextView.isUserInteractionEnabled = false
        rallyDateTimeLocationDetailsTextView.textColor = MYColor.lightBlack
        rallyDateTimeLocationDetailsTextView.font = UIFont.systemFont(ofSize: 22.17)
        rallyDateTimeLocationDetailsTextView.text = dateHelperClass().formatNSDateForDetailsScreenDateButtonTitle(rallyObject.rallyStartDateTime)
        rallyDateTimeLocationDetailsTextView.text = rallyDateTimeLocationDetailsTextView.text!+" "+dateHelperClass().formatDetailsScreenTimeButtonTitle(rallyObject.rallyStartDateTime!)
        let rallyLocation = rallyObject.rallyLocation
            // http://stackoverflow.com/questions/31272561/working-with-unicode-code-points-in-swift
        rallyDateTimeLocationDetailsTextView.text = rallyDateTimeLocationDetailsTextView.text!+" "+"\u{00B7}"+" "+rallyLocation
        //rallyLocationLabel.frame.size.width += rallyLocationLabel.contentInset.left
        //rallyDateTimeLocationDetailsTextView.contentInset.left = detailsSpacing
        
        rallyDateTimeLocationDetailsTextView.sizeToFit()
        rallyDateTimeLocationDetailsTextView.frame.size.width = ScreenDimension.width
        
        //rallyDateTimeLocationDetailsTextView.frame.origin.x = 0
        //rallyDateTimeLocationDetailsTextView.frame.size.width += rallyDateTimeLocationDetailsTextView.contentInset.left
        //rallyTime.frame.size.width += rallyTime.contentInset.left
        
        rallyDateTimeLocationDetailsTextView.center.y = centerHeightForFirstDetailsRow
        detailsContainer.addSubview(rallyDateTimeLocationDetailsTextView)
        
         var fontSize: CGFloat = 22
         let minSize: CGFloat = 16 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
         while fontSize > minSize &&  rallyDateTimeLocationDetailsTextView.sizeThatFits(CGSize(width: CGFloat(Float.greatestFiniteMagnitude),height: rallyDateTimeLocationDetailsTextView.frame.size.height)).width >= rallyDateTimeLocationDetailsTextView.frame.size.width {
         fontSize -= 1.0
         rallyDateTimeLocationDetailsTextView.font = UIFont.systemFont(ofSize: fontSize)
         }
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            rallyDateTimeLocationDetailsTextView.font = UIFont.systemFont(ofSize: fontSize-1)
        default:
            print("Do nothing...")
        }
        defineRallyDetailsContainerShadow(detailsContainer)
        view.addSubview(detailsContainer)
        //let rallyTitle = UILabel(frame: CGRectMake(ScreenDimension.screenWidth/4 + ScreenDimension.screenWidth/60, ScreenDimension.screenHeight*29/200, 120, ScreenDimension.screenHeight/10))
        //rallyTitle.text = rallyObject.RallyName
        //detailsContainer.addSubview(rallyTitle)
    }
    
    func defineRallyDetailsContainerShadow(_ detailsContainer: UIView) {
        print("func defineRallyDetailsContainerShadow is running...")
        detailsContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        detailsContainer.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        detailsContainer.layer.shadowOpacity = 1.0
        detailsContainer.layer.shadowRadius = 1.5
    }
    
    func definePeopleInRally()->[Friend] {
        print("func definePeopleInRally is running...")
        var peopleInRally = [Friend]()
        for rallyMember in rallyObject.rallyUserList {
            if let didUserAccept = rallyMember.didUserAccept {
                if didUserAccept == 1 {
                    peopleInRally.append(rallyMember)
                }
            }
        }
        return peopleInRally
    }
    
    func formatNames()-> String {
        print("func formatNames is running...")
        let rallyMembersArray: [Friend] = definePeopleInRally()
        var formattedRallyMembersLabelText = ""
        for i in 0..<rallyMembersArray.count {
            // get rallyMemberName first name saved into variable apart from last name
            guard let rallyMemberFirstName = rallyMembersArray[i].firstName else {continue}
            let numberOfNamesLeftAfterCurrentName = rallyMembersArray.count - i - 1
            // numberOfNamesNotYetListed doesn't count name rallyMemberName looking to be listed, so if this is last one, equals 0
            let namesOnlyCharacterCount = formattedRallyMembersLabelText.characters.count + rallyMemberFirstName.characters.count + 2
            /* currentlyAppendedRallyGroupMemberNames accounts for commas already in but not spaces, so i accounts for spaces. 2 accounts for space and comma of last person listed. numberOfNamesNotYetListed only accounts for comma after last person, not the last peron's space and comma*/
            var groupNamesListRemainingCharacterCount = 4 // number is for comma, space, +, number
            if numberOfNamesLeftAfterCurrentName < 100 {
                groupNamesListRemainingCharacterCount = 5 // number is for comma, space, +, number
            }
            else {
                groupNamesListRemainingCharacterCount = 6 // number is for comma, space, +, number
            }
            var nextRallyMemberFirstName: String = ""
            if numberOfNamesLeftAfterCurrentName != 0 {
                if let value = rallyMembersArray[i+1].firstName {
                    nextRallyMemberFirstName = value
                }
            }
            if formattedRallyMembersLabelText.characters.count == 0 {
                formattedRallyMembersLabelText = "\(rallyMemberFirstName)"
            }
                /* this is supposed to be if last name in group fits and covering a case the else if below doesn't cover. However, I can't tell how the case below doesn't cover this. commenting out, if I continue getting problems, consider readding this case
                else if numberOfNamesLeftAfterCurrentName == 0 && namesOnlyCharacterCount<30 {
                view.stringDataAssociatedWithView += ", \(rallyMemberFirstName)"
                }
                */
            else if nextRallyMemberFirstName.characters.count+2<=60-namesOnlyCharacterCount-groupNamesListRemainingCharacterCount {
                formattedRallyMembersLabelText += ", \(rallyMemberFirstName)"
            }
            else if numberOfNamesLeftAfterCurrentName>0 {
                /* case: At last name, last name not users and total rally group list character count remains under 26 characters after adding user name*/
                formattedRallyMembersLabelText += ", +\(numberOfNamesLeftAfterCurrentName)"
                break
            }
        }
        return formattedRallyMembersLabelText
    }
   
// scrollView methods
    func defineScrollView() {
        print("func defineScrollView is running...")
        let detailsContainerHeight = ScreenDimension.height*9.16/100
        let scrollViewHeight: CGFloat = view.bounds.height-navigationController!.toolbar.frame.height-detailsContainerHeight // need to reduce height by detailsContainer height, not sure what the exact value will be yet
        scrollView.frame = CGRect(x: 0, y: detailsContainerHeight, width: ScreenDimension.width, height: scrollViewHeight)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.delegate = self
        view.addSubview(scrollView)
        scrollView.contentSize.height = calculateScrollViewContentHeight()
        if scrollView.contentSize.height > scrollView.frame.height {
            let contentYOffset = scrollView.contentSize.height-scrollView.frame.height
            scrollView.contentOffset = CGPoint(x: 0.0, y: contentYOffset)
        }
    }
    
    func calculateScrollViewContentHeight()-> CGFloat {
        print("func calculateScrollViewContentHeight is running...")
        let font = UIFont(name: "Helvetica", size: 17)
        var textPlusMessageBlurbPaddingHeight: CGFloat = 0
        var totalSpaceBetweenMessageBlurbs: CGFloat = ScreenDimension.height*0.8/100 // starts at non-zero value to account for top-most padding in scrollView
        if let _ = rallyObject.rallyMessages {
            for message in rallyObject.rallyMessages! {
                let messageBlurbWidth = calculateMessageBlurbWidth()
                textPlusMessageBlurbPaddingHeight += calculateMessageBlurbHeight(message.rallyMessageText, font: font!, width: messageBlurbWidth, userID: message.userID)
                let rallyMemberNameLabel: UILabel? = defineMessageBlurbName(message, font: font!, messageBlurbWidth: messageBlurbWidth)
                if let _ = rallyMemberNameLabel {
                    textPlusMessageBlurbPaddingHeight += rallyMemberNameLabel!.frame.origin.y+rallyMemberNameLabel!.frame.height
                }
                totalSpaceBetweenMessageBlurbs += ScreenDimension.height*0.8/100
            }
        }
        let scrollViewContentHeight = textPlusMessageBlurbPaddingHeight + totalSpaceBetweenMessageBlurbs
        return scrollViewContentHeight
    }
// scrollView methods
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("func scrollViewDidScroll is running...")
        /*
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            println("If clause using totalNumberOfAddressBookContacts is running...")
            numberOfContactsToLoad += 12
            self.view.reloadData()
            for index in selectedTableDataIndexPaths {
                //http://stackoverflow.com/questions/19506560/reload-a-table-views-data-without-clearing-its-selection-state
                self.collectionView.selectItemAtIndexPath(index as? NSIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.None)
            }
        }
        */
        if let textInputBarAndSendButtonSuperview = view.viewWithTag(343) {
            if textInputBarAndSendButtonSuperview.frame.origin.y != ScreenDimension.heightMinusStatusAndNavigationBar-navigationController!.toolbar.frame.height { // if true, keyboard is active, dismiss keyboard
                view.endEditing(true)
            }
        }
    }
    
// toolbar methods
    func defineToolbar() {
        print("func defineToolbar is running...")
        let textInputBarAndSendButtonSuperview = UIView()
        textInputBarAndSendButtonSuperview.tag = 343
        let textInputBarAndSendButtonSuperviewYOffset = ScreenDimension.heightMinusStatusAndNavigationBar-navigationController!.toolbar.frame.height
        textInputBarAndSendButtonSuperview.frame = CGRect(x: 0,y: textInputBarAndSendButtonSuperviewYOffset,width: ScreenDimension.width,height: navigationController!.toolbar.frame.height)
        textInputBarAndSendButtonSuperview.backgroundColor = MYColor.rallyBlue
        view.addSubview(textInputBarAndSendButtonSuperview)
        let sendButton = defineSendButton()
        view.bringSubview(toFront: textInputBarAndSendButtonSuperview)
        textInputBarAndSendButtonSuperview.addSubview(textInputBar)
        textInputBarAndSendButtonSuperview.addSubview(sendButton)
    }
    
    func defineTextInputBar()-> messagingTextView {
        print("func defineTextInputBar is running...")
        textInputBar = messagingTextView() // custom textView
        // initialize public var messagingTextViewSize
        textInputBar.frame = CGRect(x: ScreenDimension.width*2.8/100, y: 0, width: ScreenDimension.width*78.75/100, height: self.navigationController!.toolbar.frame.height*67.5/100)
        textInputBar.messagingTextViewHeight = textInputBar.frame.height
        textInputBar.messagingTextViewSingleLineHeight = textInputBar.frame.height
        textInputBar.center.y = navigationController!.toolbar.frame.height/2
        textInputBar.layer.cornerRadius = 5
        textInputBar.tag = 419
        textInputBar.font = UIFont.systemFont(ofSize: 17.75)
        textInputBar.textColor = MYColor.lightBlack
        textInputBar.textAlignment = .left
        textInputBar.isScrollEnabled = false
        let textInputBarTextContainerHeight = calculateInitialTextInputBarTextContainerHeight(textInputBar.font!, width: textInputBar.frame.width)
        textInputBar.messagingTextViewTextContainerSingleLineHeight = textInputBarTextContainerHeight
        _ = textInputBar.caretRect(for: textInputBar.beginningOfDocument)
        let textInputBarYOffset = (textInputBar.bounds.size.height-textInputBarTextContainerHeight)/2
        textInputBar.textContainerInset.top = textInputBarYOffset
        textInputBar.textContainer.lineFragmentPadding = ScreenDimension.width*0.015
        /*
        var fontSize: CGFloat = 20
        while textInputBar.sizeThatFits(CGSizeMake(textInputBar.frame.width, CGFloat(FLT_MAX))).height >= textInputBar.frame.height {
            fontSize -= 1.0
            textInputBar.font = UIFont.systemFontOfSize(fontSize)
        }
        */
        return textInputBar
    }
    
    func calculateInitialTextInputBarTextContainerHeight(_ font: UIFont, width: CGFloat)-> CGFloat {
        print("func calculateInitialTextInputBarTextContainerHeight is running...")
        let sampleText = "HelgjyFo."
        let attributedText = NSMutableAttributedString(string: sampleText)
        attributedText.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, attributedText.length))
        let boundingSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingRect = attributedText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return boundingRect.height
    }
    
    func defineSendButton()-> UIButton {
        print("func defineSendButton is running...")
        let sendButton = UIButton(type: .system)
        sendButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 17.25)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            sendButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 16)
        default:
            print("Do nothing...")
        }
        sendButton.setTitle("Send", for: UIControlState())
        sendButton.sizeToFit()
        sendButton.frame.origin.x = ScreenDimension.width*85/100
        sendButton.center.y = navigationController!.toolbar.frame.height/2
        sendButton.tag = 309
        sendButton.isUserInteractionEnabled = false
        sendButton.setTitleColor(MYColor.inactiveGrey, for: UIControlState())
        sendButton.addTarget(self, action: #selector(MessagingViewController.onSendButtonTouch(_:)), for: .touchUpInside)
        return sendButton
    }
    
    func onSendButtonTouch(_ sender: UIButton!) {
        print("func onSendButtonTouch is running...")
        let textInputBarAndSendButtonSuperview = view.viewWithTag(343)!
        let textInputBar = textInputBarAndSendButtonSuperview.viewWithTag(419) as! messagingTextView
        // trimmed string is to avoid counting spaces before/after anything user types so user cannot send bodies filled with spaces: "         "
        let trimmedString = textInputBar.text.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        if /*textInputBar.text.characters.count*/ trimmedString.characters.count > 0 {
            let intent = WebServiceCallIntent.postRallyMessage(rallyObject, message: textInputBar.text)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        // get instance of sendButton
                        let sendButton = textInputBarAndSendButtonSuperview.viewWithTag(309) as! UIButton
                        // reset textInputBarandSendButtonSuperview
                        let currentTextInputBarandSendButtonSuperviewHeight = textInputBarAndSendButtonSuperview.frame.height
                        textInputBarAndSendButtonSuperview.frame.origin.y += currentTextInputBarandSendButtonSuperviewHeight-self.navigationController!.toolbar.frame.height
                        textInputBarAndSendButtonSuperview.frame.size.height = self.navigationController!.toolbar.frame.height
                        // re-center sendButton
                        sendButton.center.y = textInputBarAndSendButtonSuperview.frame.height/2
                        // reset textInputBar
                        textInputBar.removeFromSuperview()
                        let newTextInputBarInstance = self.defineTextInputBar()
                        textInputBarAndSendButtonSuperview.addSubview(newTextInputBarInstance)
                        newTextInputBarInstance.delegate = self
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
    }
    
    func defineSendButtonInteractionState(_ interactionStateValue: Bool) {
        print("func defineSendButtonInteractionState is running...")
        if let sendButton = view.viewWithTag(343)!.viewWithTag(309) as? UIButton {
            if interactionStateValue == false { // not sure if this will be updated or not yet, if not change the condition to something else
                sendButton.isUserInteractionEnabled = false
                sendButton.setTitleColor(MYColor.inactiveGrey, for: UIControlState())
            }
            else {
                sendButton.isUserInteractionEnabled = true
                sendButton.setTitleColor(UIColor.white, for: UIControlState())
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        print("textView text equals:\(text)")
        if text != "" {
            defineSendButtonInteractionState(true) // set to true
        }
        let combinedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        // Create attributed version of the text
        let attributedTextIncludingNewText = NSMutableAttributedString(string: combinedText)
        attributedTextIncludingNewText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedTextIncludingNewText.length))
        // may be error if font parameter is nil above
        // Get the padding of the text container
        let padding = textView.textContainer.lineFragmentPadding
        // Create a bounding rect size by subtracting the padding
        // from both sides and allowing for unlimited length
        let boundingSize = CGSize(width: textView.frame.size.width - padding * 2, height: CGFloat.greatestFiniteMagnitude)
        // Get the bounding rect of the attributed text in the
        // given frame
        let boundingRectIncludingNewText = attributedTextIncludingNewText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        // Compare the boundingRect plus the top and bottom padding
        // to the text view height; if the new bounding height would be
        // less than or equal to the text view height, append the text
        // get boundingRect for current text
        let attributedText = NSMutableAttributedString(string: textView.text)
        attributedText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedText.length))
        let boundingRect = attributedText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let numberOfLines = round(boundingRect.height/textView.font!.lineHeight)
        let numberOfLinesUponChangingText = round(boundingRectIncludingNewText.height/textView.font!.lineHeight)
        //if boundingRectIncludingNewText.size.height > textView.frame.size.height { // if true, this means a new line is needed
        if numberOfLines < numberOfLinesUponChangingText { // if true, add new line by updating textView and toolbar size
            //let newMessagingTextViewHeight = boundingRectIncludingNewText.height
            handleMessagingTextViewSizeChange(1)
        }
        else if numberOfLines > numberOfLinesUponChangingText { // if true, remove line by updating textView and toolbar size
            //let newMessagingTextViewHeight = boundingRectIncludingNewText.height
            handleMessagingTextViewSizeChange(-1)
        }
        if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            print("textView:shouldChangeTextInRange text is empty, it's a backspace")
            if combinedText.characters.count == 0 {
                defineSendButtonInteractionState(false) // set to false
            }
            return true
        }
        if combinedText.characters.count < 300 { // 300 is the maximum number of characters per message
            return true
        }
        else { // don't go any further, user already reached max character limit
            print("textView:shouldChangeTextInRange:false reached character limit")
            return false
        }
    }
    
    func handleMessagingTextViewSizeChange(_ numberOfNewLines: CGFloat!) {
        print("func handleMessagingTextViewSizeChange is running...")
        // update textView size
        textInputBar.frame.size.height += numberOfNewLines*textInputBar.messagingTextViewTextContainerSingleLineHeight
        let textContainerCorrectionHeight = (textInputBar.frame.height-textInputBar.textContainer.size.height)/2
        print("textContainerCorrectionHeight:\(textContainerCorrectionHeight)")
        textInputBar.textContainer.size.height += 4
        textInputBar.contentSize.height = textInputBar.frame.height
        // update toolbar size
        let textInputBarAndSendButtonSuperview = view.viewWithTag(343)!
        textInputBarAndSendButtonSuperview.frame.size.height += numberOfNewLines*textInputBar.messagingTextViewTextContainerSingleLineHeight
        textInputBarAndSendButtonSuperview.frame.origin.y += -numberOfNewLines*textInputBar.messagingTextViewTextContainerSingleLineHeight
        let sendButton = textInputBarAndSendButtonSuperview.viewWithTag(309) as! UIButton
        sendButton.frame.origin.y += numberOfNewLines*textInputBar.messagingTextViewTextContainerSingleLineHeight
        // update textView size public variable messagingTextViewSize.messagingTextViewHeight
        textInputBar.messagingTextViewHeight = textInputBar.frame.height // I think I use this to calculate cursor/carret height in custom textview method, may not need
    }
// toolbar methods
    
// message blurb methods
    func numberOfMessages()->Int {
        print("func numberOfMessages is running...")
        var numberOfMessages = 0
        if let _ = rallyObject.rallyMessages {
            numberOfMessages = rallyObject.rallyMessages!.count
        }
        return numberOfMessages
    }
    
    func defineMessageBlurb(_ rallyMessageObject: RallyMessage, font: UIFont, topMessageBlurbYOffset: CGFloat)->(UIView,UIImageView) {
        print("func defineMessageBlurb is running...")
        // define message blurb and message blurb dimension properties
        let messageBlurbWidth = calculateMessageBlurbWidth()
        let messageBlurbHeight = calculateMessageBlurbHeight(rallyMessageObject.rallyMessageText, font: font, width: messageBlurbWidth, userID: rallyMessageObject.userID)
        let messageBlurbXOffset = calculateMessageBlurbXOffset(rallyMessageObject)
        let messageBlurbYOffset = calculateMessageBlurbYOffset(messageBlurbHeight, topMessageBlurbYOffset: topMessageBlurbYOffset)
        let messageBlurb = UIView(frame: CGRect(x: messageBlurbXOffset, y: messageBlurbYOffset, width: messageBlurbWidth, height: messageBlurbHeight))
        let imageViewSideLength = calculateMessageBlurbHeight("Sample text for height", font: font, width: messageBlurbWidth, userID: MYUser.ID) // userID message blurbs are taller, so return a larger picture height for pictures which is desired
        let cornerRadius = imageViewSideLength/2 // Defines messageBlurbHeight sides to be circular
        messageBlurb.layer.cornerRadius = cornerRadius
        if rallyMessageObject.userID == MYUser.ID {
            messageBlurb.backgroundColor = MYColor.rallyBlue
            messageBlurb.frame.origin.x -= ScreenDimension.width*1.15/100 // -ScreenDimension.screenWidth*1.2/100 bc image origin is displaced left by this value
        }
        else {
            messageBlurb.backgroundColor = UIColor.white
        }
        // define user image that goes next to message blurb
        let userImageAsideMessage = defineUserImageAsideMessage(rallyMessageObject,imageViewYOffset: messageBlurbYOffset,messageBlurbHeight: messageBlurbHeight, imageViewSideLength: imageViewSideLength)
        scrollView.addSubview(userImageAsideMessage)
        let messageBlurbMessage = defineMessageBlurbMessage(rallyMessageObject, messageBlurbWidth: messageBlurbWidth, font: font)
        messageBlurb.frame.size.width += messageBlurbMessage.frame.origin.x*2
        // determine if rally member name should be added to blurb and add appropriately
        let rallyMemberNameLabel: UILabel? = defineMessageBlurbName(rallyMessageObject, font: font, messageBlurbWidth: messageBlurbWidth)
        if let _ = rallyMemberNameLabel {
            messageBlurb.addSubview(rallyMemberNameLabel!)
            // modify messageBlurb and messageBlurbMessage dimensions here instead of originally bc this is only for non-user posts so it is not applicable to do for all messages
            messageBlurb.frame.size.height += rallyMemberNameLabel!.frame.origin.y+rallyMemberNameLabel!.frame.height
            messageBlurb.frame.origin.y -= rallyMemberNameLabel!.frame.origin.y+rallyMemberNameLabel!.frame.height
            messageBlurbMessage.frame.origin.y += rallyMemberNameLabel!.frame.origin.y+rallyMemberNameLabel!.frame.height
        }
        scrollView.addSubview(messageBlurb)
        messageBlurb.addSubview(messageBlurbMessage)
        return (messageBlurb,userImageAsideMessage)
    }
    
    func defineMessageBlurbName(_ rallyMessageObject: RallyMessage, font: UIFont, messageBlurbWidth: CGFloat)->UILabel? {
        print("func defineUserImageAsideMessage is running...")
        var rallyMemberNameLabel: UILabel?
        if rallyMessageObject.userID != MYUser.ID { // include name in message blurb if message is from a rally member other than user
            rallyMemberNameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: messageBlurbWidth, height: CGFloat.greatestFiniteMagnitude))
            rallyMemberNameLabel!.numberOfLines = 1
            rallyMemberNameLabel!.lineBreakMode = .byTruncatingTail
            let rawName = rallyMessageObject.userName
            let friendNameArray = rawName?.characters.split{$0 == " "}.map(String.init)
            let firstName = friendNameArray?[0]
            //let lastName: String? = friendNameArray[1]
            /*var firstLetterLastName: String = "S."
            if let unwrappedLastName = lastName {
                firstLetterLastName = Array(arrayLiteral: unwrappedLastName)[0]+"."
            }*/
            let formattedName = firstName
            rallyMemberNameLabel!.text = formattedName
            var fontDownSize: CGFloat = 0
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                fontDownSize = 1
            default:
                print("Do nothing...")
            }
            rallyMemberNameLabel!.font = UIFont.boldSystemFont(ofSize: rallyMemberNameLabel!.font.pointSize-0.5-fontDownSize)
            rallyMemberNameLabel!.textColor = MYColor.lightBlack
            rallyMemberNameLabel!.sizeToFit()
            rallyMemberNameLabel!.frame.origin.x = ScreenDimension.width*3.6/100
            rallyMemberNameLabel!.frame.origin.y = ScreenDimension.height*1.53/100
        }
        return rallyMemberNameLabel
    }
    
    func defineUserImageAsideMessage(_ rallyMessageObject: RallyMessage, imageViewYOffset: CGFloat, messageBlurbHeight: CGFloat, imageViewSideLength: CGFloat)->UIImageView {
        print("func defineUserImageAsideMessage is running...")
        let rallyMemberImageView = UIImageView()
        let imageViewSideLength = imageViewSideLength
        var imageViewXOffset: CGFloat!
        if rallyMessageObject.userID == MYUser.ID {
            imageViewXOffset = ScreenDimension.width*8.5/100-ScreenDimension.width*1.2/100
        }
        else {
            imageViewXOffset = ScreenDimension.width*91.5/100+ScreenDimension.width*1.2/100-imageViewSideLength // ScreenDimension.screenWidth*1.2/100 is for the left/right padding added to message blurbs and the corresponding increase in message blurb width to accomodate. instead of moveing message blurbs over to keep proper spacing between blurbs and images, shifting images instead because they were kind of far from the edge anyways
        }
        let imageViewYOffset = imageViewYOffset+messageBlurbHeight-imageViewSideLength
        rallyMemberImageView.frame = CGRect(x: imageViewXOffset,y: imageViewYOffset,width: imageViewSideLength,height: imageViewSideLength)
        let rallyMember = fetchRallyMemberObjectFromRallyObjectRallyUserListUsingUserID(rallyMessageObject.userID)
        rallyMemberImageView.image = rallyMember.getProfileImage()
        return rallyMemberImageView
    }
    
    func fetchRallyMemberObjectFromRallyObjectRallyUserListUsingUserID(_ userID: Int)->Friend {
        print("func fetchRallyMemberObjectFromRallyObjectRallyUserListUsingUserID is running...")
        /*
        var rallyMember: friend!
        // this is another way we could find the rally member
        // http://stackoverflow.com/questions/28727845/find-an-object-in-array
        for rallyMemberValue in rallyObject.RallyUserList where rallyMemberValue.UserID == userID {
            print("Do we get here?")
            rallyMember = rallyMemberValue
            break
        }
        */
        let rallyMemberIndexWithinRallyObjectRallyUserListArray = rallyObject.rallyUserList.index{$0.ID!==userID}
        let rallyMember = rallyObject.rallyUserList[rallyMemberIndexWithinRallyObjectRallyUserListArray!]
        return rallyMember
    }
    
    func defineMessageBlurbMessage(_ rallyMessageObject: RallyMessage, messageBlurbWidth: CGFloat, font: UIFont)->UILabel {
        print("func defineMessageBlurbMessage is running...")
        let messageBlurbMessage = UILabel(frame: CGRect(x: 0, y: 0, width: messageBlurbWidth, height: CGFloat.greatestFiniteMagnitude))
        messageBlurbMessage.numberOfLines = 0
        messageBlurbMessage.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageBlurbMessage.font = font
        messageBlurbMessage.text = rallyMessageObject.rallyMessageText
        messageBlurbMessage.sizeToFit()
        if rallyMessageObject.userID == MYUser.ID {
            messageBlurbMessage.textColor = .white
            messageBlurbMessage.frame.origin.y = ScreenDimension.height*1.55/100
        }
        else {
            messageBlurbMessage.textColor = MYColor.lightBlack
            messageBlurbMessage.frame.origin.y = ScreenDimension.height*0.1/100
        }
        messageBlurbMessage.frame.origin.x = ScreenDimension.width*3.6/100
        return messageBlurbMessage
    }
    
    func defineMessageBlurbs() {
        print("func defineMessageBlurbs is running...")
        let font = UIFont(name: "Helvetica", size: 17)
        var topMessageBlurbYOffset = scrollView.contentSize.height // initial value is all the way at bottom because message blurbs aren't initialized until below
        if let _ = rallyObject.rallyMessages {
            for message in rallyObject.rallyMessages!.reversed() {
                let (messageBlurb,_) = defineMessageBlurb(message, font: font!, topMessageBlurbYOffset: topMessageBlurbYOffset)
                topMessageBlurbYOffset = messageBlurb.frame.origin.y
            }
        }
    }
    
    func calculateMessageBlurbXOffset(_ rallyMessageObject: RallyMessage)-> CGFloat {
        print("func calculateMessageBlurbXOffset is running...")
        var messageBlurbXOffset: CGFloat!
        // user message blurb indent value is different from other rally members
        if rallyMessageObject.userID == MYUser.ID {
            messageBlurbXOffset = ScreenDimension.width*20/100
        }
        else {
            messageBlurbXOffset = ScreenDimension.width*3/100
        }
        return messageBlurbXOffset
    }
    
    func calculateMessageBlurbYOffset(_ messageBlurbHeight: CGFloat, topMessageBlurbYOffset: CGFloat)-> CGFloat {
        print("func calculateMessageBlurbYOffset is running...")
        let messageBlurbYOffset = topMessageBlurbYOffset-ScreenDimension.height*0.8/100-messageBlurbHeight // ScreenDimension.screenHeight*0.8/100 is space between message blurbs
        return messageBlurbYOffset
    }
    
    func calculateMessageBlurbWidth(/*rallyMessageText: String, font: UIFont*/)-> CGFloat {
        print("func calculateMessageBlurbWidth is running...")
        // code block below sizes blurb to fit text, if this UI layout is preferred in future just uncomment and use this code instead of constant width value returned. this could also be a lazy var
        /*
        let label:UILabel = UILabel()
        label.font = font
        label.text = rallyMessageText
        label.sizeToFit()
        var messageBlurbWidth = ScreenDimension.screenWidth*62/100
        if label.frame.width < ScreenDimension.screenWidth*62/100 {
            messageBlurbWidth = label.frame.width
        }
        return messageBlurbWidth
        */
        // for now, make all text constant width
        return ScreenDimension.width*71/100
    }
    
    func calculateMessageBlurbHeight(_ rallyMessageText: String, font: UIFont, width: CGFloat, userID: Int)->CGFloat {
        print("func calculateMessageBlurbHeight is running...")
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = rallyMessageText
        label.sizeToFit()
        var messageBlurbHeight = label.frame.height
        print("messageBlurbHeight:\(messageBlurbHeight)")
        if userID == MYUser.ID {
            messageBlurbHeight += ScreenDimension.height*3.1/100
        }
        else {
            messageBlurbHeight += ScreenDimension.height*2.3/100 // ScreenDimension.screenHeight*2.3/100 is to add padding above and below the label, this will be appear as ScreenDimension.screenHeight*1.15/100 in the yOffset
        }
        return messageBlurbHeight
    }
// message blurb methods
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard(_ gestureRecognizer: UIPanGestureRecognizer) {
        print("func dismissKeyboard is running...")
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //http://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
        view.endEditing(true)
        self.scrollView.removeGestureRecognizer(gestureRecognizer)
    }
    
    // textView
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("func textViewDidBeginEditing is running...")
        // allow user to dismiss keyboard by swiping screen, presumably to see out-of-view messages
        let swipe = UIPanGestureRecognizer(target: self, action: #selector(MessagingViewController.dismissKeyboard(_:)))
        scrollView.addGestureRecognizer(swipe)
    }
}
