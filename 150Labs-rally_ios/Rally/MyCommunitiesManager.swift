//
//  MyCommunitiesManager.swift
//  Rally
//
//  Created by Ian Moses on 2/21/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class MyCommunitiesManager {
    var myCommunitiesCardSectionsUnderlineManager: MyCommunitiesCardSectionsUnderlineManager!
    var myCommunitiesSectionVC: HomeViewController!
    
    init(myCommunitiesSectionVC: HomeViewController) {
        print("initialize underlines")
        // initialize class properties
        self.myCommunitiesSectionVC = myCommunitiesSectionVC
        self.myCommunitiesCardSectionsUnderlineManager = MyCommunitiesCardSectionsUnderlineManager(myCommunitiesManager: self)
    }
    
    // IF section label already exists, do nothing
    // ELSE, define 'My Communities' Public/Private buttons
    
    class MyCommunitiesCardSectionsUnderlineManager {
        
        var underlineOne: MYWrapAroundUnderline { // not visible to user
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.underlineOne
            }
        }
        var underlineTwo: MYWrapAroundUnderline { // not visible to user
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.underlineTwo
            }
        }
        var quadrantZeroXOrigin: CGFloat { // not visible to user
            get {
                return self.quadrantOneXOrigin-self.underlineOne.frame.width
            }
        }
        var quadrantOneXOrigin: CGFloat { // visible to user
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.leftMostXOrigin
            }
        }
        var quadrantTwoXOrigin: CGFloat { // visible to user
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.privateCommunitySectionButton.center.x-self.underlineTwo.frame.width/2
            }
        }
        var quadrantThreeXOrigin: CGFloat { // not visible to user
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.privateCommunitySectionButton.center.x+self.underlineTwo.frame.width/2
            }
        }
        var underlineWidth: CGFloat {
            get {
                return myCommunitiesManager.myCommunitiesSectionVC.underlineWidth
            }
        }
        // need a way to keep track of percentage position of two card sections
        var myCommunitiesManager: MyCommunitiesManager!
        
        init(myCommunitiesManager: MyCommunitiesManager) {
            print("initialize underlines")
            // initialize class properties
            self.myCommunitiesManager = myCommunitiesManager
            // default positions are underline
            // need to know underline width, left-most x-origin and right-most x-origin
            
            // width
            
            // left-most x-origin
            // SET 1
            // underlineOne: underlineOne.frame.origin.x = MYCard.horizontalCardsSpacingUnit
            // underlineTwo: underlineOne.center.x = privateButton.center.x+underlineTwo.frame.width/2
            
            // SET 2
            // underlineOne: underlineOne.center.x = publicButton.center.x
            // underlineTwo: underlineOne.center.x = privateButton.center.x+underlineTwo.frame.width/2
            
            // right-most origin
            /*let publicButtonXOrigin = myCommunitiesManager.publicButton.frame.origin.x+(myCommunitiesManager.publicButton.frame.width-myCommunitiesManager.publicButton.label.frame.width)/2
            let cardXOrigin = MYCard.horizontalCardsSpacingUnit
            let publicButtonCardXOriginDiff = publicButtonXOrigin - cardXOrigin
            let adjustBy = 2.3*publicButtonCardXOriginDiff - myCommunitiesManager.privateButton.label.frame.origin.x
             */
            // _______|_______  _______|_______
        }
    }
}
