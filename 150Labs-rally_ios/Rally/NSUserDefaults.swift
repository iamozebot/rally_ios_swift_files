//
//  NSUserDefaults.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation

class NSUserDefaultsClass {
    class func updateAppHasRunOnDeviceKey() {
        UserDefaults.standard.set(true, forKey: "firstTimeAppRunKey")
    }
    
    class func updateUserStockImageIDKey(stockImageID: Int) {
        UserDefaults.standard.set(stockImageID, forKey: "userStockImageIDKey")
    }
    
    class func updateUserIDKey(userID: Int) {
        UserDefaults.standard.set(userID, forKey: "userIDKey")
    }
    
    class func updateUserPhoneNumberKey() {
        if let userPhoneNumberAsString = MYUser.phoneNumberList["Mobile"] {
            UserDefaults.standard.set("\(userPhoneNumberAsString!)", forKey: "userPhoneNumberKey")
        }
    }
    
    class func updateUserNameKey() {
        guard MYUser.firstName != nil else {return}
        guard MYUser.lastName != nil else {return}
        UserDefaults.standard.set(true, forKey: "userNameKey")
    }
    
    class func updateUser(firstNameKey: String) {
        UserDefaults.standard.set(firstNameKey, forKey: "userFirstNameKey")
    }
    
    class func updateUser(lastNameKey: String) {
        UserDefaults.standard.set(lastNameKey, forKey: "userLastNameKey")
    }
    
    class func updateRallyMembersIdentifiersResultKey(_ rallysMemberIdentifiersRallyIDIdentifierKeyValuePairDictionary: Data) { // JSON format, rallys with associated array of rally member identifiers. user identifier is not included in array
        UserDefaults.standard.set(rallysMemberIdentifiersRallyIDIdentifierKeyValuePairDictionary, forKey: "rallyMembersIdentifiersResultKey")
    }
    
    class func updateCommunityMemberIdentifiersResultKey(_ communityMemberIdentifiersRallyIDIdentifierKeyValuePairDictionary: Data) { // JSON format, rallys with associated array of rally member identifiers. user identifier is not included in array
        UserDefaults.standard.set(communityMemberIdentifiersRallyIDIdentifierKeyValuePairDictionary, forKey: "communityMembersIdentifiersResultKey")
    }
    
    class func updateStockImagesResultKey(_ stockImagesObjectsKeyValuePairDictionary: Data) { // JSON format, rallys with associated array of rally member identifiers. user identifier is not included in array
        UserDefaults.standard.set(stockImagesObjectsKeyValuePairDictionary, forKey: "stockImagesResultKey")
    }
    
// tutorial blurb methods
    class func updateTutorialStartRallyWithGroupBlurbDisplayStatus(_ blurbDisplayStatus: Bool) { // .howToUseSavedGroupCards
        UserDefaults.standard.set(blurbDisplayStatus, forKey: "tutorialStartRallyWithGroupBlurbDisplayStatus")
    }
    
    class func updateTutorialStartRallyWithCommunityBlurbDisplayStatus(_ blurbDisplayStatus: Bool) { // .howToUseRallyCommunityCard
        UserDefaults.standard.set(blurbDisplayStatus, forKey: "tutorialStartRallyWithCommunityBlurbDisplayStatus")
    }
    
    class func updateTutorialCommunityIntroBlurbDisplayStatus(_ blurbDisplayStatus: Bool) { // .whatAreCommunities
        UserDefaults.standard.set(blurbDisplayStatus, forKey: "tutorialCommunityIntroBlurbDisplayStatus")
    }
    
    class func updateTutorialSaveRallyGroupBlurbDisplayStatus(_ blurbDisplayStatus: Bool) { // .howToUseSaveButtonToSaveGroups
        UserDefaults.standard.set(blurbDisplayStatus, forKey: "tutorialSaveRallyGroupBlurbDisplayStatus")
    }
    
    class func updateTutorialInviteFriendsWithoutAppBlurbDisplayStatus(_ blurbDisplayStatus: Bool) { // .howFriendsWithoutAppReceiveInvite
        UserDefaults.standard.set(blurbDisplayStatus, forKey: "tutorialInviteFriendsWithoutAppBlurbDisplayStatus")
    }
// tutorial blurb methods
    
    class func updateRemoteNotificationsPermissionsStatus(_ permissionStatus: Data) {
        UserDefaults.standard.set(permissionStatus, forKey: "remoteNotificationsPermissionsStatus")
    }
    
    class func updateUserThumbnailUserID(dictionary: [String: Int]) {
        UserDefaults.standard.set(dictionary, forKey: "UserThumbnailUserIDDictionary")
    }
}
