//
//  NavBar.swift
//  Rally
//
//  Created by Ian Moses on 9/30/15.
//  Copyright © 2015 Ian Moses. All rights reserved.
//

import Foundation

class navBarClass {
    class func navBar(_ selfReference: UIViewController, navigationBarItemReference: UINavigationItem, leftButtonImageReference: String?, rightButtonImageReference: String?, rightMostButtonImageReference: String?, leftButtonActionReference: Selector?, rightButtonActionReference: Selector?, rightMostButtonActionReference: Selector?, titleReference: String?) {
        print("navBarClass:class func navBar is running...")
        // http://iostechsolutions.blogspot.com/2014/11/swift-add-custom-right-bar-button-item.html
        // hide default navigation controller provided back button
        navigationBarItemReference.setHidesBackButton(true, animated: false)
        // define left nav bar button
        if let _ = leftButtonImageReference as String! {
            let myCustomLeftButtonItem = UIBarButtonItem(image: UIImage(named: "\(leftButtonImageReference!)")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .plain, target: selfReference, action: leftButtonActionReference!)
            navigationBarItemReference.leftBarButtonItem  = myCustomLeftButtonItem
            let imageInsetValue = (navigationBarItemReference.leftBarButtonItem?.image!.size.height)!*15/100
            navigationBarItemReference.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(imageInsetValue, 0, imageInsetValue, imageInsetValue) // left inset set to zero for transition from login screen to homeVC. Non-zero value causes animation jumping at end
        }
        // define right nav bar button
        if let _ = rightButtonImageReference as String! {
            let myCustomRightButtonItem = UIBarButtonItem(image: UIImage(named:"\(rightButtonImageReference!)")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .plain, target: selfReference, action: rightButtonActionReference!)
            navigationBarItemReference.rightBarButtonItem = myCustomRightButtonItem
            let imageInsetValue = (navigationBarItemReference.rightBarButtonItem?.image!.size.height)!*12/100
            navigationBarItemReference.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(imageInsetValue, imageInsetValue, imageInsetValue, imageInsetValue)
        }
        // define right most nav bar button
        if let _ = rightMostButtonImageReference as String! {
            let myCustomRightMostButtonItem = UIBarButtonItem(image: UIImage(named:"\(rightMostButtonImageReference!)")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: .plain, target: selfReference, action: rightMostButtonActionReference!)
            navigationBarItemReference.rightBarButtonItem = myCustomRightMostButtonItem
            let imageInsetValue = (navigationBarItemReference.rightBarButtonItem?.image!.size.height)!*12/100
            navigationBarItemReference.rightBarButtonItem?.imageInsets = UIEdgeInsetsMake(imageInsetValue, imageInsetValue, imageInsetValue, imageInsetValue)
        }
        if let _ = titleReference as String! {
            navigationBarItemReference.title = titleReference!
        }
    }
}

