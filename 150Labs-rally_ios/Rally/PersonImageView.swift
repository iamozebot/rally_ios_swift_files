//
//  PersonImageView.swift
//  Pods
//
//  Created by Jimmy Jun on 8/24/17.
//
//

import Foundation
import UIKit



class PersonImageView: UIImageView {
    
    var person: Friend!
    
    init(person: Friend) {
        
        super.init(frame: CGRect.zero)
        self.person = person
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



