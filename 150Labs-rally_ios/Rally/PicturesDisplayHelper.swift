//
//  PicturesDisplayHelper.swift
//  Rally
//
//  Created by Ian Moses on 9/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class PicturesDisplayDataSource {
    var images: [UIImage]?
    var imageSideLength: CGFloat
    var pictureContainer: passThroughView?
    var cardTypeValue: CardType?
    var members: [Friend]
    var pictureContainerFlag: Int!
    
    init(_ images: [UIImage]?,_ members: [Friend],_ pictureContainer: passThroughView!,_ cardTypeValue: CardType!,_ imageSideLength: CGFloat!) {
        self.images = images
        self.members = members
        self.pictureContainer = pictureContainer
        self.cardTypeValue = cardTypeValue
        self.imageSideLength = imageSideLength
    }
}

class PicturesDisplayHelper {
    
    var picturesDisplayData: PicturesDisplayDataSource!
    
    init(picturesDisplayData: PicturesDisplayDataSource) {
        self.picturesDisplayData = picturesDisplayData
    }
    
    func defineRallyOrGroupCardPictures()->passThroughView {
        print("func defineRallyOrGroupCardPictures is running...")
        var numberOfImagesLeftToAdd = 4
        var rawMembersProfileImages = [UIImage]()
        var finalMembersProfileImages = [UIImage]()
        if picturesDisplayData.members.count < numberOfImagesLeftToAdd { // for case: less members than pictures needed
            for member in picturesDisplayData.members {
                rawMembersProfileImages.append(member.getProfileImage())
            }
            switch picturesDisplayData.cardTypeValue! { // switch differentiates cards that with 2 vs 4 pictures
            case .upcomingRally:
                // define profile picture array to use for card picture container
                finalMembersProfileImages = [rawMembersProfileImages[0],rawMembersProfileImages[0]]
            default:
                switch picturesDisplayData.members.count {
                case _ where picturesDisplayData.members.count == 1:
                    // define profile picture array to use for card picture container
                    finalMembersProfileImages = [rawMembersProfileImages[0],rawMembersProfileImages[0],rawMembersProfileImages[0],rawMembersProfileImages[0]]
                case _ where picturesDisplayData.members.count == 2:
                    // define profile picture array to use for card picture container
                    finalMembersProfileImages = [rawMembersProfileImages[0],rawMembersProfileImages[1],rawMembersProfileImages[0],rawMembersProfileImages[1]]
                case _ where picturesDisplayData.members.count == 3:
                    // define profile picture array to use for card picture container
                    finalMembersProfileImages = [rawMembersProfileImages[0],rawMembersProfileImages[1],rawMembersProfileImages[2],rawMembersProfileImages[0]]
                default:
                    break
                }
            }
        }
        else {
            for (index, member) in picturesDisplayData.members.enumerated() {
                if numberOfImagesLeftToAdd<picturesDisplayData.members.count-index { // check to see if there are as many pictures needing to be added as there are people left. if true, just add regardless
                    // 0, 1, 2, 3; 3-3; 3-2; 3-1; 1-1; 4-4; 4-3; 4-2; 4-1; 6-4; 6-3; 6-2; 6-1;
                    if let thumbNailProfileImage = member.thumbnailProfileImage {
                        finalMembersProfileImages.append(thumbNailProfileImage)
                        numberOfImagesLeftToAdd -= 1
                        if numberOfImagesLeftToAdd == 0 {
                            break
                        }
                    }
                }
                else { // add regardless of thumbnail existing
                    finalMembersProfileImages.append(member.getProfileImage())
                    numberOfImagesLeftToAdd -= 1
                }
            }
        }
        defineImageSetForPictureContainer(finalMembersProfileImages)
        return picturesDisplayData.pictureContainer!
    }
    
    func defineImageSetForPictureContainer(_ membersImages: [UIImage]) {
        print("func defineImageSetForPictureContainer is running...")
        print("membersImages.count:\(membersImages.count)")
        for (index, image) in membersImages.enumerated() {
            let xOffsetMultiplier = CGFloat((index+2)%2) // 0, 1, 2, 3 = 0, 1, 0, 1 left side is index value and right side is what the multiplier should be
            let yOffsetMultiplier = floor(CGFloat((index)/2)) // 0, 1, 2, 3 = 0, 0, 1, 1 left side is index value and right side is what the multiplier should be
            let imageView = UIImageView(image: image)
            picturesDisplayData.pictureContainer!.addSubview(imageView)
            imageView.frame = CGRect(x: xOffsetMultiplier*picturesDisplayData.imageSideLength,y: yOffsetMultiplier*picturesDisplayData.imageSideLength,width: picturesDisplayData.imageSideLength,height: picturesDisplayData.imageSideLength)
        }
    }
}
