//
//  RallyClass.swift
//  Rally
//
//  Created by Ian Moses on 7/13/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class Rally {
    var rallyLocation: String = "Current Location" // should we separate rallyLocation address and title. current format: Chipotle Mexican Grill 123 Alphabet Lane, Yahoo, Iowa
    var rallyStartDateTime: Date? // "2015-12-31 22:45:45" (send format) // UTC time // includes as much info as known // default values: Today; 90 minutes from current time rounded to nearest fifteen minute
    //var RallyResponseTime: Int = 120 // assigned default value of 35 minutes
    var rallyName: String = "Untitled Rally" // assigned default value of hangout
    var rallyerID: Int!
    var rallyUserList: [Friend] = []
    var isRallyActive: Int = 0
    var rallyID: Int! // RallyID received from database in rally package when fetching rallys and as webcall response in rally creation. must store in both cases (used for messaging and profile picture contact key value.
    var type: String = "upcomingRallysGlobalArray" // rally or new groups. groups are only created on home screen (only community member at time of creation is user creating the community, so no need to use this method to create the new rally object. Therefore only new rallys are created via this method)
    var createdDateTimeStamp: String! // is this always a non-nil value?
    var latitude: CGFloat = 0.0
    var longitude: CGFloat = 0.0
    var rallyDetailsID: Int!
    var distance: CGFloat = 0.0
    var rallyMessages: [RallyMessage]?
    var isMemberMaximum: Bool?
    var maximumNumberOfMembers: Int?
    var associatedCommunity: Community?
    
    convenience init(members: [Friend]) {
        self.init()
        self.rallyerID = MYUser.ID
        // check for user in rallyMembers, if doesn't exist, append to members
        var userInMembersFlag = false
        self.rallyUserList = members
        for member in members where member.ID == MYUser.ID {
            userInMembersFlag = true
        }
        // Create user object of type friend to append to front of RallyUserList if object does not already exist in RallyUserList (does in groups, doesn't when creating rallys from scratch)
        if !userInMembersFlag {
            let newMember = Friend()
            newMember.phoneNumberList = MYUser.phoneNumberList
            newMember.ID = MYUser.ID
            communityMemberThumbnailsGlobalDictionary[MYUser.ID] = MYUser.thumbnailProfileImage
            newMember.firstName = MYUser.firstName
            newMember.lastName = MYUser.lastName
            self.rallyUserList.insert(newMember, at: 0)
        }
        self.rallyStartDateTime = dateHelperClass().defaultRallyStartDateTime() // add default rallyStartDateTime
    }
    /*
    func matchRallyMembersWithPhoneContacts(completionHandler: () -> Void) {
        print("class func matchRallyMembersWithPhoneContacts is running...")
        for member in self.rallyUserList {
            if member.userID != User.userID {
                let rallyMemberPhoneNumberArray = Array(member.phoneNumberList.values)
                let rallyMemberPhoneNumber = rallyMemberPhoneNumberArray[0]
                /*for myObj in myObjList where myObj.name == "foo" { // since swift 2.1, this is another way to filter efficiently
                    //object wiht name is foo
                }*/
                
                let phoneNumberValues = friends.map{Array($0.phoneNumberList.values)} // both should be formatted as "5555555555" since all friend objects pass through backend and are formatted as such there

                let rallyIndexWithinFriendsArray = phoneNumberValues.index{$0[0]==rallyMemberPhoneNumber}

                if let index = rallyIndexWithinFriendsArray {
                    let friendToAdd = friends.remove(at: index)
                    friends.insert(friendToAdd, at: 0) // adds user at the front of the friends list
                }
                else { // no contacts with matching phone numbers to member. So, member is not in addressBook. Add member friend type object to friends array so it will be a cell on the friends screen
                    friends.insert(member, at: 0)
                }
            }
        }
        completionHandler()
    }
    */
    func rallyHasTipped()->Bool {
        print("func rallyHasTipped is running...")
        var numberOfUsersJoinedRally = 0
        for member in self.rallyUserList {
            if let didUserAcceptValue = member.didUserAccept { // does didUserAccept value exist
                if didUserAcceptValue == 1 { // is didUserAccept true (1 == true)
                    numberOfUsersJoinedRally += 1 // increment counter
                    if numberOfUsersJoinedRally > 2 { // if number of users having joined rally is greater than 2, enable goToMessaging button
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func numberOfRallyUsersThatAcceptedInvite()->Int {
        print("func numberOfRallyUsersThatAcceptedInvite is running...")
        var numberOfRallyMembersAccepted = 0
        for member in self.rallyUserList {
            if let didUserAccept = member.didUserAccept {
                if didUserAccept == 1 {
                    numberOfRallyMembersAccepted += 1
                }
            }
        }
        return numberOfRallyMembersAccepted
    }
    
    func setRallyCardDisplayedNames()->String { // creates list such as: 'Ian, Joe, Mike, +8' for ~50 characters (didn't count exact number)
        print("func setRallyCardDisplayedNames is running...")
        var memberNames = ""
        for i in 0..<self.rallyUserList.count {
            // get rallyMemberName first name saved into variable apart from last name
            guard let rallyMemberFirstName = self.rallyUserList[i].firstName else {continue}
            let numberOfNamesLeftAfterCurrentName = self.rallyUserList.count - i - 1
            // numberOfNamesNotYetListed doesn't count name rallyMemberName looking to be listed, so if this is last one, equals 0
            let namesOnlyCharacterCount = memberNames.characters.count + rallyMemberFirstName.characters.count + 2
            /* currentlyAppendedRallyGroupMemberNames accounts for commas already in but not spaces, so i accounts for spaces. 2 accounts for space and comma of last person listed. numberOfNamesNotYetListed only accounts for comma after last person, not the last peron's space and comma*/
            var groupNamesListRemainingCharacterCount = 4 // number is for comma, space, +, number
            if numberOfNamesLeftAfterCurrentName < 100 {
                groupNamesListRemainingCharacterCount = 5 // number is for comma, space, +, number
            }
            else {
                groupNamesListRemainingCharacterCount = 6 // number is for comma, space, +, number
            }
            var nextRallyMemberFirstName: String = "" // for last case, need value. for last case, the most recent check was done on current name so we know it will pass to next value
            if numberOfNamesLeftAfterCurrentName != 0 {
                if let value = self.rallyUserList[i].firstName {
                    nextRallyMemberFirstName = value
                }
            }
            if memberNames.characters.count == 0 {
                memberNames = "\(rallyMemberFirstName)"
            }
                /* this is supposed to be if last name in group fits and covering a case the else if below doesn't cover. However, I can't tell how the case below doesn't cover this. commenting out, if I continue getting problems, consider readding this case
                 else if numberOfNamesLeftAfterCurrentName == 0 && namesOnlyCharacterCount<30 {
                 view.stringDataAssociatedWithView += ", \(rallyMemberFirstName)"
                 }
                 */
            else if nextRallyMemberFirstName.characters.count+2<=43-namesOnlyCharacterCount-groupNamesListRemainingCharacterCount {
                memberNames += ", \(rallyMemberFirstName)"
            }
            else if numberOfNamesLeftAfterCurrentName>0 {
                /* case: At last name, last name not users and total rally group list character count remains under 26 characters after adding user name*/
                memberNames += ", +\(numberOfNamesLeftAfterCurrentName)"
                break
            }
        }
        // if let currentlyAppendedRallyGroupMemberNames = rallyNamesList.text as String! { // if v8 already has text, may use += operator below
        return memberNames
    }
}
