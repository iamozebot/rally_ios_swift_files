//
//  RallyDetailView.swift
//  Rally
//
//  Created by Ian Moses on 7/1/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import UIKit

class RallyDetailView: UIView {
    
    var detailType: RallyDetailType = .date // use as default, makes switch code in init func simpler
    var rally: Rally!
    var superviewHeight: CGFloat!
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = MYColor.veryDarkGrey
        let sideLength: CGFloat = ScreenDimension.width*17/100
        imageView.frame.size = CGSize(width: sideLength, height: sideLength)
        self.addSubview(imageView)
        return imageView
    }()
    lazy var textLabel: InsetLabel = {
        let textLabel = InsetLabel()
        textLabel.textColor = MYColor.detailButtonTitleGrey
        let width = ScreenDimension.width*70/100
        textLabel.frame = CGRect(x: 0.0, y: 0.0, width: width, height: CGFloat.greatestFiniteMagnitude)
        textLabel.center.y = self.imageView.center.y
        textLabel.numberOfLines = 1
        textLabel.lineBreakMode = .byTruncatingTail //NSLineBreakMode.byTruncatingTail
        textLabel.clipsToBounds = true
        textLabel.textColor = MYColor.detailButtonTitleGrey
        textLabel.font = UIFont.systemFont(ofSize: 22)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            textLabel.font = UIFont.systemFont(ofSize: 20)
        default:
            print("Do nothing...")
        }
        //messageLabel.sizeToFit()
        // set left inset for option value
        textLabel.contentInset.left = ScreenDimension.width*1.25/100
        self.addSubview(textLabel)
        return textLabel
    }()
    
    override init(frame: CGRect) {
        print("class RallyDetailView:func override init frame is running...")
        super.init(frame: frame) // calls designated initializer
    }
    
    convenience init(detailType: RallyDetailType, rally: Rally, superviewHeight: CGFloat) {
        print("class RallyDetailView:func init is running...")
        
        let xOffset: CGFloat = ScreenDimension.width*3/100
        let yOffset: CGFloat = 0 // just to initialize frame. real value set after height is calculated
        let width = ScreenDimension.width*93/100
        let frame = CGRect(x: xOffset, y: yOffset, width: width, height: CGFloat.greatestFiniteMagnitude)
        
        self.init(frame: frame) // calls the UIView designated initializer above
        
        // set properties
        self.detailType = detailType
        self.rally = rally
        self.superviewHeight = superviewHeight
        
        // define view & subviews
        var uniqueValuesForTimeButtons: (xOffsetScaler: CGFloat, yOffsetScaler: CGFloat, widthScaler: CGFloat, heightScaler: CGFloat, tagValue: Int, title: String, imageName: String)
        let buttonHeightScalar: CGFloat = 1/2-2/1000
        let buttonWidthScalar: CGFloat = 41/100
        let buttonYOffsetScalar: CGFloat = 1/1000
        
        switch detailType {
        case .time: // startTimeButton
            let startTime: String = dateHelperClass().formatDetailsScreenTimeButtonTitle(rally.rallyStartDateTime!)
            if MYUser.ID != rally.rallyerID {
                self.isUserInteractionEnabled = false
            }
            uniqueValuesForTimeButtons = (1/2, buttonYOffsetScalar, buttonWidthScalar, buttonHeightScalar, 0, startTime, "clock")
            //clockSmallVersion
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                uniqueValuesForTimeButtons = (1/2, buttonYOffsetScalar, buttonWidthScalar, buttonHeightScalar, 0, startTime, "clockSmallVersion")
            default:
                print("Do nothing...")
            }
        case .location: // locationButton
            var rallyLocation: String!
            print("defineStartTimeResponseCalendarLocationButtons:rally.RallyLocation:\(rally.rallyLocation)")
            if rally.rallyerID == MYUser.ID && rally.isRallyActive == 0 {
                rallyLocation = "Current Location"
            }
            else { // non-rally creator or rally creator upon loading detailsVC both see 'dropped pin' which is assigned to rallyLocation property during fetch rallys and communities
                rallyLocation = rally.rallyLocation
            }
            if rallyLocation.characters.count > 8 {
                self.textLabel.font = UIFont.systemFont(ofSize: 21)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    self.textLabel.font = UIFont.systemFont(ofSize: 18)
                default:
                    print("Do nothing...")
                }
            }
            var locationButtonWidthBasedOnWhetherResponseButtonIsOnLine = buttonWidthScalar-3/100 // decreasing button width bc otherwise location text extends too close to response time icon
            if rally.isRallyActive == 1 || rally.isRallyActive == 0 { // if rally has been sent, response button has moved to container view so location button title can take more of the space on the line
                locationButtonWidthBasedOnWhetherResponseButtonIsOnLine = buttonWidthScalar*2-9/100 // decreasing button width bc otherwise location text extends too close to edge of screen
            }
            uniqueValuesForTimeButtons = (1/2-buttonWidthScalar, 2*buttonYOffsetScalar+buttonHeightScalar, locationButtonWidthBasedOnWhetherResponseButtonIsOnLine, buttonHeightScalar, 2, rallyLocation, "location")
            //locationSmallVersion
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                uniqueValuesForTimeButtons = (1/2-buttonWidthScalar, 2*buttonYOffsetScalar+buttonHeightScalar, locationButtonWidthBasedOnWhetherResponseButtonIsOnLine, buttonHeightScalar, 2, rallyLocation, "locationSmallVersion")
            default:
                print("Do nothing...")
            }
        case .date: // calendarButton
            print("case 3")
            let date: String = dateHelperClass().formatNSDateForDetailsScreenDateButtonTitle(rally.rallyStartDateTime)
            if MYUser.ID != rally.rallyerID {
                self.isUserInteractionEnabled = false
            }
            if date.characters.count > 10 || date == "Wednesday" { // if text is too long, instead of truncating to fit, decrease text size slightly. this is for initial display. date changing is handled within handleLongrPress method
                self.textLabel.font = UIFont.systemFont(ofSize: 21)
                switch UIDevice().type {
                case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                    print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                    self.textLabel.font = UIFont.systemFont(ofSize: 16)
                case .iPhone6, .iPhone6S:
                    self.textLabel.font = UIFont.systemFont(ofSize: 19)
                default:
                    print("Do nothing...")
                }
            }
            uniqueValuesForTimeButtons = (1/2-buttonWidthScalar, buttonYOffsetScalar, buttonWidthScalar, buttonHeightScalar, 3, date, "calendar")
            //calendarSmallVersion
            switch UIDevice().type {
            case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                print("switch UIDevice().type:.iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                uniqueValuesForTimeButtons = (1/2-buttonWidthScalar, buttonYOffsetScalar, buttonWidthScalar, buttonHeightScalar, 3, date, "calendarSmallVersion")
            default:
                print("Do nothing...")
            }
        }
        // set tag
        self.tag = uniqueValuesForTimeButtons.tagValue
        // set frame
        self.frame = CGRect(x: ScreenDimension.width*uniqueValuesForTimeButtons.xOffsetScaler, y: superviewHeight*uniqueValuesForTimeButtons.yOffsetScaler, width: ScreenDimension.width*uniqueValuesForTimeButtons.widthScaler, height: superviewHeight*uniqueValuesForTimeButtons.heightScaler)
        // set image
        imageView.image = UIImage(named:uniqueValuesForTimeButtons.imageName)?.withRenderingMode(.alwaysTemplate)
        // set textlabel text
        print("uniqueValuesForTimeButtons.title:\(uniqueValuesForTimeButtons.title)")
        textLabel.text = uniqueValuesForTimeButtons.title
        textLabel.sizeToFit()
        textLabel.frame.size.width = ScreenDimension.width*75/100
        //rallyDetailButton.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
        self.imageView.frame.size = CGSize(width: self.imageView.frame.width/2,height: self.imageView.frame.height/2)
        // update subview frames
        self.updateSubviewFrames()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateSubviewFrames() {
        print("func updateSubviewXOffsets is running...")
        
        
        imageView.center.y = self.frame.height/2
        
        self.textLabel.frame.origin.x = self.imageView.frame.origin.x+self.imageView.frame.width+ScreenDimension.width*1.5/100
        textLabel.center.y = self.frame.height/2
    }
}
