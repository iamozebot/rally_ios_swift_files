//
//  RallyMemberPictureAndNameCustomView.swift
//  Rally
//
//  Created by Ian Moses on 1/30/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class RallyMemberImageAndNameCustomViewClass: UIView {
    let imageViewSideLength: CGFloat = ScreenDimension.width*15.33/84
    let imageViewSpacing: CGFloat = ScreenDimension.width*0.82/84
    let imageViewFirstColumnXOffset: CGFloat = ScreenDimension.width*1.935/84
    let superView = UIView()
    lazy var rallyMemberNameLabel: UILabel = {
        let rallyMemberNameLabel = UILabel()
        rallyMemberNameLabel.font = UIFont.systemFont(ofSize: 13)
        rallyMemberNameLabel.textColor = MYColor.darkGrey
        rallyMemberNameLabel.textAlignment = .center
        rallyMemberNameLabel.isUserInteractionEnabled = false
        rallyMemberNameLabel.numberOfLines = 2
        self.addSubview(rallyMemberNameLabel)
        return rallyMemberNameLabel
    }()
    lazy var plusMembersUILabel: UILabel = {
        let plusMembersUILabel = UILabel()
        plusMembersUILabel.textAlignment = .center
        plusMembersUILabel.textColor = UIColor.white
        plusMembersUILabel.isUserInteractionEnabled = true
        plusMembersUILabel.font = UIFont.systemFont(ofSize: 30)
        plusMembersUILabel.backgroundColor = MYColor.rallyBlue
        plusMembersUILabel.frame = CGRect(x: 0,y: 0,width: self.imageViewSideLength,height: self.imageViewSideLength)
        self.addSubview(plusMembersUILabel)
        return plusMembersUILabel
    }()
    lazy var rallyMemberImageView: UIImageView = {
        let rallyMemberImageView = UIImageView()
        rallyMemberImageView.frame = CGRect(x: 0,y: 0,width: self.imageViewSideLength,height: self.imageViewSideLength)
        self.addSubview(rallyMemberImageView)
        return rallyMemberImageView
    }()
    
    func xOffSet(_ index: Int)->CGFloat {
        return imageViewFirstColumnXOffset+CGFloat((index + 5) % 5)*(imageViewSpacing+imageViewSideLength)
    }
    
    func yOffSet(_ index: Int)->CGFloat {
        let rowSpacing: CGFloat = 11.0 // needs update in two places. could just increase offset without increasing height but that would make other calcs harder. by having spacing equal height, other calcs are super simple
        let textLabelHeight = self.rallyMemberNameLabel.font!.lineHeight+2 // +2 is padding between rally member picture and name
        let subviewsTotalHeight: CGFloat = imageViewSideLength+textLabelHeight+rowSpacing
        return subviewsTotalHeight*CGFloat(floor(Double(index)/5.0))
    }
}
