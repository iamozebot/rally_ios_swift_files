//
//  rallyTitleTextView.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation

class RallyTitleTextView {
    static func textViewDidBeginEditing(_ textView: UITextView) {
        /*Makes existing text in textView disappear if it is lightgray, this way lightgray text can be used as a placeholder to be displayed when not editing and if user hasn't added text*/
        print("Success")
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = MYColor.lightBlack
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "userEditingRallyDetailsIsTrue"), object: nil)
    }
    
    static func textViewDidEndEditing(_ textView: UITextView) {
        //Reassign textView text with Placeholder text if editing ends and the text field is empty
        print("Success")
        if textView.text.isEmpty {
            print("textViewDidEndEditing: textView.text.isEmpty")
            textView.text = "Untitled Rally"
            textView.textColor = UIColor.lightGray
        }
        /* NSNotificationCenter.defaultCenter().postNotificationName("userEditingRallyDetailsIsFalse", object: nil) */
    }
    
    static func textView(_ textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        // determine whether group name is too long. if characters<25, user may add additional or delete characters. If 25 characters, user may delete characters
        // replacementText = text
        let combinedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if combinedText.characters.count < 45 { // 25 is maximum number of characters group name may be
            print("textView:shouldChangeTextInRange:true")
            return true
        }
        else if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            print("textView:shouldChangeTextInRange:true")
            return true
        }
        else { // if user tries entering 26th digit, returns false
            print("textView:shouldChangeTextInRange:false")
            return false
        }
    }
}
