//
//  RandomColorRGBAlgorithm.swift
//  Rally
//
//  Created by Ian Moses on 1/14/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class RandomColorRGBAlgorithm {
    func generate() {
        for i in 0..<1000 {
            
            func someFunctionICreated() -> (Int,Int,Int) {
                // alt base two/three: use skewed distribution (different transform than box-muller (centered around baseOne, entering cutoff for each side: [255-baseOne, baseOne-1]))
                
                func proceedingBase(precedingBase: Int, multiplier: CGFloat)-> Int {
                    let minRange = min(255-precedingBase, precedingBase-1)
                    let fullRange = CGFloat(minRange*2)
                    let baseDiff: Int = Int(round(multiplier*fullRange))
                    let addToBaseTwo = minRange-baseDiff
                    let proceedingBase = precedingBase - addToBaseTwo
                    return proceedingBase
                }
                
                // generates two random, uniformly distributed numbers: (0,1)
                // transforms randomOne and randomTwo into normally distributed numbers
                let u360 = UInt32(360)
                let randomOne = CGFloat(arc4random_uniform(u360)+1)/360
                let r = (-2*log(randomOne)).squareRoot()
                let randomTwo = 0.01745*(CGFloat(arc4random_uniform(u360)+1))
                let uOne = r*cos(randomTwo)
                let uTwo = r*sin(randomTwo)
                
                // shift uOne and uTwo -> [r1,r2] to [r1-r1,r1+r2]
                let translatedUpperBound: CGFloat = 2*(-2*log(1/360)).squareRoot()
                // get base multipliers
                let baseOneMultiplier = 0.5+uOne/translatedUpperBound
                let baseTwoMultiplier = 0.5+uTwo/translatedUpperBound
                
                // get baseOne
                let baseOne = Int(arc4random_uniform(UInt32(248))+8) // first value never under 8 (less black backgrounds)
                
                // get baseTwo
                let baseTwo = proceedingBase(precedingBase: baseOne, multiplier: baseOneMultiplier)
                
                // get baseThree
                let baseThree = proceedingBase(precedingBase: baseTwo, multiplier: baseTwoMultiplier)
                
                return (baseOne, baseTwo, baseThree)
            }
            
            let (baseOne, baseTwo, baseThree) = someFunctionICreated()
            //UIColor(red: baseOne, green: baseTwo, blue: baseThree)
            print("case \(i): \n (r, g, b) = (\(baseOne), \(baseTwo), \(baseThree))")
        }
    }
}
