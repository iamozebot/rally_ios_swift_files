//
//  ReportAProblemClass.swift
//  Rally
//
//  Created by Ian Moses on 7/1/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class ReportAProblemClass: NSObject, UITextViewDelegate {
    let midGrey = UIColor( red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0 )
    var navigationControllerClassVariable: UINavigationController!
    var keyboardHeight: CGFloat = 0
    var textViewAndHeaderBarSuperView: UIView!
    var delegateInstance: UITextViewDelegate!
    var title: String!
    
    init(title: String) {
        self.title = title
    }
    
    // report a problem view generation handler
    func reportAProblemViewsHandler(_ navigationController: UINavigationController) {
        print("func reportAProblemViewsHandler is running...")
        navigationControllerClassVariable = navigationController
        startObservingKeyboardEvents()
        let cancelTitleAndSendSuperview = defineCancelTitleAndSendSuperView()
        let cancelButton = defineCancelButton(cancelTitleAndSendSuperview)
        cancelTitleAndSendSuperview.addSubview(cancelButton)
        let title = defineTitle(cancelTitleAndSendSuperview)
        cancelTitleAndSendSuperview.addSubview(title)
        let sendButton = defineSendButton(cancelTitleAndSendSuperview)
        cancelTitleAndSendSuperview.addSubview(sendButton)
        let textView = defineTextView()
        let label = defineTextViewLabel()
        textView.addSubview(label)
        textView.frame.origin.y = cancelTitleAndSendSuperview.frame.height // adjust textView to start underneath header bar
        textViewAndHeaderBarSuperView = defineTextViewAndHeaderBarSuperView()
        textViewAndHeaderBarSuperView.addSubview(textView) // add textview
        textViewAndHeaderBarSuperView.addSubview(cancelTitleAndSendSuperview) // add header bar
        print("textViewAndHeaderBarSuperView.superview, before:\(textViewAndHeaderBarSuperView.superview)")
        navigationControllerClassVariable.view.addSubview(textViewAndHeaderBarSuperView) // add superview to screen
        print("textViewAndHeaderBarSuperView.superview, after:\(textViewAndHeaderBarSuperView.superview)")
        // this is done to give textView focus, this also inherently triggers keyboard to display. keyboard displaying gives size info to properly size report a problem superview. keyboardwillshow method will trigger displayDullingAndReportAProblemView to properly animate view onto screen since it now has proper height info to calculate frame
        textView.becomeFirstResponder()
    }
    
    // report a problem cancel button
    func defineCancelButton(_ superView: UIView)-> UIButton {
        print("func defineCancelButton is running...")
        let cancelButton = UIButton(type: .system)
        cancelButton.titleLabel!.font = UIFont.systemFont(ofSize: 15.25)
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.sizeToFit()
        cancelButton.frame.origin.x = superView.frame.width*5/100
        cancelButton.center.y = superView.center.y
        cancelButton.tag = 925
        cancelButton.setTitleColor(MYColor.rallyBlue, for: UIControlState())
        cancelButton.addTarget(self, action: #selector(ReportAProblemClass.onCancelButtonTouch(_:)), for: .touchUpInside)
        return cancelButton
    }
    
    // report a problem title
    func defineTitle(_ superView: UIView)-> UILabel {
        print("func defineTitle is running...")
        let title = UILabel()
        title.text = self.title
        title.font = .boldSystemFont(ofSize: 15.25)
        title.sizeToFit()
        title.textColor = MYColor.lightBlack
        title.center = CGPoint(x: superView.center.x,y: superView.center.y)
        title.tag = 926
        return title
    }
    
    // report a problem send button
    func defineSendButton(_ superView: UIView)-> UIButton {
        print("func defineSendButton is running...")
        let sendButton = UIButton(type: .system)
        sendButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 15.25)
        sendButton.setTitle("Send", for: UIControlState())
        sendButton.sizeToFit()
        sendButton.frame.origin.x = superView.frame.width-superView.frame.width*5/100-sendButton.frame.width
        sendButton.center.y = superView.center.y
        sendButton.tag = 927
        sendButton.isUserInteractionEnabled = false
        sendButton.setTitleColor(midGrey, for: UIControlState())
        sendButton.addTarget(self, action: #selector(ReportAProblemClass.onSendButtonTouch(_:)), for: .touchUpInside)
        return sendButton
    }
    
    // report a problem cancel title and send super view
    func defineCancelTitleAndSendSuperView()->UIView {
        print("func defineTextView is running...")
        let cancelTitleAndSendSuperView = UIView(frame: CGRect(x: 0,y: 0,width: ScreenDimension.width*90/100,height: ScreenDimension.height*6/100))
        let border = defineCancelTitleAndSendSuperViewSublayer(cancelTitleAndSendSuperView.frame.width, viewHeight: cancelTitleAndSendSuperView.frame.height)
        cancelTitleAndSendSuperView.layer.addSublayer(border)
        cancelTitleAndSendSuperView.tag = 928
        return cancelTitleAndSendSuperView
    }
    
    func defineCancelTitleAndSendSuperViewSublayer(_ viewWidth: CGFloat, viewHeight: CGFloat)->CALayer {
        print("func defineTableViewRowSublayer is running...")
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = MYColor.darkGrey.cgColor
        border.frame = CGRect(x: 0, y: viewHeight-width, width:  viewWidth, height: width)
        border.borderWidth = width
        return border
    }
    
    // report a problem text view
    func defineTextView()->UITextView {
        print("func defineTextView is running...")
        let textView = UITextView(frame: CGRect(x: 0, y: 0, width: ScreenDimension.width*90/100, height: 0)) // adjust height later after sizing superview from keyboard height
        textView.tag = 929
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.textAlignment = .left
        textView.textColor = MYColor.lightBlack
        textView.isScrollEnabled = false
        textView.textContainer.maximumNumberOfLines = 14
        textView.delegate = self
        // http://stackoverflow.com/questions/28032973/implementing-uitextfielddelegate-in-a-separate-class
        delegateInstance = textView.delegate // need to create a strong reference to delegate b/c nothing owns it...I believe
        return textView
    }
    
    func defineTextViewLabel()->UILabel {
        let label = UILabel()
        label.text = "How can we improve?"
        let padding = ScreenDimension.width*1.9/100
        label.frame.origin = CGPoint(x: padding*70/100,y: padding)
        label.tag = 930
        label.font = .systemFont(ofSize: 18)
        label.sizeToFit()
        label.textColor = midGrey
        return label
    }
    
    // report a problem super view
    func defineTextViewAndHeaderBarSuperView()->UIView {
        print("func defineTextViewAndHeaderBarSuperView is running...")
        // define report A Problem Superview
        let textViewAndHeaderBarSuperView = UIView(frame: CGRect(x: ScreenDimension.width*5/100, y: ScreenDimension.height*5/100, width: ScreenDimension.width*90/100, height: ScreenDimension.height*60/100)) // height is determined by keyboard height, this is just a placeholder value
        textViewAndHeaderBarSuperView.tag = 706
        //textViewAndHeaderBarSuperView.hidden = true
        textViewAndHeaderBarSuperView.layer.cornerRadius = 2
        textViewAndHeaderBarSuperView.clipsToBounds = true
        textViewAndHeaderBarSuperView.backgroundColor = UIColor.white
        return textViewAndHeaderBarSuperView
    }
    
    func onCancelButtonTouch(_ sender: UIButton!) {
        print("func onCancelButtonTouch is running...")
        dismissDullingAndReportAProblemView(navigationControllerClassVariable.view)
    }
    
    func onSendButtonTouch(_ sender: UIButton!) {
        print("func onSendButtonTouch is running...")
        // get instance of text view
        sender.isUserInteractionEnabled = false // do not allow user to double-click button and thus send duplicate reports
        let textView = textViewAndHeaderBarSuperView.viewWithTag(929)! as! UITextView // 929 is textview tag
        // check textview for user inputted text
        if textView.text.characters.count > 0 {
            // initiate web service call to send user input to backend
            let intent = WebServiceCallIntent.addSupportRequest(textView.text)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.dismissDullingAndReportAProblemView(self.navigationControllerClassVariable.view)
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
            // in future versions add another box verifying to user that feedback was received, something like "Thank you, your feedback helps us make Rally better for everyone."
            // dismiss report a problem superView and dulling view
            // should technically use a completion handler to call below line on completion but if we don't receive the user's data package it doesn't make the app crash, we may just lose a couple feedback submissions at the end of the day, not worth the time...
        }
    }
    
    func defineSendButtonInteractionState(_ interactionStateValue: Bool) {
        print("func defineSendButtonInteractionState is running...")
        if let sendButton = textViewAndHeaderBarSuperView.viewWithTag(928)!.viewWithTag(927) as? UIButton { // 928 is cancel title and send button superview tag, and 927 is send button tag
            if interactionStateValue == false { // not sure if this will be updated or not yet, if not change the condition to something else
                sendButton.isUserInteractionEnabled = false
                sendButton.setTitleColor(midGrey, for: UIControlState())
            }
            else {
                sendButton.isUserInteractionEnabled = true
                sendButton.setTitleColor(MYColor.rallyBlue, for: UIControlState())
            }
        }
    }
    
    /*
    func displayDullingAndReportAProblemView(viewControllerView: UIView, dullingViewForViewControllerView: UIView) {
        print("func displayDullingAndReportAProblemView is running...")
        let padding = UIApplication.sharedApplication().statusBarFrame.height // doesn't need to be this but it's a good padding amount so I'm just using it
        textViewAndHeaderBarSuperView.frame.size.height = screenDimension.screenHeight-keyboardHeight-padding*3 // one padding amount is for the status bar height
        textViewAndHeaderBarSuperView.frame.origin.y = padding*2
        let cancelTitleAndSendSuperView = textViewAndHeaderBarSuperView.viewWithTag(928)!
        let textView = textViewAndHeaderBarSuperView.viewWithTag(929)! as! UITextView // 929 is textview tag
        textView.frame.size.height = textViewAndHeaderBarSuperView.frame.height-cancelTitleAndSendSuperView.frame.height // one padding amount is for the status bar height
        viewControllerView.addSubview(dullingViewForViewControllerView)
        viewControllerView.bringSubviewToFront(textViewAndHeaderBarSuperView)
    }*/
    
    func dismissDullingAndReportAProblemView(_ viewControllerView: UIView) {
        print("func dismissDullingAndReportAProblemView is running...")
        stopObservingKeyboardEvents()
        if let dullingViewForViewControllerView = viewControllerView.viewWithTag(165) {
            dullingViewForViewControllerView.removeFromSuperview()
        }
        let textView = textViewAndHeaderBarSuperView.viewWithTag(929)! as! UITextView // 929 is textview tag
        textView.resignFirstResponder()
        textViewAndHeaderBarSuperView.removeFromSuperview()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        print("textView text equals:\(text)")
        if text != "" {
            defineSendButtonInteractionState(true) // set to true
            textView.textColor = MYColor.lightBlack
            if let label = textView.viewWithTag(930) { // 930 is "How can we improve" label tag
                label.isHidden = true
            }
        }
        let combinedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        // Create attributed version of the text
        let attributedTextIncludingNewText = NSMutableAttributedString(string: combinedText)
        attributedTextIncludingNewText.addAttribute(NSFontAttributeName, value: textView.font!, range: NSMakeRange(0, attributedTextIncludingNewText.length))
        // may be error if font parameter is nil above
        // Get the padding of the text container
        let padding = textView.textContainer.lineFragmentPadding
        // Create a bounding rect size by subtracting the padding
        // from both sides and allowing for unlimited length
        let boundingSize = CGSize(width: textView.frame.size.width - padding * 2, height: CGFloat.greatestFiniteMagnitude)
        // Get the bounding rect of the attributed text in the
        // given frame
        let boundingRectIncludingNewText = attributedTextIncludingNewText.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        if text == "" { // allows user to backspace in the event phoneNumberDigitsAsString.characters.count <= maxtext in not true. This occurs at 10 digits
            print("textView:shouldChangeTextInRange text is empty, it's a backspace")
            if combinedText.characters.count == 0 {
                defineSendButtonInteractionState(false) // set to false
                if let label = textView.viewWithTag(930) { // 930 is "How can we improve" label tag
                    label.isHidden = false
                }
            }
            print("return true")
            return true
        }
        if boundingRectIncludingNewText.height >= textView.frame.height-textView.font!.lineHeight*2 {
            print("return false")
            return false
        }
        else {
            print("return true")
            print("textView.frame.height:\(textView.frame.height)")
            print("textView.frame.width:\(textView.frame.width)")
            print("boundingRectIncludingNewText.height:\(boundingRectIncludingNewText.height)")
            print("textView.textContainer.size.width:\(textView.textContainer.size.width)")
            print("textView.font!.lineHeight:\(textView.font!.lineHeight)")

            return true
        }
    }
    
    func startObservingKeyboardEvents() {
        print("func startObservingKeyboardEvents is running...")
        NotificationCenter.default.addObserver(self, selector: #selector(ReportAProblemClass.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object:nil)
    }
    
    func stopObservingKeyboardEvents() {
        print("func stopObservingKeyboardEvents is running...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    /*
    //may use this method as an alternative to dismiss textview focus if resignFirstResponder method isn't working as expected
    func dismissKeyboard() {
        print("func dismissKeyboard is running...")
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //http://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
        view.endEditing(true)
    }
    */
 }
