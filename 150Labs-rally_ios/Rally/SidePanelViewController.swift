//
//  SidePanelViewController.swift
//  Rally
//
//  Created by Ian Moses on 8/21/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import UIKit
import SwiftyJSON

class LeftPanelViewControllerDataSource {
    var community: Community?
    var communityNotificationsStatus: NotificationsStatus? {
        get {
            if let value = self.community {
                if let secondValue = value.notificationsStatus {
                    return secondValue
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
        set(newValue) {
            self.community?.notificationsStatus = newValue
        }
    }
    var activeThread: Thread? {
        get {
            if let value = self.community {
                if let secondValue = value.activeThread {
                    return secondValue
                }
                else {
                    return nil
                }
            }
            else {
                return nil
            }
        }
        set(newValue) {
            self.community?.activeThread = newValue
        }
    }
}


//, UIScrollViewDelegate, UITextViewDelegate

class LeftPanelViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchControllerDelegate, /*UISearchResultsUpdating,*/ UISearchBarDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, ThreadsDirectMessagesCollectionView {

    var leftPanelData = LeftPanelViewControllerDataSource()
    var leftPanelDataComputedProperty: LeftPanelViewControllerDataSource {
        get {
            return leftPanelData
        }
    }
    var tableView: UITableView!
    var delegate: SidePanelViewControllerDelegate?
    var profilePictureContainer = passThroughView() // subview of inviteView
    let menuContainer = UIView()
    let threadsTableView = UITableView()
    let directMessagesTableView = UITableView()
    let notificationsCommunityButton = UIButton()
    let inviteToCommunityButton = UIButton()
    let reportMemberButton = UIButton()
    let leaveCommunityButton = UIButton()
    let centerContainer = UIView()
    let totalMembersMenuLabel = UILabel()
    let menuSeparatorLine = UIView()
    let centerSeparatorLine = UIView()
    let centerContainerCommunityName = UILabel()
    var threadIndexes = [Int]() // current cell indexes within first section that are 'thread' cells (as opposed to thread member cells). this is updated everytime state changes occur (new threads, expand/collapse thread, etc)
    fileprivate lazy var collectionView: UICollectionView = {
        // NOTE: Includes two sections: 'threads' and 'direct messages'
        let yOrigin = self.centerSeparatorLine.frame.origin.y+self.centerSeparatorLine.frame.height
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: yOrigin, width: self.centerContainer.frame.width, height: ScreenDimension.height-yOrigin), collectionViewLayout: self.flowLayout)
        collectionView.register(StickyHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "stickyHeader")
        collectionView.register(CenterContainerCollectionViewCell.self, forCellWithReuseIdentifier: "centerContainerCollectionViewCell")
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        //collectionView.allowsMultipleSelection = true
        return collectionView
    }()
    fileprivate lazy var flowLayout: StickyHeadersCollectionViewFlowLayout = {
        let flowLayout = StickyHeadersCollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: self.centerContainer.frame.width, height: ScreenDimension.height/25)
        return flowLayout
    }()
    /*
    lazy var customSearchController: CustomSearchController = {
        let customSearchController = CustomSearchController(searchResultsController: nil)
        customSearchController.searchResultsUpdater = self
        customSearchController.dimsBackgroundDuringPresentation = false
        customSearchController.searchBar.barTintColor =  .white
        //customSearchController.searchBar.isTranslucent = true // for searchBar animation to avoid nav bar color showing through
        customSearchController.searchBar.layer.borderColor = MYColor.midGrey.cgColor
        customSearchController.searchBar.layer.borderWidth = 1.0
        customSearchController.searchBar.sizeToFit()
        customSearchController.searchBar.frame.size.width = self.centerContainer.frame.width*88/100
        customSearchController.searchBar.frame.origin.x = self.centerContainer.frame.width*4/100
        customSearchController.searchBar.frame.origin.y = self.centerContainer.frame.height*8.5/100
        customSearchController.hidesNavigationBarDuringPresentation = false
        customSearchController.delegate = self
        customSearchController.searchBar.delegate = self
        customSearchController.searchBar.placeholder = "Search community"
        
        /*
         let searchBarParentView = UIView(frame: CGRect(x: screenDimension.screenWidth*13.5/100,y: screenDimension.screenWidth*4/100,width: screenDimension.screenWidth*80/100,height: UINavigationBarTaller.navigationBarHeight-screenDimension.screenWidth*4/100))
         customSearchController!.searchBar.frame = CGRect(x: 0,y: 0,width: searchBarParentView.frame.width,height: customSearchController!.searchBar.frame.height)
         searchBarParentView.backgroundColor = MYColor.rallyBlue
         let rect = CGRect(x: 0, y: 0, width: customSearchController!.searchBar.frame.width, height: customSearchController!.searchBar.frame.height)
         UIGraphicsBeginImageContextWithOptions(customSearchController!.searchBar.frame.size, false, 0)
         MYColor.rallyBlue.setFill()
         UIRectFill(rect)
         let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
         UIGraphicsEndImageContext()
         customSearchController!.searchBar.setBackgroundImage(image, for: .any, barMetrics: .default)
         customSearchController!.searchBar.backgroundColor = UIColor.clear
         navigationController?.navigationBar.addSubview(searchBarParentView)
         */
        
        
        return customSearchController
    }()
    */
    var selectedItemIndexPath: IndexPath?
    
    override func viewDidLoad() {
        print("class LeftPanelViewController, func viewDidLoad is running...")
        super.viewDidLoad()
        view.backgroundColor = .white
        
        // initialize subviews
        defineMenuContainer()
        defineCenterContainer()
        
        // initialize subviews to subviews
        defineProfilePictureContainer()
        defineTotalMembersMenuLabel()
        //defineNotificationsCommunityButton()
        defineInviteToCommunityButton()
        defineReportMemberButton()
        defineLeaveCommunityButton()
        defineMenuSeparatorLine()
        defineCenterSeparatorLine()
        defineCenterContainerCommunityNameLabel()
        
        // add subviews to view
        view.addSubview(menuContainer)
        view.addSubview(centerContainer)
        
        // add subviews to subviews
        menuContainer.addSubview(profilePictureContainer)
        //menuContainer.addSubview(notificationsCommunityButton)
        menuContainer.addSubview(inviteToCommunityButton)
        menuContainer.addSubview(reportMemberButton)
        menuContainer.addSubview(leaveCommunityButton)
        menuContainer.addSubview(totalMembersMenuLabel)
        menuContainer.addSubview(menuSeparatorLine)
        menuContainer.addSubview(centerSeparatorLine)
        centerContainer.addSubview(centerContainerCommunityName)
        centerContainer.addSubview(collectionView)
        // NOTE: Commented out for v1.0 release
        //centerContainer.addSubview(customSearchController.searchBar)
        print("class LeftPanelViewController, func viewDidLoad is finished running...")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class CommunityMessagingViewController, func viewWillAppear is running...")
        super.viewWillAppear(animated)
    }
    
    func reloadUI() {
        print("func reloadUI is running...")
        
        // reload blue menu bar (profile picture & total member label)
        defineProfilePictureContainer()
        defineTotalMembersMenuLabel()
        
        // reload community name
        defineCenterContainerCommunityNameLabel()
        
        // reload collectionview data
        collectionView.reloadData()
    }
    
    func defineMenuContainer() {
        print("func defineMenuContainer is running...")
        let containerWidth: CGFloat = ScreenDimension.width*17.9/100
        let containerHeight: CGFloat = ScreenDimension.height
        menuContainer.frame.size = CGSize(width:containerWidth,height:containerHeight)
        menuContainer.backgroundColor = MYColor.rallyBlue
    }
    
    func defineCenterContainer() {
        print("func defineCenterContainer is running...")
        let centerContainerXOrigin: CGFloat = menuContainer.frame.origin.x+menuContainer.frame.width
        // duplicate definition from containerVC, having trouble calling self.parent so moving on for now
        let centerPanelExpandedOffset: CGFloat = ScreenDimension.width*71.8/100
        let centerContainerWidth: CGFloat = centerPanelExpandedOffset-menuContainer.frame.width
        let centerContainerHeight: CGFloat = ScreenDimension.height
        centerContainer.frame = CGRect(x:centerContainerXOrigin,y:0,width:centerContainerWidth,height:centerContainerHeight)
    }
    
    func defineProfilePictureContainer() {
        print("func defineProfilePictureContainer is running...")
        //CommunityProfilePictureVersions.communityMenu
        if let community = leftPanelData.community {
            let sideLength = menuContainer.frame.width*39/100
            let picturesDisplayData: PicturesDisplayDataSource = PicturesDisplayDataSource(nil, community.members, profilePictureContainer, .userJoinedCommunity(nil), sideLength)
            profilePictureContainer = PicturesDisplayHelper(picturesDisplayData: picturesDisplayData).defineRallyOrGroupCardPictures()
            profilePictureContainer.frame.size = CGSize(width:profilePictureContainer.subviews[0].frame.width*2,height:profilePictureContainer.subviews[0].frame.width*2)
            profilePictureContainer.center.x = menuContainer.center.x
            profilePictureContainer.frame.origin.y = (menuContainer.frame.width-sideLength)/2
            profilePictureContainer.layer.cornerRadius = profilePictureContainer.frame.height/2
            profilePictureContainer.clipsToBounds = true
        }
        //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        // edit community profile picture feature to be added in v1.1
        /*
         let handleProfilePictureViewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(EditProfileViewController.onEditButtonTouch(_:)))
         profilePictureContainer.addGestureRecognizer(handleProfilePictureViewTap)
         */
    }
    
    func defineTotalMembersMenuLabel() {
        print("func defineTotalMembersMenuLabel is running...")
        var totalMembers = 0
        if let community = leftPanelData.community {
            totalMembers = community.members.count
        }
        // set text
        if totalMembers != 1 {
            totalMembersMenuLabel.text = "\(totalMembers) members"
        }
        else {
            totalMembersMenuLabel.text = "1 member"
        }
        // set frame
        totalMembersMenuLabel.sizeToFit()
        totalMembersMenuLabel.frame.size.width = menuContainer.frame.width*92/100
        totalMembersMenuLabel.center.x = menuContainer.center.x
        totalMembersMenuLabel.center.y = ScreenDimension.height*14.75/100
        totalMembersMenuLabel.textColor = .white
        // set frame
        var fontSize: CGFloat = 11
        totalMembersMenuLabel.font = UIFont.systemFont(ofSize: fontSize)
        let minSize: CGFloat = 5 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
        while fontSize > minSize &&  totalMembersMenuLabel.sizeThatFits(CGSize(width: totalMembersMenuLabel.frame.width,height: totalMembersMenuLabel.frame.height)).width >= totalMembersMenuLabel.frame.size.width {
            fontSize -= 1.0
            totalMembersMenuLabel.font = UIFont.systemFont(ofSize: fontSize)
        }
        totalMembersMenuLabel.sizeToFit()
        totalMembersMenuLabel.center.x = menuContainer.center.x
    }
    
    func defineMenuSeparatorLine() {
        print("func defineMenuSeparatorLine is running...")
        let width = menuContainer.frame.width*69/100
        let height = ScreenDimension.height*0.17/100
        menuSeparatorLine.frame.size = CGSize(width:width,height:height)
        menuSeparatorLine.backgroundColor = .white
        menuSeparatorLine.center.x = menuContainer.center.x
        menuSeparatorLine.center.y = ScreenDimension.height*16.5/100
    }
    
    func defineCenterSeparatorLine() {
        print("func defineCenterSeparatorLine is running...")
        let width = centerContainer.frame.width
        let height = ScreenDimension.height*0.17/100
        centerSeparatorLine.frame.size = CGSize(width:width,height:height)
        centerSeparatorLine.backgroundColor = MYColor.darkGrey
        centerSeparatorLine.center.x = centerContainer.center.x
        centerSeparatorLine.center.y = ScreenDimension.height*16.5/100
    }
    
    func defineCenterContainerCommunityNameLabel() {
        print("func defineCenterContainerCommunityNameLabel is running...")
        //centerContainerCommunityName.frame = CGRect(x:,y:,width:centerContainer.frame.width,height:)
        // set text
        centerContainerCommunityName.text = "Untitled Community"
        if let community = leftPanelData.community {
            centerContainerCommunityName.text = community.name
        }
        // set frame
        centerContainerCommunityName.sizeToFit()
        centerContainerCommunityName.frame.size.width = centerContainer.frame.width*86/100
        centerContainerCommunityName.frame.origin.x = centerContainer.frame.width*3.05/100
        centerContainerCommunityName.center.y = ScreenDimension.height*3.9/100
        centerContainerCommunityName.textColor = MYColor.lightBlack
        // set frame
        var fontSize: CGFloat = 19
        centerContainerCommunityName.font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightMedium)
        let minSize: CGFloat = 15 // allows setting a minimum font size to assure title is readable if title character length restriction is removed, for now this doesn't affect while logic statement
        while fontSize > minSize &&  centerContainerCommunityName.sizeThatFits(CGSize(width: centerContainerCommunityName.frame.width,height: centerContainerCommunityName.frame.height)).width >= centerContainerCommunityName.frame.size.width {
            fontSize -= 1.0
            centerContainerCommunityName.font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightMedium)
        }
        centerContainerCommunityName.sizeToFit()
        print("func defineCenterContainerCommunityNameLabel is finished running...")
    }
    
    func defineNewThreadButton(headerHeight: CGFloat) -> UIButton {
        print("func defineNewThreadButton is running...")
        let newThreadButton = UIButton()
        newThreadButton.setImage(UIImage(named:"add")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        newThreadButton.imageView!.tintColor = MYColor.inactiveGrey
        let inset = ScreenDimension.width*1.95/100
        newThreadButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        newThreadButton.sizeToFit()
        newThreadButton.frame.origin.x = centerContainer.frame.width-newThreadButton.frame.width
        newThreadButton.frame.origin.y = headerHeight*92.75/100-newThreadButton.frame.height
        newThreadButton.addTarget(self, action: #selector(LeftPanelViewController.onNewThreadButtonTouch(_:)), for: .touchUpInside)
        newThreadButton.isUserInteractionEnabled = true
        guard leftPanelData.community != nil else {
            newThreadButton.isUserInteractionEnabled = false
            newThreadButton.imageView!.tintColor = MYColor.inactiveMenuItem
            return newThreadButton
        }
        return newThreadButton
    }
    
    func defineNotificationsCommunityButton() {
        print("func defineNotificationsCommunityButton is running...")
        notificationsCommunityButton.imageView!.tintColor = .white
        toggleCommunityNotificationsStatus(nil) // sets correct picture for current notification status
        let inset = menuContainer.frame.width*10.75/100
        notificationsCommunityButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        notificationsCommunityButton.sizeToFit()
        notificationsCommunityButton.center.x = menuContainer.center.x
        notificationsCommunityButton.center.y = ScreenDimension.height*22.5/100
        //notificationsCommunityButton.frame = CGRect(x:, y:, width:, height:)
        notificationsCommunityButton.addTarget(self, action: #selector(LeftPanelViewController.toggleCommunityNotificationsStatus(_:)), for: .touchUpInside)
    }
    
    func defineInviteToCommunityButton() {
        print("func defineNotificationsCommunityButton is running...")
        inviteToCommunityButton.setImage(UIImage(named:"addMember")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        inviteToCommunityButton.imageView!.tintColor = .white
        let inset = menuContainer.frame.width*10.75/100
        inviteToCommunityButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        inviteToCommunityButton.sizeToFit()
        inviteToCommunityButton.center.x = menuContainer.center.x
        inviteToCommunityButton.center.y = ScreenDimension.height*22.5/100
        //notificationsCommunityButton.frame = CGRect(x:, y:, width:, height:)
        inviteToCommunityButton.addTarget(self, action: #selector(LeftPanelViewController.onInviteToCommunityButtonTouch(_:)), for: .touchUpInside)
    }
    
    func defineReportMemberButton() {
        print("func defineReportMemberButton is running...")
        reportMemberButton.setImage(UIImage(named:"reportMember")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        reportMemberButton.imageView!.tintColor = .white
        let inset = menuContainer.frame.width*10.75/100
        reportMemberButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        reportMemberButton.sizeToFit()
        reportMemberButton.center.x = menuContainer.center.x*1.05
        reportMemberButton.center.y = ScreenDimension.height*31.5/100
        reportMemberButton.addTarget(self, action: #selector(LeftPanelViewController.onReportMemberButtonTouch(_:)), for: .touchUpInside)
    }
    
    func defineLeaveCommunityButton() {
        print("func defineLeaveCommunityButton is running...")
        leaveCommunityButton.setImage(UIImage(named:"leave")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        leaveCommunityButton.imageView!.tintColor = .white
        let inset = menuContainer.frame.width*10.75/100
        leaveCommunityButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        leaveCommunityButton.sizeToFit()
        leaveCommunityButton.center.x = menuContainer.center.x
        leaveCommunityButton.center.y = ScreenDimension.height*40/100
        leaveCommunityButton.addTarget(self, action: #selector(LeftPanelViewController.onLeaveCommunityButtonTouch(_:)), for: .touchUpInside)
        leaveCommunityButton.isUserInteractionEnabled = true
        guard leftPanelData.community != nil else {
            leaveCommunityButton.isUserInteractionEnabled = false
            leaveCommunityButton.imageView!.tintColor = MYColor.inactiveMenuItem
            return
        }
    }
    
    func onNewThreadButtonTouch(_ sender: UIButton?) {
        print("func onNewThreadButtonTouch is running...")
        let createThreadViewControllerInstance: CreateThreadViewController = CreateThreadViewController(nibName: nil, bundle: nil)
        let secondaryContainerVC = self.parent as! ContainerViewController // messagingVC & leftSidePanelVC container
        //let primaryContainerVC = secondaryContainerVC.parent as! ContainerViewController
        let communityMessagingViewControllerVCInstance = secondaryContainerVC.centerNavigationControllerCP.viewControllers.first as? CommunityMessagingViewController
        //secondaryContainerVC.view.frame.origin.y = 80
        //primaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = false
        //secondaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = true
        createThreadViewControllerInstance.delegate = communityMessagingViewControllerVCInstance
        secondaryContainerVC.navigationController!.pushViewController(createThreadViewControllerInstance, animated: false)
    }
    
    func toggleCommunityNotificationsStatus(_ sender: UIButton?) {
        print("func toggleCommunityNotificationsStatus is running...")
        guard let notificationsStatus = leftPanelData.communityNotificationsStatus else {
            // use notifications on as default
            notificationsCommunityButton.setImage(UIImage(named:"notificationsOn")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
            // disable user interaction (given there is no associated  community)
            notificationsCommunityButton.isUserInteractionEnabled = false
            notificationsCommunityButton.imageView!.tintColor = MYColor.inactiveMenuItem
            return
        }
        notificationsCommunityButton.isUserInteractionEnabled = true
        notificationsCommunityButton.imageView!.tintColor = .white
        if notificationsStatus == .muted {
            if sender != nil { // if non-nil, means button was pressed to toggle status
                leftPanelData.communityNotificationsStatus = .active
            }
        }
        else {
            if sender != nil { // if non-nil, means button was pressed to toggle status
                leftPanelData.communityNotificationsStatus = .muted
            }
        }
        switch leftPanelData.communityNotificationsStatus! {
        case .muted:
            notificationsCommunityButton.setImage(UIImage(named:"notificationsOff")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        case .active:
            notificationsCommunityButton.setImage(UIImage(named:"notificationsOn")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        }
    }
    
    func defineCollectionViewHeaderSublayer(edge: Edge, cellWidth: CGFloat, cellHeight: CGFloat) -> CALayer {
        print("func defineCollectionViewHeaderSublayer is running...")
        let border = CALayer()
        let width: CGFloat = 0.5
        border.borderWidth = width
        border.borderColor = MYColor.darkGrey.cgColor
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0.0, width:  cellWidth, height: width)
        case .bottom:
            border.frame = CGRect(x: 0, y: (cellHeight-width)*89/100, width:  cellWidth, height: width)
        default:
            break
        }
        return border
    }
    
    /*
    func defineLeaveCommunityConfirmationDialogueBox() {
        print("func defineLeaveCommunityConfirmationDialogueBox is running...")
        
    }
    */
    
    func onInviteToCommunityButtonTouch(_ sender: UIButton?) {
        print("func onInviteToCommunityButtonTouch is running...")
        // add code to segue to inviteVC
    }
    
    func onReportMemberButtonTouch(_ sender: UIButton?) {
        print("func onReportMemberButtonTouch is running...")
        ReportAProblemClass(title: "Report Member").reportAProblemViewsHandler(navigationController!)
    }
    
    func onLeaveCommunityButtonTouch(_ sender: UIButton?) {
        print("func onLeaveCommunityButtonTouch is running...")
        //defineLeaveCommunityConfirmationDialogueBox()
        
        /*
         let leaveCommunityConfirmationDialogueSuperivew = UIView()
         leaveCommunityConfirmationDialogueSuperivew.frame.size = CGSize(width: screenDimension.screenWidth*25/100,height:screenDimension.screenHeight*15/100)
         leaveCommunityConfirmationDialogueSuperivew.center = CGPoint(x:screenDimension.screenWidth/2,y: screenDimension.screenHeight/2)
         let dismissLeaveCommunityButton = UIButton()
         let width = leaveCommunityConfirmationDialogueSuperivew.frame.width/2
         dismissLeaveCommunityButton.frame.size = CGSize(width: width, height: )
         let yOrigin = leaveCommunityConfirmationDialogueSuperivew.frame.height-dismissLeaveCommunityButton.frame.height
         dismissLeaveCommunityButton.frame.origin = CGPoint(x: 0.0, y: yOrigin)
         let confirmLeaveCommunityButtonTouch = UIButton()
         confirmLeaveCommunityButtonTouch.frame.size = CGSize(width: , height: )
         confirmLeaveCommunityButtonTouch.frame.origin = CGPoint(x: 0.0, y: yOrigin)
         
         leaveCommunityConfirmationDialogueSuperivew.addSubview(dismissLeaveCommunityButton)
         leaveCommunityConfirmationDialogueSuperivew.addSubview(confirmLeaveCommunityButtonTouch)
         */
        
        var presentingViewController: UIViewController = self
        if let searchController = self.presentedViewController {
            // do nothing, something is already presented
            presentingViewController = searchController
        }
        // do something, present alert
        // http://stackoverflow.com/questions/25511945/swift-alert-view-ios8-with-ok-and-cancel-button-which-button-tapped
        let communityName = leftPanelData.community!.name
        let alert = UIAlertController(title: "Leave \(communityName)?", message: "Are you sure you would like to leave the community?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Leave Community", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            // disallow user from multi-pressing buttons
            alert.actions[0].isEnabled = false
            alert.actions[1].isEnabled = false
            UIApplication.shared.beginIgnoringInteractionEvents()
            // leave community (submits to WCF which returns result, if successful,
            
            // make request to server for user to leave community
            print("about to send backend request to leave community: \(self.leftPanelData.community!)")
            print("leftPanelData.community!.communityID: \(self.leftPanelData.community!.communityID)")
            let intent = WebServiceCallIntent.leaveCommunity(communityID: self.leftPanelData.community!.communityID)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    // Do not set new active community
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.leaveCommunity()
                    })
                }
                else { // post message saying could not update profile pic. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                        UIApplication.shared.endIgnoringInteractionEvents()
                    })
                }
            })
        }))
        alert.addAction(UIAlertAction(title: "Nevermind", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            alert.dismiss(animated: true, completion: nil)
        }))
        presentingViewController.present(alert, animated: true, completion: nil)
    }
    /*
    func onDismissLeaveCommunityButtonTouch(_ sender: UIButton?) {
        print("func onDismissLeaveCommunityButtonTouch is running...")
        
    }
    
    func onConfirmLeaveCommunityButtonTouch(_ sender: UIButton?) {
        print("func onConfirmLeaveCommunityButtonTouch is running...")

    }
    */
    func leaveCommunity() {
        print("func leaveCommunity is running...")
        // bring user back to home screen collapsing community messaging VC
        let secondaryContainerVC = self.parent as! ContainerViewController // messagingVC & leftSidePanelVC container
        let primaryContainerVC = secondaryContainerVC.parent as! ContainerViewController
        primaryContainerVC.displayStatusBar()
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        /*
        if customSearchController.searchBar.isFirstResponder {
            view.endEditing(true)
            //customSearchController.searchBar.resignFirstResponder()
            //view.removeGestureRecognizer(gestureRecognizer)
            //self.customSearchController.isActive = false
        }
         */
        secondaryContainerVC.animateCenterPanelXPosition(targetPosition: 0) { finished in
            secondaryContainerVC.currentState = .bottomPanelExpanded
            secondaryContainerVC.view.frame.origin.y = 80
            primaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = false
            secondaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = true
            secondaryContainerVC.animateCenterPanelYPosition(targetPosition: ScreenDimension.height-ScreenDimension.threadBarHeight) { _ in
                secondaryContainerVC.currentState = .bothCollapsed
                // post notification user profile pic has changed
                DispatchQueue.main.async(execute: { () -> Void in
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "leaveCommunityResultReturnedFromWCF"), object: nil)
                })
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
    }
    
    func display(thread: Thread) {
        print("func display:thread is running...")
        let secondaryContainerVC = self.parent as! ContainerViewController // messagingVC & leftSidePanelVC container
        let primaryContainerVC = secondaryContainerVC.parent as! ContainerViewController
        primaryContainerVC.displayStatusBar()
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        /*
        if customSearchController.searchBar.isFirstResponder {
            view.endEditing(true)
        }
        */
        secondaryContainerVC.animateCenterPanelXPosition(targetPosition: 0) { finished in
        }
    }
    
// collectionView methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("func numberOfSections In CollectionView is running...")
        return 2 // 'THREADS' and 'DIRECT MESSAGES'
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("func collectionView numberOfItemsInSection is running...")
        guard leftPanelData.community != nil else {
            return 0
        }
        switch section {
        case 0:
            threadIndexes.removeAll() // need to reset array every time collectionView reloads
            var numberOfItems = 0 // don't need to account for supplementary view, but need one cell minimum in section to draw supplementary view (swift functions are written requiring 1, I guess, breaks otherwise)
            for thread in leftPanelData.community!.threads { // there should always be 1 thread (community chat)
                threadIndexes.append(numberOfItems) // index starts at 0
                numberOfItems += 1
                if thread.currentState == .expanded { // if expanded, need additional cell to display each member in thread
                    numberOfItems += thread.members.count
                }
            }
            return numberOfItems
        case 1:
            return leftPanelData.community!.members.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("func collectionView:sizeForItemAtIndexPath is running...")
        /*if (indexPath as NSIndexPath).row < 7 {
            return CGSize(width: collectionView.frame.width/7, height: (containerViewHeight*90.25/100)/12) // should be 20/7, rounded to 2.86 because sunday was bleeding into next row. ultimately, need to get rid of all these hard coded numbers...
        }
        else {
            let collectionViewHeight = containerViewHeight*90.25/100
            return CGSize(width: collectionView.frame.width/7, height: (collectionViewHeight-collectionViewHeight/6.75-collectionViewHeight/12)/6) // should be 20/7, rounded to 2.86 because sunday was bleeding into next row. ultimately, need to get rid of all these hard coded numbers...
        }*/
        return CGSize(width: centerContainer.frame.width, height: centerContainer.frame.height*4.5/100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("func collectionView cellForItemAt is running...")
        let cell: CenterContainerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "centerContainerCollectionViewCell", for: indexPath) as! CenterContainerCollectionViewCell
        //cell.backgroundColor = UIColor(red:0.2, green:0.25, blue:0.3, alpha:1.0)
        for subview in cell.contentView.subviews {
            subview.removeFromSuperview()
        }
        
        let myBackView = UIView(frame:cell.frame)
        myBackView.backgroundColor = MYColor.selectedThread
        cell.selectedBackgroundView = myBackView
        
        
        //cell.backgroundColor = nil
        
        guard let community = leftPanelData.community else {
            return cell
        }
        switch indexPath.section {
        case 0:
            //if indexPath.row > 0 { // 1 cell required for header section, need to check if empty cell or thread
            let row = indexPath.row
            let isThread: Bool = threadIndexes.contains(row) // threadindex starts at 0 for first index value
            if isThread {
                print("thread row: generate thread subviews")
                let x = indexPath.row
                let closest = threadIndexes.enumerated().min( by: { abs($0.1 - x) < abs($1.1 - x) && x >= $0.1} )!
                let nearestThreadIndex = closest.offset
                let expandCollapseButton = defineExpandCollapseButton(for: community.threads[nearestThreadIndex], superViewHeight: cell.frame.height)
                cell.contentView.addSubview(expandCollapseButton)
                if community.threads[nearestThreadIndex].notifications != 0 { // check if thread has new notifications, if so add notifications box aside thread title
                    let notificationBox = defineThreadNotificationBoxes(superViewHeight: cell.frame.height, numberOfNotifications: 3)
                    cell.contentView.addSubview(notificationBox)
                }
                if let title = community.threads[nearestThreadIndex].title {
                    cell.setUpCellWith(text: title)
                    cell.viewWithTag(212)?.frame.origin.x = expandCollapseButton.frame.origin.x+expandCollapseButton.frame.width
                    cell.viewWithTag(212)?.center.y = cell.frame.height/2
                }
                
                //if let value = selectedItemIndexPath {
                if let activeThread = leftPanelData.activeThread {
                    let index: Int = threadIndexes.index(of: row)!
                    print("index:\(index)")
                    print("activeThread.communityID:\(activeThread.communityID)")
                    print("community.threads[index].communityID:\(community.threads[index].communityID)")
                    if community.threads[index].threadID == activeThread.threadID {
                        self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
                        cell.isSelected = true
                    }
                }
                //}
            }
            else {
                // get reference to correct member
                // 1. find nearest less than index value inside threadIndexes array
                // 2. the difference between the two is the index of the member to display +1, so -1 to offset as index for members array
                print("member row: generate member subviews")
                let x = indexPath.row
                let closest = threadIndexes.enumerated().min( by: { abs($0.1 - x) < abs($1.1 - x) && x >= $0.1} )!
                let nearestThreadIndex = closest.offset // i.e. offset = 1 (second thread),
                var aboveExpandedMembers = 0
                for i in 0..<nearestThreadIndex { // loop through all prior threads, if expanded add member count to aboveExpandedMembers to account for correct member index in 'diff' calculation below
                    // if respective thread
                    if community.threads[i].currentState == .expanded {
                        aboveExpandedMembers += community.threads[i].members.count
                    }
                }
                let diff = indexPath.row-nearestThreadIndex-1-aboveExpandedMembers
                let member = community.threads[nearestThreadIndex].members[diff]
                // add member profile picture
                let memberProfilePicture = defineMemberProfilePicture(superViewHeight: cell.frame.height, member: member)
                cell.contentView.addSubview(memberProfilePicture)
                if let firstName = member.firstName {
                    cell.setUpCellWith(text: firstName)
                    // tag 212 is cell's custom label
                    cell.viewWithTag(212)?.frame.origin.x = memberProfilePicture.frame.origin.x+memberProfilePicture.frame.width+centerContainer.frame.width*2/100
                    cell.viewWithTag(212)?.center.y = cell.frame.height/2
                }
            }
            //}
        case 1:
            //print("members.count:\(community.members.count)")
            let member = community.members[indexPath.row]
            if let firstName = member.firstName {
                cell.setUpCellWith(text: firstName)
            }
            
            // add member profile picture
            let memberProfilePicture = defineMemberProfilePicture(superViewHeight: cell.frame.height, member: member)
            cell.contentView.addSubview(memberProfilePicture)
            
            // align text first name next to profile picture
            if let firstName = member.firstName {
                cell.setUpCellWith(text: firstName)
                // tag 212 is cell's custom label
                cell.viewWithTag(212)?.frame.origin.x = memberProfilePicture.frame.origin.x+memberProfilePicture.frame.width+centerContainer.frame.width*2/100
                cell.viewWithTag(212)?.center.y = cell.frame.height/2
            }
        default:
            break
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("func collectionView:didSelectItemAtIndexPath is running...")
        //display(thread)
        //let thread = leftPanelData.community!.threads[indexPath.row]
        //let cell = collectionView.cellForItem(at: indexPath)
        //print("cell?.isSelected:\(cell?.isSelected)")
        switch indexPath.section {
        case 0:
            //let isThread: Bool = threadIndexes.contains(indexPath.row)
            if let index = threadIndexes.index(of: indexPath.row) {
                let thread = leftPanelData.community!.threads[index]
                displayThreadOnMessagingVC(activeThread: thread)
                
                /*
                let tecell = collectionView.cellForItem(at: indexPath)
                for subview in tecell!.contentView.subviews {
                    subview.removeFromSuperview()
                }*/
            }
            else {
                /*
                 let x = indexPath.row
                 let closest = threadIndexes.enumerated().min( by: { abs($0.1 - x) < abs($1.1 - x) && x >= $0.1} )!
                 let nearestThreadIndex = closest.offset
                 let diff = indexPath.row-nearestThreadIndex-1
                 let member = leftPanelData.community!.threads[nearestThreadIndex].members[diff]
                 // add member profile picture
                 let memberProfilePicture = defineMemberProfilePicture(superViewHeight: cell.frame.height, member: member)
                 cell.addSubview(memberProfilePicture)
                 //if let firstName = member.firstName {
                 cell.setUpCellWith(text: "fake friends")
                 // tag 212 is cell's custom label
                 cell.viewWithTag(212)?.frame.origin.x = memberProfilePicture.frame.origin.x+memberProfilePicture.frame.width
                 cell.viewWithTag(212)?.center.y = cell.frame.height/2
                 //}
                 */
            }
        case 1:
            print("direct message with community member")
            
        default:
            break
        }
        /*
         let myBackView = UIView(frame:cell.frame)
         myBackView.backgroundColor = MYColor.selectedThread
         cell.selectedBackgroundView = myBackView
        
        if community.threads[nearestThreadIndex].currentState == .expanded {
            
        }
         */
        
        //communityMessagingVC.communityMessagingData.activeThread = MYColor.selectedThread
        //let indexPath = for: thread
        
        //let cell = collectionView.cellForItem(at: indexPath)
        //cell?.backgroundColor = MYColor.selectedThread
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("func collectionView viewForSupplementaryElementOfKind is running...")
        let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "stickyHeader", for: indexPath) as! StickyHeaderView
        for childView in supplementaryView.subviews {
            if(childView is UIButton) {
                childView.removeFromSuperview()
            }
        }
        switch indexPath.section {
        case 0:
            supplementaryView.setupStickyHeaderView(headerText: "MY THREADS", centerContainer: centerContainer)
            let newThreadButton = defineNewThreadButton(headerHeight: supplementaryView.frame.height)
            supplementaryView.addSubview(newThreadButton)
        case 1:
            supplementaryView.setupStickyHeaderView(headerText: "DIRECT MESSAGES", centerContainer: centerContainer)
        default:
            break
        }
        supplementaryView.backgroundColor = .white
        // add top and bottom border to header view
        //let topBorder = defineCollectionViewHeaderSublayer(edge: .top, cellWidth: supplementaryView.frame.width, cellHeight: supplementaryView.frame.height)
        let bottomBorder = defineCollectionViewHeaderSublayer(edge: .bottom, cellWidth: supplementaryView.frame.width, cellHeight: supplementaryView.frame.height)
        //supplementaryView.layer.addSublayer(topBorder)
        supplementaryView.layer.addSublayer(bottomBorder)
        return supplementaryView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        print("func collectionView layout collectionViewLayout is running...")
        let headerHeight = ScreenDimension.height*8/100
        return CGSize(width: collectionView.frame.width, height: headerHeight)
    }
    
    func onExpandCollapseButtonTouch(_ sender: UIButton!) {
        print("func onExpandCollapseButtonTouch is running...")
        let expandCollapseButton = sender as! MYCollapseExpandButton
        switch expandCollapseButton.thread.currentState {
        case .collapsed:
            //expandCollapseButton.imageView!.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
            expandCollapseButton.thread.currentState = .expanded
        case .expanded:
            //expandCollapseButton.imageView!.transform = CGAffineTransform(rotationAngle: 0.0)
            expandCollapseButton.thread.currentState = .collapsed
        }
        expandCollapseButton.removeFromSuperview()
        collectionView.reloadData()
    }
    
    func defineExpandCollapseButton(for thread: Thread, superViewHeight: CGFloat)-> MYCollapseExpandButton {
        print("func defineExpandCollapseButton is running...")
        let expandCollapseButton = MYCollapseExpandButton(thread)
        expandCollapseButton.setImage(UIImage(named:"navigateNext")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        if thread.currentState == .expanded {
            expandCollapseButton.imageView!.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        }
        expandCollapseButton.imageView!.tintColor = MYColor.detailButtonTitleGrey
        let inset = ScreenDimension.width*1.1/100
        expandCollapseButton.imageEdgeInsets = UIEdgeInsetsMake(inset,inset,inset,inset)
        expandCollapseButton.sizeToFit()
        expandCollapseButton.frame.origin.x = centerContainer.frame.width*2.5/100
        expandCollapseButton.center.y = superViewHeight/2
        expandCollapseButton.addTarget(self, action: #selector(LeftPanelViewController.onExpandCollapseButtonTouch(_:)), for: .touchUpInside)
        expandCollapseButton.isUserInteractionEnabled = true
        return expandCollapseButton
    }
    
    func defineThreadNotificationBoxes(superViewHeight: CGFloat, numberOfNotifications: Int)-> UIView {
        print("func defineThreadNotificationBoxes is running...")
        let notificationBox = UIView()
        notificationBox.backgroundColor = MYColor.rallyBlue
        notificationBox.frame.size = CGSize(width:superViewHeight*77/100,height:superViewHeight*77/100)
        notificationBox.frame.origin.x = centerContainer.frame.width*95/100-notificationBox.frame.width
        notificationBox.center.y = superViewHeight/2
        let numberOfNotificationsLabel = UILabel()
        numberOfNotificationsLabel.text = String(numberOfNotifications)
        numberOfNotificationsLabel.font = UIFont.systemFont(ofSize: 15)
        numberOfNotificationsLabel.sizeToFit()
        numberOfNotificationsLabel.textColor = .white
        numberOfNotificationsLabel.center.y = notificationBox.frame.height/2
        numberOfNotificationsLabel.center.x = notificationBox.frame.width/2
        notificationBox.addSubview(numberOfNotificationsLabel)
        return notificationBox
    }
    
    func defineMemberProfilePicture(superViewHeight: CGFloat, member: Friend)-> UIImageView {
        print("func defineMemberProfilePicture is running...")
        let profilePictureView = UIImageView()
        let sideLength: CGFloat = centerContainer.frame.width*14/100
        let xOffset: CGFloat = centerContainer.frame.width*7/100
        let yOffset: CGFloat = (superViewHeight-sideLength)/2
        profilePictureView.frame = CGRect(x: xOffset, y: yOffset, width: sideLength, height: sideLength)
        profilePictureView.image = member.getProfileImage()
        profilePictureView.layer.cornerRadius = profilePictureView.frame.height/2
        profilePictureView.clipsToBounds = true
        return profilePictureView
    }
    
    func displayThreadOnMessagingVC(activeThread: Thread) {
        print("func displayThreadOnMessagingVC is running...")
        // set active thread
        
        // animate back to community messagingVC
        let containerViewController = self.parent as! ContainerViewController
        let communityMessagingVC: CommunityMessagingViewController = containerViewController.centerNavigationControllerCP.viewControllers.first as! CommunityMessagingViewController
        communityMessagingVC.communityMessagingData.activeThread = activeThread
        // fetch messages for active thread
        communityMessagingVC.fetchActiveThreadMessagesFromWebService()
        // on completion, reload UI
        communityMessagingVC.reloadUI()
        
        let secondaryContainerVC = self.parent as! ContainerViewController // messagingVC & leftSidePanelVC container
        let primaryContainerVC = secondaryContainerVC.parent as! ContainerViewController
        primaryContainerVC.displayStatusBar()
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        /*
        if customSearchController.searchBar.isFirstResponder {
            view.endEditing(true)
            //customSearchController.searchBar.resignFirstResponder()
            //view.removeGestureRecognizer(gestureRecognizer)
            //self.customSearchController.isActive = false
        }
        */
        secondaryContainerVC.animateCenterPanelXPosition(targetPosition: 0) { finished in
            secondaryContainerVC.currentState = .bottomPanelExpanded
            //secondaryContainerVC.view.frame.origin.y = 80
            //primaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = false
            //secondaryContainerVC.centerNavigationControllerCP.isNavigationBarHidden = true
        }
    }
// collectionView methods
    
/*
// search controller methods
    func updateSearchResults(for searchController: UISearchController) {
        print("func updateSearchResults For SearchController running...")
        // assign shorter variable for query text for better code readability
        // Nil-Coalescing Operator, shorthand for: a != nil ? a! : b
        let queryText = customSearchController.searchBar.text ?? ""
        // empty filtered member/thread search arrays
        leftPanelData.community!.filteredMembers.removeAll()
        leftPanelData.community!.filteredThreads.removeAll()
        if customSearchController.searchBar.text!.isEmpty {
            // set filtered arrays equal to unfiltered arrays
            leftPanelData.community!.filteredMembers = leftPanelData.community!.members
            leftPanelData.community!.filteredThreads = leftPanelData.community!.threads
        }
        else {
            // update filtered members and threads arrays filtering by updated user query keyword, append all matching results
            // filter members with queryText
            leftPanelData.community!.filteredMembers = filter(members: leftPanelData.community!.members, by: queryText)
            // filter threads with queryText
            leftPanelData.community!.filteredThreads = filter(threads: leftPanelData.community!.threads, by: queryText)
        }
        self.collectionView.reloadData()
        ///////// ALERT: YOU ARE HERE > ADD DATA PROPERTY TO TRACK INDEXPATH OF ACTIVE DATA (thread/direct message)
        let activeDataIndexPath = IndexPath()
        self.collectionView.selectItem(at: activeDataIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        print("func didPresentSearchController is running...")
        self.centerContainer.addSubview(self.customSearchController.searchBar)
        self.customSearchController.searchBar.frame.size.width = self.centerContainer.frame.width*88/100
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("func scrollViewDidScroll is running...")
        if customSearchController.searchBar.isFirstResponder {
            view.endEditing(true)
            //customSearchController.searchBar.resignFirstResponder()
            //view.removeGestureRecognizer(gestureRecognizer)
        }
    }
// search controller methods
 */
}
