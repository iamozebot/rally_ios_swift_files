//
//  StringExtension.swift
//  Rally
//
//  Created by Ian Moses on 8/20/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

extension  String {
    func isADigit() -> Bool {
        print("func isADigit is running...")
        let c : Character = Character(self)
        let s = String(c).unicodeScalars
        let uni = s[s.startIndex]
        let digits = NSCharacterSet.decimalDigits as NSCharacterSet
        let isADigit = digits.longCharacterIsMember(uni.value)
        return isADigit
    }
}

/*
// https://github.com/jernejstrasner/SwiftCrypto/blob/master/SwiftCrypto/Crypto.swift

extension String {
    
    // MARK: HMAC
    
    func hmac(_ algorithm: CryptoAlgorithm, key: String) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        let keyStr = key.cString(using: String.Encoding.utf8)
        let keyLen = Int(key.lengthOfBytes(using: String.Encoding.utf8))
        
        CCHmac(algorithm.HMACAlgorithm, keyStr!, keyLen, str!, strLen, result)
        
        let digest = stringFromResult(result, length: digestLen)
        
        result.deallocate(capacity: digestLen)
        
        return digest
    }
    
    // MARK: Digest
    
    var md5: String {
        return digest(.md5)
    }
    
    var sha1: String {
        return digest(.sha1)
    }
    
    var sha224: String {
        return digest(.sha224)
    }
    
    var sha256: String {
        return digest(.sha256)
    }
    
    var sha384: String {
        return digest(.sha384)
    }
    
    var sha512: String {
        return digest(.sha512)
    }
    
    func digest(_ algorithm: CryptoAlgorithm) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        algorithm.digestAlgorithm(str!, strLen, result)
        
        let digest = stringFromResult(result, length: digestLen)
        
        result.deallocate(capacity: digestLen)
        
        return digest
    }
    
    // MARK: Private
    
    fileprivate func stringFromResult(_ result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash)
    }
    
}*/
