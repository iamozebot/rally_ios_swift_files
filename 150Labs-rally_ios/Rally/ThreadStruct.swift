//
//  ThreadStruct.swift
//  Rally
//
//  Created by Ian Moses on 10/18/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation

class Thread {
    var members = [Friend]()
    var messages = [CommThreadMessage]()
    var messageBlurbs = [MYMessageBlurb]()
    var title: String? {
        get {
            if let title = self.savedTitle {
                return title
            }
            else {
                return nil //formatNames(members)
            }
        }
        set(title) {
            self.savedTitle = title
        }
    }
    private var savedTitle: String?
    var expanded: Bool = false // default value
    var notifications: Int = 0 // default value
    var notificationsStatus: NotificationsStatus!
    var createdDateTimeStamp: String!
    var threadID: String = "" // default value because optional value is coming out when getting reference
    var createdByUserID: Int!
    var communityID: Int!
    var currentState: CollapsedExpandedState = .collapsed // default state is defined as being collapsed
    /*
     ["CommunityThreadPackages"][swiftyJSONThreadPackageListIndex]
     let newThread = Thread()
     newThread.firstName = swiftyJSONCommunityThreadList["CommThreadID"].string
     newThread.lastName = swiftyJSONCommunityThreadList["CreatedByUserID"].string
     newThread.stockImageID = Int(swiftyJSONCommunityThreadList["CommunityID"].stringValue)
     newThread.userID = Int(swiftyJSONCommunityThreadList["Name"].stringValue)!
     */
}

class CommThreadMessage {
    var commThreadMessageID: Int = 0
    var commThreadID: Int = 0
    var createdByUserID: Int = 0
    var firstName: String = ""
    var lastName: String = ""
    var member: Friend {
        get {
            let member = Friend()
            member.ID = createdByUserID
            member.firstName = firstName
            member.lastName = lastName
            return member
        }
    }
    var numberOfLikes: Int = 0
    var commThreadMessageTypeID: Int = 1 // 1 = textBasedMessage ; 2 = smsBasedMessage
    var messageByteString: String = "" // asciII byte string (NOTE: normal strings don't support emojis)
    var text: String {
        get {
            return messageByteString
        }
    }
    var createdDateTimeStamp: String!
}
