//
//  TimeDelayFunctionExecution.swift
//  Rally
//
//  Created by Ian Moses on 9/3/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
// http://stackoverflow.com/questions/24034544/dispatch-after-gcd-in-swift/24318861#24318861
// credit Matt from StackOverFlow

import Foundation

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
