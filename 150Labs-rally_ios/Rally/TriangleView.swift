//
//  TriangleView.swift
//  Rally
//
//  Created by Ian Moses on 6/29/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class TriangeViewClass : passThroughView {
    var redValue: CGFloat = 0.0
    var greenValue: CGFloat = 0.0
    var blueValue: CGFloat = 0.0
    var alphaValue: CGFloat = 1.0
    
    init(frame: CGRect, colorValues: [String:CGFloat]) {
        redValue = colorValues["redValue"]!
        greenValue = colorValues["greenValue"]!
        blueValue = colorValues["blueValue"]!
        alphaValue = colorValues["alphaValue"]!
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        let ctx : CGContext = UIGraphicsGetCurrentContext()!
        
        ctx.beginPath()
        ctx.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        ctx.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        ctx.addLine(to: CGPoint(x: (rect.maxX/2.0), y: rect.minY))
        ctx.closePath()
        
        ctx.setFillColor(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
        ctx.fillPath()
    }
}
