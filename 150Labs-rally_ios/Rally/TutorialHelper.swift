//
//  TutorialHelper.swift
//  Rally
//
//  Created by Ian Moses on 4/20/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

class CustomLabel: UILabel {
    var leftInset: CGFloat = 0
    var rightInset: CGFloat = 0
    var topInset: CGFloat = 0
    var bottomInset: CGFloat = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(insets:[String: CGFloat]) {
        super.init(frame: CGRect.zero)
        self.layer.cornerRadius = self.bounds.width/2
        self.clipsToBounds = true
        self.textColor = UIColor.white
        self.setProperties()
        print("CustomLabel:leftInset assign value...")
        leftInset = insets["leftInsetValue"]!
        rightInset = insets["rightInsetValue"]!
        topInset = insets["topInsetValue"]!
        bottomInset = insets["bottomInsetValue"]!
    }
    
    func setProperties() {
        self.backgroundColor = MYColor.rallyBlue
        self.textColor = UIColor.white
        self.layer.cornerRadius = 5.0
        self.font = .systemFont(ofSize: 14.75)
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
    
    override func drawText(in rect: CGRect) {
        print("CustomLabel:override func drawTextInRect(rect: CGRect)")
        let insets = UIEdgeInsets.init(top: self.topInset, left: self.leftInset, bottom: self.bottomInset, right: self.rightInset)
        /*
         let stringTextAsNSString = textValue as NSString
         var labelStringSize = stringTextAsNSString.boundingRectWithSize(CGSizeMake(CGRectGetWidth(self.frame), CGFloat.max),
         options: NSStringDrawingOptions.UsesLineFragmentOrigin,
         attributes: [NSFontAttributeName: font],
         context: nil).size
         super.drawTextInRect(CGRectMake(0, 0, CGRectGetWidth(self.frame), ceil(labelStringSize.height)))
         */
        var newRect = rect
        newRect.origin.y = 0
        super.drawText(in: UIEdgeInsetsInsetRect(newRect, insets))
    }
}

class tutorialLabelAndLabelTailSuperview: UIView {
    var tutorialType: tutorialBlurbType = .howToUseSavedGroupCards // .howToUseSavedGroupCards is only being used as default to satisfy compiler
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    init(typeValue: tutorialBlurbType) {
        super.init(frame: CGRect.zero)
        tutorialType = typeValue
    }
    /*
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.hidden && subview.alpha > 0 && subview.userInteractionEnabled && subview.pointInside(convertPoint(point, toView: subview), withEvent: event) {
                return true
            }
        }
        return false
    }*/
}

class TutorialHelper: NSObject {
    static let tutorialHelperClassSingleton = TutorialHelper()
    var navigationControllerClassVariable: UINavigationController!
    var cardsArray: [UIButton?] = []
    
    fileprivate override init() { // marking private disallows any initialization of a permissionsHelperClass instance from outside class (to avoid duplicates), may only be accessed from inside which permissionsHelperClassSingleton meets this criteria
        super.init()
    }
    
    class func removeTutorialBlurbsFromViewControllerView<T: UIView>(_ viewControllerView: T) {
        print("func removeTutorialBlurbsFromViewControllerView is running...")
        let (viewsToRemoveFromSuperview,_):([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType]) = TutorialHelper.tutorialViewsInViewControllerView(viewControllerView)
        for view in viewsToRemoveFromSuperview {
            view.removeFromSuperview()
        }
    }
    
    class func tutorialViewsInViewControllerView<T:UIView>(_ viewControllerView: T)->([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType]) {
        print("func tutorialViewsInViewControllerView is running...")
        var navTutorialSubviews = [tutorialLabelAndLabelTailSuperview]()
        var navTutorialSubviewTypes = [tutorialBlurbType]()
        for subview in viewControllerView.subviews {
            if subview.isMember(of: tutorialLabelAndLabelTailSuperview.self) {
                navTutorialSubviews.append((subview as! tutorialLabelAndLabelTailSuperview))
                let tutorialSuperviewType = (subview as! tutorialLabelAndLabelTailSuperview).tutorialType
                navTutorialSubviewTypes.append(tutorialSuperviewType)
            }
        }
        return (navTutorialSubviews,navTutorialSubviewTypes)
    }
    
    func onGotItButtonTouch(_ sender: UIButton!) {
        print("func onGotItButtonTouch is running...")
        let superview = sender.superview as! tutorialLabelAndLabelTailSuperview
        let type = superview.tutorialType
        superview.removeFromSuperview()
        markMessageBlurbAsRead(type)
    }
    
    func markMessageBlurbAsRead(_ type: tutorialBlurbType) {
        print("func markMessageBlurbAsRead is running...")
        switch type { // set display status as false, which means no need to display
        case .howToUseSavedGroupCards:
            NSUserDefaultsClass.updateTutorialStartRallyWithGroupBlurbDisplayStatus(false)
        case .whatAreCommunities:
            NSUserDefaultsClass.updateTutorialCommunityIntroBlurbDisplayStatus(false)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "whatAreCommunitiesTutorialBlurbMarkedRead"), object: nil)
        case .howToUseSaveButtonToSaveGroups:
            NSUserDefaultsClass.updateTutorialSaveRallyGroupBlurbDisplayStatus(false)
        case .howToUseRallyCommunityCard:
            NSUserDefaultsClass.updateTutorialStartRallyWithCommunityBlurbDisplayStatus(false)
        case .howFriendsWithoutAppReceiveInvite:
            NSUserDefaultsClass.updateTutorialInviteFriendsWithoutAppBlurbDisplayStatus(false)
        }
    }
    
    func markMessageBlurbAsUnread() {
        print("func markMessageBlurbAsUnread is running...")
        NSUserDefaultsClass.updateTutorialCommunityIntroBlurbDisplayStatus(true)
        NSUserDefaultsClass.updateTutorialStartRallyWithGroupBlurbDisplayStatus(true)
        NSUserDefaultsClass.updateTutorialSaveRallyGroupBlurbDisplayStatus(true)
        NSUserDefaultsClass.updateTutorialStartRallyWithCommunityBlurbDisplayStatus(true)
        NSUserDefaultsClass.updateTutorialInviteFriendsWithoutAppBlurbDisplayStatus(true)
    }
    
    func shouldDisplayTutorialBlurb<T: UIView>(_ tutorialTypeToDisplayIfNeeded: tutorialBlurbType, viewControllerView: T, referenceCard: UIButton?)/*(caseType: tutorialBlurbType, referenceCard: UIButton?)*/->Bool {
        print("func shouldDisplayTutorialBlurb is running...")
        let key = tutorialTypeToDisplayIfNeeded.rawValue // enum value returns key for respective enum type
        if let displayMessage = UserDefaults.standard.value(forKey: key) as? Bool { // get value whether blurb should be displayed using case key, use valueForKey to get optional type, BoolForKey returns false instead of nil. default would need to be true for displaying tutorial blurbs to be usable
            print("PLEase be printing:\(displayMessage)")
            if displayMessage { // if true, continue checks
            }
            else {
                return false
            }
        }
        else { // key's value hasn't been defined yet which means tutorial blurbs needs to be displayed
            // continue letting method execute below to do remaining checks
        }
        // determine tutorial superviews (blurbs) already defined and added as subviews within navController view
        let (_,types):([tutorialLabelAndLabelTailSuperview],[tutorialBlurbType])  = TutorialHelper.tutorialViewsInViewControllerView(viewControllerView)
        if types.contains(tutorialTypeToDisplayIfNeeded) { // determine if tutorial type in question is one of the views in the array
            // do not define, already exists. (assumes no hiding of tutorial blurb, if there is hiding, handled within a different method)
            return false // no card to reference for tutorial blurb position definition
        }
        else { // doesn't exist, generate tutorial blurb for parameter 'tutorialTypeToDisplayIfNeeded'
            var requiresCard = true // default just satisfies compiler...
            switch tutorialTypeToDisplayIfNeeded {
            case .howToUseSavedGroupCards:
                requiresCard = true
            case .whatAreCommunities:
                requiresCard = true
            case .howToUseSaveButtonToSaveGroups:
                requiresCard = false
            case .howToUseRallyCommunityCard:
                requiresCard = true
            case .howFriendsWithoutAppReceiveInvite:
                requiresCard = false
            }
            if requiresCard {
                if let _ = referenceCard {
                    return true
                }
                else {
                    return false // no card to reference for tutorial blurb position definition
                }
            }
            else {
                return true
            }
            
        }
    }
    
    // NOTE: VCViewAssociatedWithCard is used in converting card coordinates to nav coordinates for tutorial blurb origin value
    func tutorialHandler<T:UIView>(_ tutorialTypeToDisplayIfNeeded: tutorialBlurbType, viewControllerView: T, navigationController: UINavigationController!, referenceCard: UIButton?, VCViewAssociatedWithCard: UIView?, barButtonItem: UIBarButtonItem?, barButtonItemImageViewFrame: CGRect?) { // determine whether tutorialBlurbTypes passed need displaying and handle accordingly
        print("func tutorialHandler is running...")
        // assign navigationController to class property for easier accessibility
        navigationControllerClassVariable = navigationController
        let shouldDisplay = shouldDisplayTutorialBlurb(tutorialTypeToDisplayIfNeeded, viewControllerView: viewControllerView, referenceCard: referenceCard)
        if shouldDisplay { //display tutorial blurb
            // define labelAndLabelTailSuperview
            let labelAndLabelTailSuperview: tutorialLabelAndLabelTailSuperview = defineLabelAndLabelTailSuperview(tutorialTypeToDisplayIfNeeded)
            // define gotItButton
            let gotItButton: UIButton = defineGotItButton()
            // define label
            let label: CustomLabel = defineLabel(tutorialTypeToDisplayIfNeeded, gotItButton: gotItButton)
            // define labelTail
            let labelTail: TriangeViewClass = defineLabelTail(label, superview: labelAndLabelTailSuperview)
            updateXAndYOriginForViews(label, labelTail: labelTail, labelAndLabelTailSuperview: labelAndLabelTailSuperview, gotItButton: gotItButton, referenceCard: referenceCard, VCViewAssociatedWithCard: viewControllerView, barButtonItem: barButtonItem, barButtonItemImageViewFrame: barButtonItemImageViewFrame)
            // assign view-superview relationships
            labelAndLabelTailSuperview.addSubview(label)
            labelAndLabelTailSuperview.addSubview(labelTail)
            labelAndLabelTailSuperview.addSubview(gotItButton)
            viewControllerView.addSubview(labelAndLabelTailSuperview)
            // define labelAndLabelTailSuperview frame
            labelAndLabelTailSuperview.frame.size.width = label.frame.width
            labelAndLabelTailSuperview.frame.size.height = label.frame.height+labelTail.frame.height
        }
    }
    
    func defineGotItButton()-> UIButton {
        print("func defineGotItButton is running...")
        let gotItButton = UIButton()
        gotItButton.tag = 5002
        gotItButton.setTitle("Got It", for: UIControlState())
        gotItButton.setTitleColor(UIColor.white, for: UIControlState())
        gotItButton.titleLabel!.font = .systemFont(ofSize: 16.25)
        gotItButton.layer.cornerRadius = 5.0
        gotItButton.addTarget(TutorialHelper.tutorialHelperClassSingleton, action: #selector(TutorialHelper.onGotItButtonTouch(_:)), for: .touchUpInside)
        gotItButton.backgroundColor = MYColor.darkBlue
        gotItButton.sizeToFit()
        //gotItButton.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0)
        let widthIncreaseIncrement: CGFloat = gotItButton.frame.width*36/100
        gotItButton.frame.size.width += widthIncreaseIncrement
        let heightIncreaseIncrement: CGFloat = gotItButton.frame.height*5.25/100
        gotItButton.frame.size.height += gotItButton.titleEdgeInsets.top+gotItButton.titleEdgeInsets.bottom+heightIncreaseIncrement
        return gotItButton
    }
    
    func defineLabel(_ type: tutorialBlurbType, gotItButton: UIButton)->CustomLabel {
        print("func defineMessageBlurb is running...")
        let additionalBottomPadding: CGFloat = gotItButton.frame.height
        let insets: [String:CGFloat] = ["leftInsetValue":20.0,"rightInsetValue":20.0,"bottomInsetValue":13.25*14/10+additionalBottomPadding,"topInsetValue":14]
        let label = CustomLabel(insets: insets)
        label.tag = 5000
        label.frame.size.width = ScreenDimension.width*78/100
        label.text = type.description
        label.sizeToFit()
        label.frame.size.height += insets["topInsetValue"]!+insets["bottomInsetValue"]!
        label.frame.size.width += insets["leftInsetValue"]!+insets["rightInsetValue"]!
        return label
    }
    
    func defineLabelTail(_ label: CustomLabel, superview: tutorialLabelAndLabelTailSuperview)->TriangeViewClass { // could just pass type directly instead of passing superview but takes a little more setup code in calling method to do so...
        print("func defineLabelTail is running...")
        let triangleHeight = ScreenDimension.height*2.35/100
        var colorValues: [String:CGFloat]!
        let colorComponents = MYColor.rallyBlue.cgColor.components
        let redComponentValue = colorComponents?[0]
        let greenComponentValue = colorComponents?[1]
        let blueComponentValue = colorComponents?[2]
        let alphaComponentValue = colorComponents?[3]
        colorValues = ["redValue":redComponentValue!]
        colorValues["greenValue"] = greenComponentValue
        colorValues["blueValue"] = blueComponentValue
        colorValues["alphaValue"] = alphaComponentValue
        let labelTail = TriangeViewClass(frame: CGRect(x: 0, y: 0, width: triangleHeight*2, height: triangleHeight), colorValues: colorValues)
        labelTail.tag = 5001
        labelTail.backgroundColor = UIColor(red: 1.0,green: 1.0,blue:1.0, alpha: 0.0)
        let adjoiningSideOfLabel: adjoiningLabelSide = superview.tutorialType.side
        switch adjoiningSideOfLabel {
        case .bottom: // rotate labelTail orientation by 180 degrees so point of tail faces downwards
            labelTail.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        default:
            break
        }
        return labelTail
    }
    
    func defineLabelAndLabelTailSuperview(_ type: tutorialBlurbType)->tutorialLabelAndLabelTailSuperview {
        print("func defineLabelAndLabelTailSuperview is running...")
        let labelAndLabelTailSuperview = tutorialLabelAndLabelTailSuperview(typeValue: type)
        return labelAndLabelTailSuperview
    }
    
    func updateXAndYOriginForViews(_ label: CustomLabel, labelTail: TriangeViewClass, labelAndLabelTailSuperview: tutorialLabelAndLabelTailSuperview, gotItButton: UIButton, referenceCard: UIButton?, VCViewAssociatedWithCard: UIView?, barButtonItem: UIBarButtonItem?, barButtonItemImageViewFrame: CGRect?) {
        print("func updateXAndYOriginForViews is running...")
        // switch statement based on tutorial type; for all X/Y origin statements
        var labelTailAdjoiningCenterWithRespectToLabel: CGFloat!
        switch labelAndLabelTailSuperview.tutorialType {
        case .howToUseSavedGroupCards:
            labelTailAdjoiningCenterWithRespectToLabel = label.frame.width*50/100
        case .whatAreCommunities:
            labelTailAdjoiningCenterWithRespectToLabel = label.frame.width*50/100
        case .howToUseSaveButtonToSaveGroups:
            labelTailAdjoiningCenterWithRespectToLabel = label.frame.width*92/100
        case .howToUseRallyCommunityCard:
            labelTailAdjoiningCenterWithRespectToLabel = label.frame.width*22/100 //1/6 is superview positioning
        case .howFriendsWithoutAppReceiveInvite:
            labelTailAdjoiningCenterWithRespectToLabel = label.frame.width*50/100
        }
        // define X & Y origin for label
            // x-origin is zero
            // take y-point from tip of labelTail
            let labelXOrigin: CGFloat = 0.0
            var labelYOrigin: CGFloat!
            switch labelAndLabelTailSuperview.tutorialType.side {
            case .top:
                labelYOrigin = labelTail.frame.height
            case .bottom:
                labelYOrigin = 0.0
            }
            label.frame.origin = CGPoint(x: labelXOrigin,y: labelYOrigin)
        // define X & Y origin for labelTail
            let labelTailXOrigin: CGFloat = labelTailAdjoiningCenterWithRespectToLabel-labelTail.frame.width/2
            var labelTailYOrigin: CGFloat!
            switch labelAndLabelTailSuperview.tutorialType.side {
                case .top:
                labelTailYOrigin = 0
                case .bottom:
                labelTailYOrigin = label.frame.height
            }
        labelTail.frame.origin = CGPoint(x: labelTailXOrigin,y: labelTailYOrigin)
        // this shouldn't be necessary but it assures labelTail edge is overlapping label so no space can be seen in betwee (was seeing a small line in some cases)
        let adjoiningSideOfLabel: adjoiningLabelSide = labelAndLabelTailSuperview.tutorialType.side
        switch adjoiningSideOfLabel {
        case .bottom: // rotate labelTail orientation by 180 degrees so point of tail faces downwards
            //labelTail.frame.offsetInPlace(dx: 0, dy: -2.5)
            labelTail.frame.origin.y += -2.5
        case .top:
            //labelTail.frame.offsetInPlace(dx: 0, dy: 2.5)
            labelTail.frame.origin.y += 2.5
        }
        // define X & Y origin for gotItButton
        // NOTE: defined after labelTail bc it uses labelTail origin in calc
        let gotItButtonHeight = gotItButton.frame.height
        let gotitButtonXOrigin = (label.frame.width-gotItButton.frame.width)/2
        var gotItButtonYOrigin: CGFloat!
        switch labelAndLabelTailSuperview.tutorialType.side {
        case .top:
            gotItButtonYOrigin = label.frame.height-gotItButton.frame.height-(label.bottomInset-gotItButtonHeight)*60/100+labelTail.frame.height+labelTail.frame.origin.y
        case .bottom:
            gotItButtonYOrigin = label.frame.height-gotItButton.frame.height-(label.bottomInset-gotItButtonHeight)*60/100
        }
        gotItButton.frame.origin = CGPoint(x: gotitButtonXOrigin, y: gotItButtonYOrigin)
        // define X & Y origin for labelAndLabelTailSuperview
        var tutorialBlurbSeparationDistanceFromObjectOfInterest = ScreenDimension.height*0.55/100
        switch labelAndLabelTailSuperview.tutorialType.side {
        case .bottom: // negate the value being added so displacement occurs in proper direction
            tutorialBlurbSeparationDistanceFromObjectOfInterest = (-1)*tutorialBlurbSeparationDistanceFromObjectOfInterest
        default:
            break
        }
        var superviewXOrigin: CGFloat! // with respect to navigationController.view
        var superviewYOrigin: CGFloat = tutorialBlurbSeparationDistanceFromObjectOfInterest // with respect to viewControllerview
        switch labelAndLabelTailSuperview.tutorialType {
        case .howToUseSavedGroupCards: //NEED TO ADD NAVBAR HEIGHT AND MAYBE STATUS BAR HEIGHT FOR YORIGIN
            if let card = referenceCard { // reference card index 0 is for savedGroupCard reference
                superviewXOrigin = card.center.x-labelTail.frame.width/2-labelTail.frame.origin.x
                if let _ = VCViewAssociatedWithCard {
                    superviewYOrigin += cardYOriginWithRespectToView(card, toView: VCViewAssociatedWithCard!)-label.frame.height-labelTail.frame.height
                }
            }
        case .whatAreCommunities: //NEED TO ADD NAVBAR HEIGHT AND MAYBE STATUS BAR HEIGHT FOR YORIGIN
            superviewXOrigin = ScreenDimension.width/2-label.frame.width/2
            // 'start new community' card to calculate cardBottom position
            if let _ = referenceCard {
                if let _ = VCViewAssociatedWithCard {
                    switch UIDevice().type {
                    case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
                        print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
                        superviewYOrigin += cardYOriginWithRespectToView(referenceCard!, toView: VCViewAssociatedWithCard!)+referenceCard!.frame.height-navigationControllerClassVariable.navigationBar.frame.height-ScreenDimension.height*0.55/100
                    case .iPhone6, .iPhone6S, .iPhone6plus, .iPhone6Splus:
                        let originWithRespectToView = cardYOriginWithRespectToView(referenceCard!, toView: VCViewAssociatedWithCard!)
                        let offsetBy = originWithRespectToView+referenceCard!.frame.height-navigationControllerClassVariable.navigationBar.frame.height-UIApplication.shared.statusBarFrame.height+ScreenDimension.height*0.55/100
                        superviewYOrigin += offsetBy
                    default:
                        print("I am not equipped to handle this device")
                    }
                    // this is fucked. totally contrived, not sure why calc is wrong but this gets it to correct place on screen....
                    print("height of start a community card in tutorial helper y-origin calc:\(referenceCard!.frame.height)")
                }
            }
        case .howToUseSaveButtonToSaveGroups:
            if barButtonItem != nil { // clean this up/get rid of sometime, too lazy atm since it's hurting nothing
                if let unwrappedBarButtonItemImageViewFrame = barButtonItemImageViewFrame {
                    let rightButtonItemPositionOfBottom = unwrappedBarButtonItemImageViewFrame.origin.y+unwrappedBarButtonItemImageViewFrame.height
                    superviewXOrigin = unwrappedBarButtonItemImageViewFrame.origin.x + unwrappedBarButtonItemImageViewFrame.width/2 - labelTail.frame.width/2 - labelTail.frame.origin.x
                    superviewYOrigin += rightButtonItemPositionOfBottom + UINavigationBarTaller.navigationBarHeight*25/100 // offset from button bottom by a small constant padding // should be over navigationcontroller in the same way menu does on home screen but will take a bunch of extra time to enable superviews to be added to views and navigation controller views so just add at start of inviteVC view
                }
            }
        case .howToUseRallyCommunityCard: //NEED TO ADD NAVBAR HEIGHT AND MAYBE STATUS BAR HEIGHT FOR YORIGIN
            if let card = referenceCard { // reference card index 1 is for savedGroupCard reference
                superviewXOrigin = card.center.x-labelTail.frame.width/2-labelTail.frame.origin.x
                if let _ = VCViewAssociatedWithCard {
                    superviewYOrigin += cardYOriginWithRespectToView(card, toView: VCViewAssociatedWithCard!)-label.frame.height-labelTail.frame.height
                }
            }
        case .howFriendsWithoutAppReceiveInvite:
            superviewXOrigin = ScreenDimension.width/2-label.frame.width/2
            superviewYOrigin += navigationControllerClassVariable.toolbar.frame.origin.y-label.frame.height-labelTail.frame.height-navigationControllerClassVariable.navigationBar.frame.height-UIApplication.shared.statusBarFrame.height
        }
        labelAndLabelTailSuperview.frame.origin.x = superviewXOrigin
        labelAndLabelTailSuperview.frame.origin.y = superviewYOrigin
    }
    
    func cardYOriginWithRespectToView<T:UIView>(_ card: UIButton, toView: T)->CGFloat {
        print("func cardYOriginWithRespectToVCView is running...")
        let yOrigin: CGFloat!
        /*
        if let superview = card.superview {
            for superview in
        }
        for superview in card.superview {
            if let superview.superview
        }*/
        let frame = card.convert(card.frame, to:toView)
        yOrigin = frame.origin.y
        print("yOrigin:\(yOrigin)")
        return yOrigin
    }
}
