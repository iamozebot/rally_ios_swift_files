//
//  UIImageExtension.swift
//  Rally
//
//  Created by Ian Moses on 5/30/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

public extension UIImage {
    // https://stackoverflow.com/questions/26542035/create-uiimage-with-solid-color-in-swift
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
