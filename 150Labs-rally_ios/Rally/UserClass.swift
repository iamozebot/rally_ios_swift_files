//
//  UserClass.swift
//  Rally
//
//  Created by Ian Moses on 9/8/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class User: Person {
    var largeProfileImage: UIImage?
    var ID: Int?
    var stockImageID: Int?
    var phoneNumberList = [String: String?]()
    var thumbnailProfileImage: UIImage?
    var GCMToken: String?
    var latitude: Double?
    var longitude: Double?
    var identifier: String? // iOS addressBook Contacts framework ID; webcall calls this PhoneContactID
    
    // Person protocol properties
    var firstName: String?
    var lastName: String?
    var fullName: String?
    
    func getProfileImage(size: ProfileImageSize = .thumbnail)-> UIImage { // default profile picture is thumbnail for method
        print("func getProfilePicture is running...")
        if let value = self.thumbnailProfileImage {
            //cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            return value
        }
        if let stockImageID = self.stockImageID {
            return stockImageGlobalDictionary[stockImageID]!
        }
        else {
            let image = UIImage(color: .white)!
            return image
        }
    }
}
