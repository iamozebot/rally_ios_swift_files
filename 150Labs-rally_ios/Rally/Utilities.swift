//
//  Utilities.swift
//  Rally
//
//  Created by Ian Moses on 9/8/17.
//  Copyright © 2017 Ian Moses. All rights reserved.
//

import Foundation

class ScreenDimension {
    static let width: CGFloat! = UIScreen.main.bounds.width
    static let height: CGFloat! = UIScreen.main.bounds.height
    static let heightMinusStatusAndNavigationBar: CGFloat! = UIScreen.main.bounds.height-UINavigationBarTaller.navigationBarHeight-UIApplication.shared.statusBarFrame.height
    static let heightMinusStatusNavigationAndToolbar: CGFloat = ScreenDimension.heightMinusStatusAndNavigationBar - UINavigationController().toolbar.frame.height
    static let threadBarHeight = ScreenDimension.height*9.16/100
}

class StartupRunTimer {
    static var start: Date!
    static var mid: Date!
    static var end: Date!
}

class WebCallStatus {
    static var editProfileViewControllerLoaded = 0
    static var createCommunityViewControllerLoaded = 0
    static var createUserWebCall = 0
    static var postUserPhoneNumberToWebService = 0
    static var postRallyToWebService = 0
    static var fetchRallyUserInfoFromWebService = 0
    static var postUserMessageToWebService = 0
    static var fetchAddressBookContacts = 0
    static var verifyPhoneNumberViewControllerLoaded = 0
    static var homeViewControllerLoaded = 0
    static var inviteViewControllerLoaded = 0
    static var detailsViewControllerLoaded = 0
    static var messagingViewControllerLoaded = 0
    static var communityMessagingViewControllerLoaded = 0
    static var getCommunitiesFromWebService = 0
    static var fetchStockImagesFromWebService = 0
    static var updateContactsInfoFromWebService = 0
    static var fetchCommunitiesNearbyUserFromWebService = 0
    static var homePageLoadUIHandler = 0
    static var testVariable = 0
    static var appDelegateStartupSkippedDueToNoInternetConnection = 0
    static var createThreadViewControllerLoaded = 0
}

class PermissionsFlags {
    static var permissionsNotEnabledUserIDNil = false
}

let 😎 = "!^Rr)@bfk$%H!&vVvDS##@dln^"
