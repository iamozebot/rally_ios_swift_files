//
//  VerifyPhoneNumberViewController.swift
//  Rally
//
//  Created by Ian Moses on 7/24/15.
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import UIKit
import SwiftyJSON

class VerifyPhoneNumberViewControllerDataSource {
    var mode: VerifyPhoneNumberMode = .phoneNumber // initial value
    var confirmationCode: String = ""
    var phoneNumber: String {
        get {
            guard let value = MYUser.phoneNumberList["Mobile"] else {
                return ""
            }
            guard let unwrappedValue = value else {
                return ""
            }
            return unwrappedValue
        }
        set {
            MYUser.phoneNumberList["Mobile"] = newValue
        }
    }// format is 15555555555
    var navBarTitle: String {
        get {
            switch self.mode {
            case .phoneNumber:
                return "My Mobile #"
            case .confirmationCode:
                return "Enter Confirmation Code"
            }
        }
    }
    var bodyText: String {
        get {
            switch self.mode {
            case .phoneNumber:
                return "Please verify your phone number so we know you’re a real person!"
            case .confirmationCode:
                // format phone number
                // separate phoneNumber into array
                var phoneNumberArray : [String] = self.phoneNumber.characters.map{String($0)}
                // remove first digit (1)
                phoneNumberArray.remove(at:0)
                // add additional formatting characters
                phoneNumberArray.insert("-", at: 6)
                phoneNumberArray.insert("\n", at: 3)
                phoneNumberArray.insert(")", at: 3)
                phoneNumberArray.insert("(", at: 0)
                // re-join array to into string
                let finalString: String = phoneNumberArray.joined()
                return "Enter the code we sent to"+" "+finalString
            }
        }
    }
    var numberInputViewText: String {
        get {
            switch self.mode {
            case .phoneNumber:
                return "Mobile #"
            case .confirmationCode:
                return "Confirmation Code"
            }
        }
    }
    var submitButtonText: String {
        get {
            switch self.mode {
            case .phoneNumber:
                return "Submit"
            case .confirmationCode:
                return "Continue"
            }
        }
    }
    var attributedText: NSMutableAttributedString {
        get {
            switch self.mode {
            case .phoneNumber:
                let attributedString = NSMutableAttributedString(string: "By tapping Submit, you agree to the Terms of Service and Privacy Policy.")
                attributedString.addAttribute(NSLinkAttributeName, value: "https://150labs.net", range: NSRange(location: 36, length: 17))
                attributedString.addAttribute(NSLinkAttributeName, value: "https://150labs.net", range: NSRange(location: 57, length: 15))
                return attributedString
            case .confirmationCode:
                let attributedString = NSMutableAttributedString(string: "Not working? Resend code or edit number.")
                attributedString.addAttribute(NSLinkAttributeName, value: "https://resendcode.net", range: NSRange(location: 13, length: 11))
                attributedString.addAttribute(NSLinkAttributeName, value: "https://editnumber.net", range: NSRange(location: 28, length: 11))
                return attributedString
            }
        }
    }
}

class VerifyPhoneNumberViewController: UIViewController, UITextViewDelegate {
    
    var VerifyPhoneNumberViewControllerData = VerifyPhoneNumberViewControllerDataSource()
    var numberInputView = UITextView()
    var submitButton = UIButton()
    var bodyTextLabel = UILabel()
    var legalDisclaimer = UITextView()
    var replacementText = ""
    var viewControllerTransitionOnButtonTouch = 0
    var keyboardHeight: CGFloat = 0 // default value (apple review team error using simulator due to nil keyboardHeight return value in delegate method keyboardwillshow)
    
    override func viewDidLoad() {
        print("class VerifyPhoneNumberViewController, func viewDidLoad is running...")
        WebCallStatus.verifyPhoneNumberViewControllerLoaded = 1
        // initialize view
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        // initialize nav bar
        navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        navBarClass.navBar(self, navigationBarItemReference: navigationItem, leftButtonImageReference: nil, rightButtonImageReference: nil, rightMostButtonImageReference: nil, leftButtonActionReference: nil, rightButtonActionReference: nil, rightMostButtonActionReference: nil, titleReference: VerifyPhoneNumberViewControllerData.navBarTitle)
        
        // initialize subviews
        defineNumberInputView()
        defineBodyTextLabel()
        defineLegalDisclaimer(numberInputView.frame)
        
        // add subviews to view
        view.addSubview(numberInputView)
        view.addSubview(bodyTextLabel)
        view.addSubview(legalDisclaimer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("class VerifyPhoneNumberViewController, func viewWillAppear is running...")
        UIApplication.shared.isStatusBarHidden = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(VerifyPhoneNumberViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("func viewDidDisappear is running...")
        // setanimationsenabled false was added bc keyboard was 'jumping' off screen once homeVC loaded, seems to not be a problem now but keeping here in case. would need to add UIView.setAnimationsEnabled(true) to homeVC if re-added at any point
        //UIView.setAnimationsEnabled(false) // I think this works in viewDidDisappear instead of having to use delay function
        DismissKeyboard()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func DismissKeyboard() {
        print("func DismissKeyboard is running...")
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        //http://stackoverflow.com/questions/24126678/close-ios-keyboard-by-touching-anywhere-using-swift
        numberInputView.resignFirstResponder()
        view.endEditing(true)
    }
    
// define subview methods
    func defineNumberInputView() {
        print("func defineNumberInputView is running...")
        numberInputView.delegate = self
        numberInputView.frame = CGRect(x: 0, y: ScreenDimension.height*33/100, width: ScreenDimension.width, height: ScreenDimension.height/12.25)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            numberInputView.frame = CGRect(x: 0, y: ScreenDimension.height*13/100, width: ScreenDimension.width, height: ScreenDimension.height/11.5)
        default:
            print("I am not equipped to handle this device")
        }
        numberInputView.font = UIFont.systemFont(ofSize: ScreenDimension.height/21.5)
        numberInputView.sizeToFit() // Sizes textView frame and indirectly centers it around text
        numberInputView.frame.size.width = ScreenDimension.width
        // numberInputView.textContainerInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 0)
        numberInputView.backgroundColor = UIColor(red:0.9882352941176471, green:0.98039215686274506,blue:1,alpha:1.0)
        numberInputView.keyboardType = UIKeyboardType.numberPad
        numberInputView.textContainer.lineFragmentPadding = ScreenDimension.width/20
        numberInputView.isScrollEnabled = false
        numberInputView.becomeFirstResponder()
    }
    
    func defineSubmitButton() {
        print("func defineSubmitButton is running...")
        submitButton.frame = CGRect(x: 0, y: 0, width: ScreenDimension.width, height: ScreenDimension.height/11)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            submitButton.frame.size.height = ScreenDimension.height/13
        default:
            print("I am not equipped to handle this device")
        }
        submitButton.frame.origin.y = ScreenDimension.height-submitButton.frame.height-UINavigationBarTaller.navigationBarHeight-keyboardHeight-UIApplication.shared.statusBarFrame.height
        submitButton.layer.masksToBounds = true
        submitButton.setTitle(VerifyPhoneNumberViewControllerData.submitButtonText, for: UIControlState())
        submitButton.setTitleColor(UIColor.white, for: UIControlState())
        submitButton.titleLabel!.lineBreakMode = .byTruncatingTail
        // self.submitPhoneNumberButton.titleLabel!.font = UIFont.systemFontOfSize(12)
        submitButton.addTarget(self, action: #selector(VerifyPhoneNumberViewController.onSubmitButtonTouch(_:)), for: UIControlEvents.touchUpInside)
        updateSubmitButtonInteractionState()
    }
    
    func defineBodyTextLabel() {
        print("func defineBodyTextLabel is running...")
        bodyTextLabel.frame = CGRect(x: 0,y: 0,width: ScreenDimension.width*93/100,height: CGFloat.greatestFiniteMagnitude)
        bodyTextLabel.text = VerifyPhoneNumberViewControllerData.bodyText
        bodyTextLabel.lineBreakMode = .byWordWrapping
        bodyTextLabel.numberOfLines = 4
        bodyTextLabel.textColor = MYColor.lightBlack
        bodyTextLabel.font = .systemFont(ofSize: 18.25)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            bodyTextLabel.font = .systemFont(ofSize: 15)
        default:
            print("I am not equipped to handle this device")
        }
        bodyTextLabel.textAlignment = .center
        bodyTextLabel.sizeToFit()
        bodyTextLabel.center = CGPoint(x: ScreenDimension.width/2, y: ScreenDimension.height*13.5/100)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            bodyTextLabel.center.y = ScreenDimension.height*5.0/100
        default:
            print("I am not equipped to handle this device")
        }
    }
    
    func defineLegalDisclaimer(_ phoneNumberTextfieldFrame:CGRect) {
        print("func defineLegalDisclaimer is running...")
        legalDisclaimer.delegate = self
        legalDisclaimer.isEditable = false
        legalDisclaimer.textColor = MYColor.lightBlack
        legalDisclaimer.font = .systemFont(ofSize: 11.75)
        switch UIDevice().type {
        case .iPhone4, .iPhone4S, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3:
            print("switch UIDevice().type:.iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE, .iPad2, .iPad3, .iPad4, .iPadAir1, .iPadAir2, .iPadMini1, .iPadMini2, .iPadMini3: case running...")
            legalDisclaimer.font = .systemFont(ofSize: 10.5)
        default:
            print("I am not equipped to handle this device")
        }
        legalDisclaimer.attributedText = VerifyPhoneNumberViewControllerData.attributedText
        let size = legalDisclaimer.sizeThatFits(CGSize(width: ScreenDimension.width*78/100,  height: CGFloat(Float.greatestFiniteMagnitude)))
        legalDisclaimer.frame.size = size
        legalDisclaimer.textAlignment = .center
        let phoneNumberTextfieldBottomYValue = phoneNumberTextfieldFrame.origin.y+phoneNumberTextfieldFrame.height
        legalDisclaimer.center.x = ScreenDimension.width/2
        legalDisclaimer.frame.origin.y = phoneNumberTextfieldBottomYValue+ScreenDimension.height*0.75/100
    }
// define subview methods
    
// number input view methods
    func textViewDidChange(_ textView: UITextView) {
        print("func textViewDidChange is running...")
        //http://stackoverflow.com/questions/30342744/swift-how-to-get-integer-from-string-and-convert-it-into-integer
        // filter number to digits only
        let digits = "0123456789"
        let numbers: [Int] = numberInputView.text.characters.split(whereSeparator: { !digits.characters.contains($0) }).map { String($0) }.map { Int($0)! }
        //http://stackoverflow.com/questions/25581324/swift-how-can-string-join-work-custom-types
        // http://stackoverflow.com/questions/39353189/how-to-join-array-of-structs-in-swift-3
        let numberAsString = numbers.map { $0.description }.joined(separator: "")
        switch VerifyPhoneNumberViewControllerData.mode {
        case VerifyPhoneNumberMode.phoneNumber:
            VerifyPhoneNumberViewControllerData.phoneNumber = numberAsString
            let formattedNumberPriorToNewDigit = String(self.numberInputView.text.characters.dropLast())
            var numberInputViewTextAppendedValue = ""
            if replacementText != "" {
                numberInputViewTextAppendedValue = String(self.numberInputView.text.characters.last!)
            }
            // http://stackoverflow.com/questions/32413247/swift-2-0-string-with-substringwithrange
            // format number: +1 (555) 555-5555
            let digitCount = VerifyPhoneNumberViewControllerData.phoneNumber.characters.count
            print("textViewDidChange:digitCount:\(digitCount)")
            print("textViewDidChange:replacementText:\(replacementText)")
            switch replacementText {
            case "": // backspace
                // remove plus
                if digitCount == 0 {
                    numberInputView.text = ""
                }
                // remove plus, one, and open parenthesis (on backspace of first digit in area code)
                if digitCount == 1 {
                    numberInputView.text = ""
                }
                // remove close parenthesis
                else if digitCount == 3 {
                    //numberInputView.text.character.dropLast()
                    let index = self.numberInputView.text.index(before: self.numberInputView.text.endIndex)
                    numberInputView.text.remove(at: index)
                }
                // remove space
                else if digitCount == 4 {
                    let index = self.numberInputView.text.index(before: self.numberInputView.text.endIndex)
                    numberInputView.text.remove(at: index)
                }
                // remove hyphen
                else if digitCount == 7 {
                    // determine if last character is a digit, if so remove additional character (to account for removing hyphen)
                    let c = self.numberInputView.text.characters.last!
                    let s = String(c)
                    if s == "-" {
                        let index = self.numberInputView.text.index(before: self.numberInputView.text.endIndex)
                        numberInputView.text.remove(at: index)
                    }
                }
            default: // format given appended digit
                // add plus (given user typed one)
                if digitCount == 1 && replacementText == "1" {
                    numberInputView.text = "+"+numberInputViewTextAppendedValue
                }
                // add plus, one, and open parenthesis (given user did not type one)
                else if digitCount == 1 {
                    numberInputView.text = "+1 ("+numberInputViewTextAppendedValue
                }
                // add space and open parenthesis
                else if digitCount == 2 {
                    numberInputView.text = formattedNumberPriorToNewDigit+" ("+numberInputViewTextAppendedValue
                }
                // add close parenthesis
                else if digitCount == 4 {
                    numberInputView.text = formattedNumberPriorToNewDigit+numberInputViewTextAppendedValue+")"
                }
                // add space
                else if digitCount == 5 {
                    numberInputView.text = formattedNumberPriorToNewDigit+" "+numberInputViewTextAppendedValue
                }
                // add hyphen
                else if digitCount == 8 {
                    numberInputView.text = formattedNumberPriorToNewDigit+"-"+numberInputViewTextAppendedValue
                }
            }
        case VerifyPhoneNumberMode.confirmationCode:
            VerifyPhoneNumberViewControllerData.confirmationCode = numberAsString
        }
        // update submit button interaction state
        updateSubmitButtonInteractionState()
    }
    
    //http://stackoverflow.com/questions/25223407/max-length-uitextfield-swift
    func textView(_ numberInputView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("func textView:shouldChangeTextInRange is running...")
        // update class property replacementText
        replacementText = text
        // return true for all backspaces
        if text == "" { // allows user to backspace in the event
            return true
        }
        // check for non-int entry
        // http://stackoverflow.com/questions/24502669/swift-how-to-find-out-if-letter-is-alphanumeric-or-digit
        if !text.isADigit() { // NOTE: isADigit is a string extension function
            return false
        }
        // continue to mode specific checks
        switch VerifyPhoneNumberViewControllerData.mode {
        case VerifyPhoneNumberMode.phoneNumber:
            var maxtext = 9
            if numberInputView.text.characters.count > 0 {
                let index = VerifyPhoneNumberViewControllerData.phoneNumber.index(VerifyPhoneNumberViewControllerData.phoneNumber.startIndex, offsetBy: 1)
                let firstDigit = VerifyPhoneNumberViewControllerData.phoneNumber.substring(to: index)
                if firstDigit == "1" {
                    maxtext = 10
                }
            }
            //If the text is larger than the maxtext, the return is false
            if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count <= maxtext {
                return true
            }
            else { // if user tries entering 11th digit, returns false.
                return false
            }
        case VerifyPhoneNumberMode.confirmationCode:
            // update class property
            replacementText = text
            // return true for characters 0-4 (code is 4 digits in length)
            if VerifyPhoneNumberViewControllerData.confirmationCode.characters.count <= 3 {
                return true
            }
            else { // if user tries entering 5th digit, returns false.
                return false
            }
        }
    }
    
    func textViewDidBeginEditing(_ numberInputView: UITextView) { // Makes existing text in textView disappear if it is lightgray, this way lightgray text can be used as a placeholder to be displayed when not editing and if user hasn't added text
        print("func textViewDidBeginEditing is running...")
        if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count == 0 {
            if  numberInputView.textColor == UIColor.lightGray {
                numberInputView.text = nil
                numberInputView.textColor = .black
            }
        }
    }
    
    func textViewDidEndEditing(_ numberInputView: UITextView) {
        //Reassign textView text with Placeholder text if editing ends and the text field is empty
        print("func textViewDidEndEditing is running...")
        if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count == 0 {
            if  numberInputView.textColor == .black {
                numberInputView.text = VerifyPhoneNumberViewControllerData.numberInputViewText
                numberInputView.textColor = UIColor.lightGray
            }
        }
    }
// number input view methods
    
// submit button methods
    func onSubmitButtonTouch(_ sender: UIButton!) {
        print("func onSubmitButtonTouch is running...")
        sender.isUserInteractionEnabled = false
        switch VerifyPhoneNumberViewControllerData.mode {
        case .phoneNumber:
            print("onSubmitButtonTouch: phoneNumber mode is running...")
            print("User.GCMToken:\(String(describing: MYUser.GCMToken))")
            if let _ = MYUser.GCMToken {
                makeCreateUserWebServiceCallRequest()
                // switch to confirmation mode (for data model)
                self.VerifyPhoneNumberViewControllerData.mode = .confirmationCode
                // update UI for confirmationCode mode
                self.reloadOnScreenText()
            }
        case .confirmationCode:
            print("onSubmitButtonTouch: confirmationCode mode is running...")
            // make request to confirm user phone number in database
            let intent = WebServiceCallIntent.confirmValidationCode(Int(VerifyPhoneNumberViewControllerData.confirmationCode)!)
            let webServiceCallManagerInstance = WebServiceCallManager(intent: intent)
            webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    self.viewControllerTransitionOnButtonTouch = 1 // NOTE: need this flag for keyboard animation during screen transition to home screen
                    self.moveToEditProfileViewController()
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                        print("Could not be verified...")
                    })
                }
            })
        }
    }
    
    func makeCreateUserWebServiceCallRequest() { // pulling this out into own function allows re-use of this code block for re-sending confirmation code
        print("func makeCreateUserWebServiceCallRequest is running...")
        // make request to create user in database
        let webServiceCallManagerInstance = WebServiceCallManager(intent: .createUser)
        webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
            if succeeded {
                print("onSubmitButtonTouch: create user call succeeded")
            }
            else { // post message saying could not join group. try again later
                DispatchQueue.main.async(execute: { () -> Void in
                })
            }
        })
    }
    
    func updateSubmitButtonInteractionState() {
        print("func updateSubmitButtonInteractionState is running...")
        switch VerifyPhoneNumberViewControllerData.mode {
        case .phoneNumber:
            // determine button interaction state
            var enabled: Bool = false // default value
            if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count  != 0 {
                let index = VerifyPhoneNumberViewControllerData.phoneNumber.index(VerifyPhoneNumberViewControllerData.phoneNumber.startIndex, offsetBy: 1)
                let firstDigit = VerifyPhoneNumberViewControllerData.phoneNumber.substring(to: index)
                if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count == 10 && firstDigit != "1"  {
                    enabled = true
                }
                else if VerifyPhoneNumberViewControllerData.phoneNumber.characters.count == 11 { // firstDigit check not required, 11 characters only accepted if first digit is 1
                    enabled = true
                }
            }
            // update button properties for interaction state
            if enabled {
                self.submitButton.isUserInteractionEnabled = true
                self.submitButton.backgroundColor = MYColor.rallyBlue
            }
            else {
                self.submitButton.isUserInteractionEnabled = false
                self.submitButton.backgroundColor = MYColor.greyBlue
            }
        case .confirmationCode:
            // determine button interaction state
            var enabled: Bool = false // default value
            if VerifyPhoneNumberViewControllerData.confirmationCode.characters.count == 4 {
                enabled = true
            }
            // update button properties for interaction state
            if enabled {
                self.submitButton.isUserInteractionEnabled = true
                self.submitButton.backgroundColor = MYColor.rallyBlue
            }
            else {
                self.submitButton.isUserInteractionEnabled = false
                self.submitButton.backgroundColor = MYColor.greyBlue
            }
        }
    }
// submit button methods
    
    func reloadOnScreenText() {
        print("func reloadOnScreenText is running...")
        // update nav bar title
        self.title = VerifyPhoneNumberViewControllerData.navBarTitle
        // update body text
        self.bodyTextLabel.text = VerifyPhoneNumberViewControllerData.bodyText
        // update text field text
        numberInputView.text = ""
        // update button title
        submitButton.setTitle(VerifyPhoneNumberViewControllerData.submitButtonText, for: UIControlState())
        // update button interaction state
        updateSubmitButtonInteractionState()
        // update legal disclaimer to re-send confirmation code/re-submit mobile number
        legalDisclaimer.attributedText = VerifyPhoneNumberViewControllerData.attributedText
    }
    
    func moveToEditProfileViewController() {
        print("func moveToEditProfileViewController is running...")
        navigationController!.popViewController(animated: true) // NOTE: if transition animation goes awry, try removing this line
        let editProfileViewControllerInstance = EditProfileViewController(nibName: nil, bundle: nil)
        navigationController!.setViewControllers([editProfileViewControllerInstance], animated: true)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        print("func textView:shouldInteractWithURL is running...")
        switch VerifyPhoneNumberViewControllerData.mode {
        case .phoneNumber:
            print("textView:shouldInteractWithURL:.phoneNumber")
            UIApplication.shared.openURL(URL)
            return true
        case .confirmationCode:
            print("textView:shouldInteractWithURL:.confirmationCode")
            let value = URL.absoluteString
            switch value {
            case "https://resendcode.net":
                print("resend code")
                makeCreateUserWebServiceCallRequest()
                return false
            case "https://editnumber.net":
                print("change mode back to edit number and reload screen")
                // switch to confirmation mode (for data model)
                self.VerifyPhoneNumberViewControllerData.mode = .phoneNumber
                // update UI for confirmationCode mode
                self.reloadOnScreenText()
                return false
            default:
                return false
            }
        }
    }
}
