//
//  WebServiceCallManager.swift
//  Rally
//
//  Created by Ian Moses on 9/4/16.
//  Copyright © 2016 Ian Moses. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import CryptoSwift

class WebServiceCallDataSource {
    let webServiceCallURL = "https://rallydevelopment.azurewebsites.net/RallyWebService.svc/RallyAPI" //developmentWebAppURL
    // productionWebAppURL: "https://150labs.azurewebsites.net/RallyWebService.svc/RallyAPI"
    var intent: WebServiceCallIntent
    var imageData: Data?
    // Request data
    var payload: String? {
        get { // returns up-to-date formatted payload to use inside package
            return self.formatPayload()
        }
    }
    var package: URLRequest? {
        get{ // returns up-to-date formatted package for web service call
            return self.formatPackage()
        }
    }
    var response: DataResponse<Any>!
    var responseSucceeded: Bool!
    var swiftyJSONResponse: JSON!
    var swiftyJSONResponsePayload: JSON!
    //var response: (Alamofire.Response<AnyObject, NSError>)?
    var responseReturnParameters: WebServiceCallReturnParameters? // returnable parameters unique to each call (defined in enum type)
    var requestType: WebServiceCallRequestType { // request, stream, upload, etc
        get {
            // returns up-to-date formatted payload to use inside package
            return self.getRequestType()
        }
    }
    var checkpointData: WebServiceCallCheckpointData!
    
    init(intent: WebServiceCallIntent) {
        self.intent = intent
    }
    
    func getRequestType()-> WebServiceCallRequestType {
        print("func getRequestType is running...")
        switch self.intent { // add non-default cases to specify custom requests
        // case .updateProfilePicture:
            // return .upload
        default:
            return .request
        }
    }
    
    func formatPackage()-> URLRequest? {
        print("func formatPackage is running...")
        let keychainHelper = KeychainHelper()
        // set private key
        var privateKey = ""
        if let value = keychainHelper.privateKey {
            privateKey = value
        }
        // set public key
        var publicKey = ""
        if let value = keychainHelper.publicKey {
            publicKey = value
        }
        // concatonate keys
        let concatonatedKey = publicKey+privateKey
        let asdf = concatonatedKey.utf8.map({$0})
        // generate hmac md5 keyed-hash
        guard let payload = formatPayloadJSONForPackage() else {
            return nil
        }
        var hmacmd5Base64String: String = ""
        do {
            let hmacmd5ByteArray: Array<UInt8> = try HMAC(key: asdf, variant: .md5).authenticate(formatPayloadJSONForDigest(payload))
            let hmacmd5data = Data(bytes: hmacmd5ByteArray)
            hmacmd5Base64String = hmacmd5data.base64EncodedString()
        }
        catch _ {
            print("error...")
        }
        // get rally version
        let rallyVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        // get intent (as type string)
        var intentAsString = String(describing: self.intent)
        print("intentAsString:\(intentAsString)")
        let leftParenthesisIndex = intentAsString.range(of: "(")?.lowerBound
        print("leftParenthesisIndex:\(String(describing: leftParenthesisIndex))")
        if let value = leftParenthesisIndex {
            print("value:\(value)")
            intentAsString = intentAsString.substring(to: value)
        }
        // assemble JSON
        let requestPackageJSON = "{\"APICheckpoint\":{\"UserID\":\(MYUser.ID),\"DeviceType\":\"Apple\",\"AppVersion\":\"\(rallyVersion)\",\"ApiKey\":\"\(publicKey)\",\"Intent\":\"\(intentAsString)\",\"Payload\":\"\(payload)\",\"HashedPayload\":\"\(hmacmd5Base64String)\"}}"
        // initialize URL request
        var requestPackage = URLRequest(url: NSURL(string: webServiceCallURL)! as URL)
        requestPackage.httpMethod = "POST"
        requestPackage.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        requestPackage.addValue("application/json", forHTTPHeaderField: "Accept")
        //code from http://stackoverflow.com/questions/24566180/how-to-post-a-json-with-new-apple-swift-language/24567242#24567242
        requestPackage.httpBody = requestPackageJSON.data(using: String.Encoding.utf8, allowLossyConversion: true)
        print("formatPackage:requestPackageJSON:\(requestPackageJSON)")
        return requestPackage
    }
    
    func formatPayloadJSONForDigest(_ payload: String)->Array<UInt8> {
        print("func formatPayloadJSONForDigest is running...")
        // !^Rr)@bfk$%H!&vVvDS##@dln^ = 😎
        //let PayloadJSONForDigest = self.payload?.replacingOccurrences(of: "😎", with: "")
        let PayloadJSONForDigest = payload.replacingOccurrences(of: "😎", with: "")
        let payloadByteArray: Array<UInt8> = PayloadJSONForDigest.utf8.map({$0})
        //print("payloadByteArray:\(payloadByteArray)")
        return payloadByteArray
    }
    
    func formatPayloadJSONForPackage()->String? {
        print("func formatPayloadJSONForPackage is running...")
        let PayloadJSONForPackage = self.payload?.replacingOccurrences(of: "😎", with: "\\")
        print("PayloadJSONForPackage:\(String(describing: PayloadJSONForPackage))")
        return PayloadJSONForPackage
    }
    
    func formatPayload()-> String? {
        print("func formatPayload is running...")
        switch self.intent {
        case .createRally(let rallyObject):
            return generateCreateRallyJSON(rallyObject)
        case .createCommunity(let community):
            return generateCreateCommunityJSON(community)
        case .getRallys:
            return generateGetRallysJSON()
        case .getCommunities:
            return generateGetCommunitiesJSON()
        case .postRallyMessage(let rallyObject, let message):
            return generatePostRallyMessageJSON(rallyObject, message: message)
        case .getNewStockImages:
            return generateGetNewStockImagesJSON()
        case .updateIsRallyActiveFlag(let rallyObject):
            return generateUpdateIsRallyActiveFlagJSON(rallyObject)
        case .updateRallyDetails(let rallyObject):
            return generateUpdateRallyDetailsJSON(rallyObject)
        case .getNearbyCommunitySearchResults(let searchText):
            return generateGetNearbyCommunitySearchResultsJSON(searchText)
        case .getRallyMessages(let rallyObject):
            return generateGetRallyMessagesJSON(rallyObject)
        case .getUpdatedRallyPackage(_, let rallyID):
            return generateGetUpdatedRallyPackageJSON(rallyID)
        case .joinCommunity(let community):
            return generateJoinCommunityJSON(community)
        case .updateDidUserAccept(let rallyObject, let didUserAccept):
            return generateUpdateDidUserAcceptJSON(rallyObject, didUserAccept: didUserAccept)
        case .createUsersWithPhoneContacts:
            return generateCreateUsersWithPhoneContactsJSON()
        case .getSingleRallyMessage(let rallyMessageID):
            return generateGetSingleRallyMessageJSON(rallyMessageID)
        case .addSupportRequest(let issueDescription):
            return generateAddSupportRequestJSON(issueDescription)
        case .createUser:
            return generateCreateUserJSON()
        case .confirmValidationCode(let validationCode):
            return generateValidationCodeJSON(validationCode)
        case .uploadLargeProfileImage(let largeProfilePicture):
            return generateUploadLargeProfileImageJSON(largeProfilePicture)
        case .updateUserNames(let firstName, let lastName):
            return generateUpdateUserNamesJSON(firstName, lastName)
        case .getThumbnailProfileImage(let memberID, _):
            return generateGetThumbnailProfileImageJSON(memberID)
        case .leaveCommunity(let communityID):
            return generateLeaveCommunityJSON(communityID)
        case .getCommThreadMessages(let thread):
            return generateGetCommThreadMessagesJSON(for: thread)
        case .getCommThreadMessage(let commThreadMessageID):
            return generateGetCommThreadMessageJSON(commThreadMessageID: commThreadMessageID)
        case .checkForUpdatedThumbnailProfileImage(let userID, let thumbnailProfileImageID):
            return generateCheckForUpdatedThumbnailProfileImageJSON(userID, thumbnailProfileImageID)
        case .postCommThreadMessage(let commThreadMessage, _):
            return generatePostCommThreadMessageJSON(commThreadMessage: commThreadMessage)//CommThreadMessage
        case .getLargeProfileImage(let memberID, _):
            return generateGetLargeProfileImageJSON(memberID: memberID)
        }
    }
    
    func generateCreateRallyJSON(_ rallyObject: Rally)-> String {
        /*web service user login POST. Returns CreateUserResult. If UserID returns 0, there was an error and the user was not created. If the StatusFlag is 0, it means the user is newly created, and the phone application must now run through the phone number verification screens. If the StatusFlag is 1, it means the user is an existing user, and there is no need to verify the phone number again.*/
        print("func generateCreateRallyJSON is running...")
        let rallyLocation: String = rallyObject.rallyLocation
        let rallyStartDateTime: String = dateHelperClass().localTimeNSDateToUTCString(rallyObject.rallyStartDateTime)
        let jsonString = "{😎\"RallyPackageList😎\":{😎\"CreatedByUserID😎\":\(MYUser.ID),😎\"CreatedByDeviceType😎\": 2, 😎\"IsRallyActive😎\":\(rallyObject.isRallyActive),😎\"RallyDetails😎\":{😎\"RallyLocationAddress😎\":😎\"\(rallyLocation)😎\",😎\"RallyName😎\":😎\"\(rallyObject.rallyName)😎\",😎\"RallyStartDateTime😎\":😎\"\(rallyStartDateTime)😎\",😎\"RallyLocationLongitude😎\":\(rallyObject.longitude),😎\"RallyLocationLatitude😎\":\(rallyObject.latitude)},😎\"RallyUserList😎\":["
        var JSONStringConcatonationMethodFriendIndex = 0
        var  createRallyWebServiceCallJSON = ""
        var rallyUserListMember = ""
        for friend in rallyObject.rallyUserList {
            var firstName = "{😎\"FirstName😎\":😎\"nil😎\","
            if let value = friend.firstName {
                firstName = "{😎\"FirstName😎\":😎\"\(value)😎\","
            }
            var lastName = "😎\"LastName😎\":😎\"nil😎\","
            if let value = friend.lastName {
                lastName = "😎\"LastName😎\":😎\"\(value)😎\","
            }
            var phoneNumberListString = "😎\"PhoneNumberList😎\":["
            var phoneNumberListIndex = 0
            for (key, value) in friend.phoneNumberList {
                if let valueAsString = value {
                    // print("Dictionary key \(key) -  Dictionary value \(valueAsString)")
                    phoneNumberListString += "{😎\"Type😎\":😎\"\(key)😎\",😎\"Number😎\":😎\"\(valueAsString)😎\"}"
                }
                else {
                    // print("Dictionary key \(key) -  Dictionary value nil")
                    phoneNumberListString += "{😎\"Type😎\":😎\"\(key)😎\",😎\"Number😎\":😎\"nil😎\"}"
                }
                if friend.phoneNumberList.count - 1 > phoneNumberListIndex {
                    /* does this person have more numbers, if so use this format*/
                    phoneNumberListString += ","
                }
                else if JSONStringConcatonationMethodFriendIndex < rallyObject.rallyUserList.count - 1 {
                    /* if there aren't any more phone number pairs to list, is this the last person in the rally? If not, use this format. If so, go to else statement and close entire object.*/
                    phoneNumberListString += "]},"
                }
                else {
                    phoneNumberListString += "]}]}}"
                }
                phoneNumberListIndex += 1
            }
            rallyUserListMember += "\(firstName)"
            rallyUserListMember += "\(lastName)"
            rallyUserListMember += "\(phoneNumberListString)"
            // print(rallyUserListMember)
            JSONStringConcatonationMethodFriendIndex += 1
            createRallyWebServiceCallJSON = jsonString + "\(rallyUserListMember)"
        }
        return createRallyWebServiceCallJSON
    }
    
    func generateCreateCommunityJSON(_ community: Community)-> String {
        print("func generateCreateCommunityJSON is running...")
        var isPrivateForBackend: Int = 0
        if community.isPrivate == true { // private is 1, public is 0
            isPrivateForBackend = 1
        }
        let createCommunityJSON = "{😎\"Community😎\":{😎\"CreatedByUserID😎\":\(MYUser.ID),😎\"CommunityName😎\":😎\"\(community.name)😎\",😎\"Latitude😎\":😎\"\(community.latitude)😎\",😎\"Longitude😎\":😎\"\(community.longitude)😎\",😎\"IsPrivate😎\":😎\"\(isPrivateForBackend)😎\",😎\"MaximumMembers😎\":😎\"\(community.maximumNumberOfMembers)😎\"}}"
        return createCommunityJSON
    }
    
    func generateGetRallysJSON()-> String {
        print("func generateGetRallysJSON is running...")
        let getRallysJSON = "{😎\"User😎\":{😎\"UserID😎\":\(MYUser.ID)}}"
        return getRallysJSON
    }
    
    func generateGetCommunitiesJSON()-> String {
        print("func generateGetCommunitiesJSON is running...")
        let getCommunitiesJSON = "{😎\"User😎\":{😎\"UserID😎\":\(MYUser.ID)}}"
        return getCommunitiesJSON
    }
    
    func generatePostRallyMessageJSON(_ rallyObject: Rally, message: String)-> String {
        print("func generatePostRallyMessageJSON is running...")
        let postRallyMessageJSON = "{😎\"RallyMessage😎\":{😎\"RallyMessageText😎\":😎\"\(message)😎\",😎\"RallyID😎\":\(rallyObject.rallyID),😎\"UserID😎\":\(MYUser.ID)}}"
        return postRallyMessageJSON
    }
    
    func generateGetNewStockImagesJSON()-> String {
        print("func generateGetNewStockImagesJSON is running...")
        var getNewStockImagesJSON = ""
        if let stockImagesObjects = UserDefaults.standard.data(forKey: "stockImagesResultKey") { // check if stock image object are saved
            getNewStockImagesJSON += "["
            let stockImagesObjects = JSON(data: stockImagesObjects) // save into json format
            for i in 0..<stockImagesObjects.count { // loop through json to format stockImageID: ID key pair package for webservice
                // append key pair formatted value to jsonString
                getNewStockImagesJSON += "{😎\"StockImageID😎\":\(stockImagesObjects[i]["StockImageID"])},"
            }
            getNewStockImagesJSON = String(getNewStockImagesJSON.characters.dropLast()) // drops last comma
            getNewStockImagesJSON += "]" // close JSON
        }
        print("generateGetNewStockImagesJSON:getNewStockImagesJSON:\(getNewStockImagesJSON)")
        return getNewStockImagesJSON
    }
    
    func generateUpdateIsRallyActiveFlagJSON(_ rallyObject: Rally)-> String {
        print("func generateUpdateIsRallyActiveFlagJSON is running...")
        let updateIsRallyActiveFlagJSON = "{😎\"UpdateIsRallyActiveFlagPackage😎\":{😎\"IsRallyActive😎\":\(rallyObject.isRallyActive),😎\"RallyID😎\":\(rallyObject.rallyID)}}"
        return updateIsRallyActiveFlagJSON
    }
    
    func generateUpdateRallyDetailsJSON(_ rallyObject: Rally)-> String {
        print("func generateUpdateRallyDetailsJSON is running...")
        let rallyStartDateTime: String = dateHelperClass().localTimeNSDateToUTCString(rallyObject.rallyStartDateTime) // this call removes the +0000 apple puts at end of string (formatting required for backend)
        let updateRallyDetailsJSON = "{😎\"UpdateRallyDetailsPackage😎\":{😎\"CreatedDateTimeStamp😎\":😎\"\(rallyObject.createdDateTimeStamp)😎\",😎\"RallyLocationAddress😎\":😎\"\(rallyObject.rallyLocation)😎\",😎\"RallyName😎\":😎\"\(rallyObject.rallyName)😎\",😎\"RallyStartDateTime😎\":😎\"\(rallyStartDateTime)😎\",😎\"RallyDetailsID😎\":\(rallyObject.rallyDetailsID),😎\"RallyID😎\":\(rallyObject.rallyID),😎\"RallyLocationLatitude😎\":\(rallyObject.latitude),😎\"RallyLocationLongitude😎\":\(rallyObject.longitude)}}"
        return updateRallyDetailsJSON
    }
    
    func generateGetNearbyCommunitySearchResultsJSON(_ searchText: String)-> String {
        print("func generateGetNearbyCommunitySearchResultsJSON is running...")
        let getNearbyCommunitySearchResultsJSON = "{😎\"CommunitySearchPackage😎\":{😎\"SearchText😎\":😎\"\(searchText)😎\",😎\"Latitude😎\": \(MYUser.latitude),😎\"Longitude😎\":\(MYUser.longitude),😎\"UserID😎\":\(MYUser.ID)}}"
        return getNearbyCommunitySearchResultsJSON
    }
    
    func generateGetRallyMessagesJSON(_ rallyObject: Rally)-> String {
        print("func generateGetRallyMessagesJSON is running...")
        let getRallyMessagesJSON = "{😎\"RallyPackage😎\":{😎\"RallyID😎\":\(rallyObject.rallyID)}}"
        return getRallyMessagesJSON
    }
    
    func generateGetUpdatedRallyPackageJSON(_ rallyID: Int)-> String {
        print("func generateGetUpdatedRallyPackageJSON is running...")
        let getUpdatedRallyPackage = "{😎\"RallyPackage😎\":{😎\"RallyID😎\":\(rallyID)}}"
        return getUpdatedRallyPackage
    }
    
    func generateJoinCommunityJSON(_ community: Community)-> String {
        print("func generateJoinCommunityJSON is running...")
        let joinCommunity = "{😎\"CommunityUsersXref😎\":{😎\"CommunityID😎\":\(community.communityID),😎\"UserID😎\":\(MYUser.ID)}}"
        return joinCommunity
    }
    
    func generateUpdateDidUserAcceptJSON(_ rallyObject: Rally, didUserAccept: Int)-> String {
        print("func generateUpdateDidUserAcceptJSON is running...")
        let updateDidUserAcceptJSON = "{😎\"UpdateDidUserAcceptPackage😎\":{😎\"DidUserAccept😎\":\(didUserAccept),😎\"RallyID😎\":\(rallyObject.rallyID),😎\"UserID😎\":\(MYUser.ID)}}"
        return updateDidUserAcceptJSON
    }
    
    func generateCreateUsersWithPhoneContactsJSON()-> String {
        print("func generateCreateUsersWithPhoneContactsJSON is running...")
        /*
        var rallyUserListMember = ""
        for (JSONStringConcatonationMethodFriendIndex,friend) in friends.enumerated() {
            var firstName = "{😎\"FirstName😎\":😎\"nil😎\","
            var lastName = "😎\"FirstName😎\":😎\"nil😎\","
            if let value = friend.firstName {
                firstName = "{😎\"FirstName😎\":😎\"\(value)😎\","
            }
            if let value = friend.lastName {
                lastName =  "😎\"LastName😎\":😎\"\(value)😎\","
            }
            let appleContactIdentifier = "😎\"AppleContactIdentifier😎\":😎\"\(friend.identifier!)😎\","
            let imageDataAvailable = "😎\"ImageDataAvailable😎\":\(friend.imageDataAvailable!),"
            var phoneNumberListString = "😎\"PhoneNumberList😎\":["
            var counter = 0
            for (key, value) in friend.phoneNumberList {
                if let valueAsString = value {
                    // print("Dictionary key \(key) -  Dictionary value \(valueAsString)")
                    phoneNumberListString += "{😎\"Number😎\":😎\"\(valueAsString)😎\",😎\"Type\":😎\"\(key)😎\"}"
                }
                else {
                    // print("Dictionary key \(key) -  Dictionary value nil")
                    phoneNumberListString += "{😎\"Number😎\":😎\"nil😎\",😎\"Type😎\":😎\"\(key)😎\"}"
                }
                if friend.phoneNumberList.count-1 > counter {
                    /* does this person have more numbers, if so use this format*/
                    phoneNumberListString += ","
                }
                else if JSONStringConcatonationMethodFriendIndex < friends.count - 1 {
                    /* if there aren't any more phone number pairs to list, is this the last person in the rally? If not, use this format. If so, go to else statement and close entire object.*/
                    phoneNumberListString += "]},"
                }
                else {
                    phoneNumberListString += "]}]"
                }
            }
            rallyUserListMember += "\(firstName)"+"\(lastName)"+"\(appleContactIdentifier)"+"\(imageDataAvailable)"+"\(phoneNumberListString)"
            counter += 1
        }
        let createUsersWithPhoneContactsJSON = "["+"\(rallyUserListMember)"
        return createUsersWithPhoneContactsJSON
         */
        return "asdf"
    }
    
    func generateGetSingleRallyMessageJSON(_ rallyMessageID: Int)-> String {
        print("func generateGetSingleRallyMessageJSON is running...")
        let getSingleRallyMessage = "{😎\"RallyMessage😎\":{😎\"RallyMessageID😎\":\(rallyMessageID)}}"
        return getSingleRallyMessage
    }
    
    func generateAddSupportRequestJSON(_ issueDescription: String)-> String {
        print("func generateAddSupportRequestJSON is running...")
        var firstName = ""
        var lastName = ""
        if let value = MYUser.firstName {
            firstName = value
        }
        if let value = MYUser.lastName {
            lastName = value
        }
        let addSupportRequestJSON = "{😎\"SupportRequest😎\":{😎\"FirstName\":😎\"\(firstName)😎\",😎\"LastName😎\":😎\"\(lastName)😎\",😎\"IssueType😎\":😎\"Bug😎\",😎\"IssueDescription😎\":😎\"\(issueDescription)😎\"}}"
        return addSupportRequestJSON
    }
    
    func generateCreateUserJSON()-> String {
        print("func generateCreateUserJSON is running...")
        // mark call as being made
        WebCallStatus.createUserWebCall = 1
        
        var userPhoneNumber = ""
        var firstName = ""
        var lastName = ""
        var GCMToken = ""
        if let value = MYUser.phoneNumberList["Mobile"] {
            if let value2 = value {
                userPhoneNumber = value2
            }
        }
        if let value = MYUser.firstName {
            firstName = value
        }
        if let value = MYUser.lastName {
            lastName = value
        }
        if let value = MYUser.GCMToken {
            GCMToken = value
        }
        let deviceType = "Apple"
        let deviceModel = UIDevice().type.rawValue
        let screenWidth = String(describing: UIScreen.main.bounds.width)
        let screenHeight = String(describing: UIScreen.main.bounds.height)
        let deviceDisplayMetrics = "screenWidth x screenHeight (points):\(screenWidth) x \(screenHeight)"
        let OSVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let createUserJSON = "{😎\"User😎\":{😎\"DeviceType😎\":😎\"\(deviceType)😎\",😎\"DeviceModel😎\":😎\"\(deviceModel)😎\",😎\"DeviceDisplayMetrics😎\":😎\"\(deviceDisplayMetrics)😎\",😎\"OSVersion😎\":😎\"\(OSVersion)😎\",😎\"AppVersion😎\":😎\"\(appVersion)😎\",😎\"PhoneNumber😎\":😎\"\(userPhoneNumber)😎\",😎\"FirstName😎\":😎\"\(firstName)😎\",😎\"LastName😎\":😎\"\(lastName)😎\",😎\"GCMToken😎\":😎\"\(GCMToken)😎\"}}"
        /*
        "{\"User\":{\"DeviceType\":\"\(deviceType)\",\"DeviceModel\":\"\(deviceModel)\",\"DeviceDisplayMetrics\":\"\(deviceDisplayMetrics)\",\"OSVersion\":\"\(OSVersion)\",\"AppVersion\":\"\(appVersion)\",\"PhoneNumber\":\"\(userPhoneNumber)\",\"FirstName\":\"\(firstName)\",\"LastName\":\"\(lastName)\",\"GCMToken\":\"\(GCMToken)\"}}"
        */
        print("generateCreateUserJSON:createUserJSON:\(createUserJSON)")
        return createUserJSON
    }
    
    func generateValidationCodeJSON(_ validationCode: Int)-> String {
        print("func generateValidationCodeJSON is running...")
        let validationCodeJSON = "{😎\"ValidationCode😎\":{😎\"Code😎\":\(validationCode),😎\"UserID😎\":\(MYUser.ID)}}"
        return validationCodeJSON
    }
    
    func generateUploadLargeProfileImageJSON(_ largeProfilePicture: Data)-> String {
        print("func generateUploadLargeProfileImageJSON is running...")
        //Encode to base64 string
        let base64LargeProfilePicture = largeProfilePicture.base64EncodedString()
        let uploadLargeProfileImageJSON = "{😎\"LargeProfileImage😎\":{😎\"ImageData😎\":😎\"\(base64LargeProfilePicture)😎\",😎\"UserID😎\":\(MYUser.ID)}}"
        return uploadLargeProfileImageJSON
    }
    
    func generateUpdateUserNamesJSON(_ firstName: String, _ lastName: String)-> String {
        print("func generateUpdateUserNamesJSON is running...")
        var userPhoneNumber = ""
        var firstName = ""
        var lastName = ""
        var GCMToken = ""
        if let userPhoneNumberInstance = MYUser.phoneNumberList["Mobile"] {
            userPhoneNumber = userPhoneNumberInstance!
        }
        if let value = MYUser.firstName {
            firstName = value
        }
        if let value = MYUser.lastName {
            lastName = value
        }
        if let GCMTokenInstance = MYUser.GCMToken {
            GCMToken = GCMTokenInstance
        }
        let deviceType = "Apple"
        let deviceModel = UIDevice().type.rawValue
        let screenWidth = String(describing: UIScreen.main.bounds.width)
        let screenHeight = String(describing: UIScreen.main.bounds.height)
        let deviceDisplayMetrics = "screenWidth x screenHeight (points):\(screenWidth) x \(screenHeight)"
        let OSVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let updateUserNamesJSON = "{😎\"User😎\":{😎\"DeviceType😎\":😎\"\(deviceType)😎\",😎\"DeviceModel😎\":😎\"\(deviceModel)😎\",😎\"DeviceDisplayMetrics😎\":😎\"\(deviceDisplayMetrics)😎\",😎\"OSVersion😎\":😎\"\(OSVersion)😎\",😎\"AppVersion😎\":😎\"\(appVersion)😎\",😎\"PhoneNumber😎\": 😎\"\(userPhoneNumber)😎\",😎\"FirstName😎\":😎\"\(firstName)😎\",😎\"LastName😎\":😎\"\(lastName)😎\",😎\"GCMToken😎\":😎\"\(GCMToken)😎\",😎\"UserID😎\":\(MYUser.ID)}}"
        return updateUserNamesJSON
    }
    
    func generateGetThumbnailProfileImageJSON(_ memberID: Int)-> String {
        print("func generateGetThumbnailProfileImageJSON is running...")
        let getThumbnailProfileImageJSON = "{😎\"User😎\":{😎\"UserID😎\":\(memberID)}}"
        return getThumbnailProfileImageJSON
    }
    
    func generateLeaveCommunityJSON(_ communityID: Int)-> String {
        print("func generateLeaveCommunityJSON is running...")
        let leaveCommunityJSON = "{😎\"CommunityUsersXRef😎\":{😎\"CommunityID😎\":\(communityID),😎\"UserID😎\":\(MYUser.ID)}}"
        return leaveCommunityJSON
    }
    
    func generateGetCommThreadMessagesJSON(for thread: Thread)-> String {
        print("func generateGetCommThreadMessagesJSON is running...")
        let getCommThreadMessagesJSON = "{😎\"CommThread😎\":{😎\"CommThreadID😎\":\(thread.threadID)}}"
        return getCommThreadMessagesJSON
    }
    
    func generateGetCommThreadMessageJSON(commThreadMessageID: Int)-> String {
        print("func generateGetCommThreadMessageJSON is running...")
        let getCommThreadMessageJSON = "{😎\"CommThreadMessage😎\":{😎\"CommThreadMessageID😎\":\(commThreadMessageID)}}"
        return getCommThreadMessageJSON
    }
    
    func generateCheckForUpdatedThumbnailProfileImageJSON(_ userID: Int,_ thumbnailProfileImageID: Int)-> String {
        print("func generateCheckForUpdatedThumbnailProfileImageJSON is running...")
        let checkForUpdatedThumbnailProfileImageJSON = "{😎\"ThumbnailProfileImage😎\":{😎\"UserID😎\":\(userID),😎\"ThumbnailProfileImageID😎\":\(thumbnailProfileImageID)}}"
        return checkForUpdatedThumbnailProfileImageJSON
    }
    
    func generatePostCommThreadMessageJSON(commThreadMessage: CommThreadMessage)-> String {
        print("func generatePostCommThreadMessageJSON is running...")
        let PostCommThreadMessageJSON = "{😎\"CommThreadMessage😎\":{😎\"CommThreadID😎\":\(commThreadMessage.commThreadID),😎\"CreatedByUserID😎\":\(commThreadMessage.createdByUserID),😎\"CommThreadMessageTypeID😎\":\(commThreadMessage.commThreadMessageTypeID),😎\"MessageByteString😎\":😎\"\(commThreadMessage.messageByteString)😎\"}}"
        return PostCommThreadMessageJSON
    }
    
    
    func generateGetLargeProfileImageJSON(memberID: Int)-> String {
        print("func generateLargeProfileJSON is running...")
        let getLargeProfileImageJSON = "{😎\"User😎\":{😎\"UserID😎\":\(memberID)}}"
        return getLargeProfileImageJSON
    }
}

struct WebServiceCallManager {
    let webServiceCallData: WebServiceCallDataSource!
    
    init(intent: WebServiceCallIntent) {
        print("struct WebServiceCallManager:init func is running...")
        self.webServiceCallData = WebServiceCallDataSource(intent: intent)
    }

    func makeRequest(onPostCompleted:@escaping (_ succeeded: Bool, _ returnParameters: WebServiceCallReturnParameters?)->Void) {
        print("func makeRequest is running...")
        print("func makeRequest: webServiceCallData.intent:\(webServiceCallData.intent)")
        /*
        if let preRequest = webServiceCallData.package {
            if let value = preRequest.httpBody {
                do {
                    let jsonArray = try JSONSerialization.jsonObject(with: value, options: [])
                    print("preRequest as Array: \(jsonArray)")
                }
                catch {
                    print("Error: \(error)")
                }
            }

        }
        */
        DispatchQueue.global(qos: .userInitiated).async {
            if Reachability.isConnectedToNetwork() { // Continue with attempting call
                print("webServiceCallBoilerplate Reachability.isConnectedToNetwork() is true")
                switch self.webServiceCallData.requestType {
                case .request:
                    
                    let AFRequest = Alamofire.request(self.webServiceCallData.package!)
                        .responseJSON { response in
                            
                            StartupRunTimer.end = Date()
                            let totalTimeElapsedStartEnd = StartupRunTimer.end.timeIntervalSince(StartupRunTimer.start)
                            let totalTimeElapsedMidEnd = StartupRunTimer.end.timeIntervalSince(StartupRunTimer.mid)
                            let intentAsString = String(describing: self.webServiceCallData.intent)
                            //print("intentAsString:\(intentAsString)")
                            //print("totalTimeElapsed:\(totalTimeElapsedStartEnd)")
                            //print("totalTimeElapsed:\(totalTimeElapsedMidEnd)")
                            
                            self.webServiceCallData.response = response
                            self.handleResponse()
                            print("self.handleResponse() completed, about to run onPostCompleted...")
                            if let preRequest = self.webServiceCallData.package {
                                if let value = preRequest.httpBody {
                                    do {
                                        let jsonArray = try JSONSerialization.jsonObject(with: value, options: [])
                                        print("preRequest as Array: \(jsonArray)")
                                    }
                                    catch {
                                        print("Error: \(error)")
                                    }
                                }
                                
                            }
                            //print("self.webServiceCallData.responseSucceeded:\(self.webServiceCallData.responseSucceeded)")
                            //print("self.webServiceCallData.responseReturnParameters:\(self.webServiceCallData.responseReturnParameters)")
                            
                            
                            
                            
                            
                            onPostCompleted(self.webServiceCallData.responseSucceeded, self.webServiceCallData.responseReturnParameters)
                            print("self.handleResponse() completed, finished running onPostCompleted...")
                    }
                    //StartupRunTimer.mid = Date()
                    
                    if let requestBody = AFRequest.request?.httpBody {
                        do {
                            let jsonArray = try JSONSerialization.jsonObject(with: requestBody, options: [])
                            print("AFRequest.request as Array: \(jsonArray)")
                        }
                        catch {
                            print("Error: \(error)")
                        }
                    }
                    
                case .upload:
                    print("Add photo call handler here")
                    /*
                    Alamofire.upload(
                        .POST,
                        webServiceCallData.webServiceCallURL,
                        //headers: ["Authorization" : "Basic xxx"],
                        multipartFormData: { multipartFormData in
                            multipartFormData.appendBodyPart(data: self.webServiceCallData.imageData!, name: "imagefile",
                                fileName: "image.jpg", mimeType: "image/jpeg")
                        },
                        encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.validate()
                                upload.responseJSON { response in
                                    self.webServiceCallData.response = response
                                    self.handleResponse()
                                    onPostCompleted(succeeded: self.webServiceCallData.responseSucceeded, returnParameters: self.webServiceCallData.responseReturnParameters)
                                }
                            case .failure(let encodingError):
                                print("encodingError:\(encodingError)")
                                self.webServiceCallData.responseSucceeded = false
                                onPostCompleted(succeeded: self.webServiceCallData.responseSucceeded, returnParameters: nil)
                            }
                        }
                    )*/
                case .stream:
                    print("Do nothing...currently no calls using this request type")
                }
            }
            else {
                print("webServiceCallBoilerplate Reachability.isConnectedToNetwork() is false")
                // initialize alertview data model
                self.webServiceCallData.checkpointData = WebServiceCallCheckpointData(customValidationType: .NetworkReachability)
                // display maintenance mode/no internet connection/version out of date alert view
                self.displayWebServiceCallError()
                self.webServiceCallData.responseSucceeded = false
                onPostCompleted(self.webServiceCallData.responseSucceeded, nil)
            }
        }
    }
    
    func handleResponse() { // resolve non-payload returned data: maintenanceMode, networkReachability (nil-returned package), appVersionSupported, isUserBanned, isPhoneNumberVerified, isUserAuthenticated, isPayloadClean
        print("func handleResponse is running...")
        // check for errors captured in response (alamofire catches 200-299 acceptable, others are errors, other general error checks and includes in this property)
        guard self.webServiceCallData.response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print("web service call returned an error")
            print(self.webServiceCallData.response.result.error!)
            webServiceCallData.responseSucceeded = false
            return
        }
        // check to make sure response value is not nil
        guard let value = self.webServiceCallData.response.result.value else {
            webServiceCallData.responseSucceeded = false
            return
        }
        
        print("response.data:\(value)")
        switch self.webServiceCallData.response.result {
        case .success(let value):
            let json = JSON(value)
            print("FIND ME JSON: \(json)")
        case .failure(let error):
            print(error)
        }
 
        /*
        guard let responseJSON = response.result.value as? [String: AnyObject],
            uploadedFiles = responseJSON["uploaded"] as? [AnyObject],
            firstFile = uploadedFiles.first as? [String: AnyObject],
            firstFileID = firstFile["id"] as? String else {
                print("Invalid information received from service")
                return
        }
        */
        webServiceCallData.swiftyJSONResponse = JSON(value)
        // check to make sure swiftyJSONResponse is not nil (this may not be necessary, the swiftyJSON conversion may always work)
        guard webServiceCallData.swiftyJSONResponse["APIValidation"] != "NULL" else {
            // Web Service validation error, stop workflow and instead display alertview to alert user of validation error type
            // initialize alertview data model
            webServiceCallData.checkpointData = WebServiceCallCheckpointData(webServiceCallResponse: webServiceCallData.swiftyJSONResponse)
            // display error alert view to user
            self.displayWebServiceCallError()
            // set response value for completion handler function in calling method
            webServiceCallData.responseSucceeded = false
            return
        }
        // check for payloadType "ErrorResponse"
        guard webServiceCallData.swiftyJSONResponse["PayloadType"] != "ErrorResponse" else {
            // Web Service validation error, stop workflow and instead display alertview to alert user of validation error type
            // initialize alertview data model
            webServiceCallData.checkpointData = WebServiceCallCheckpointData(webServiceCallResponse: webServiceCallData.swiftyJSONResponse)
            print("webServiceCallCheckpointData.className:\(String(describing: webServiceCallData.checkpointData.className))")
            print("webServiceCallCheckpointData.methodName:\(String(describing: webServiceCallData.checkpointData.methodName))")
            print("webServiceCallCheckpointData.errorCode:\(String(describing: webServiceCallData.checkpointData.errorCode))")
            print("webServiceCallCheckpointData.errorDescription:\(String(describing: webServiceCallData.checkpointData.errorDescription))")
            print("webServiceCallCheckpointData.validationMessage:\(String(describing: webServiceCallData.checkpointData.validationMessage))")
            print("webServiceCallCheckpointData.validationType:\(String(describing: webServiceCallData.checkpointData.validationType))")
            print("webServiceCallCheckpointData.validationTitle:\(String(describing: webServiceCallData.checkpointData.validationTitle))")
            // display error alert view to user
            self.displayWebServiceCallError()
            // set response value for completion handler function in calling method
            webServiceCallData.responseSucceeded = false
            return
        }
        //let adsf = webServiceCallData.swiftyJSONResponse["PayloadType"]["GenericResponse"]["SuccessCode"]
        //let fdsa = webServiceCallData.swiftyJSONResponse["PayloadType"]["GenericResponse"]
        //print("adsf:\(adsf)")
        //print("fdsa:\(fdsa)")
        // check for PayloadType "Generic Response" "SuccessCode" of false
        guard webServiceCallData.swiftyJSONResponse["PayloadType"]["GenericResponse"]["SuccessCode"] != "false" else { // not positive successcode response is string false
            print("error: webservice PayloadType is GenericResponse and the Success Code is false")
            webServiceCallData.responseSucceeded = false
            return
        }
        // parse payload value and re-churn to get r/n/ out of JSON by converting back to Data and re-initializing JSON
        if let value = webServiceCallData.swiftyJSONResponse["Payload"].string {
            let encodedString : Data = (value as NSString).data(using: String.Encoding.utf8.rawValue)!
            webServiceCallData.swiftyJSONResponsePayload = JSON(data: encodedString)
            print("webServiceCallData.swiftyJSONResponsePayload:\(webServiceCallData.swiftyJSONResponsePayload)")
        }
        else {
            print("error: webservice response payload could not re-initialize final JSON format")
            webServiceCallData.responseSucceeded = false
            return
        }
        guard self.webServiceCallData.swiftyJSONResponsePayload != nil else {
            print("error: webservice response payload is nil")
            webServiceCallData.responseSucceeded = false
            return
        }
        // zero non-payload errors detected, safe to handle returned payload
        self.parseResponse()
        print("func handleResponse is finished running...")
    }
    
    func parseResponse() {
        print("func parseResponse is running...")
        switch webServiceCallData.intent {
        case .createRally:
            return parseCreateRallyJSON()
        case .createCommunity:
            return parseCreateCommunityJSON()
        case .getRallys:
            return parseGetRallysJSON()
        case .getCommunities:
            return parseGetCommunitiesJSON()
        case .postRallyMessage:
            return parsePostRallyMessageJSON()
        case .getNewStockImages:
            return parseGetNewStockImagesJSON()
        case .updateIsRallyActiveFlag:
            return parseUpdateIsRallyActiveFlagJSON()
        case .updateRallyDetails:
            return parseUpdateRallyDetailsJSON()
        case .getNearbyCommunitySearchResults:
            return parseGetNearbyCommunitySearchResultsJSON()
        case .getRallyMessages:
            return parseGetRallyMessagesJSON()
        case .getUpdatedRallyPackage:
            return parseGetUpdatedRallyPackageJSON()
        case .joinCommunity:
            return parseJoinCommunityJSON()
        case .updateDidUserAccept:
            return parseUpdateDidUserAcceptJSON()
        case .createUsersWithPhoneContacts:
            return parseCreateUsersWithPhoneContactsJSON()
        case .getSingleRallyMessage:
            return parseGetSingleRallyMessageJSON()
        case .addSupportRequest:
            return parseAddSupportRequestJSON()
        case .createUser:
            return parseCreateUserJSON()
        case .uploadLargeProfileImage:
            return parseUploadLargeProfileImageJSON()
        case .confirmValidationCode:
            return parseConfirmValidationCodeJSON()
        case .updateUserNames:
            return parseUpdateUserNamesJSON()
        case .getThumbnailProfileImage:
            return parseGetThumbnailProfileImageJSON()
        case .leaveCommunity:
            return parseLeaveCommunityJSON()
        case .getCommThreadMessages:
            return parseGetCommThreadMessagesJSON()
        case .getCommThreadMessage:
            return parseGetCommThreadMessageJSON()
        case .checkForUpdatedThumbnailProfileImage:
            return parseCheckForUpdatedThumbnailProfileImageJSON()
        case .postCommThreadMessage:
            return parsePostCommThreadMessageJSON()
        case .getLargeProfileImage:
            return parseGetLargeProfileImageJSON()
    
        }
    }
    
    ////////////////////////////////////////////
    func parseCreateRallyJSON() {
        print("func parseCreateRallyJSON is running...")
        //webServiceCallData.response
        if case let WebServiceCallIntent.createRally(rallyObject) = webServiceCallData.intent {
            if webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyID"].int! != 0 { // not sure it's called RallyPackageList or that a number is required...
                print(webServiceCallData.swiftyJSONResponsePayload["RallyID"])
                if rallyObject.isRallyActive == 1 {
                    upcomingRallysGlobalArray.append(rallyObject)
                }
                rallyObject.createdDateTimeStamp = webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["CreatedDateTimeStamp"].stringValue
                rallyObject.rallyID = webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyID"].int!
                formatJSONClass.rallyMembersIdentifiersArrayConcatonation(rallyObject.rallyID, rallyObject: rallyObject) // formats new rallyMembersIdentifiers JSON to store locally as NSData
                rallyObject.rallyDetailsID = webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyDetailsID"].int!
                webServiceCallData.responseSucceeded = true
                return
            }
            else {
                webServiceCallData.responseSucceeded = false
            }
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
    }
    
    func parseCreateCommunityJSON() {
        print("func parseCreateCommunityJSON is running...")
        if case let WebServiceCallIntent.createCommunity(newCommunity) = webServiceCallData.intent {
            print("newCommunity:\(newCommunity)")
            // Community object properties returned by backend, only parsing for two properties: CommunityID and CreatedDateTimeStamp
            // CommunityID, CreatedDateTimeStamp, CommunityName, Latitude, Longitude, IsPrivate, MaximumMembers, QuadrantID, CreatedByUserID
            guard let communityID = webServiceCallData.swiftyJSONResponsePayload["CommunityID"].int else {
                webServiceCallData.responseSucceeded = false
                return
            }
            newCommunity.communityID = communityID
            if let createdDateTimeStampValue = webServiceCallData.swiftyJSONResponsePayload["CreatedDateTimeStamp"].string {
                newCommunity.createdDateTimeStamp = createdDateTimeStampValue
            }
            webServiceCallData.responseSucceeded = true
        }
        
        print("func parseCreateCommunityJSON is finished running...")
    }
    
    func parseGetRallysJSON() {
        print("func parseGetRallysJSON is running...")
        upcomingRallysGlobalArray.removeAll()
        for swiftyJSONRallyObjectIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count { // ..< includes lower value, not upper value
            if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex] != JSON.null { // Check if rally package is empty before creating a rally object
                let newRally = Rally()
                let rallyPackage = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex]
                newRally.rallyID = rallyPackage["RallyID"].intValue
                newRally.rallyDetailsID = rallyPackage["RallyDetails"]["RallyDetailsID"].intValue
                newRally.isRallyActive = rallyPackage["IsRallyActive"].intValue
                newRally.rallyerID = rallyPackage["CreatedByUserID"].intValue
                newRally.createdDateTimeStamp = rallyPackage["CreatedDateTimeStamp"].stringValue
                let rallyName = rallyPackage["RallyDetails"]["RallyName"].stringValue
                newRally.rallyName = rallyName
                //newRally.RallyResponseTime = rallyPackage["RallyDetails"]["RallyResponseTime"].intValue
                let rallyLocation = rallyPackage["RallyDetails"]["RallyLocationAddress"].stringValue
                if rallyPackage["RallyDetails"]["RallyLocationAddress"] != "null" {
                    newRally.rallyLocation = rallyLocation
                }
                let rallyStartDateTimeAsString = rallyPackage["RallyDetails"]["RallyStartDateTime"].stringValue
                newRally.rallyStartDateTime = dateHelperClass().utcStringToLocalTimeNSDate(rallyStartDateTimeAsString) // converts UTC string to local NSDateTime
                for swiftyJSONRallyUserListIndex in 0..<rallyPackage["RallyUserList"].count {
                    var swiftyJSONRallyUserList = rallyPackage["RallyUserList"][swiftyJSONRallyUserListIndex]
                    let newFriend = Friend()
                    newFriend.firstName = swiftyJSONRallyUserList["FirstName"].stringValue
                    newFriend.lastName = swiftyJSONRallyUserList["LastName"].stringValue
                    newFriend.stockImageID = swiftyJSONRallyUserList["StockImageID"].intValue
                    newFriend.ID = swiftyJSONRallyUserList["UserID"].intValue
                    newFriend.didUserAccept = swiftyJSONRallyUserList["DidUserAccept"].intValue
                    for swiftyJSONPhoneNumberListIndex in 0..<swiftyJSONRallyUserList["PhoneNumberList"].count {
                        let key = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
                        let value = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
                        newFriend.phoneNumberList.updateValue("\(value)", forKey: "\(key)")
                    }
                    newRally.rallyUserList.append(newFriend)
                }
                //newRally.arrayIndex = webServiceCallData.swiftyJSONResponsePayload["RallyPackageList"].count-swiftyJSONRallyObjectIndex
                if newRally.isRallyActive == 1 {
                    newRally.type = "upcomingRallysGlobalArray"
                    upcomingRallysGlobalArray.append(newRally)
                }
            }
        }
        webServiceCallData.responseSucceeded = true
        
        WebCallStatus.fetchRallyUserInfoFromWebService = 1
        if WebCallStatus.getCommunitiesFromWebService == 1 && WebCallStatus.fetchStockImagesFromWebService == 1 {
            WebCallStatus.getCommunitiesFromWebService = 0
            WebCallStatus.fetchRallyUserInfoFromWebService = 0
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchStockImageDataFromWebServiceClassResponseCompleted"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUpcomingRallyDataFromWebServiceClassResponseCompleted"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUserJoinedCommunitiesDataFromWebServiceClassResponseCompleted"), object: nil)
            })
        }
    }
    
    func parseGetCommunitiesJSON() {
        print("func parseGetCommunitiesJSON is running...")
        publicCommunitiesGlobalArray.removeAll()
        privateCommunitiesGlobalArray.removeAll()
        for swiftyJSONRallyObjectIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count { // ..< includes lower value, not upper value
            if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex] != JSON.null { // Check if rally package is empty before creating a rally object
                print("count equals number of communities")
                let newCommunity = Community()
                let communityPackage = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex]
                parse(communityPackage: communityPackage, into: newCommunity)
            }
        }
        webServiceCallData.responseSucceeded = true
        WebCallStatus.getCommunitiesFromWebService = 1
        if WebCallStatus.fetchRallyUserInfoFromWebService == 1 && WebCallStatus.fetchStockImagesFromWebService == 1 { // because it means homeViewController is call this function and therefore no other logic will be placing these rallys onto the screen
            // only post notification once rallys, groups, and stock images are fetched. When the last one comes back, post notification. This way, only one notification posts. Change home code to execute rallys and groups on the one notification post
            WebCallStatus.getCommunitiesFromWebService = 0
            WebCallStatus.fetchRallyUserInfoFromWebService = 0
            DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchStockImageDataFromWebServiceClassResponseCompleted"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUpcomingRallyDataFromWebServiceClassResponseCompleted"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUserJoinedCommunitiesDataFromWebServiceClassResponseCompleted"), object: nil)
            })
        }
    }
    
    func parse(communityPackage: JSON, into communityObject: Community) {
        print("func parse(communityObject:,communityPackage:) is running...")
        // webServiceCallData.swiftyJSONResponsePayload
        communityObject.communityID = communityPackage["CommunityID"].intValue
        communityObject.userID = Int(communityPackage["CreatedByUserID"].stringValue)
        communityObject.createdDateTimeStamp = communityPackage["CreatedDateTimeStamp"].stringValue
        communityObject.name = communityPackage["CommunityName"].stringValue
        communityObject.maximumNumberOfMembers = communityPackage["MaximumMembers"].intValue
        if communityPackage["IsPrivate"].intValue == 0 {   // 0 is public, 1 is private
            communityObject.isPrivate = false
        }
        else {
            communityObject.isPrivate = true
        }
        let latitude = communityPackage["Latitude"].stringValue
        if let latitudeValue = NumberFormatter().number(from: latitude) {
            communityObject.latitude = CGFloat(latitudeValue)
        }
        let longitude = communityPackage["Longitude"].stringValue
        if let longitudeValue = NumberFormatter().number(from: longitude) {
            communityObject.longitude = CGFloat(longitudeValue)
        }
        let distance = communityPackage["Distance"].stringValue
        if let distanceValue = NumberFormatter().number(from: distance) {
            communityObject.distance = CGFloat(distanceValue)
        }
        for swiftyJSONRallyUserListIndex in 0..<communityPackage["CommunityUsersList"].count {
            var swiftyJSONRallyUserList = communityPackage["CommunityUsersList"][swiftyJSONRallyUserListIndex]
            let newFriend = Friend()
            newFriend.firstName = swiftyJSONRallyUserList["FirstName"].string
            newFriend.lastName = swiftyJSONRallyUserList["LastName"].string
            newFriend.stockImageID = Int(swiftyJSONRallyUserList["StockImageID"].stringValue)
            newFriend.ID = Int(swiftyJSONRallyUserList["UserID"].stringValue)
            newFriend.hasThumbnailProfileImage = swiftyJSONRallyUserList["HasProfileThumbnail"].intValue
            for swiftyJSONPhoneNumberListIndex in 0..<swiftyJSONRallyUserList["PhoneNumberList"].count {
                let key = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
                let value = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
                newFriend.phoneNumberList.updateValue("\(value)", forKey: "\(key)")
            }
            communityObject.members.append(newFriend)
        }
        if communityObject.isPrivate {
            privateCommunitiesGlobalArray.append(communityObject)
        }
        else {
            publicCommunitiesGlobalArray.append(communityObject)
        }
        
        // threads
        let asdf = communityPackage["CommThreadPackages"]
        print("asdf:\(asdf)")
        for swiftyJSONThreadPackageListIndex in 0..<communityPackage["CommThreadPackages"].count {
            var swiftyJSONCommunityThreadList = communityPackage["CommThreadPackages"][swiftyJSONThreadPackageListIndex]
            var newThread = Thread()
            let threadID = swiftyJSONCommunityThreadList["CommThreadID"].stringValue
            newThread.threadID = threadID
            print("newThread.threadID:\(threadID)")
            newThread.createdByUserID = Int(swiftyJSONCommunityThreadList["CreatedByUserID"].stringValue)
            newThread.communityID = Int(swiftyJSONCommunityThreadList["CommunityID"].stringValue)
            newThread.title = swiftyJSONCommunityThreadList["Name"].stringValue
            print("newThread.title:\(String(describing: newThread.title))")
            newThread.createdDateTimeStamp = swiftyJSONCommunityThreadList["CreatedDateTimeStamp"].stringValue
            for swiftyJSONCommThreadUsersIndex in 0..<swiftyJSONCommunityThreadList["CommThreadUsers"].count {
                var swiftyJSONThreadMember = swiftyJSONCommunityThreadList["CommThreadUsers"][swiftyJSONCommThreadUsersIndex]
                let newMember = Friend()
                newMember.ID = Int(swiftyJSONThreadMember["UserID"].stringValue)!
                newMember.firstName = swiftyJSONThreadMember["FirstName"].stringValue
                newMember.lastName = swiftyJSONThreadMember["LastName"].stringValue
                newMember.stockImageID = Int(swiftyJSONThreadMember["StockImageID"].stringValue)!
                newMember.hasThumbnailProfileImage = swiftyJSONThreadMember["HasProfileThumbnail"].intValue
                for swiftyJSONPhoneNumberListIndex in 0..<swiftyJSONThreadMember["PhoneNumberList"].count {
                    let key = swiftyJSONThreadMember["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
                    let value = swiftyJSONThreadMember["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
                    newMember.phoneNumberList.updateValue("\(value)", forKey: "\(key)")
                }
                newThread.members.append(newMember)
            }
            communityObject.threads.append(newThread)
        }
    }
    

    func parsePostRallyMessageJSON() {
        print("func parsePostRallyMessageJSON is running...")
        if webServiceCallData.swiftyJSONResponsePayload["RallyMessageID"].int != nil {
            webServiceCallData.responseSucceeded = true
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
        //return
    }

    func parseGetNewStockImagesJSON() {
        print("func parseGetNewStockImagesJSON is running...")
        var jsonString = "["
        let defaults = UserDefaults.standard
        if webServiceCallData.swiftyJSONResponsePayload.isEmpty == false { // is array empty? If not, need to append new stock images to local file
            if let stockImagesObjects = defaults.value(forKey: "stockImagesResultKey") as? Data { // append existing stock image objects to jsonString to prepare to then have new stock image objects appended
                let swiftyJSON2 = JSON(data: stockImagesObjects)
                for i in 0..<swiftyJSON2.count { // for any stockImage objects in returned package, format to save
                    let textToAppend = "{\"StockImageID\":\(swiftyJSON2[i]["StockImageID"]),\"StockImageName\":\"\(swiftyJSON2[i]["StockImageName"])\",\"StockImageURL\":\"\(swiftyJSON2[i]["StockImageURL"])\",\"CreatedDateTimeStamp\":\"\(swiftyJSON2[i]["CreatedDateTimeStamp"])\"},"
                    jsonString += textToAppend
                }
            }
            for i in 0..<webServiceCallData.swiftyJSONResponsePayload.count where webServiceCallData.swiftyJSONResponsePayload[i]["StockImageID"] != JSON.null { // for any stockImage objects in returned package, format to save locally
                print("RECIEVED EXTRA STOCK IMAGES TO SAVE LOCALLY; SAVING LOCALLY")
                let stockImageIDBeingAppended = self.webServiceCallData.swiftyJSONResponsePayload[i]["StockImageID"]
                print("stockImageIDBeingAppended:\(stockImageIDBeingAppended)")
                let textToAppend = "{\"StockImageID\":\(webServiceCallData.swiftyJSONResponsePayload[i]["StockImageID"]),\"StockImageName\":\"\(webServiceCallData.swiftyJSONResponsePayload[i]["StockImageName"])\",\"StockImageURL\":\"\(webServiceCallData.swiftyJSONResponsePayload[i]["StockImageURL"])\",\"CreatedDateTimeStamp\":\"\(webServiceCallData.swiftyJSONResponsePayload[i]["CreatedDateTimeStamp"])\"},"
                jsonString += textToAppend
                // check if directory exists
                // check if sub-directory exists
                // check if file exists
                // check if image exists
                // convert image from url to UIImage
                // add image to file
                // save file
                let fileName = "stockImageFileStockImageID\(webServiceCallData.swiftyJSONResponsePayload[i]["StockImageID"])"
                let stockImageFile = fileSaveHelper(fileName: "\(fileName)", fileExtension: .PNG, subDirectory: "SavingStockImageFiles", directory: .libraryDirectory) // fileSaveHelper handles checking if filePath exists (subdirectory; directory must exist. if it doesn't, must change fileSaveHelper:createDirectoryAtPath:withIntermediateDirectories to true. also checks if file already exists. if so, overwrites
                print("Directory exists: \(stockImageFile.directoryExists)")
                print("File exists: \(stockImageFile.fileExists)")
                let newStockImage: UIImage!
                if let url = URL(string: "\(webServiceCallData.swiftyJSONResponsePayload[i]["StockImageURL"])") {
                    if let data = try? Data(contentsOf: url){
                        newStockImage = UIImage(data: data)
                        do {
                            try stockImageFile.saveFile(image: newStockImage)
                            print("successfully saved new stock image number \(i) to local file")
                        }
                        catch {
                            print("There was an error saving the file: \(error)")
                        }
                    }
                }
            }
            // complete saving updated stock image objects to NSUserDefaults. Images themselves are stored in Library directory
            jsonString = String(jsonString.characters.dropLast()) // drops last comma
            jsonString += "]"
            let data = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let JSONAsNSDataValue = data {
                NSUserDefaultsClass.updateStockImagesResultKey(JSONAsNSDataValue)
            }
            WebCallStatus.fetchStockImagesFromWebService = 1
            if WebCallStatus.fetchRallyUserInfoFromWebService == 1 && WebCallStatus.getCommunitiesFromWebService == 1 {
                WebCallStatus.getCommunitiesFromWebService = 0
                WebCallStatus.fetchRallyUserInfoFromWebService = 0
                DispatchQueue.main.async(execute: { () -> Void in
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchStockImageDataFromWebServiceClassResponseCompleted"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUpcomingRallyDataFromWebServiceClassResponseCompleted"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchUserJoinedCommunitiesDataFromWebServiceClassResponseCompleted"), object: nil)
                })
            }
            webServiceCallData.responseSucceeded = true
            return
        }
        else {
            webServiceCallData.responseSucceeded = false
            return
        }
    }

    func parseUpdateIsRallyActiveFlagJSON() {
        print("func parseUpdateIsRallyActiveFlagJSON is running...")

        /*
         print("postRallyDetailsUpdatesToWebService:unwrappedData exists!")
         let isRallyActive = webServiceCallData.swiftyJSONResponse["IsRallyActive"]
         
         friends.indexOf{Array($0.PhoneNumberList.values)[0]==rallyMemberPhoneNumber}
         */
        webServiceCallData.responseSucceeded = true
    }
    
    func parseUpdateRallyDetailsJSON() {
        print("func parseUpdateRallyDetailsJSON is running...")
        guard webServiceCallData.swiftyJSONResponsePayload["RallyDetailsID"] != "NULL" else {
            webServiceCallData.responseSucceeded = false
            return
        }
        webServiceCallData.responseSucceeded = true
    }
    
    func parseGetNearbyCommunitySearchResultsJSON() {
        print("func parseGetNearbyCommunitySearchResultsJSON is running...")
        communitiesNearbyGlobalArray.removeAll() // empty array in case method has already been run
        
        for swiftyJSONRallyObjectIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count { // ..< includes lower value, not upper value
            if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex] != JSON.null { // Check if rally package is empty before creating a rally object
                print("count equals number of communities")
                let nearbyCommunity = Community()
                let communityPackage = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyObjectIndex]
                nearbyCommunity.communityID = communityPackage["CommunityID"].intValue
                nearbyCommunity.userID = Int(communityPackage["CreatedByUserID"].stringValue)
                nearbyCommunity.createdDateTimeStamp = communityPackage["CreatedDateTimeStamp"].stringValue
                nearbyCommunity.name = communityPackage["CommunityName"].stringValue
                nearbyCommunity.maximumNumberOfMembers = communityPackage["MaximumMembers"].intValue
                nearbyCommunity.memberCount = communityPackage["MemberCount"].intValue
                let latitude = communityPackage["Latitude"].stringValue
                if let latitudeValue = NumberFormatter().number(from: latitude) {
                    nearbyCommunity.latitude = CGFloat(latitudeValue)
                }
                let longitude = communityPackage["Longitude"].stringValue
                if let longitudeValue = NumberFormatter().number(from: longitude) {
                    nearbyCommunity.longitude = CGFloat(longitudeValue)
                }
                let distance = communityPackage["Distance"].stringValue
                if let distanceValue = NumberFormatter().number(from: distance) {
                    nearbyCommunity.distance = CGFloat(distanceValue)
                }
                
                for swiftyJSONRallyUserListIndex in 0..<communityPackage["CommunityUsersList"].count {
                    var swiftyJSONRallyUserList = communityPackage["CommunityUsersList"][swiftyJSONRallyUserListIndex]
                    let newFriend = Friend()
                    newFriend.firstName = swiftyJSONRallyUserList["FirstName"].string
                    newFriend.lastName = swiftyJSONRallyUserList["LastName"].string
                    newFriend.stockImageID = Int(swiftyJSONRallyUserList["StockImageID"].stringValue)
                    newFriend.ID = Int(swiftyJSONRallyUserList["UserID"].stringValue)
                    newFriend.hasThumbnailProfileImage = swiftyJSONRallyUserList["HasProfileThumbnail"].intValue
                    for swiftyJSONPhoneNumberListIndex in 0..<swiftyJSONRallyUserList["PhoneNumberList"].count {
                        let key = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
                        let value = swiftyJSONRallyUserList["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
                        newFriend.phoneNumberList.updateValue("\(value)", forKey: "\(key)")
                    }
                    nearbyCommunity.members.append(newFriend)
                }
                communitiesNearbyGlobalArray.append(nearbyCommunity)
            }
        }
        webServiceCallData.responseSucceeded = true
    }

    func parseGetRallyMessagesJSON() {
        print("func parseGetRallyMessagesJSON is running...")
        if case let WebServiceCallIntent.getRallyMessages(rallyObject) = webServiceCallData.intent {
            if let _ = rallyObject.rallyMessages {
                rallyObject.rallyMessages = []
            }
            else {
                rallyObject.rallyMessages = [RallyMessage]()
            }
            if let jsonArray = webServiceCallData.swiftyJSONResponsePayload.array {
                for rallyMessagePackage in jsonArray {
                    let message = RallyMessage()
                    message.rallyID = Int(String(stringInterpolationSegment: rallyMessagePackage["RallyID"]))!
                    message.userName = String(stringInterpolationSegment: rallyMessagePackage["UserName"])
                    message.userID = rallyMessagePackage["UserID"].intValue
                    message.rallyMessageText = String(stringInterpolationSegment: rallyMessagePackage["RallyMessageText"])
                    message.createdDateTimeStamp = String(stringInterpolationSegment: rallyMessagePackage["CreatedDateTimeStamp"])
                    rallyObject.rallyMessages!.append(message)
                }
            }
            webServiceCallData.responseSucceeded = true
            let returnParameters = WebServiceCallReturnParameters.getRallyMessages(updatedRally: rallyObject)
            webServiceCallData.responseReturnParameters = returnParameters
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
    }
    
    func parseGetUpdatedRallyPackageJSON() {
        print("func parseGetUpdatedRallyPackageJSON is running...")
        // format updated rally member objects array
        if case let WebServiceCallIntent.getUpdatedRallyPackage(rallyObject, _) = webServiceCallData.intent {
            var rallyMembers = [Friend]()
            for rallyMember in webServiceCallData.swiftyJSONResponsePayload["RallyUserList"].array! {
                let newUpdatedFriend = Friend()
                newUpdatedFriend.firstName = rallyMember["FirstName"].string
                newUpdatedFriend.lastName = rallyMember["LastName"].string
                newUpdatedFriend.stockImageID = rallyMember["StockImageID"].int
                newUpdatedFriend.ID = rallyMember["UserID"].int
                newUpdatedFriend.didUserAccept = rallyMember["DidUserAccept"].int
                for swiftyJSONPhoneNumberListIndex in 0..<rallyMember["PhoneNumberList"].count {
                    let key = rallyMember["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
                    let value = rallyMember["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
                    newUpdatedFriend.phoneNumberList.updateValue("\(value)", forKey: "\(key)")
                }
                rallyMembers.append(newUpdatedFriend)
            }
            var rallyObjectInstance: Rally!
            if let _ = rallyObject { // use existing rally object
                print("fetchUpdatedRallyFromWebService a rallyObject already exists")
                rallyObjectInstance = rallyObject!
                rallyObjectInstance.rallyUserList = rallyMembers
            }
            else {
                // create new rally object
                rallyObjectInstance = Rally(members: rallyMembers)
                // default type is active, no need to change
                rallyObjectInstance.rallyID = Int(String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyID"]))
                rallyObjectInstance.createdDateTimeStamp = String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["CreatedDateTimeStamp"])
                rallyObjectInstance.rallyerID = webServiceCallData.swiftyJSONResponsePayload["CreatedByUserID"].intValue
                rallyObjectInstance.isRallyActive = webServiceCallData.swiftyJSONResponsePayload["IsRallyActive"].intValue
            }
            rallyObjectInstance.rallyName = String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyName"])
            //rallyObjectInstance.RallyResponseTime = Int(String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyResponseTime"]))!
            let rallyLocation = String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyLocationAddress"])
            if webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyLocationAddress"].string != "null" || webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyLocationAddress"].string != "nil" {
                rallyObjectInstance.rallyLocation = rallyLocation
            }
            let rallyStartDateTimeAsString = webServiceCallData.swiftyJSONResponsePayload["RallyDetails"]["RallyStartDateTime"].stringValue
            rallyObjectInstance.rallyStartDateTime = dateHelperClass().utcStringToLocalTimeNSDate(rallyStartDateTimeAsString) // converts UTC string to local NSDateTime
            webServiceCallData.responseSucceeded = true
            let returnParameters = WebServiceCallReturnParameters.getUpdatedRallyPackage(updatedRally: rallyObjectInstance)
            webServiceCallData.responseReturnParameters = returnParameters
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
    }
    
    func parseJoinCommunityJSON() {
        print("func parseJoinCommunityJSON is running...")
        // check to see if webservice successully added user to rally. these parameters are each marked 0 if unsuccessful
        if case let WebServiceCallIntent.joinCommunity(community) = webServiceCallData.intent {
            if webServiceCallData.swiftyJSONResponse["PayloadType"].stringValue == "GenericResponse" {
                // do nothing if successcode is true, picture is up to date
                let returnParameters = WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID: 0, thumbnailUpdated: false)
                webServiceCallData.responseReturnParameters = returnParameters
                webServiceCallData.responseSucceeded = true
                // if successful, add to array and pass success message back to homeViewController to allow rally card creation
                if community.isPrivate {
                    privateCommunitiesGlobalArray.append(community)
                }
                else {
                    publicCommunitiesGlobalArray.append(community)
                }
                return
            }
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
    }
    
    func parseUpdateDidUserAcceptJSON() {
        print("func parseUpdateDidUserAcceptJSON is running...")
        webServiceCallData.responseSucceeded = true
        //return
    }
    
    func parseCreateUsersWithPhoneContactsJSON() {
        print("func parseCreateUsersWithPhoneContactsJSON is running...")

        //StartupRunTimer.end = NSDate()
        //let totalTimeElapsedTwo = StartupRunTimer.end.timeIntervalSinceDate(StartupRunTimer.start)
        //print("StartupRunTimer:totalTimeElapsed:\(totalTimeElapsedTwo)")
        // print("postUserPhoneContactsToWebService data:webServiceCallData.swiftyJSONResponsePayload:\(webServiceCallData.swiftyJSONResponsePayload)")
        
        
        /*
         friends.removeAll()
         // can't unhide sv on home screen until this point bc if friends array is cleared while trying to be loaded on collectionView, index array occurs
         if #available(iOS 9.0, *) {
         var contactStore = CNContactStore()
         for swiftyJSONRallyUserListIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count {
         if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["UserID"].int == User.UserID {
         continue // do not add user to friends list, move to next contact
         }
         let newFriend = friend()
         newFriend.firstName = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["FirstName"].string
         newFriend.lastName = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["LastName"].string
         newFriend.identifier = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["AppleContactIdentifier"].string
         newFriend.imageDataAvailable = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["ImageDataAvailable"].bool
         newFriend.stockImageID = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["StockImageID"].int
         newFriend.UserID = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["UserID"].int
         for swiftyJSONPhoneNumberListIndex in 0..<webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"].count {
         let key = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
         let value = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
         newFriend.PhoneNumberList.updateValue("\(value)", forKey: "\(key)")
         }
         if newFriend.imageDataAvailable! { // checking imageDataAvailable and refetching to get picture if contact has picture is quicker than checking for picture each time, even though you don't have to recall (this is according to Justin and
         // https://www.safaribooksonline.com/library/view/ios-9-swift/9781491936689/ch04.html
         do{
         let contact = try contactStore.unifiedContactWithIdentifier(
         newFriend.identifier!, keysToFetch: [CNContactImageDataKey]) // contact identifier fetched by default, not required to be specified in keysToFetch
         if let contactImage = contact.imageData {
         newFriend.largeProfilePicture = UIImage(data: contactImage)
         if webCallStatus.inviteViewControllerLoaded == 0 { // three conditions are for: don't want to add new contacts to front of contacts array once screen is loaded and contacts have been assigned to cells, would throw off the indexing. the goal is to take people with pictures and move them up to the top, so if no picture they don't move. last, they must have a phone number to be added to the array, otherwise if they're included in a rally, no message will be able to be sent to that individual
         var randomNumberNValue = friends.count
         if friends.count>16 {
         randomNumberNValue = 16
         }
         let randomIndex = Int(arc4random_uniform(UInt32(randomNumberNValue))) // insert randomly among first 24 index values in array. this way, people's profile pictures will be mixed in with stock images instead of real profile pics and stock images being blocked where all real profile pics are at the very top. this stark contrast looks bad
         friends.insert(newFriend, atIndex: randomIndex)
         }
         else { // phone number required to be added to array, otherwise if included in a rally, there's no destination to send message
         friends.append(newFriend)
         }
         }
         else {
         friends.append(newFriend)
         }
         }
         catch let err{
         print(err)
         }
         }
         else {
         friends.append(newFriend)
         }
         }
         }
         else {
         for swiftyJSONRallyUserListIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count {
         if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["UserID"].int == User.UserID {
         continue // do not add user to friends list, move to next contact
         }
         let newFriend = friend()
         newFriend.firstName = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["FirstName"].string
         newFriend.lastName = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["LastName"].string
         newFriend.identifier = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["AppleContactIdentifier"].string
         newFriend.imageDataAvailable = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["ImageDataAvailable"].bool
         newFriend.stockImageID = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["StockImageID"].int
         newFriend.UserID = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["UserID"].int
         for swiftyJSONPhoneNumberListIndex in 0..<webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"].count {
         let key = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Type"].stringValue
         let value = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONRallyUserListIndex]["PhoneNumberList"][swiftyJSONPhoneNumberListIndex]["Number"].stringValue
         newFriend.PhoneNumberList.updateValue("\(value)", forKey: "\(key)")
         }
         friends.append(newFriend)
         }
         }
         //
         // Did the JSONObjectWithData constructor return an error? If so, log the error to the console: (err != nil)
         /*else {
         println(err!.localizedDescription)
         let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
         println("Error could not parse JSON: '\(jsonStr)'")
         postCompleted(succeeded: false, msg: "Error")
         }*/
         /*
         let viewControllerView = permissionsHelperClass.permissionsHelperClassSingleton.navigationControllerClassVariable.topViewController!.view // get instance of current view controller's view
         // if dulling view and permissions view exist (should always exist as pair), dismiss. this case occurs when address book permission was denied on app start , user enabled permissions, then ran logic to load contacts into friends array
         if let permissionsView = viewControllerView.viewWithTag(890) {
         if let dullingView = viewControllerView.viewWithTag(165) {
         permissionsHelperClass.permissionsHelperClassSingleton.dismissDullingAndPermissionsView(viewControllerView)
         }
         }
         */
         */
        
        //StartupRunTimer.end = NSDate()
        //let totalTimeElapsedThree = StartupRunTimer.end.timeIntervalSinceDate(StartupRunTimer.start)
        //print("StartupRunTimer:totalTimeElapsed:\(totalTimeElapsedThree)")
        webServiceCallData.responseSucceeded = true
        /*
        if webServiceCallData.responseSucceeded == true {
            webCallStatus.updateContactsInfoFromWebService = 1
            if webCallStatus.fetchRallyUserInfoFromWebService == 1 && webCallStatus.getCommunitiesFromWebService == 1 && webCallStatus.fetchStockImagesFromWebService == 1 {
                webCallStatus.getCommunitiesFromWebService = 0
                webCallStatus.fetchRallyUserInfoFromWebService = 0
                DispatchQueue.main.async(execute: { () -> Void in
                NotificationCenter.default.post(name: Notification.Name(rawValue: "fetchRallyAndGroupUserInfoFromWebServiceClassResponseCompleted"), object: nil)
                })
            }
        }*/
    }
    
    func parseGetSingleRallyMessageJSON() {
        print("func parseGetSingleRallyMessageJSON is running...")
        let newRallyMessageObject: RallyMessage = RallyMessage()
        newRallyMessageObject.rallyID = Int(String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyID"]))!
        newRallyMessageObject.userName = String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["UserName"])
        newRallyMessageObject.rallyMessageText = String(stringInterpolationSegment: webServiceCallData.swiftyJSONResponsePayload["RallyMessageText"])
        newRallyMessageObject.createdDateTimeStamp = webServiceCallData.swiftyJSONResponsePayload["CreatedDateTimeStamp"].stringValue
        newRallyMessageObject.userID = webServiceCallData.swiftyJSONResponsePayload["UserID"].intValue
        webServiceCallData.responseSucceeded = true
        
        if webServiceCallData.responseSucceeded == true {
            let returnParameters = WebServiceCallReturnParameters.getSingleRallyMessage(rallyMessage: newRallyMessageObject)
            webServiceCallData.responseReturnParameters = returnParameters
        }
    }
    
    func parseAddSupportRequestJSON() {
        print("func parseAddSupportRequestJSON is running...")
        webServiceCallData.responseSucceeded = false
    }
    
    func parseCreateUserJSON() {
        print("func parseCreateUserJSON is running...")
        
        // check if public and private keys exist
        let keychainHelper = KeychainHelper()
        // set public key
        if keychainHelper.publicKey == nil { // If public key is not already saved in KeychainHelper (does not exist), save public key
            let publicKey = webServiceCallData.swiftyJSONResponsePayload["PublicKey"].string
            if let value = publicKey {
                keychainHelper.savePublicKey(value)
            }
        }
        // set private key
        if keychainHelper.privateKey == nil { // If private key is not already saved in KeychainHelper (does not exist), save private key
            let privateKey = webServiceCallData.swiftyJSONResponsePayload["PrivateKey"].string
            if let value = privateKey {
                keychainHelper.savePrivateKey(value)
            }
        }
        
        // set user info
        MYUser.ID = webServiceCallData.swiftyJSONResponsePayload["UserID"].intValue // 'Value' after .int unwraps optional int type
        NSUserDefaultsClass.updateUserIDKey(userID: MYUser.ID)
        MYUser.firstName = webServiceCallData.swiftyJSONResponsePayload["FirstName"].string
        MYUser.lastName = webServiceCallData.swiftyJSONResponsePayload["LastName"].string
        MYUser.stockImageID = webServiceCallData.swiftyJSONResponsePayload["StockImageID"].intValue
        NSUserDefaultsClass.updateUserStockImageIDKey(stockImageID: MYUser.stockImageID)
        MYUser.phoneNumberList["Mobile"] = webServiceCallData.swiftyJSONResponsePayload["PhoneNumber"].string
        
        //
        if WebCallStatus.getCommunitiesFromWebService == 0 && WebCallStatus.fetchRallyUserInfoFromWebService == 0 { // .createUser is called on remote notifications permission enabled for new app case. in case where user disable and re-enables remote notification permissions, this check assures this call isn't made twice bc notification is broadcast once these calls finish to load rally cards. doing so would cause duplicates
            let webServiceCallManagerGetRallys = WebServiceCallManager(intent: .getRallys)
            webServiceCallManagerGetRallys.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
            let webServiceCallManagerGetRallyGroups = WebServiceCallManager(intent: .getCommunities)
            webServiceCallManagerGetRallyGroups.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                if succeeded {
                    DispatchQueue.main.async(execute: { () -> Void in
                        // home view controller has successfully updated user communities to phone, post notification so any class needing default community info can update data (before user has manually pressed a community card to select a community focus)
                        
                        // post notification
                        print("posting userCommunitiesReturnedFromBackend")
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "userCommunitiesReturnedFromBackend"), object: nil)
                    })
                }
                else { // post message saying could not join group. try again later
                    DispatchQueue.main.async(execute: { () -> Void in
                    })
                }
            })
        }
        webServiceCallData.responseSucceeded = true
    }
    
    func parseUploadLargeProfileImageJSON() {
        print("func parseUploadLargeProfileImageJSON is running...")
        if case let WebServiceCallIntent.uploadLargeProfileImage(largeProfilePicture) = webServiceCallData.intent {
            // update User.largeProfilePicture
            guard let value = webServiceCallData.swiftyJSONResponsePayload["SuccessCode"].bool else { // if backend upload was successful, update data sources
                webServiceCallData.responseSucceeded = false
                return
            }
            if value {
                // initialize uiimage (large) profile picture
                guard let image = UIImage(data:largeProfilePicture) else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                // save (large) image locally
                LoadLocalFile.saveLargeProfileImageToLocalFile(largeProfileImage: image)
                // create thumbnail, save locally
                print("Do Something...")
                // update User data source profile picture property
                MYUser.largeProfileImage = image
                webServiceCallData.responseSucceeded = true
            }
            else {
                webServiceCallData.responseSucceeded = false
            }
        }
        else {
            webServiceCallData.responseSucceeded = false
        }
    }
    
    func parseConfirmValidationCodeJSON() {
        print("func parseConfirmValidationCodeJSON is running...")
        guard let value = webServiceCallData.swiftyJSONResponsePayload["SuccessCode"].bool else { // if backend upload was successful, update data sources
            webServiceCallData.responseSucceeded = false
            return
        }
        if value {
            // update NSUserDefaults with user phoneNumber
            NSUserDefaultsClass.updateUserPhoneNumberKey()
            webServiceCallData.responseSucceeded = true
        }
        else {
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parseUpdateUserNamesJSON() {
        print("func parseUpdateUserNamesJSON is running...")
        if case let WebServiceCallIntent.updateUserNames(firstName, lastName) = webServiceCallData.intent {
            guard let value = webServiceCallData.swiftyJSONResponsePayload["SuccessCode"].bool else { // if backend upload was successful, update data sources
                webServiceCallData.responseSucceeded = false
                return
            }
            if value {
                MYUser.firstName = firstName
                MYUser.lastName = lastName
                // update NSUserDefaults user name key value (used in startup to determine if backend received first, last name from editProfileVC)
                NSUserDefaultsClass.updateUserNameKey()
                webServiceCallData.responseSucceeded = true
            }
            else {
                webServiceCallData.responseSucceeded = false
                return
            }
        }
        else {
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parseGetThumbnailProfileImageJSON() {
        print("func parseGetThumbnailProfileImageJSON is running...")
        if case let WebServiceCallIntent.getThumbnailProfileImage(memberID, membersArrayMemberIndex) = webServiceCallData.intent {
            // get instance of base 64 string uiimage from package
            if let baseSixtyFourThumbnail = webServiceCallData.swiftyJSONResponsePayload["ImageData"].string {
                // convert to Data
                guard let thumbnailData = Data(base64Encoded: baseSixtyFourThumbnail) else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                // convert to uiimage
                guard let image = UIImage(data:thumbnailData) else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                guard let thumbnailProfileImageID = webServiceCallData.swiftyJSONResponsePayload["ThumbnailProfileImageID"].int else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                // update global user profile pictures dictionary with matching userID and profile picture
                communityMemberThumbnailsGlobalDictionary[memberID] = image
                webServiceCallData.responseSucceeded = true
                let returnParameters = WebServiceCallReturnParameters.getThumbnailProfileImage(membersArrayMemberIndex: membersArrayMemberIndex, imageFetched: true, thumbnailProfileImageID: thumbnailProfileImageID)
                webServiceCallData.responseReturnParameters = returnParameters
            }
        }
        else {
            print("@#$%")
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parseLeaveCommunityJSON() {
        print("func parseLeaveCommunityJSON is running...")
        //CHANGE BELOW (copied from above parse method)
        guard let value = webServiceCallData.swiftyJSONResponsePayload["SuccessCode"].bool else { // if backend upload was successful, update data sources
            webServiceCallData.responseSucceeded = false
            return
        }
        if value {
            if case let WebServiceCallIntent.leaveCommunity(communityID) = webServiceCallData.intent {
        
            // update global data (done here on success)
                //upcomingRallysGlobalArray
                    // 1. Remove all upcoming rallys with communityID of community left
                        // filter upcoming rallys array $0.communityID, mapping to index of community element in upcomingRallysArray
                        // Reverse result, run for loop removing elements at these indexes from array
                print("successful response, update global arrays")
                
                // NOTE: Below code doesn't work until communityID property is added to getRallyPackage WCF call
                /*
                let upcomingRallysIDArr = upcomingRallysGlobalArray.map { $0.rallyID! }
                print("1:\(communityID)")
                let filteredUpcomingRallysToRemove = upcomingRallysGlobalArray.filter { $0.associatedCommunity!.communityID == communityID }
                print("1.5")
                let upcomingRallysToRemove = filteredUpcomingRallysToRemove.map { upcomingRallysIDArr.index(of:$0.rallyID!) } // get arr indexes of communities to remove
                print("2")
                for index in upcomingRallysToRemove.reversed() {
                    print("3")
                    upcomingRallysGlobalArray.remove(at: index!)
                }
                print("upcomingRallysGlobalArray updated")
                */
                
                
                // https://stackoverflow.com/questions/24028860/how-to-find-index-of-list-item-in-swift
                //publicCommunitiesGlobalArray
                    // 1. Remove all upcoming rallys with communityID of community left
                        // filter/reduce upcoming rallys array $0.communityID, mapping to index of community element in upcomingRallysArray
                        // Reverse result, run for loop removing elements at these indexes from array
                
                let publicCommunitiesIndex = publicCommunitiesGlobalArray.index(where: { $0.communityID == communityID }) // test if this is the item you're looking for
                if let value = publicCommunitiesIndex {
                    publicCommunitiesGlobalArray.remove(at: value)
                }
                //privateCommunitiesGlobalArray
                    // 1. Remove all upcoming rallys with communityID of community left
                        // filter/reduce upcoming rallys array $0.communityID, mapping to index of community element in upcomingRallysArray
                        // Reverse result, run for loop removing elements at these indexes from array
                let privateCommunitiesIndex = privateCommunitiesGlobalArray.index(where: { $0.communityID == communityID })
                if let value = privateCommunitiesIndex {
                    privateCommunitiesGlobalArray.remove(at: value)
                }

            // update screen specific data: user cannot move until calls complete (sidePanelVC completion handler does this) (nav controller already does this automatically, just disable actions after first press to prevent multi-pressing)
                
                // dismiss sidePanelVC and communityMessagingVC (already does this)
                
                // update cards, messagingCommunityVC and sidePanelVC UI (homeVC does this via notification post upon completing animation back to homeVC)
                
                //func updateDataAndUIForLeavingCommunity(_ notification:Notification)
                
                
                
                // post notification user profile pic has changed
                DispatchQueue.main.async(execute: { () -> Void in
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "profilePictureUploadResultReturnedFromWCF"), object: nil)
                })
                
                webServiceCallData.responseSucceeded = true
            }
        }
        else {
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func displayWebServiceCallError() {
        print("func displayWebServiceCallError is running...")
        // initialize alertview
        if webServiceCallData.checkpointData.validationType != .IsPhoneNumberVerified {
            let alertView = CustomUIWindowAlertview(dataObject: webServiceCallData.checkpointData)
            // display alertview
            alertView.show()
        }
        else {
            print("Do nothing...")
        }
        
        if let value = webServiceCallData.swiftyJSONResponsePayload["SuccessCode"].bool {
            if !value {
                
            }
        }
        /*
         {  "ApiValidation": 
            null,
         "PayloadType": 
            "GenericResponse",
         "Payload": 
            "{\"SuccessCode\": true,\"Message\":\"Validation code was valid for user. User IsVerified flag has been activated.\", \"ErrorResponse\": asdf
            }"
         }
         */
    }
    
    func parseGetCommThreadMessagesJSON() {
        print("func parseGetCommThreadMessagesJSON is running...")
        // three responses:
        // first: good response
        // second: good response (generic response)
        // third: error response
        // array of comm thread message objects
        if case WebServiceCallIntent.getCommThreadMessages(let thread) = webServiceCallData.intent {
            thread.messages = []
            for swiftyJSONCommunityThreadArrayIndex in 0..<webServiceCallData.swiftyJSONResponsePayload.count { // ..< includes lower value, not upper value
                if webServiceCallData.swiftyJSONResponsePayload[swiftyJSONCommunityThreadArrayIndex] != nil { // Check if rally package is empty before creating a rally object
                    let newCommThreadMessage = CommThreadMessage()
                    let communityThread = webServiceCallData.swiftyJSONResponsePayload[swiftyJSONCommunityThreadArrayIndex]
                    newCommThreadMessage.commThreadMessageID = Int(communityThread["CommThreadMessageID"].stringValue)!
                    newCommThreadMessage.commThreadID = Int(communityThread["CommThreadID"].stringValue)!
                    newCommThreadMessage.createdByUserID = Int(communityThread["CreatedByUserID"].stringValue)!
                    newCommThreadMessage.firstName = communityThread["FirstName"].stringValue
                    newCommThreadMessage.lastName = communityThread["LastName"].stringValue
                    newCommThreadMessage.commThreadMessageTypeID = Int(communityThread["CommThreadMessageTypeID"].stringValue)! // 1 = textBasedMessage ; 2 = smsBasedMessage
                    newCommThreadMessage.messageByteString = communityThread["MessageByteString"].stringValue // asciII byte string (NOTE: normal strings don't support emojis)
                    newCommThreadMessage.createdDateTimeStamp = communityThread["CreatedDateTimeStamp"].stringValue
                    thread.messages.append(newCommThreadMessage)
                }
                else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
            }
            webServiceCallData.responseSucceeded = true
        }
        else {
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parseGetCommThreadMessageJSON() {
        print("func parseGetCommThreadMessageJSON is running...")
        // three responses:
        // first: good response
        // second: good response (generic response)
        // third: error response
        // array of comm thread message objects
        let newCommThreadMessage = CommThreadMessage()
        if let responsePayload = webServiceCallData.swiftyJSONResponsePayload {
            newCommThreadMessage.commThreadMessageID = responsePayload["CommThreadMessageID"].intValue
            newCommThreadMessage.commThreadID = responsePayload["CommThreadID"].intValue
            newCommThreadMessage.createdByUserID = responsePayload["CreatedByUserID"].intValue
            newCommThreadMessage.firstName = responsePayload["FirstName"].stringValue
            newCommThreadMessage.lastName = responsePayload["LastName"].stringValue
            newCommThreadMessage.commThreadMessageTypeID = responsePayload["CommThreadMessageTypeID"].intValue // 1 = textBasedMessage ; 2 = smsBasedMessage
            newCommThreadMessage.messageByteString = responsePayload["MessageByteString"].stringValue // asciII byte string (NOTE: normal strings don't support emojis)
            newCommThreadMessage.createdDateTimeStamp = responsePayload["CreatedDateTimeStamp"].stringValue
            
            // find community thread object to add newCommThreadMessage
            let messageThreadID: String = String(newCommThreadMessage.commThreadID)
            let globalCommunitiesArray = [publicCommunitiesGlobalArray, privateCommunitiesGlobalArray]
            // check private & public communities for thread
            communitiesForLoop: for globalCommunity in globalCommunitiesArray {
                for community in globalCommunity {
                    let commThread: [Thread] = community.threads.filter { $0.threadID == messageThreadID } // there should be 0 or 1 matches
                    if !commThread.isEmpty { // add message to messages property in thread object
                        commThread[0].messages.append(newCommThreadMessage)
                        break communitiesForLoop
                    }
                }
            }
            let returnParameters = WebServiceCallReturnParameters.getCommThreadMessage(threadID: newCommThreadMessage.commThreadID, commThreadMessage: newCommThreadMessage)
            webServiceCallData.responseReturnParameters = returnParameters
            webServiceCallData.responseSucceeded = true
        }
        else {
            webServiceCallData.responseSucceeded = false
            let emptyCommThreadMessage = CommThreadMessage()
            let returnParameters = WebServiceCallReturnParameters.getCommThreadMessage(threadID: 0, commThreadMessage: emptyCommThreadMessage) // just giving values to avoid needing to use optionals, doesn't matter as they won't be used in failed case
            webServiceCallData.responseReturnParameters = returnParameters
            return
        }
    }
    
    func parseCheckForUpdatedThumbnailProfileImageJSON() {
        print("func parseCheckForUpdatedThumbnailProfileImageJSON is running...")
        if webServiceCallData.swiftyJSONResponse["PayloadType"].stringValue == "GenericResponse" {
            // do nothing if successcode is true, picture is up to date
            let returnParameters = WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID: 0, thumbnailUpdated: false)
            webServiceCallData.responseReturnParameters = returnParameters
            webServiceCallData.responseSucceeded = true
            return
        }
        if webServiceCallData.swiftyJSONResponse["PayloadType"].stringValue == "ThumbnailProfileImage" {
            // parse payload, get updated image
            if case let WebServiceCallIntent.checkForUpdatedThumbnailProfileImage(userID, _) = webServiceCallData.intent {
                // get instance of base 64 string uiimage from package
                if let baseSixtyFourThumbnail = webServiceCallData.swiftyJSONResponsePayload["ImageData"].string {
                    // convert to Data
                    guard let thumbnailProfileData = Data(base64Encoded: baseSixtyFourThumbnail) else {
                        webServiceCallData.responseSucceeded = false
                        return
                    }
                    // convert to uiimage
                    guard let thumbnailProfileImage = UIImage(data:thumbnailProfileData) else {
                        webServiceCallData.responseSucceeded = false
                        return
                    }
                    
                    // parse updated thumbnailProfileImageID
                    guard let thumbnailProfileImageID = webServiceCallData.swiftyJSONResponsePayload["ThumbnailProfileImageID"].int else {
                        webServiceCallData.responseSucceeded = false
                        return
                    }
                    
                    // update image in communityMemberThumbnails local directory. method also updates communityMemberThumbnailUserIDsGlobalDictionary and communityMemberThumbnailsGlobalDictionary upon successfully storing locally
                    LoadLocalFile.saveCommunityMember(thumbnailProfileImage: thumbnailProfileImage, thumbnailProfileImageID: thumbnailProfileImageID, for: userID)
                    
                    let returnParameters = WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID: userID, thumbnailUpdated: true)
                    webServiceCallData.responseReturnParameters = returnParameters
                    webServiceCallData.responseSucceeded = true
                    return
                }
                else {
                    let returnParameters = WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID: userID, thumbnailUpdated: false)
                    webServiceCallData.responseReturnParameters = returnParameters
                    webServiceCallData.responseSucceeded = false
                    return
                }
            }
        }
        else { // should never happen
            let returnParameters = WebServiceCallReturnParameters.checkForUpdatedThumbnailProfileImage(userID: 0, thumbnailUpdated: false)
            webServiceCallData.responseReturnParameters = returnParameters
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parsePostCommThreadMessageJSON() {
        print("func parsePostCommThreadMessageJSON is running...")
        if webServiceCallData.swiftyJSONResponse["PayloadType"].stringValue == "GenericResponse" {
            // do nothing if successcode is true
            if case let WebServiceCallIntent.postCommThreadMessage(_, messageBlurb) = webServiceCallData.intent {
                let returnParameters = WebServiceCallReturnParameters.postCommThreadMessage(messageBlurb: messageBlurb)
                webServiceCallData.responseReturnParameters = returnParameters
                webServiceCallData.responseSucceeded = true
                return
            }
        }
        else {
            // do nothing if successcode is false (in future versions, potentially use for updating sent status in UI
            webServiceCallData.responseSucceeded = false
            return
        }
    }
    
    func parseGetLargeProfileImageJSON(){
        print("func parseGetLargeProfileImageJSON is running...")
        
        if case let WebServiceCallIntent.getLargeProfileImage(memberID, imageView) = webServiceCallData.intent {
            // get instance of base 64 string uiimage from package
            if let baseSixtyFourLargeProfileImage = webServiceCallData.swiftyJSONResponsePayload["ImageData"].string {
                // convert to Data
                guard let largeProfileImageData = Data(base64Encoded: baseSixtyFourLargeProfileImage) else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                // convert to uiimage
                guard let image = UIImage(data:largeProfileImageData) else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                // do not believe largeProfileImageID is needed, but keeping code here in case it is later needed
                /*
                guard let largeProfileImageID = webServiceCallData.swiftyJSONResponsePayload["largeProfileImageID"].int else {
                    webServiceCallData.responseSucceeded = false
                    return
                }
                */
                // update global user profile pictures dictionary with matching userID and profile picture
                communityMemberLargeProfileImageGlobalDictionary[memberID] = image
                webServiceCallData.responseSucceeded = true
                let returnParameters = WebServiceCallReturnParameters.getLargeProfileImage(imageFetched: true, memberID: memberID, imageView: imageView)
                webServiceCallData.responseReturnParameters = returnParameters
            }
        }
        else {
            print("@#$%")
            webServiceCallData.responseSucceeded = false
            return
        }
    }
}

class WebServiceCallCheckpointData: NSObject {
    var validationTitle: String?
    var validationMessage: String?
    var className: String?
    var methodName: String?
    var errorCode: Int?
    var errorDescription: String?
    var validationType: WebServiceValidationType?
    
    convenience init(webServiceCallResponse: JSON) {
        // title: errorType.title, message: messageReference!,
        print("class WebServiceCallCheckpointData: func convenience init(webServiceCallResponse: is running...")
        // parse validation type
        let rawType: String? = webServiceCallResponse["APIValidation"]["ValidationType"].string
        // http://stackoverflow.com/questions/25919075/swift-enumerations-toraw-and-fromraw-with-xcode-6-1
        print("rawType:\(String(describing: rawType))")
        var validationType: WebServiceValidationType?
        if let value = rawType {
            validationType = WebServiceValidationType(rawValue: value)
        }
        print("validationType:\(String(describing: validationType))")
        self.init(validationType: validationType, webServiceCallResponse: webServiceCallResponse)
    }
    
    init(validationType: WebServiceValidationType?, webServiceCallResponse: JSON?) {
        print("class WebServiceCallCheckpointData: func init(validationType: is running...")
        // set validation type
        self.validationType = validationType
        if let response = webServiceCallResponse {
            // set className
            self.className = response["Payload"]["ClassName"].string
            
            // set methodName
            self.methodName = response["Payload"]["MethodName"].string
            
            // set errorCode
            self.errorCode = response["Payload"]["ErrorCode"].int
            
            // set errorDescription
            self.errorDescription = response["Payload"]["ErrorDescription"].string
            
            // set validation message
            self.validationMessage = validationType?.message != nil ? validationType?.message : response["APIValidation"]["Message"].string
        }
        else {
            // set validation message
            self.validationMessage = self.validationType?.message
        }
        
        // set validation title (defined for iOS locally within enum type)
        self.validationTitle = self.validationType?.title
    }
    
    convenience init(customValidationType: WebServiceValidationType) { // requires message for enum type
        print("class WebServiceCallCheckpointData: func convenience init(customValidationType: is running...")
        self.init(validationType: customValidationType, webServiceCallResponse: nil)
    }
}
