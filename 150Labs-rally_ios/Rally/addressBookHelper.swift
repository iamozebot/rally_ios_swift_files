//
//  AddressBookHelper.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Recreated by Ian Moses on 6/29/15 as AddressBookHelper from addressBookHelper
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation
import UIKit
import AddressBook
import Contacts
import SwiftyJSON

class AddressBookHelperClass {
    
    //fetch IPhone contacts code, trying it within viewController for ease of access
    /*
    func loadAddressBookContacts() {
        let (contactsIOS9AccessGranted,_): (Bool,CNAuthorizationStatus) = addressBookIOS9AccessStatus()
        if contactsIOS9AccessGranted {
            print("func loadAddressBookContacts is running, access granted...")
            //let key = [CNContactImageDataAvailableKey, CNContactGivenNameKey, CNContactPhoneNumbersKey]
            let fetchRequest = CNContactFetchRequest(keysToFetch: [CNContactPhoneNumbersKey as CNKeyDescriptor,CNContactImageDataAvailableKey as CNKeyDescriptor,CNContactFormatter.descriptorForRequiredKeys(for: .fullName) as CNKeyDescriptor])
            // commenting out single container, tested on a phone, only had 16 of 421 contacts stored in the iphone's contacts database. upon looping through all containers, got all other contacts
            //let containerId = CNContactStore().defaultContainerIdentifier() // enumerates through all contacts in iphone's contacts database
            //let predicate: NSPredicate = CNContact.predicateForContactsInContainerWithIdentifier(containerId) // doesn't check ICloud, exchange, etc containers. just phone container
            
            ///////////TESTING/////////////////////////
            var allContainers: [CNContainer] = []
            do {
                allContainers = try CNContactStore().containers(matching: nil)
            } catch {
                print("Error fetching containers")
            }
            for container in allContainers {
                let predicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            fetchRequest.predicate = predicate
            // http://stackoverflow.com/questions/32669612/how-to-fetch-all-contacts-record-in-ios-9-using-contacts-framework
            // if there is a future need to pull contacts from other containers (icloud, exchange, etc), above url illustrates this
            do {
                let contactStore = CNContactStore()
                try contactStore.enumerateContacts(with: fetchRequest) { (contact, pointer) -> Void in
                    // phoneNumberMatchedFlag = 1
                    let newFriend = Friend() //Create new friend object
                    newFriend.identifier = contact.identifier
                    for number in contact.phoneNumbers {
                        let phoneNumberKey = number.label
                        // http://stackoverflow.com/questions/33212459/contacts-framework-access-to-phones-values
                        let phoneNumberValueAsString = number.value.value(forKey: "digits") as! String
                        //print("phoneNumberValueAsString: \(phoneNumberValueAsString)")
                        if let value = phoneNumberKey {
                            newFriend.phoneNumberList[value] = phoneNumberValueAsString
                        }
                    }
                    // http://www.appcoda.com/ios-contacts-framework/
                    newFriend.firstName = contact.givenName
                    newFriend.lastName = contact.familyName
                    // http://stackoverflow.com/questions/33131013/ios-9-contacts-framework-raise-always-exception-reading-imagedata-in-objective-c
                    newFriend.imageDataAvailable = contact.imageDataAvailable
                    if newFriend.phoneNumberList.count != 0 {
                        friends.append(newFriend)
                    }
                }
                
                let webServiceCallManagerInstance = WebServiceCallManager(intent: .createUsersWithPhoneContacts)
                webServiceCallManagerInstance.makeRequest(onPostCompleted: {(succeeded, returnParameters: WebServiceCallReturnParameters?) -> Void in
                    if succeeded {
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                    else { // post message saying could not join group. try again later
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                })
            }
            catch let error as NSError {
                print(error.description, separator: "", terminator: "\n")
            }
            }
        }
        print("final number of contacts successfully loaded: \(friends.count)")
        webCallStatus.fetchAddressBookContacts = 1
    }
    */
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
        case .denied, .notDetermined:
            CNContactStore().requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        DispatchQueue.main.async(execute: { () -> Void in
                            /*
                            let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
                            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                            appDelegate.window?.rootViewController!.presentViewController(alert, animated: true, completion: nil) // this is a risky line of code, should be tested. deny access and see if it pops up on home screen with no issue
                             */
                        })
                    }
                }
            })
        default:
            completionHandler(false)
        }
    }
    
    func addressBookIOS9AccessStatus()-> (Bool,CNAuthorizationStatus) {
        print("func addressBookIOS9AccessStatus is running...")
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            return (true,.authorized)
        case .notDetermined:
            return (false,.notDetermined)
        case .denied, .restricted:
            return (false,.denied) // could also return .Restricted but extra unneccessary code for same result, this app doesn't distinguish a difference b/w restricted and denied
        }
    }
    /*
    func fetchRallyMembersProfilePictures(_ startingCardPositionIndex: Int, numberOfCardsToDefineAndAddAsSubview: Int, cardTypeValue: cardType) {
        
        let (contactsIOS9AccessGranted,_): (Bool,CNAuthorizationStatus) = addressBookIOS9AccessStatus()
        if contactsIOS9AccessGranted {
            print("func fetchRallyMembersProfilePictures is running...")
            var rallyArray: [rally]!
            var resultKey: String!
            switch cardTypeValue {
            case .upcomingRally:
                rallyArray = upcomingRallysGlobalArray
                resultKey = "rallyMembersIdentifiersResultKey"
            case .userJoinedCommunity:
                rallyArray = userJoinedCommunitiesGlobalArray
                resultKey = "communityMembersIdentifiersResultKey"
            case .communityNearby:
                print("Do nothing...") // no profile pictures currently added to nearby community cards. add code here in a future version to add feature
            default:
                break
            }
            // fetch rallyMember identifiers for range of rally cards being compiled
            let defaults = UserDefaults.standard
            if let rallyMembersIdentifiersJSON = defaults.value(forKey: "\(resultKey)") as? Data { // get instance of all user rallys and associated member contact identifiers
                /* swiftyJSON example format
                [
                {
                "rallyID1":
                {
                "6306745840":"rallyMemberIdentifier1",
                "6306745841":"rallyMemberIdentifier2",
                "6306745842":"rallyMemberIdentifier3"
                },
                "rallyID2":
                {
                "6306745843":"rallyMemberIdentifier4",
                "6306745844":"rallyMemberIdentifier5",
                "6306745845":"rallyMemberIdentifier6"
                }
                }
                ]
                */
                let swiftyJSON = JSON(data: rallyMembersIdentifiersJSON)
                let contactStore = CNContactStore()
                let key = [CNContactImageDataAvailableKey]
                if startingCardPositionIndex<numberOfCardsToDefineAndAddAsSubview {
                    for rallyArrayIndex in startingCardPositionIndex..<numberOfCardsToDefineAndAddAsSubview { // loop through rallys/groups
                        if rallyArrayIndex != 0 { // don't do this for new rally or new group card
                            let rallyID = rallyArray[rallyArrayIndex-1].RallyID // rallyArrayIndex is equal to zero to represent new rally card; rallyArray cases don't hold card object so first index is first rally. therefore, -1 aligns indexing
                            for rallyMember in rallyArray[rallyArrayIndex-1].RallyUserList {
                                if rallyMember.UserID != userClass.UserID {
                                    let phoneNumberArray = Array(rallyMember.PhoneNumberList.values)
                                    if let rallyMemberPhoneNumberDatabaseValue = phoneNumberArray[0] { // webservice only returns one phone number for each rally member
                                        let memberIdentifier = swiftyJSON[0]["\(rallyID)"]["\(rallyMemberPhoneNumberDatabaseValue)"] // gets memberIdentifier value from local file formatted as JSON type for the specified rally member. these rally members come from a rally's RallyUserList property. All the relevant rallys being built on the home screen are getting looped through as well
                                        let identifierAsString = memberIdentifier.stringValue // converts member identifier from JSON to string type
                                        if identifierAsString != "" || identifierAsString != "nil" || identifierAsString != "null" {
                                            do{
                                                let contact = try contactStore.unifiedContact(
                                                    withIdentifier: identifierAsString, keysToFetch: key as [CNKeyDescriptor])
                                                if contact.imageDataAvailable { // checking imageDataAvailable and refetching to get picture if contact has picture is quicker than checking for picture each time, even though you don't have to recall (this is according to Justin and
                                                    // https://www.safaribooksonline.com/library/view/ios-9-swift/9781491936689/ch04.html
                                                    let keyToFetch = [CNContactImageDataKey]
                                                    do{
                                                        let refetchedContact = try contactStore.unifiedContact (
                                                            withIdentifier: contact.identifier, keysToFetch: keyToFetch as [CNKeyDescriptor]) // contact identifier fetched by default, not required to be specified in keysToFetch
                                                        if let contactImage = refetchedContact.imageData {
                                                            rallyMember.largeProfilePicture = UIImage(data: contactImage)
                                                            print("Rally member contacts picture successfully found and grabbed instance for use")
                                                        }
                                                    }
                                                    catch let err {
                                                        print("We are throwing an error")
                                                        print(err)
                                                    }
                                                }
                                            }
                                            catch let error as NSError {
                                                print("We're throwing another type of error")
                                                print(error.description, separator: "", terminator: "\n")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    */
}
