//
//  PassThroughView.swift
//  Rally
//
//  Created by Ian Moses on 9/13/15.
//  Recreated by Ian Moses on 6/19/15 as PassThroughView from passThroughView
//  Copyright (c) 2015 Ian Moses. All rights reserved.
//

import Foundation
import UIKit

/*passThroughView is the superclass for FBLoginButtonTopLayerSubview which allows superview(FBSDKLoginButton)
touch functionality sitting behind topview to work when clicking button. http://stackoverflow.com/questions/3046813/how-can-i-click-a-button-behind-a-transparent-uiview */
class passThroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.alpha > 0 && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
    var stringDataAssociatedWithView = ""
}
